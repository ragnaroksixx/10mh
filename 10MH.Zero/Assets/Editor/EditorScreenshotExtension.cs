using System;
using System.IO;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using Object = UnityEngine.Object;

public static class EditorScreenshotExtension
{
    const int resWidth = 1920;
    const int resHeight = 1080;
    [MenuItem("Screenshot/Take Screenshot %#k")]
    private static void ScreenShotScene()
    {
        if (SceneView.GetAllSceneCameras().Length <= 0)
        {
            Debug.LogError("Scene View not selected");
            return;
        }
        Camera camera = SceneView.GetAllSceneCameras()[0];


        Texture2D texture = ScreenShotUtilities.ScreenShotTexture(camera, resWidth, resHeight);

        string name = "screenshot_" + DateTime.Now.ToString().Replace('/', '_').Trim(' ').Replace(':', '_') + ".png";
        string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) + "/10MH_Screenshots/";
        if (!Directory.Exists(folderPath))
            Directory.CreateDirectory(folderPath);

        // encode the Texture2D to a PNG
        // you might want to change this to JPG for way less file size but slightly worse quality
        // if you do don't forget to also change the file extension above
        var bytes = texture.EncodeToPNG();

        // In order to avoid bloading Texture2D into memory destroy it
        Object.DestroyImmediate(texture);
        File.WriteAllBytes(folderPath + name, bytes);

    }
}