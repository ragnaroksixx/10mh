﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEditor.SceneManagement;
using System;

public class EditorLevelLoader
{
    const string toolsHeader10MH = "10 Minute Tools/";

    [MenuItem(toolsHeader10MH + "Load Master Scene")]
    [Button(ButtonSizes.Large)]
    public static void OpenMasterScene()
    {
        LoadScene("MasterScene");
    }

    [MenuItem(toolsHeader10MH + "Load Start Scene")]
    [Button]
    public static void LoadStartScene()
    {
        LoadScene("StartScene");
    }
    [MenuItem(toolsHeader10MH + "Load Combat Scene")]
    public static void AddBattleScene()
    {
        OpenMasterScene();
        LoadScene("CombatScene", OpenSceneMode.Additive);
    }
    [MenuItem(toolsHeader10MH + "Load Map Scene")]
    [Button]
    public static void AddMapScene()
    {
        //OpenMasterScene();
        LoadScene("MapScene", OpenSceneMode.Single);
    }
    [MenuItem(toolsHeader10MH + "Load Areas Scenes")]
    [Button]
    public static void LoadAllAreas()
    {
        ScriptableObjectDictionary<AreaData> allAreas;
        allAreas = new ScriptableObjectDictionary<AreaData>("ScriptableObjects/Area/Areas");
        allAreas.Init(false);
        OpenMasterScene();
        foreach (AreaData area in allAreas.AllItems)
        {
            AddScenes(area.scene);
        }
    }
    [MenuItem(toolsHeader10MH + "Load Norman Room Scene")]
    [Button(ButtonSizes.Medium)]
    public static void LoadNormanRoom()
    {
        OpenMasterScene();
        LoadScene("Areas/Norman Room/Norman Room", OpenSceneMode.Additive);
    }

    [Button(ButtonSizes.Large, ButtonStyle.Box, Expanded = true)]
    public static void GoTo(string area)
    {
        OpenMasterScene();
        ScriptableObjectDictionary<AreaTransitionData> allAreas;
        allAreas = new ScriptableObjectDictionary<AreaTransitionData>("ScriptableObjects/Area/TransitionData");
        allAreas.Init(false);

        AreaTransitionData at = allAreas.GetItem(area);

        if (at != null)
        {
            AddScenes(at.GetArea().sceneAsset);
        }
    }

    [Button(ButtonSizes.Large, ButtonStyle.Box, Expanded = true)]
    public static void AdditiveGoTo(string area)
    {
        ScriptableObjectDictionary<AreaTransitionData> allAreas;
        allAreas = new ScriptableObjectDictionary<AreaTransitionData>("ScriptableObjects/Area/TransitionData");
        allAreas.Init(false);

        AreaTransitionData at = allAreas.GetItem(area);

        if (at != null)
        {
            AddScenes(at.GetArea().sceneAsset);
        }
    }

    public static void AddScenes(params string[] paths)
    {
        List<string> scenes = new List<string>();
        for (int i = 0; i < EditorSceneManager.sceneCount; i++)
        {
            scenes.Add(EditorSceneManager.GetSceneAt(i).path);
        }
        foreach (string path in paths)
        {
            if (!scenes.Contains(path))
                LoadScene("Areas/" + path, OpenSceneMode.Additive);
        }
    }
    public static void AddScenes(params SceneAsset[] sceneAssets)
    {
        List<string> scenes = new List<string>();
        for (int i = 0; i < EditorSceneManager.sceneCount; i++)
        {
            scenes.Add(EditorSceneManager.GetSceneAt(i).name);
        }
        foreach (SceneAsset scene in sceneAssets)
        {
            if (!scenes.Contains(scene.name))
                LoadScene(scene, OpenSceneMode.Additive);
        }
    }
    public static void LoadScene(string scene, OpenSceneMode mode = OpenSceneMode.Single)
    {
        SceneController.LoadSceneEditor(scene, mode);
    }
    public static void LoadScene(SceneAsset scene, OpenSceneMode mode = OpenSceneMode.Single)
    {
        SceneController.LoadSceneEditor(scene, mode);
    }
    public static void CloseScene(string sceneName)
    {
        UnityEngine.SceneManagement.Scene s = EditorSceneManager.GetSceneByName(sceneName);
        EditorSceneManager.CloseScene(s, true);
    }
    [MenuItem(toolsHeader10MH + "Enable Super Ultra Developer Mode")]
    public static void DevMode()
    {
        UnityEditor.EditorPrefs.SetBool("DeveloperMode", true);
    }
    [MenuItem(toolsHeader10MH + "Become Basic")]
    public static void NoDevMode()
    {
        UnityEditor.EditorPrefs.SetBool("DeveloperMode", false);
    }
}
