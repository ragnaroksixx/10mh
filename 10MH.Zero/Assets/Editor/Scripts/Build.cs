using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class Build
{
    public static void OnBuild()
    {
        BuildInfo b = Resources.Load<BuildInfo>("BuildInfo");
        b.buildNumber = PlayerSettings.Android.bundleVersionCode.ToString();
        AssetDatabase.Refresh();

    }
}
