﻿using UnityEditor;
using Sirenix.OdinInspector.Editor;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using System;

public class VisualTools10MH
{
    public string CalamityString => "Calamity Status: " + (IsCalamity ? "POST-Calamity" : "PRE-Calamity");
    public bool CalamityInEditor { get => EditorPrefs.GetBool("calamityInEditor"); set => EditorPrefs.SetBool("calamityInEditor", value); }
    public static VisualTools10MH Instance;

    public bool IsCalamity
    {
        get
        {
            if (Application.isPlaying)
            {
                return TimeController.IsCalamity();
            }
            return CalamityInEditor;
        }
    }

    EffectsManager Effects
    {
        get => GameObject.FindObjectOfType<EffectsManager>();
    }

    public VisualTools10MH()
    {
        EditorSceneManager.sceneOpened += OnSceneLoad;
        EditorApplication.playModeStateChanged += PlayModeChanged;
        Instance = this;
    }

    ~VisualTools10MH()
    {
        EditorSceneManager.sceneOpened -= OnSceneLoad;
        EditorApplication.playModeStateChanged -= PlayModeChanged;
        Instance = null;
    }

    private void OnSceneLoad(Scene scene, OpenSceneMode mode)
    {
        Debug.Log("Scene Loaded");
        if (CalamityInEditor)
            SetCalamity();
        else
            SetPreCalamity();
    }
    private void PlayModeChanged(PlayModeStateChange state)
    {
        Debug.Log(state);
        if (state == PlayModeStateChange.EnteredEditMode)
        {
            if (CalamityInEditor)
                SetCalamity();
            else
                SetPreCalamity();
        }
    }


    [InfoBox("$CalamityString")]
    [Button(ButtonSizes.Medium)]
    public void SetCalamity()
    {
        if (Application.isPlaying) return;
        CalamityInEditor = true;
        Effects?.SetCalamity();
        GameObject.FindObjectsOfType<CalamityTrigger>(true).ForEach(x => { x.SetCalamity(true); EditorUtility.SetDirty(x); });
        LightControllerData.Instance.SetCalamity(true);
        AreaTools.Instance.SetLighting(true);
    }

    [Button(ButtonSizes.Medium)]
    public void SetPreCalamity()
    {
        if (Application.isPlaying) return;
        CalamityInEditor = false;
        Effects?.SetPreCalamity();
        GameObject.FindObjectsOfType<CalamityTrigger>(true).ForEach(x => { x.SetCalamity(false); EditorUtility.SetDirty(x); });
        AreaTools.Instance.SetLighting(false);
        LightControllerData.Instance.SetCalamity(false);
    }

    //[Button(ButtonSizes.Gigantic)]
    //public void BuildLighting()
    //{
    //    if (EditorSceneManager.sceneCount > 1)
    //    {
    //        EditorLevelLoader.CloseScene("MasterScene");
    //        Debug.LogError("Multiple Scenes Loaded");
    //        return;
    //    }
    //    Lightmapping.BakeAsync();
    //}

    [Button(ButtonSizes.Gigantic)]
    public void Play()
    {
        EditorPrefs.SetBool("restartOnLaunch", true);
        EditorApplication.ExecuteMenuItem("Edit/Play");
    }
}
