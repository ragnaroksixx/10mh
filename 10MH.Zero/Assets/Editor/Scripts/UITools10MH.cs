﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;


public class UITools10MH
{
    [EnumToggleButtons, HideLabel]
    [OnValueChanged("OnValueChanged")]
    public UIElements uiElementsMask = UIElements.HUD;

    [System.Flags]
    public enum UIElements
    {
        INVENTORY = 1 << 1,
        DIALOGUE = 1 << 2,
        BATTLE = 1 << 3,
        HUD = 1 << 4,
    }
    PlayerInfoMenu pInfoMenu => GameObject.FindObjectOfType<PlayerInfoMenu>(true);
    DialogueRenderer dRender => GameObject.FindObjectOfType<DialogueRenderer>(true);
    UIController uiCont => GameObject.FindObjectOfType<UIController>(true);
    InventoryManager invManager => GameObject.FindObjectOfType<InventoryManager>(true);
    BattleMenu battleUI => GameObject.FindObjectOfType<BattleMenu>(true);
    HUDController hud => GameObject.FindObjectOfType<HUDController>(true);

    public void OnValueChanged()
    {
        SetUI(uiElementsMask);
    }
    public void SetUI(UIElements preset)
    {
        if (Application.isPlaying)
            return;

        if (preset.HasFlag(UIElements.INVENTORY))
        {
            invManager.anim.AnimateOnScreenEditor();
        }
        else
        {
            invManager.anim.AnimateOffScreenEditor();
        }

        if (preset.HasFlag(UIElements.DIALOGUE)
            || preset.HasFlag(UIElements.INVENTORY)
            || preset.HasFlag(UIElements.BATTLE))
        {
            uiCont.cinemaBars.AnimateOnScreenEditor();
            uiCont.playerAnim.AnimateOnScreenEditor();
        }
        else
        {
            uiCont.cinemaBars.AnimateOffScreenEditor();
            uiCont.playerAnim.AnimateOffScreenEditor();
        }


        battleUI.gameObject.SetActive(preset.HasFlag(UIElements.BATTLE));
    }
}
