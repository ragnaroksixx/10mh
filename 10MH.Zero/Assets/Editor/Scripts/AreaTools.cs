﻿using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

public class AreaTools
{
    [InlineEditor(ObjectFieldMode = InlineEditorObjectFieldModes.Hidden)]
    public AreaData currentArea;

    public static AreaTools Instance;

    public AreaTools()
    {
        Instance = this;
    }
    [Button]
    public void GetAreaData()
    {
        AreaSpawnPoint asp = GameObject.FindAnyObjectByType<AreaSpawnPoint>();
        if (asp == null)
        {
            currentArea = null;
            return;
        }
        currentArea = asp.spawnData.GetArea();
    }
    /*
     * Bake Light Probes
     * -Find or Create Adaptive Probe Vloume
     * -Enable both calamity and pre calamity assets
     * -Set APVs to fit scene
     * -Create default scenario
     * -Create calamity scenario
     * -Bake probes
     * 
     * Bake Pre/Post Lighting
     * -Set Pre/Post Calamity
     * -Set scenario and bake  

    */

    [Button]
    public void SetupLightProbes()
    {
        ProbeVolume apv = GameObject.FindAnyObjectByType<ProbeVolume>();
        if(apv==null)
        {
            GameObject g = new GameObject("Adapative Probe Volume");
            apv = g.AddComponent<ProbeVolume>();
        }
        GameObject.FindObjectsOfType<SetActiveCalamity>(true)
            .ForEach(x => { x.ForceActive(); EditorUtility.SetDirty(x); });

        ProbeReferenceVolume probeRefVolume = ProbeReferenceVolume.instance;
        probeRefVolume.currentBakingSet.TryAddScenario("Default");
        probeRefVolume.currentBakingSet.TryAddScenario("Calamity");
    }



    [Button]
    public void SetCalamity()
    {
        VisualTools10MH.Instance.SetCalamity();
    }
    [Button]
    public void SetPreCalamity()
    {
        VisualTools10MH.Instance.SetPreCalamity();
    }
    [Button]
    public void BuildLighting()
    {
       // Lightmapping.Bake();
    }
    [Button]
    public void SetMeshRenderersToProbes()
    {
        MeshRenderer[] renderers = GameObject.FindObjectsOfType<MeshRenderer>(true);
        foreach (MeshRenderer item in renderers)
        {
            item.receiveGI = ReceiveGI.LightProbes;
            EditorUtility.SetDirty(item);
        }

    }
    public void SetLighting(bool calamity)
    {
        ProbeReferenceVolume.instance.lightingScenario = calamity ? "Calamity" : "Default";
    }
}

