﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;

[System.Serializable]
public class BattleTools10MH
{
    public BattleEncounter testEncounter;
    public BattleTools10MH()
    {
    }
    [Button(ButtonSizes.Medium)]
    public void StartBattle()
    {
        BattleController.StartBattle(testEncounter);
    }
}
