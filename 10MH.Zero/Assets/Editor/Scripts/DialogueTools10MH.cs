﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class DialogueTools10MH
{
    [FolderPath]
    public string dialogueFolder = "Assets\\DialogueScripts";
    public string folderCategory;
    public NewDialogueData[] scripts;
    string projectPath => Path.Combine(Application.dataPath, "../");

    [Button]
    public void Create()
    {
        foreach (NewDialogueData script in scripts)
        {
            Create(script);
        }
    }
    public void Create(NewDialogueData ndd)
    {

        string text = "{\n<name %TITLE%>\n\n%BODY%\n}";
        text = text.Replace("%TITLE%", ndd.Name);
        text = text.Replace("%BODY%", ndd.Text);

        string path = dialogueFolder + "/" + folderCategory + "/" + ndd.Name + ".txt";
        string fullPath = projectPath + "/" + path;
        if (File.Exists(fullPath))
        {
            if (!UnityEditor.EditorUtility.DisplayDialog("Overwrite Existing File?",
                "FILE: " + fullPath,
                "YES",
                "NO"))
            {
                return;
            }
        }
        else
        {
            File.CreateText(fullPath).Close();
        }
        FileAttributes attributes = File.GetAttributes(fullPath);
        attributes = attributes.RemoveAttribute(FileAttributes.ReadOnly);
        File.SetAttributes(fullPath, attributes);
        File.WriteAllText(fullPath, text);
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = AssetDatabase.LoadAssetAtPath<TextAsset>(path.Replace("/Assets", ""));
    }

    [Button]
    public void SwapTextFontsInScene(TMP_FontAsset oldFont, TMP_FontAsset newFont)
    {
        TMP_Text[] texts = GameObject.FindObjectsByType<TMP_Text>(FindObjectsSortMode.None);
        foreach (TMP_Text item in texts)
        {
            if (item.font == oldFont)
            {
                item.font = newFont;
                EditorUtility.SetDirty(item);
            }
        }
    }
}
public static class DialogueExtenstions
{
    public static FileAttributes RemoveAttribute(this FileAttributes attributes, FileAttributes attributesToRemove)
    {
        return attributes & ~attributesToRemove;
    }
}
[System.Serializable]
public struct NewDialogueData
{
    public string Name;
    [TextArea(5, 20)]
    public string Text;

}