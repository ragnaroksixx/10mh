using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuickTools10MH : OdinMenuEditorWindow
{
    private VisualTools10MH visualSettings;
    private EditorLevelLoader sceneLoader;
    private BattleTools10MH battleTools;
    private UITools10MH uiTools;
    private DialogueTools10MH dialogueTools;
    private AreaTools areaTools;

    [MenuItem("10MH Tools/Open Window")]
    private static void OpenWindow()
    {
        GetWindow<QuickTools10MH>().Show();
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        visualSettings = null;
        sceneLoader = null;
        battleTools = null;
        uiTools = null;
        dialogueTools = null;
        areaTools = null;
    }
    protected override OdinMenuTree BuildMenuTree()
    {
        var tree = new OdinMenuTree();
        tree.Selection.SupportsMultiSelect = true;
        
        if (visualSettings == null) visualSettings = new VisualTools10MH();
        if (sceneLoader == null) sceneLoader = new EditorLevelLoader();
        if (battleTools == null) battleTools = new BattleTools10MH();
        if (uiTools == null) uiTools = new UITools10MH();
        if (dialogueTools == null) dialogueTools = new DialogueTools10MH();
        if (areaTools == null) areaTools = new AreaTools();
        //tree.Add("Settings", GeneralDrawerConfig.Instance);
        tree.Add("Scenes", sceneLoader);
        tree.Add("Area", areaTools);
        tree.Add("Visual Tools", visualSettings);
        tree.AddAssetAtPath("Play Settings", "Resources/PlaySetting10MH.asset");
        PlaySetting10MH.Instance.Init();
        tree.Add("Battle Tools", battleTools);
        tree.Add("UI Tools", uiTools);
        tree.Add("Dialogue Tools", dialogueTools);

        OdinMenuItem odinMenuItem = new OdinMenuItem(tree, "Redraw", tree.DefaultMenuStyle);
        odinMenuItem.OnRightClick += Redraw;
        tree.Add("Mouse 1 to Redraw", odinMenuItem);
        return tree;
    }



    public void Redraw(OdinMenuItem i )
    {
        Repaint();
    }
}