﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using DG.Tweening;

public class Character : MonoBehaviour
{
    [OnValueChanged("UpdateName")]
    public CharacterData characterData;
    [HideInInspector]
    public SpriteRenderer sRenderer;
    static Dictionary<CharacterData, Character> loadedChararcters = new Dictionary<CharacterData, Character>();
    public CharacterExtruder meshHandler;
    public ImageKey defaultSpriteKey = ImageKey.NORMAL;
    public Collider collision;
    public Transform graphic;
    public Sol sol;
    float scale = 1;
    private void Awake()
    {
        scale = graphic.localScale.x;
        sRenderer = GetComponentInChildren<SpriteRenderer>();
        loadedChararcters.Add(characterData, this);
        sol.Init(this);
    }
    private void Start()
    {
        SetSprite(characterData.GetImage(defaultSpriteKey));
    }

    private void OnDestroy()
    {
        loadedChararcters.Remove(characterData);
    }
    public static Character FindCharacter(CharacterData cd)
    {
        return loadedChararcters[cd];
    }
    public virtual void SetSprite(Sprite s)
    {
        sRenderer.sprite = s;
        meshHandler.UpdateMeshPoints();
    }
    public virtual void SetSprite(string sName)
    {
        SetSprite(characterData.GetImage(sName));
    }
    [Button]
    private void UpdateName()
    {
        if (characterData == null) return;
        characterData.Init();
        name = "Character (" + characterData.displayName + ")";
        GetComponentInChildren<SpriteRenderer>().sprite = (characterData.GetImage(defaultSpriteKey));
        meshHandler.UpdateMeshEditor();
    }

    public void MoveTo(Vector3 pos)
    {
        //Exit
        //Sequence exitEffect = DOTween.Sequence();
        //Sequence enterEffect = DOTween.Sequence();
        //float disappearTime = 1;
        //exitEffect.Append(transform.DOScale(0));
        transform.position = pos;
    }
    public void Leave(bool instant)
    {
        if (instant)
        {
            OnExit();
            return;
        }
        collision.enabled = false;
        graphic.transform.DOScaleY(0f, 2.0f)
            .SetEase(Ease.InOutElastic)
            .OnComplete(() => { OnExit(); });
    }
    void OnExit()
    {
        Area.ExitCharacter(characterData);
        graphic.gameObject.SetActive(false);
    }
    public void Enter()
    {
        if (IsDead())
        {
            sol.gameObject.SetActive(true);
            graphic.gameObject.SetActive(false);
            return;
        }
        Area.EnterCharacter(characterData);
        graphic.transform.localScale = new Vector3(scale, 0, scale);
        graphic.gameObject.SetActive(true);
        collision.enabled = false;
        graphic.transform.DOScaleY(scale, 2.0f)
            .SetEase(Ease.InOutElastic)
            .OnComplete(() => { collision.enabled = true; });
    }

    bool IsDead()
    {
        return CycleManager.Instance.HasEvent(
            characterData.Identifier, "dead");
    }

    public void Die()
    {
        characterData.Die();
        sol.gameObject.SetActive(true);
        graphic.gameObject.SetActive(false);
        return;
    }
    public void OnDialogueExit()
    {
        gameObject.SetActive(true);
    }

    public void OnDialogueEnter()
    {
        gameObject.SetActive(false);
    }
    public static bool IsCharacterInArea(CharacterData cd)
    {
        return Area.IsCharacterInArea(cd);
    }
}
