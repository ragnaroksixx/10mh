﻿using EZCameraShake;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeResetEffectHandler : MonoBehaviour
{
    public Material whiteLayer;
    public Material outlineLayer;
    public float minWidth, maxWidth;
    float currentWidth;
    bool pulseDirection;
    public float pulseSpeed = 8;
    float shakeTrack = 0;
    public float minValue;
    public float maxValue = 0.25f;
    public float rampScale = 2;
    public static TimeResetEffectHandler Instance;
    private void Awake()
    {
        Instance = this;
    }
    private void Update()
    {
        UpdatePulse();
    }
    void UpdatePulse()
    {
        int value = pulseDirection ? 1 : -1;
        currentWidth += value * Time.deltaTime / pulseSpeed;
        shakeTrack -= Time.deltaTime;

        if (currentWidth > maxWidth)
        {
            pulseDirection = !pulseDirection;
            currentWidth = maxWidth;
            if (shakeTrack <= 0)
            {
                CameraShaker.Instance.ShakeOnce(1, 5, 1, 0.1f);
                shakeTrack = 1;
            }
        }
        if (currentWidth < minWidth)
        {
            pulseDirection = !pulseDirection;
            currentWidth = minWidth;
        }

        outlineLayer.SetFloat("_FadeBurnWidth", currentWidth);
    }
    public void SetSubSpace(float val) //[0->1]
    {
        if (val > 1)
            val = 1;
        if (val < 0)
            val = 0;

        float whiteVal = val * maxValue;
        float outlineValue = val * maxValue;

        whiteVal = 1 - whiteVal;
        outlineValue = 1 - outlineValue;

        whiteVal = Mathf.Pow(whiteVal, rampScale);
        outlineValue = Mathf.Pow(outlineValue, rampScale);

        whiteLayer.SetFloat("_FadeAmount", whiteVal);
        outlineLayer.SetFloat("_FadeAmount", outlineValue);
    }
}
