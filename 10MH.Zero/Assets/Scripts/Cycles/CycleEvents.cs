﻿using UnityEngine;
using System.Collections;

public class CycleEvents : ScriptableObject, IUnquieScriptableObject
{
    public string id;
    public bool isPermanent;
    public string description;

    public string Identifier => id;
    public string[] Alias => new string[] { };
    public void Init()
    {

    }
}
