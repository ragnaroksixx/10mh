﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NHG.Dialogue;
using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;

public class CycleManager : MonoBehaviour
{
    public static CycleManager Instance;
    public AreaTransitionData startSpawn;
    MemorySet permaEvents, cycleEvents;
    PlayerUnit player;
    public CharacterData playerCharacterData;
    public static PlayerUnit PlayerCombatUnit { get => Instance.player; }
    public MemorySet PermanentEvents
    {
        get
        {
            if (permaEvents == null)
                permaEvents = SaveData.currentSave.memories;
            return permaEvents;
        }
    }

    public MemorySet PermaEvents { get => PermanentEvents; }

    string gameStartMemory = "gamestart";
    [FoldoutGroup("CycleOne")]
    public AreaTransitionData cycleOneSpawn;
    [FoldoutGroup("CycleOne")]
    public TextAsset cycleOneScene;
    [FoldoutGroup("CycleOne")]
    public string cycleOneCompleteMemory;

    private void Awake()
    {
        Time.timeScale = 0;

        Instance = this;
        cycleEvents = new MemorySet();
        player = new PlayerUnit();
        player.Init(playerCharacterData);
    }
    public void ResetPlayerCharacter()
    {
        SetPlayerCharacter(playerCharacterData);
    }
    public void SetPlayerCharacter(CharacterData cd)
    {
        player = new PlayerUnit();
        player.Init(cd);
        BattleController.Instance.SetPlayerCharacter(player);
        if (Player.Instance != null)
            Player.Instance.characterData = cd;
    }
    private void Start()
    {
        ResetPlayerCharacter();
        CheckStartEvents();

        AreaTransitionData spawn = startSpawn;
#if UNITY_EDITOR
        if (PlaySetting10MH.Instance.useDebugSpawn && PlaySetting10MH.Instance.debugSpawn)
            spawn = PlaySetting10MH.Instance.debugSpawn;
        if (PlaySetting10MH.Instance.startInBattle && PlaySetting10MH.Instance.startEncounter)
            spawn = GlobalData10MH.Instance.areaTransitionDictionary.GetItem("dev");
#endif
        if (SaveData.currentSave.numCycles == 0)
        {
#if UNITY_EDITOR
            if (!PlaySetting10MH.SKIP_INTRO)
            {
                spawn = cycleOneSpawn;
                DialogueArea.SetDialogueArea(cycleOneScene, startSpawn);
            }
#endif
        }
        else
        {
            MusicPlayer.Instance.PlayNext();
        }
        SaveData.currentSave.numCycles++;


        if (ScreenShotUtilities.cache.Count == 0)
            SceneController.SetActiveEnvironment(spawn, null, "AreaTransitionLoading");
        else
            SceneController.SetActiveEnvironment(spawn, null, "RewindTransition");
        StartCoroutine(CacheScreenLoop());
    }

    IEnumerator CacheScreenLoop()
    {
        while (WalkController10MH.Instance == null)
            yield return null;

        yield return null;

        while (true)
        {
            if (Time.timeScale == 0) { yield return null; continue; }
            if (SceneTransitionEffect.Instance != null) { yield return null; continue; }

            yield return CacheScreenCoroutine();

            yield return new WaitForSeconds(2);
        }
    }
    IEnumerator CacheScreenCoroutine()
    {
        yield return new WaitForEndOfFrame();

        Texture2D screenImage = new Texture2D(Screen.width, Screen.height);
        screenImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);

        yield return new WaitForSeconds(3);

        screenImage.Apply();
        ScreenShotUtilities.CacheScreen(TimeController.Instance.CurrentTime.minute, screenImage);
    }
    IEnumerator CacheScreenInstant()
    {
        yield return new WaitForEndOfFrame();

        Texture2D screenImage = new Texture2D(Screen.width, Screen.height);
        screenImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);

        screenImage.Apply();
        ScreenShotUtilities.CacheScreen(TimeController.Instance.CurrentTime.minute, screenImage);
    }

    public void CacheScreen()
    {
        StartCoroutine(CacheScreenInstant());
    }
    public void RestartCycle()
    {
        ScreenShotUtilities.CacheScreen();
        if (!HasEvent("", cycleOneCompleteMemory))
        {
            AddPermanentEvent("", cycleOneCompleteMemory);
        }
        if (BattleController.IsInBattle)
            BattleController.Instance.BattleSceneCleanUp();
        Global10MH.ClearCachePose();
        SaveData.currentSave.UpdateSave();
        SceneController.LoadMainGame();
        PathManager.SetPath(null);
        PhysicsCacher.Clear();
    }
    void CheckStartEvents()
    {
        if (!HasEvent("", gameStartMemory))
        {
            AddPermanentEvent("", gameStartMemory);
        }
    }

    public void AddPermanentEvent(string catergory, string val)
    {
        PermaEvents.Add(catergory, val);
    }
    public void AddTempEvent(string catergory, string val)
    {
        cycleEvents.Add(catergory, val);
    }
    public void AddTempEvent(string key, int value)
    {
        cycleEvents.Add(key, value);
    }
    public int GetEventValue(string key, int defaultValue)
    {
        return cycleEvents.GetEventValue(key, defaultValue);
    }
    public bool HasEvent(string catergory, string val)
    {
        return PermaEvents.HasEvent(catergory, val)
            || cycleEvents.HasEvent(catergory, val);
    }
    public void RemoveEvent(string catergory, string val)
    {
        PermaEvents.Remove(catergory, val);
        cycleEvents.Remove(catergory, val);
    }
    public void AddPermanentEvent(IMemorable val)
    {
        AddPermanentEvent(val.memoryCategory, val.memoryID);
    }
    public void AddTempEvent(IMemorable val)
    {
        AddTempEvent(val.memoryCategory, val.memoryID);
    }
    public bool HasEvent(IMemorable val)
    {
        return HasEvent(val.memoryCategory, val.memoryID);
    }
    public bool HasAreaUnlocked(AreaData area)
    {
        return HasEvent("Unlocked Areas", area.Identifier);
    }
    public void UnlockArea(AreaData area)
    {
        AddPermanentEvent("Unlocked Areas", area.Identifier);
        //Publisher.Raise(new ObjectiveEvent("Area Unlocked", area.displayName));
    }
    public static bool IsFirstCycle()
    {
        return SaveData.currentSave.numCycles <= 1;
    }
    public static bool IsIntroCycles()
    {
        return SaveData.currentSave.numCycles <= 2;
    }
    public static List<ItemPlacementData> GetInventory(string key)
    {
        if (key == null) return null;

        if (Instance.PermaEvents.HasInventory(key))
        {
            return Instance.PermaEvents.GetInventory(key);
        }

        if (Instance.cycleEvents.HasInventory(key))
        {
            return Instance.cycleEvents.GetInventory(key);
        }

        return null;
    }
    public static List<ItemPlacementData> GetInventory(string key, List<ItemPlacementData> defaultInv)
    {
        List<ItemPlacementData> result = GetInventory(key);
        if (result == null)
            result = defaultInv;
        return result;
    }
    public static void CacheInventory(string key, List<ItemPlacementData> inv, bool isPerma)
    {
        if (key == null)
            throw new System.Exception("Null Inventory Cache Key");
        if (isPerma)
        {
            Instance.PermaEvents.UpdateInventory(key, inv);
        }
        else
        {
            Instance.cycleEvents.UpdateInventory(key, inv);
        }
    }
}
