﻿using System.Collections;
using UnityEngine;


public class DestroyIfNotCycle : MonoBehaviour
{
    public bool cycleOne;
    public bool cycleTwo;
    private void Awake()
    {
        bool keep = false;
        if (cycleOne && SaveData.currentSave.numCycles <= 1)
            keep = true;
        if (cycleTwo && SaveData.currentSave.numCycles <= 2)
            keep = true;

        if (!keep)
            Destroy(this.gameObject);
    }
}
