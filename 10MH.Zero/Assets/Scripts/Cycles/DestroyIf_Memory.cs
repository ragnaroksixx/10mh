﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

public class DestroyIf_Memory : MemoryListener
{
    public bool invert = false;
    public bool disableInstead = false;
    [ShowIf("hasMultipleMems")]
    public bool AND = true;
    public string category = "";
    [ListDrawerSettings(Expanded = true)]
    public List<string> memories = new List<string>();

    void Evaluate()
    {
        bool result = true;
        foreach (string item in memories)
        {
            if (AND)
            {
                result = result && CycleManager.Instance.HasEvent(category, item);
                if (!result)
                    break;
            }
            else // OR
            {
                result = CycleManager.Instance.HasEvent(category, item);
                if (result)
                    break;
            }
        }
        if (invert)
            result = !result;

        if (disableInstead)
        {
            gameObject.SetActive(!result);
        }
        else
        {
            if (result)
                Destroy(this.gameObject);
        }
    }
    bool hasMultipleMems()
    {
        return memories.Count > 1;
    }

    public override void OnMemoryGained()
    {
        Evaluate();
    }
}
