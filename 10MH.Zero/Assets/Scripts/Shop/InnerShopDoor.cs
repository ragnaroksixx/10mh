﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;

public class InnerShopDoor : AreaTransitionInteraction
{
    public Scene exitScene;
    public override void OnInteract()
    {
        if (PlayerInventory.Instance.HasPurchaseItems())
            DialogueRenderer.Instance.PlayScene(exitScene, null);
        else
            base.OnInteract();
    }
}
