﻿using UnityEngine;
using System.Collections;

namespace NHG.Dialogue
{
    public class ShopTotalText : CharacterTextElement
    {
        public override string eventName => base.eventName+"shop";

        public override IEnumerator Process(Layer layer)
        {
            tags.SetText(GetTotalText());
            yield return base.Process(layer);
        }
        public static int GetTotal()
        {
            int price = 0;
            foreach (ItemPlacementData item in PlayerInventory.Instance.GetAllPurchaseables())
            {
                price += (item.item as PurchaseItem).price;
            }
            return price;
        }
        public static string GetTotalText()
        {
            return Format(GetTotal());
        }
        public static string Format(int val)
        {
            return "$" + val;
        }
    }
}
