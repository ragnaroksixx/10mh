﻿using UnityEngine;
using System.Collections;

public class PurchaseItem : Item
{
    public Item brokenItem;
    public Item purchasedItem;
    public int price;

    public string GetPriceText()
    {
        return "$" + price;
    }
    public void AddGrabEvent()
    {
       // CycleManager.Instance.AddTempEvent(GrabID());
    }
    public void RemoveGrabEvent()
    {
       // CycleManager.Instance.RemoveTempEvent(GrabID());
    }
    public string GrabID()
    {
        return "GRABBED" + Identifier;
    }
}
