﻿using UnityEngine;
using System.Collections;

namespace NHG.Dialogue
{
    public class PurchaseItemsAction : DialogueAction
    {
        public override string eventName => "purchase";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            PlayerInventory.Instance.PurchaseItems();
        }
    }
}
