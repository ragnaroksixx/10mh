﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShopPlacementSlot : ItemPickUpInteraction
{
    public Image image;
    public PurchaseItem inSlotItem;
    public override void Awake()
    {
        SetItem(item.item as PurchaseItem);
        base.Awake();
    }
    public override ItemPlacementData GetItem()
    {
        if (inSlotItem == null)
            return null;
        item.item = inSlotItem;
        return item;
    }
    protected override void OnClose()
    {
        if (inSlotItem)
            inSlotItem.AddGrabEvent();
        if (invCache.inventoryItems.Count > 0)
        {
            SetItem(invCache.inventoryItems[0].item as PurchaseItem);
        }
        else
        {
            DestoryThis();
        }
        if (inSlotItem)
            inSlotItem.RemoveGrabEvent();
        base.OnClose();
    }
    public override string ID()
    {
        if (inSlotItem == null)
            return string.Empty;
        return inSlotItem.GrabID();
    }
    public override void DestoryThis()
    {
        inSlotItem = null;
        image.sprite = GlobalData10MH.Instance.transparentImage;
        //inspectText = "";
    }
    public void SetItem(PurchaseItem i)
    {
        inSlotItem = i;
        image.sprite = inSlotItem.image;
        //inspectText = i.description;
    }
}
