﻿using UnityEngine;
using System.Collections;

public class ShopInventoryDropArea : InventoryDropArea
{
    public override bool CanPlace(ItemPlacementData i)
    {
        return i.item is PurchaseItem && Inventory.inventoryItems.Count == 0;
    }
}
