﻿using UnityEngine;
using System.Collections;

public class ShopDoor : AreaTransitionInteraction
{
    public override bool IsUnLocked()
    {
        if (TimeController.IsPast(ShopArea.closingTime))
            return false;
        return base.IsUnLocked();
    }
}
