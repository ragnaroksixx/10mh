﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;
public class ShopArea : Area
{
    public static TimeRef closingTime = new TimeRef(4, 0);
    public Scene closingTimeScene;
    public AreaTransitionData classRoomArea;
    public override void OnAreaEnter()
    {
        base.OnAreaEnter();
        TimeController.AddTimeEvent(closingTime, CloseShop);
    }
    public override void OnAreaExit()
    {
        TimeController.RemoveTimeEvent(closingTime, CloseShop);
        base.OnAreaExit();
    }
    public void CloseShop()
    {
        StartCoroutine(CloseShopRoutine());
    }
    IEnumerator CloseShopRoutine()
    {
        //InventoryManager.ReleaseHeld();
        //InventoryManager.DestoryLeftInventory();
       // InventoryManager.Instance.CloseInventory();
        yield return DialogueRenderer.Instance.PlayScene(closingTimeScene, null);
        //Remove all purchase Items
        PlayerInventory.Instance.RemoveAllPurchasables();
        //Kick player to classroom area
        SceneController.SetActiveEnvironment(classRoomArea);
    }
}
