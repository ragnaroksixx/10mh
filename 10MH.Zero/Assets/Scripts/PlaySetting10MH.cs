﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu()]
public class PlaySetting10MH : SingletonScriptableObject<PlaySetting10MH>
{
    public bool skipIntro;

    public bool useDebugSpawn = false;
    [ShowIf("useDebugSpawn")]
    public AreaTransitionData debugSpawn;

    public bool startInBattle = false;
    [ShowIf("startInBattle")]
    public BattleEncounter startEncounter;
    public static bool SKIP_INTRO => Instance != null ? Instance.skipIntro : false;


#if UNITY_EDITOR
    [OnValueChanged("OnSceneChanged")]
    public bool useCustomStartScene;
    [OnValueChanged("OnSceneChanged")]
    [ShowIf("useCustomStartScene")]
    public SceneAsset customMainScene;

    protected override void OnInitialize()
    {
        base.OnInitialize();
        if (EditorSceneManager.playModeStartScene != default)
        {
            useCustomStartScene = true;
            customMainScene = EditorSceneManager.playModeStartScene;
        }
        else if (useCustomStartScene)
        {
            EditorSceneManager.playModeStartScene = customMainScene;
        }
    }
    public void Init()
    {
        OnInitialize();
    }
    void OnSceneChanged()
    {
        if (!useCustomStartScene || customMainScene == null)
        {
            EditorSceneManager.playModeStartScene = default;
        }
        else
        {
            EditorSceneManager.playModeStartScene = customMainScene;
        }
    }

    [Button(ButtonSizes.Large)]
    public void UseMasterScene()
    {
        useCustomStartScene = true;
        SceneAsset firstScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(SceneUtility.GetScenePathByBuildIndex(1));
        customMainScene = firstScene;
        OnSceneChanged();
    }
    [Button(ButtonSizes.Medium)]
    public void ClearData()
    {
        PlayerPrefs.DeleteAll();
    }

    [Button(ButtonSizes.Medium)]
    public void CreateNewGamePlusData()
    {
        SaveData sd = StartScreen.CreateNewGamePlusData();
        SaveData.SaveGame(sd);
    }
#endif
}
