﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public abstract class AreaTransitionLock : MonoBehaviour
{
    public DialogueVariable lockedScene;
    public UnityEvent onLocked;
    // Use this for initialization
    protected virtual void Start()
    {

    }

    public abstract bool IsLocked(); 
}
