﻿using System.Collections;
using UnityEngine;

public class ManualLock : AreaTransitionLock
{
    public bool isLocked=true;
    public override bool IsLocked()
    {
        return isLocked;
    }

}

