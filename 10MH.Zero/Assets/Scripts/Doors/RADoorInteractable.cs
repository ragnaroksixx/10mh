﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RADoorInteractable : Interactable
{
    public ItemLock transition;
    public Pose doorOpenPose;
    public Light[] lights;
    public Transform door;
    // Use this for initialization
    public override void Start()
    {
        TimeController.TriggerOrAddCalamity(OnCalamity);
    }
    private void OnDestroy()
    {
        TimeController.TriggerOrAddCalamity(OnCalamity);
    }
    void OnCalamity()
    {
        transition.requiredItems = new List<Item>();
        foreach (Light item in lights)
        {
            item.enabled = true;
        }
        door.transform.localPosition = doorOpenPose.position;
        door.transform.localRotation = doorOpenPose.rotation;
    }
}
