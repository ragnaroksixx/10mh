﻿using System.Collections;
using UnityEngine;


public class CalamityLock : InteractionBlocker
{
    public override bool IsBlocked()
    {
        return TimeController.IsCalamity();
    }
}
