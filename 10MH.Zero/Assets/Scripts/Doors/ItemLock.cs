﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemLock : AreaTransitionLock
{
    public List<Item> requiredItems;
    public override bool IsLocked()
    {
        if (requiredItems.Count == 0)
            return false;
        return !PlayerInventory.Instance.ContainsAny(requiredItems);
    }
}
