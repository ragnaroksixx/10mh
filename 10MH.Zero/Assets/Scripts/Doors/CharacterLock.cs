﻿using UnityEngine;
using System.Collections;

public class CharacterLock : AreaTransitionLock
{
    public CharacterData character;

    public override bool IsLocked()
    {
        return Area.IsCharacterInArea(character);
    }
}
