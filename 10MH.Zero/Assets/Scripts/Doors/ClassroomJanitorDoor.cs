﻿using System.Collections;
using UnityEngine;


public class ClassroomJanitorDoor : MonoBehaviour
{
    public ManualLock firstCycleLock;

    private void Start()
    {
        if (CycleManager.IsFirstCycle() && TimeController.IsPreCalamity())
        {
            firstCycleLock.isLocked = true;
            firstCycleLock.onLocked.AddListener(OnFirstCycleInteract);
        }
    }

    public void OnFirstCycleInteract()
    {
        PathManager.SetPath(null);
        TimeController.Instance.ClearHelperTiime();

    }
}
