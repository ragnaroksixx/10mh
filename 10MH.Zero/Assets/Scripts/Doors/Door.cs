﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;


public class Door : MonoBehaviour
{
    public float height = 0;

    public GameObject spawnPoint;
    [HideIf("NoSpawnPoint")]
    public float spawnPointDistance = 1.5f;
    public bool NoSpawnPoint => spawnPoint == null;
    [Button(DirtyOnClick = true)]
    public void TryAlign()
    {
        float heightOffset = 3;
        Vector3[] dirs = new Vector3[4]
        {
            transform.forward,
            transform.right,
            -transform.forward,
            -transform.right
        };
        foreach (Vector3 dir in dirs)
        {
            for (float i = 0.5f; i < heightOffset; i += 0.5f)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position + Vector3.up * heightOffset, dir, out hit, i))
                {
                    if (hit.transform.IsChildOf(transform) || hit.transform == transform)
                        continue;
                    Vector3 pos = hit.point;
                    pos.y = height;
                    transform.position = pos;
                    transform.forward = -hit.normal;
                    return;
                }
            }
        }
    }

    [Button(DirtyOnClick =true)]
    [HideIf("NoSpawnPoint")]
    public void TryMoveWaypoint()
    {
        spawnPoint.transform.position = transform.position - transform.forward * spawnPointDistance;
        spawnPoint.transform.forward = -transform.forward;

    }
}
