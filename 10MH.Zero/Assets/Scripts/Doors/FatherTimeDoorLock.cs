﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FatherTimeDoorLock : AreaTransitionLock
{
    public InventoryInteraction left, right;

    public override bool IsLocked()
    {
        ItemPlacementData[] leftInv = left.GetInvItems();
        ItemPlacementData[] rightInv = right.GetInvItems();

        foreach (ItemPlacementData itemL in leftInv)
        {
            foreach (ItemPlacementData itemR in rightInv)
            {
                if (itemL.item == itemR.item)
                    return false;
            }
        }
        return true;
    }
}
