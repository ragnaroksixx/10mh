﻿using System.Collections;
using UnityEngine;


public class FilterSearchAttribute : PropertyAttribute
{
    public string filter { get; private set; }

    public FilterSearchAttribute(string fil)
    {
        filter = fil;
    }
}
