﻿using NHG.Dialogue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

public static class DialogueEventLibrary
{
    public static Dictionary<string, Type> eventTypes = null;
    static DialogueEventLibrary()
    {
        eventTypes = new Dictionary<string, Type>();
        string definedIn = typeof(DialogueAction).Assembly.GetName().Name;
        foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
        {
            // Note that we have to call GetName().Name.  Just GetName() will not work.  The following
            // if statement never ran when I tried to compare the results of GetName().
            if ((!assembly.GlobalAssemblyCache) && ((assembly.GetName().Name == definedIn) || assembly.GetReferencedAssemblies().Any(a => a.Name == definedIn)))
                foreach (Type type in assembly.GetTypes())
                    if (type.IsSubclassOf((typeof(DialogueAction))))
                    {
                        if (type.IsAbstract) continue;
                        DialogueAction da = Activator.CreateInstance(type) as DialogueAction;
                        eventTypes.Add(da.eventName.AsKey(), type);
                        foreach (string name in da.eventAlias)
                        {
                            eventTypes.Add(name.AsKey(), type);
                        }
                    }
        }
    }

    public static void Init() { }
}
public static class CombatEventLibrary
{
    public static Dictionary<string, Type> eventTypes = null;
    static CombatEventLibrary()
    {
        eventTypes = new Dictionary<string, Type>();
        string definedIn = typeof(CombatActionEvent).Assembly.GetName().Name;
        foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
        {
            // Note that we have to call GetName().Name.  Just GetName() will not work.  The following
            // if statement never ran when I tried to compare the results of GetName().
            if ((!assembly.GlobalAssemblyCache) && ((assembly.GetName().Name == definedIn) || assembly.GetReferencedAssemblies().Any(a => a.Name == definedIn)))
                foreach (Type type in assembly.GetTypes())
                    if (type.IsSubclassOf((typeof(CombatActionEvent))))
                    {
                        if (type.IsAbstract) continue;
                        CombatActionEvent da = Activator.CreateInstance(type) as CombatActionEvent;
                        eventTypes.Add(da.eventName.AsKey(), type);
                        foreach (string name in da.eventAlias)
                        {
                            eventTypes.Add(name.AsKey(), type);
                        }
                    }
        }
    }
    public static void Init() { }
}
public static class LogicLibary
{
    public static Dictionary<string, Type> logicTypes = null;
    public static Dictionary<string, Type> conditionTypes = null;
    static LogicLibary()
    {
        logicTypes = new Dictionary<string, Type>();
        conditionTypes = new Dictionary<string, Type>();
        string definedInLOGIC = typeof(DialogueLogic).Assembly.GetName().Name;
        string definedInCONDITION = typeof(DialogueLogic).Assembly.GetName().Name;
        foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
        {
            // Note that we have to call GetName().Name.  Just GetName() will not work.  The following
            // if statement never ran when I tried to compare the results of GetName().
            if ((!assembly.GlobalAssemblyCache) && ((assembly.GetName().Name == definedInLOGIC) || assembly.GetReferencedAssemblies().Any(a => a.Name == definedInLOGIC)
                || (assembly.GetName().Name == definedInCONDITION) || assembly.GetReferencedAssemblies().Any(a => a.Name == definedInCONDITION)))
                foreach (Type type in assembly.GetTypes())
                    if (type.IsSubclassOf((typeof(DialogueLogic))))
                    {
                        if (type.IsAbstract) continue;
                        DialogueLogic dl = Activator.CreateInstance(type) as DialogueLogic;
                        logicTypes.Add(dl.logicID.ToLower().Trim(), type);
                    }else if (type.IsSubclassOf((typeof(DialogueCondition))))
                    {
                        if (type.IsAbstract) continue;
                        DialogueCondition dc = Activator.CreateInstance(type) as DialogueCondition;
                        conditionTypes.Add(dc.conditionID.ToLower().Trim(), type);
                    }
        }
    }
    public static void Init() { }
}

