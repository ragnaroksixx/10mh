﻿using System.Collections;
using UnityEngine;


public class Clock : MonoBehaviour
{
    public Transform minuteHand;
    public Transform secondHand;
    public Transform hourHand;
    private const float
       hoursToDegrees = 360f / 12f,
       minutesToDegrees = 360f / 60f,
       secondsToDegrees = 360f / 60f;
    // Use this for initialization
    void Start()
    {
        hourHand.localRotation =
    Quaternion.Euler(TimeController.START_HOUR * -hoursToDegrees, 0f, 0);
        minuteHand.localRotation =
            Quaternion.Euler(TimeController.Instance.CurrentTime.minute * -minutesToDegrees, 0f, 0);
        secondHand.localRotation =
            Quaternion.Euler(TimeController.Instance.CurrentTime.second * -secondsToDegrees, 0f, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Area.activeArea.freezeTime)
        {
            float seconds = TimeController.Instance.CurrentTime.second;
            seconds += Mathf.Repeat(Time.time, 0.5f);
            secondHand.localRotation =
                Quaternion.Euler(seconds * -secondsToDegrees, 0f, 0);
        }
        else
        {
            hourHand.localRotation =
                Quaternion.Euler(TimeController.START_HOUR * -hoursToDegrees, 0f, 0);
            minuteHand.localRotation =
                Quaternion.Euler(TimeController.Instance.CurrentTime.minute * -minutesToDegrees, 0f, 0);
            secondHand.localRotation =
                Quaternion.Euler(TimeController.Instance.CurrentTime.second * -secondsToDegrees, 0f, 0);
        }
    }
}
