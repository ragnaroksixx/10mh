﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ExcessInventory : Inventory
{
    public static bool overExerted = false;
    public Color exertedColor;
    public CanvasGroup cGroup;
    private IEnumerator Start()
    {
        heldItemRoot = InventoryManager.Instance.heldItemRoot;
        yield return Init(null, false, new ItemPlacementData[0]);
        UpdateStatus();
        Publisher.Subscribe<InventoryChangedEvent>(OnInventoryChanged);
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        Publisher.Unsubscribe<InventoryChangedEvent>(OnInventoryChanged);
    }
    public override void Add(ItemPlacementData i, bool release, InventoryItemUI ui)
    {
        base.Add(i, release, ui);
        UpdateStatus();
        ui.SetGridColor(exertedColor);
    }
    public override void Remove(ItemPlacementData i, InventoryItemUI ui, bool hold)
    {
        base.Remove(i, ui, hold);
        UpdateStatus();
    }
    void OnInventoryChanged(InventoryChangedEvent e)
    {
        SetColor();
        foreach (ItemPlacementData item in inventoryItems)
        {
            item.runtimeUI?.SetGridColor(exertedColor);
        }
    }
    void UpdateStatus()
    {
        canPlaceItems = inventoryItems.Count == 0;
        overExerted = !canPlaceItems;
        SetColor();
    }
    void SetColor()
    {
        float a = 0;
        bool held = InventoryManager.IsItemHeld();
        if (overExerted)
            a = 1;
        else if (held && !BattleController.IsInBattle)
            a = 0.35f;
        else
            a = 0;
        cGroup.DOFade(a, 0.25f);
    }
}
