﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "New ID Item", menuName = "Inventory/ID")]
public class CharacterIDItem : Item
{
    [OnValueChanged("UpdateFields")]
    public CharacterData characterRef;

    public override string Identifier => (characterRef.displayName + "id").ToLower();
    [Button]
    private void UpdateFields()
    {
        if (characterRef == null) return;
        id = characterRef.Identifier + "id";
        itemName = characterRef.displayName + "'s" + " ID";
        size = new Vector2Int(1, 1);
        imageRotation = 30;
        imageScale = 1;
        description = "A student ID for " + characterRef.FullName();
        lockInventoryChange = false;
    }
    public override string GetName()
    {
        if (characterRef == null)
            return "Character ID - MISSING CHARACTER";
        return "Character ID - " + characterRef.displayName;
    }
}
