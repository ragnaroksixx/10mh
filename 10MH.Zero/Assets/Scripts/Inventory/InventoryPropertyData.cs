﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "New Inventory Data", menuName = "Inventory/Inventory Info")]
public class InventoryPropertyData : ScriptableObject, IUnquieScriptableObject
{
    public string displayName="";
    public int width, height;
    public List<Vector2Int> invalidLocations;
    public int Size { get => width * height; }

    public string Identifier => displayName;
    public string[] Alias => new string[] { };

    public void Init()
    {
    }
}
