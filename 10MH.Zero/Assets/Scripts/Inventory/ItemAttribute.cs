﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEditor;
using UnityEngine;


[CreateAssetMenu(menuName = "Inventory/Attribute")]
public class ItemAttribute : ScriptableObject
{
    public string displayName;
    public Sprite icon;

    [Button("UpdateName")]
    public virtual void UpdateName()
    {
#if UNITY_EDITOR
        string path = AssetDatabase.GetAssetPath(GetInstanceID());
        string targetName = displayName;
        if (name != targetName)
        {
            name = targetName;
            AssetDatabase.RenameAsset(path, targetName);
            AssetDatabase.Refresh();

        }
#endif
    }
}
