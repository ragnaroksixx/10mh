﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class InventorySet : ScriptableObject, IUnquieScriptableObject
{
    public string identifier;
    public InventoryPropertyData inventorySize;

    [ListDrawerSettings(Expanded = true)]
    public List<ItemPlacementData> items = new List<ItemPlacementData>();

    public string Identifier => identifier;

    public string[] Alias => new string[0];

    public void Init()
    {
    }
    public void LoadInventory()
    {
        PlayerInventory.Instance.RemoveAll();
        PlayerInventory.Instance.TrySpawnItems(items.ToArray());
    }
}
