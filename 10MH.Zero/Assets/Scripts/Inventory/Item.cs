﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject, IUnquieScriptableObject
{
    [SerializeField]
    protected string id;
    public string itemName;
    public Vector2Int size = new Vector2Int(1, 1);
    public List<Vector2Int> ignoreSpaces = new List<Vector2Int>();
    public Sprite image;
    public float imageRotation = 0;
    public float imageScale = 1;
    [TextArea(5, 20)]
    public string description;
    public bool lockInventoryChange;
    public bool lockMovement;
    public ItemOrientationX defaultOrientation = ItemOrientationX.RIGHT;
    public virtual string Identifier => id;
    public string[] Alias => new string[] { };

    public string Description => description;
    public string Name => itemName;
    public virtual int AP => -1;
    public virtual int Durability => -1;

    [FoldoutGroup("Sol Bound", expanded: false)]
    public bool isSoulBound;
    [FoldoutGroup("Sol Bound", expanded: false)]
    public List<Vector3Int> soulSpaces = new List<Vector3Int>();
    [Button("Validate")]
    public virtual void Validate()
    {
#if UNITY_EDITOR
        string path = AssetDatabase.GetAssetPath(GetInstanceID());
        string targetName = GetName();
        if (name != targetName)
        {
            name = targetName;
            AssetDatabase.RenameAsset(path, targetName);
            AssetDatabase.Refresh();

        }
#endif
    }
    public virtual string GetName()
    {
        string targetName = Identifier.ToString() + " - " + itemName;
        targetName = targetName.RemoveSpecialCharacters();
        return targetName;
    }
    public void Init()
    {
    }
}
public enum ItemOrientationX
{
    RIGHT = 0,
    DOWN = 1,
    LEFT = 2,
    UP = 3
}

