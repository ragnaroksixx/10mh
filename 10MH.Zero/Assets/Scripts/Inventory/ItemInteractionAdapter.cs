﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


public static class CombatInteractionAdapter
{
    static List<ActionItem> usedItems = new List<ActionItem>();

    public static List<ActionItem> CombatQueue { get => usedItems; }
    public static IEnumerable<ActionItem> CombatQueueActions { get => usedItems.Where(x => x != null); }

    public static void AddToQueue(InventoryItemUI itemUI, CharacterDropArea hb)
    {
        CombatItem ci = (itemUI.ItemData.item as CombatItem);
        int queueReduction = 0;//GetQueueReduction();
        int apCost = Mathf.Max(0, ci.GetCastTime() - queueReduction);
        ActionItem a = new ActionItem()
        {
            item = itemUI,
            target = hb.ui.Unit,
        };

        CycleManager.PlayerCombatUnit.currentAP -= apCost;
        BattleController.Instance.brains.OnActionUsed(a);

        PerformAction(a);
        PlayerInventory.Instance.UpdateInventoryUI();
    }

    public static int CountInQueue(InventoryItemUI ui)
    {
        int count = 0;
        foreach (ActionItem item in usedItems)
        {
            if (item != null && item.item == ui)
            {
                count++;
            }
        }
        return count;
    }

    public static void ClearQueue()
    {
        List<ActionItem> temp = new List<ActionItem>(usedItems);
        usedItems.Clear();
        foreach (ActionItem aItem in temp)
        {
            aItem?.OnClear();
        }
    }

    public static ActionItem Peek()
    {
        if (usedItems.Count == 0) return null;
        return usedItems[usedItems.Count - 1];
    }

    public static ActionItem Pop()
    {
        if (usedItems.Count == 0) return null;
        ActionItem ai = usedItems[usedItems.Count - 1];
        usedItems.RemoveAt(usedItems.Count - 1);
        ai?.OnPop();
        return ai;
    }
    public static void RemoveFromQueue(InventoryItemUI ui)
    {
        int index = -1;
        foreach (ActionItem actionItem in usedItems)
        {
            index++;
            if (actionItem.item == ui)
            {
                break;
            }
        }
        if (index < 0 || index >= usedItems.Count) return;
        ActionItem ai = usedItems[index];
        usedItems.RemoveAt(index);
        ai?.OnPop();

    }
    public static void PerformTopAction()
    {
        if (usedItems.Count == 0)
            return;
        ActionItem ai = Pop();
        PerformAction(ai);
    }
    public static void PerformAction(ActionItem ai)
    {
        HandleInteraction(ai);
    }
    public static bool IsValidInteraction(InventoryItemUI i, CharacterDropArea area)
    {
        if (!(i.Item is CombatItem)) return false;
        if (!BattleController.Instance.brains.CanPlayerUseItem(i))
            return false;
        CombatItem cItem = i.Item as CombatItem;
        BattleUnit target = (area as CharacterDropArea).ui.Unit;

        if (target == CycleManager.PlayerCombatUnit)
        {
            if (cItem.CanTargetSelf())
                return true;
        }
        else
        {
            if (cItem.CanTargetEnemy())
                return true;
        }
        return false;
    }
    public static void HandleInteraction(ActionItem ai)
    {
        InventoryItemUI ui = ai.item;
        BattleUnit target = ai.target;

        if (target.IsDead())
        {
            return;
        }
        CombatItem cItem = ui.Item as CombatItem;
        int value = 0;

        if (target == CycleManager.PlayerCombatUnit)
        {
            cItem.PerformActions(CycleManager.PlayerCombatUnit, target);
        }
        else
        {
            cItem.PerformActions(CycleManager.PlayerCombatUnit, target);
            //if (cItem.IsHeal())
            //{
            //    value = cItem.CalcHeal();
            //    target.Heal(value);
            //}
            //else if (cItem.IsAttack())
            //{
            //    //only damage
            //    value = cItem.CalcDamage(CycleManager.PlayerCombatUnit, target as EnemyUnit);
            //    if (CycleManager.PlayerCombatUnit.lastUsedItem.IsBroken)
            //    {
            //        value *= 2;
            //        FloatingTextController.PopUpText("BREAK!",
            //            Color.red,
            //            target.unitController.UI.hpRoot.position, FloatingText.TextType.TIME);
            //    }
            //    Attack(CycleManager.PlayerCombatUnit, target, value);
            //    //target.TakeDamage(value, CycleManager.Player);
            //    //later apply status effects
            //}
        }
        CombatInfo ci = new CombatInfo(CycleManager.PlayerCombatUnit, target, value);
        cItem.OnUse(ci);

        if (!(cItem.IsBlock() && target == CycleManager.PlayerCombatUnit))
        {
            bool fatigue = CycleManager.PlayerCombatUnit.UseStamina(cItem.actionPoints * 30);
            float castTime = 1;
            CycleManager.PlayerCombatUnit.LockStaminaRegen(fatigue ? castTime * 2 : castTime);
        }

        //CycleManager.PlayerCombatUnit.UseActionATB(ui);
    }

    public static void Attack(BattleUnit source, BattleUnit target, int amount)
    {
        target.TakeDamage(amount, source);
    }

    public static int CalcDamage(int basePower, float additionalCritRate, BattleUnit user, BattleUnit target)
    {
        float damageUse = (basePower * user.data.stats.ATK) / (target.data.stats.DEF);

        int atkUpStacks = user.GetStatusCount(CombatEffectsLibrary.Instance.attackUp);

        damageUse += (damageUse * 0.25f) * atkUpStacks;

        bool crit = user.data.stats.LCK > user.data.Range(0, 10) + additionalCritRate;

        if (crit)
            damageUse *= 1.25f;

        damageUse *= user.data.Range(0.85f, 1);
        return Mathf.Max(1, (int)damageUse);
    }

    public static bool HasAnyQueuedActions()
    {
        return usedItems.Count != 0;
    }

    public static bool CanPickUpItem(InventoryItemUI ui)
    {
        return BattleController.Instance.brains.CanPlayerPickUpItem(ui);
    }
}
public class ActionItem
{
    public InventoryItemUI item;
    public BattleUnit target;
    public CombatItem CombatItem => item.Item as CombatItem;

    public bool IsBlock => CombatItem.IsBlock() && target == CycleManager.PlayerCombatUnit;
    public virtual void Resolve()
    {

    }
    public void OnClear()
    {
        PlayerInventory.Instance.UpdateInventoryUI();
    }
    public void OnPop()
    {
        PlayerInventory.Instance.UpdateInventoryUI();
    }
}
public static class DialogueInteractionAdapter
{
    public static void HandleInteraction(InventoryItemUI ui, DropArea area)
    {
        CharacterDropArea characterDropArea = area as CharacterDropArea;
        if (characterDropArea.ItemEvents != null && characterDropArea.ItemEvents.ContainsKey(ui.Item))
        {
            NHG.Dialogue.ItemEvents itemEvent
                = characterDropArea.ItemEvents[ui.Item];
            Sequence successfulGiveFlash = ui.FlashColor(ui.validGiveInteractionColor, 2, false);
            successfulGiveFlash.OnComplete(() => { ResolveUsefulInteraction(ui, itemEvent); });
        }
        else
        {
            ui.Inventory.Add(ui.ItemData, true, null);
            ui.ReturnToCacheInvalid();
        }
    }
    static void ResolveUsefulInteraction(InventoryItemUI ui, NHG.Dialogue.ItemEvents itemEvent)
    {
        InventoryManager.Instance.CloseInventory();
        DialogueRenderer.SetJumpSceneIDS(itemEvent.jumpID);
        if (itemEvent.takeItem)
        {
            ui.OnGive();
            //if (ui.ItemData.durability > 0)
            //{
            //    ui.Inventory.Add(ui.ItemData, true, null);
            //    ui.ReturnToCacheFancy();
            //}
        }
        else
        {
            ui.Inventory.Add(ui.ItemData, true, null);
            ui.ReturnToCacheFancy();
        }
    }
    public static bool IsValidInteraction(InventoryItemUI i, DropArea area)
    {
        return true;
    }
}

