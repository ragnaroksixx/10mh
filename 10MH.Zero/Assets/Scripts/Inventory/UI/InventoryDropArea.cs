﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

public class InventoryDropArea : DropArea
{
    public Vector2Int pos;
    Inventory inv;
    public Image emptyImage;
    Color defaultColor;
    public CanvasGroup cGroup;
    public Inventory Inventory { get => inv; }

    public void Init(Inventory invObj, int gridIndex)
    {
        inv = invObj;
        int x, y;
        x = gridIndex % inv.data.width;
        y = Mathf.FloorToInt(gridIndex / inv.data.width);
        pos = new Vector2Int(x, y);
        bool invalid = invObj.data.invalidLocations.Contains(pos);
        if (invalid)
        {
            //xImage.enabled = false;
            cGroup.alpha = 0.25f;
            cGroup.blocksRaycasts = false;
        }
        defaultColor = emptyImage.color;
        inv.updateInventoryUI += UpdateUI;
        UpdateUI();
    }
    private void OnDestroy()
    {
        inv.updateInventoryUI -= UpdateUI;
    }
    public override bool CanResolve(IClickableSource source)
    {
        if (!(source is InventoryItemUI))
            return false;
        InventoryItemUI ui = source as InventoryItemUI;
        if (BattleController.IsInBattle)
            return false;
        if (ui.Item.lockMovement)
            return false;
        if (ui.Item.lockInventoryChange && (ui.Inventory != Inventory))
            return false;

        return CanPlace((source as InventoryItemUI).ItemData);
    }
    public override void Resolve(IClickableSource source)
    {
        InventoryItemUI ui = source as InventoryItemUI;
        ui.Drop(this);
    }
    public virtual bool CanPlace(ItemPlacementData ipd)
    {
        Item i = ipd.item;
        if (!inv.canPlaceItems)
            return false;
        bool canPlace = true;

        bool isSmallerThanInv;
        PositionData position = ItemPlacementData.GetBounds(i, pos, ipd.orientation);
        switch (ipd.orientation)
        {
            case ItemOrientationX.RIGHT:
                isSmallerThanInv = (position.bottomRight.x < inv.data.width);
                isSmallerThanInv = isSmallerThanInv && (position.bottomRight.y < inv.data.height);
                break;
            case ItemOrientationX.DOWN:
                isSmallerThanInv = (position.topLeft.x >= 0);
                isSmallerThanInv = isSmallerThanInv && (position.bottomRight.y < inv.data.height);
                break;
            case ItemOrientationX.LEFT:
                isSmallerThanInv = (position.topLeft.x >= 0);
                isSmallerThanInv = isSmallerThanInv && (position.topLeft.y >= 0);
                break;
            case ItemOrientationX.UP:
                isSmallerThanInv = (position.bounds.z < inv.data.width);
                isSmallerThanInv = isSmallerThanInv && (position.topLeft.y >= 0);
                break;
            default:
                isSmallerThanInv = false;
                break;
        }

        canPlace = canPlace && isSmallerThanInv;


        canPlace = canPlace && !ipd.IncludesLocation(inv.data.invalidLocations.ToArray());
        if (!canPlace) return false;

        //check if spots are occupied
        for (int x = 0; x < position.width; x++)
        {
            for (int y = 0; y < position.height; y++)
            {
                Vector2Int vec = position.topLeft + new Vector2Int(x, y);
                if (position.IsIgnoreLocal(new Vector2Int(x, y)))
                    continue;
                if (inv.IsOccupied(vec))
                    return false;
            }
        }

        return true;
    }

    public void UpdateUI()
    {
        bool isOccupied = inv.IsOccupied(pos);
        bool isSolBound = inv.IsSoulBoundPos(pos);
        bool show = isSolBound && !BattleController.IsInBattle && !isOccupied;
        emptyImage.DOKill();
        if (show)
        {
            emptyImage.gameObject.SetActive(false);
            //emptyImage.transform.localScale = Vector3.zero;
            emptyImage.gameObject.SetActive(true);
           // emptyImage.transform.DOScale(0.5f, 0.1f)
           //     .SetDelay(0.25f);
        }
        else
        {
            emptyImage.gameObject.SetActive(false);
        }
    }
}
