﻿using NHG.Dialogue;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class WorldElementGrid : MonoBehaviour
{
    public RectTransform root;
    public HorizontalLayoutGroup horz;
    public VerticalLayoutGroup vert;

    private void OnValidate()
    {
        root.sizeDelta = WorldElement.size;

        horz.spacing = (int)WorldElement.size.x / WorldElement.divisons;

        vert.spacing = (int)WorldElement.size.y / WorldElement.divisons;
    }
}
