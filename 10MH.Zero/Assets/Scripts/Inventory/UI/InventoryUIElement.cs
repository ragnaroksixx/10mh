﻿using System.Collections;
using UnityEngine;


public class InventoryUIElement : MonoBehaviour
{
    public InventoryItemUI itemUI;
    protected virtual void Awake()
    {
        itemUI.OnItemChanged += OnItemChanged;
        enabled = false;
    }
    private void OnDestroy()
    {
        itemUI.OnItemChanged -= OnItemChanged;
    }
    protected virtual void OnItemChanged()
    {

    }

}
