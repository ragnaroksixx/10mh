﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public abstract class DropArea : MonoBehaviour, IClickableTarget
{
    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("Pointer Enter");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.Log("Pointer Exit");
    }

    public void ResolveInteraction(IClickableSource source)
    {
        Resolve(source);
    }

    public abstract void Resolve(IClickableSource source);
    public abstract bool CanResolve(IClickableSource source);

    public void OnDoubleClick()
    {
    }

    public bool IsInteractable { get => true; }

}

