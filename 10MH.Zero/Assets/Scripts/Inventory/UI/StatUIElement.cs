﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StatUIElement : MonoBehaviour
{
    public Image icon;
    public TMP_Text text;

    public void SetSprite(Sprite s)
    {
        icon.sprite = s;
        icon.enabled = s != null;
    }
    public virtual void SetText(int count)
    {
        text.text = count.ToString();
    }
}
