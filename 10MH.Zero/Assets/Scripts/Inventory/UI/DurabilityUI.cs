﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DurabilityUI : MonoBehaviour
{
    public Blip iconPrefab;
    List<Blip> icons;
    private void Awake()
    {
        icons = new List<Blip>();
    }
    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
    public void SetItem(InventoryItemUI i)
    {
        CombatItem cItem = i.Item as CombatItem;
        while (icons.Count < cItem.Durability)
            icons.Add(Instantiate<Blip>(iconPrefab, transform, false));

        while (icons.Count > cItem.Durability)
        {
            if (icons.Count == 0)
                break;
            Destroy(icons[icons.Count - 1].gameObject);
            icons.RemoveAt(icons.Count - 1);
        }

        for (int x = 0; x < icons.Count; x++)
        {
            icons[x].Set(1);
        }
    }
    public void UpdateDurability(InventoryItemUI i)
    {
        float durability = 10;// i.Durability;
        for (int x = 0; x < icons.Count; x++)
        {
            if (x + 1 <= durability)
            {
                icons[x].Set(1);
            }
            else if (durability - x > 0 && durability - x < 1)
                icons[x].Set(0.5f);
            else
                icons[x].Set(0);
        }
    }
}
