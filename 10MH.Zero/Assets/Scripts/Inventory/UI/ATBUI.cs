﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ATBUI : MonoBehaviour
{
    public Image image;
    public TMP_Text atbText;
    private void Awake()
    {
        Hide();
    }
    public void UpdateATB(float value)
    {
        if (value < 1)
            image.fillAmount = value;
        else
            image.fillAmount = value % (Mathf.FloorToInt(value));
        atbText.text = Mathf.FloorToInt(value).ToString();
    }
    public void Show()
    {
        gameObject.SetActive(true);
        atbText.transform.parent.gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
        atbText.transform.parent.gameObject.SetActive(false);
    }

    public void SetScale(float scale)
    {
        atbText.transform.parent.localScale = scale * Vector3.one;
    }
}
