﻿using System.Collections;
using TMPro;
using UnityEngine;

public class ItemStatsPanel : MonoBehaviour
{
    public InventoryItemUI item;
    public RectTransform hpRoot, powerRoot;
    public TMP_Text hpText, powerText;

    private void Awake()
    {
        Hide();
        Publisher.Subscribe<ItemHoverEvent>(Set);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<ItemHoverEvent>(Set);
    }
    public void Set(ItemHoverEvent e)
    {
        if (e.itemUI == item)
            Show();
        else
            Hide();
    }
    public void Show()
    {
        //CombatItem ci = item.Item as CombatItem;
        //if (ci == null)
        //    Hide();
        //else
        //{
        //    if (BattleController.IsInBattle)
        //        hpText.text = item.CombatDurability.ToString();
        //    else
        //        hpText.text = item.CycleMaxDurability.ToString();

        //    powerText.text = ci.power.ToString();
        //    hpText.gameObject.SetActive(!ci.unlimitedDurability);
        //    hpRoot.gameObject.SetActive(true);
        //    powerRoot.gameObject.SetActive(true);
        //}
    }
    public void Hide()
    {
        //hpRoot.gameObject.SetActive(false);
        //powerRoot.gameObject.SetActive(false);
    }
}
