﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonAudio : MonoBehaviour
{
    public AudioVariable highlight, unhighlight, click;
    IEnumerator Start()
    {
        EventTrigger b = gameObject.AddComponent<EventTrigger>();
        yield return null;
        if (b)
        {
            if (click!=null)
            {
                EventTrigger.Entry onclick = new EventTrigger.Entry();
                onclick.eventID = EventTriggerType.PointerClick;
                onclick.callback.AddListener((BaseEventData e) => { click.Play(); });
                b.triggers.Add(onclick);
            }
            if (highlight != null)
            {
                EventTrigger.Entry onHighlight = new EventTrigger.Entry();
                onHighlight.eventID = EventTriggerType.PointerEnter;
                onHighlight.callback.AddListener((BaseEventData e) => { highlight.Play(); });
                b.triggers.Add(onHighlight);
            }
            if (unhighlight != null)
            {
                EventTrigger.Entry onUnhighlight = new EventTrigger.Entry();
                onUnhighlight.eventID = EventTriggerType.PointerExit;
                onUnhighlight.callback.AddListener((BaseEventData e) => { unhighlight.Play(); });
                b.triggers.Add(onUnhighlight);
            }
        }
    }
}
