﻿using UnityEngine;
using UnityEngine.UI;

public class Blip : MonoBehaviour
{
    public Image bg;
    public Image image;

    public void Set(float val)
    {
        image.fillAmount = val;
    }
}
