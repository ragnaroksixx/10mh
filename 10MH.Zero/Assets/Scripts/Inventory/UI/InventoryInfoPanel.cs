﻿using UnityEngine;
using System.Collections;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;

public class InventoryInfoPanel : MonoBehaviour
{
    public TMP_Text nameText, descText;
    private void Awake()
    {
        SetItem(null);
        Publisher.Subscribe<ItemHoverEvent>(Set);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<ItemHoverEvent>(Set);
    }
    public void Set(ItemHoverEvent e)
    {
        SetItem(e.itemUI);
    }
    public void SetItem(InventoryItemUI i)
    {
        if (i != null)
        {
            nameText.text = i.Item.Name;
            descText.text = i.Item.Description;
        }
        else
        {
            nameText.text = "";
            descText.text = "";

        }
    }
}

public class ItemHoverEvent : PublisherEvent
{
    public InventoryItemUI itemUI;

    public ItemHoverEvent(InventoryItemUI itemUI)
    {
        this.itemUI = itemUI;
    }
}
public class ItemPickupEvent : PublisherEvent
{
    public InventoryItemUI itemUI;

    public ItemPickupEvent(InventoryItemUI itemUI)
    {
        this.itemUI = itemUI;
    }
}

