﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class APStatsUIElement : StatUIElement
{
    public Image[] apIcons;

    public override void SetText(int count)
    {
        if (count >= apIcons.Length)
        {
            ShowIcons(0);
            icon.gameObject.SetActive(true);
            text.gameObject.SetActive(true);
            base.SetText(count);
        }else
        {
            icon.gameObject.SetActive(false);
            text.gameObject.SetActive(false);
            ShowIcons(count);
        }
    }

    void ShowIcons(int i)
    {
        for (int x = 0; x < apIcons.Length; x++)
        {
            apIcons[x].gameObject.SetActive(x < i);
        }
    }
}
