﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class Draggable : MonoBehaviour, IClickableSource
{
    public CanvasGroup canvasGroup;
    public Vector3 cachePos;
    public Transform cachePosObject = null;
    protected bool followCursor = false;


    public virtual void PickUp()
    {
        followCursor = true;
        SetInteractable(false);
    }
    private void Update()
    {
        if (followCursor)
        {
            canvasGroup.transform.position = CursorController.CursorPosition;
        }
    }
    public void SetInteractable(bool val)
    {
        canvasGroup.blocksRaycasts = val;
    }
    public virtual void Drop(InventoryDropArea area)
    {
        followCursor = false;
    }
    public virtual void PlaceOn(DropArea area)
    {
        canvasGroup.transform.position = area.transform.position;
        canvasGroup.transform.rotation = area.transform.rotation;
        SetCachePos(area.transform);
    }
    public virtual void ReturnToCachePosition()
    {
        followCursor = false;
        canvasGroup.transform.position = CachePos;
        SetInteractable(true);
    }
    public Vector3 CachePos
    {
        get
        {
            if (cachePosObject)
                return cachePosObject.position;
            return cachePos;
        }
    }

    public bool IsInteractable { get => CanInteract(); }

    public void SetCachePos(Transform pos)
    {
        cachePosObject = pos;
    }
    public void SetCachePos(Vector3 pos)
    {
        cachePosObject = null;
        cachePos = pos;
    }

    public virtual bool CanInteract()
    {
        return canvasGroup.blocksRaycasts && !followCursor;
    }

    public void OnSource()
    {
        PickUp();
    }
    public virtual bool CanCancel(CancelSource reason)
    {
        return true;
    }
    public virtual void OnCancel(CancelSource reason)
    {
        ReturnToCachePosition();
    }
    public virtual void OnDoubleClick()
    {

    }
}
