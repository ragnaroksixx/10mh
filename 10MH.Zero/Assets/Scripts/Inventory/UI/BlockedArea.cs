﻿using UnityEngine;
using System.Collections;

public class BlockedArea : DropArea
{
    public override bool CanResolve(IClickableSource source)
    {
        return false;
    }

    public override void Resolve(IClickableSource source)
    {
    }
}
