﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeUI : MonoBehaviour
{
    public Blip iconPrefab;
    List<Blip> icons;
    int currentValue = 0;
    private void Awake()
    {
        icons = new List<Blip>();
    }
    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
    public void OnBattleStart(InventoryItemUI i)
    {
        CombatItem cItem = i.Item as CombatItem;
        int maxCharge = 5;
        while (icons.Count < maxCharge)
            icons.Add(Instantiate<Blip>(iconPrefab, transform, false));

        while (icons.Count > maxCharge)
        {
            if (icons.Count == 0)
                break;
            Destroy(icons[icons.Count - 1].gameObject);
            icons.RemoveAt(icons.Count - 1);
        }

        for (int x = 0; x < icons.Count; x++)
        {
            icons[x].Set(1);
        }
        Show();
    }
    public void UpdateCharge(InventoryItemUI i)
    {
        float charge = i.charge;// i.Durability;
        for (int x = 0; x < icons.Count; x++)
        {
            if (x + 1 <= charge)
            {
                icons[x].Set(1);
            }
            else if (charge - x > 0 && charge - x < 1)
                icons[x].Set(0.5f);
            else
                icons[x].Set(0);
        }
    }
    public void OnBattleStart(int max)
    {
        while (icons.Count < max)
            icons.Add(Instantiate<Blip>(iconPrefab, transform, false));

        while (icons.Count > max)
        {
            if (icons.Count == 0)
                break;
            Destroy(icons[icons.Count - 1].gameObject);
            icons.RemoveAt(icons.Count - 1);
        }

        for (int x = 0; x < icons.Count; x++)
        {
            icons[x].Set(1);
            icons[x].bg.gameObject.SetActive(false);
            icons[x].gameObject.SetActive(false);
        }
        Show();
    }
    public void UpdateCharge(int count)
    {
        if (currentValue == count)
            return;
        for (int x = 0; x < icons.Count; x++)
        {
            if (x < count)
            {
                icons[x].gameObject.SetActive(true);
            }
            else
            {
                icons[x].gameObject.SetActive(false);
            }
        }
        currentValue = count;
    }
}
