﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using UnityEngine.EventSystems;
using DG.Tweening;
using TMPro;
using System.Collections.Generic;
using System;
using UnityEngine.Events;

public class InventoryItemUI : Draggable, IPointerEnterHandler, IPointerExitHandler
{
    public Image itemImage;
    List<GridSlot> slots;
    List<Image> solArrows;
    public GridLayoutGroup grid;
    public Transform solArrowsRoot;
    ItemPlacementData itemData;
    Inventory inventory;
    bool isAnimating = false;
    Sequence moveSequence = null;
    public UnityAction OnItemChanged;
    public GameObject brokenUI;

    public ItemPlacementData ItemData { get => itemData; }
    public Item Item { get => itemData.item; }
    public Inventory Inventory { get => inventory; }
    public InventoryDropArea CacheInvPosition { get => cacheInvPosition; set => cacheInvPosition = value; }
    public int AP => (Item as CombatItem).AP;
    public CombatItem CombatItem => (Item as CombatItem);
    public bool IsBroken { get; set; }

    public GameObject root;
    public Image statusImageColor;
    public Color invalidInteractionColor;
    public Color validGiveInteractionColor;
    public Vector2 gridSize = new Vector2(70.5f, 70.5f);
    //Color defaultColor, defaultColor2;
    InventoryDropArea cacheInvPosition;
    ItemOrientationX cacheOrientation;
    public CustomAudioClip pickUpSFX, dropSFX, dropInvalidSFX, hoverSFX;
    Color defaultColor;
    public Image outlineShapeImage;

    public CanvasGroup arrowGroup, outlineGroup;

    public GameObject solboundPrefab;
    public Gradient durabilityColor;
    public Dictionary<Inventory, Vector2> previousPositions = new Dictionary<Inventory, Vector2>();

    public TMP_Text powerText, defenseText;

    public ChargeUI chargeUI;
    public DurabilityUI durabilityUI;
    public ATBUI atbUI;

    public int maxCharge;
    public int charge = 0;
    public int count;

    private void Awake()
    {
        once = true;
        slots = new List<GridSlot>();
        for (int i = 0; i < 20; i++)
        {
            slots.Add(new GridSlot(grid.transform.GetChild(i).GetComponent<Image>()));
        }
        powerText.transform.parent.gameObject.SetActive(false);
        defenseText.transform.parent.gameObject.SetActive(false);
        //defaultColor = itemSlots[0].color;
        //defaultColor = solBoundImage.color;
        //defaultColor2 = solBoundImage2.color;
    }
    public void SetItem(ItemPlacementData i, Inventory inv)
    {
        once = true;
        i.runtimeUI = this;
        canvasGroup.blocksRaycasts = !i.item.lockMovement;
        inventory = inv;
        inventory.onInventoryOpenClose += OnInventoryOpenClose;
        inventory.updateInventoryUI += UpdateUI;
        itemData = i;

        UpdateUI();
        if (i.item is CombatItem)
        {
            maxCharge = 5;
            powerText.text = CombatItem.attack.ToString();
            defenseText.text = CombatItem.defense.ToString();
        }
        else
        {
            maxCharge = -1;
        }

        OnItemChanged?.Invoke();
        root.name = "Item (" + i.item.itemName + ")";
    }
    [Button]
    void SetInEditor(ItemPlacementData i)
    {
        once = true;
        slots = new List<GridSlot>();
        for (int j = 0; j < 20; j++)
        {
            slots.Add(new GridSlot(grid.transform.GetChild(j).GetComponent<Image>()));
        }
        itemData = i;
        UpdateUI();
    }

    public void SetParent(Transform t)
    {
        canvasGroup.transform.SetParent(t);
        Publisher.Raise(new InventoryChangedEvent(inventory));
    }
    public void Rotate(bool clockwise)
    {
        int newOri;
        if (clockwise)
            newOri = ((int)itemData.orientation + 1) % 4;
        else
        {
            newOri = ((int)itemData.orientation - 1);
            if (newOri < 0)
            {
                newOri += 4;
            }
        }
        SetOrientation((ItemOrientationX)newOri);
        UpdateRotation(true);
    }
    public void SetOrientation(ItemOrientationX ori)
    {
        itemData.orientation = ori;
    }
    public void CreateOutline()
    {
        int width = (int)gridSize.x * Item.size.x;
        int height = (int)gridSize.y * Item.size.y;
        Texture2D tex = new Texture2D(width, height);
        int dist = 2;
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (ColorPixel(i, j))
                {
                    if (IsAdjClear(i, j))
                        tex.SetPixel(i, height - j - 1, Color.white);
                    else
                        tex.SetPixel(i, height - j - 1, Color.clear);
                }
                else
                {
                    //Debug.LogError(i + "-" + j);
                    tex.SetPixel(i, height - j - 1, Color.clear);
                }
            }
        }
        tex.filterMode = FilterMode.Point;
        tex.Apply();
        //outlineShapeImage.sprite = Sprite.Create(tex, new Rect(0, 0, width, height), Vector2.zero);
        outlineShapeImage.rectTransform.sizeDelta = new Vector2(width, height);
        bool ColorPixel(int x, int y)
        {
            Image slot = ConvertToSlot(x, y);
            if (slot == null)
            {
                return false;
            }
            bool val = !(!slot.gameObject.activeInHierarchy || !slot.enabled);
            return val;
        }
        bool IsAdjClear(int x, int y)
        {
            bool result = false;
            result = result || !ColorPixel(x + dist, y);
            result = result || !ColorPixel(x - dist, y);
            result = result || !ColorPixel(x, y + dist);
            result = result || !ColorPixel(x, y - dist);
            result = result || !ColorPixel(x - dist, y - dist);
            result = result || !ColorPixel(x + dist, y + dist);
            result = result || !ColorPixel(x - dist, y + dist);
            result = result || !ColorPixel(x + dist, y - dist);
            return result;
        }
        Image ConvertToSlot(int x, int y)
        {
            if (x < 0 || y < 0)
            {
                return null;
            }
            int xCord = x / (int)gridSize.x;
            int yCord = y / (int)gridSize.y;

            if (xCord >= Item.size.x || yCord >= Item.size.y)
            {
                return null;
            }
            int index = (yCord * Item.size.x) + xCord;
            return slots[index].bg;
        }
    }
    void CreateSoulIcons()
    {
        solArrows = new List<Image>();
        foreach (Transform item in solArrowsRoot)
        {
            solArrows.Add(item.GetComponentInChildren<Image>(true));
            item.GetChild(0).gameObject.SetActive(false);
        }
        for (int i = 0; i < Item.soulSpaces.Count; i++)
        {
            solArrows[i].gameObject.SetActive(true);
            (solArrows[i].transform.parent as RectTransform).anchoredPosition
                = new Vector2(Item.soulSpaces[i].x * gridSize.x, -Item.soulSpaces[i].y * gridSize.y);
            solArrows[i].transform.parent.localEulerAngles = new Vector3(0, 0, Item.soulSpaces[i].z);
        }
    }
    bool once = true;
    public void UpdateUI()
    {
        if (once)
        {
            CreateSoulIcons();
        }
        UpdateSolBind();
        itemImage.transform.localEulerAngles = new Vector3(0, 0, -Item.imageRotation);
        itemImage.transform.localScale = Item.imageScale * Vector3.one;
        grid.constraintCount = Item.size.y; //row constraint

        grid.cellSize = new Vector2(gridSize.x, gridSize.y);

        (grid.transform as RectTransform).anchoredPosition =
            new Vector2(-gridSize.x / 2, (-gridSize.y / 2) * ((Item.size.y - 1)));

        int gridMax = (Item.size.x) * (Item.size.y);
        for (int j = 0; j < slots.Count; j++)
        {
            slots[j].SetActive(j < gridMax);
            int x = j % (int)Item.size.x;
            int y = j / (int)Item.size.x;

            slots[j].Enable(!Item.ignoreSpaces.Contains(new Vector2Int(x, y)));
            //slots[j].bg.color = Item.Color;
        }
        itemImage.sprite = Item.image;
        if (once)
        {
            CreateOutline();
            once = false;
        }

        UpdateRotation(false);
    }
    void UpdatePreviousPositions()
    {
        if (inventory == null)
            return;

        if (previousPositions.ContainsKey(inventory))
        {
            previousPositions[inventory] = itemData.pivot;
        }
        else
        {
            previousPositions.Add(inventory, itemData.pivot);
        }
    }
    public void UpdateRotation(bool tween)
    {
        float rotation = 0;
        switch (itemData.orientation)
        {
            case ItemOrientationX.RIGHT:
                rotation = 0;
                break;
            case ItemOrientationX.DOWN:
                rotation = -90;
                break;
            case ItemOrientationX.LEFT:
                rotation = 180;
                break;
            case ItemOrientationX.UP:
                rotation = 90;
                break;
            default:
                break;
        }
        rotationTween?.Kill();
        if (tween)
            rotationTween = root.transform.DORotate(new Vector3(0, 0, rotation), 0.125f).SetOptions(true);
        else
            root.transform.localEulerAngles = new Vector3(0, 0, rotation);
    }
    Tween rotationTween;

    private void OnDestroy()
    {
        inventory.onInventoryOpenClose -= OnInventoryOpenClose;
        inventory.updateInventoryUI -= UpdateUI;
    }
    void OnInventoryOpenClose()
    {
        ReturnToCacheInstant();
        canvasGroup.blocksRaycasts = true;
    }

    public override void Drop(InventoryDropArea area)
    {
        base.Drop(area);
        dropSFX.Play();
        ChangeInventory(area.Inventory, area);
        SetInteractable(true);
        Publisher.Raise(new ItemHoverEvent(null));
        Publisher.Raise(new ItemPickupEvent(null));
    }

    public void ChangeInventory(Inventory newInventory, InventoryDropArea newArea)
    {
        itemData.x = newArea.pos.x;
        itemData.y = newArea.pos.y;
        inventory.Remove(itemData, this, true);
        inventory.ReleaseIfHeld(itemData);

        inventory.onInventoryOpenClose -= OnInventoryOpenClose;
        inventory.updateInventoryUI -= UpdateUI;

        inventory = newInventory;
        SetParent(newInventory.itemsRoot);
        inventory.onInventoryOpenClose += OnInventoryOpenClose;
        inventory.updateInventoryUI += UpdateUI;

        inventory.Add(itemData, true, this);
        PlaceOn(newArea);
        UpdateSolBind();
        Publisher.Raise(new InventoryChangedEvent(inventory));
    }
    public override void PickUp()
    {
        if (BattleController.IsInBattle)
        {
            if (!CombatInteractionAdapter.CanPickUpItem(this))
            {
                CursorController.CancelInteraction(CancelSource.USER_CANCELLED);
                return;
            }
        }
        base.PickUp();
        pickUpSFX.Play();
        inventory.Remove(itemData, this, true);
        SetParent(inventory.heldItemRoot);
        canvasGroup.transform.SetAsLastSibling();
        Publisher.Raise(new ItemHoverEvent(this));
        Publisher.Raise(new ItemPickupEvent(this));
        SetGridColor(Color.white);
        SetSolArrows(false);
        Publisher.Raise(new InventoryChangedEvent(inventory));
    }
    public override bool CanCancel(CancelSource reason)
    {
        return cacheInvPosition != null;
    }
    public override void OnCancel(CancelSource reason)
    {
        Publisher.Raise(new ItemHoverEvent(null));
        Publisher.Raise(new ItemPickupEvent(null));

        if (reason == CancelSource.DESTORYED)
        {
            DestroyUI();
            Publisher.Raise(new InventoryChangedEvent(inventory));
            return;
        }
        //base.OnCancel();
        SetOrientation(cacheOrientation);
        inventory.AddBackIfHeld(itemData, this);
        switch (reason)
        {
            case CancelSource.USER_CANCELLED:
            case CancelSource.EMPTY_SOURCE:
                ReturnToCachePosition();
                break;
            default:
                if (BattleController.IsInBattle)
                    ReturnToCachePosition();
                else
                    ReturnToCacheInvalid();
                break;
        }
    }
    public override bool CanInteract()
    {
        bool val = base.CanInteract();
        val = val && !InventoryManager.Instance.IsHidden;
        val = val && !isAnimating;
        return val;
    }
    bool isDisabled;
    public Color disabledColor = Color.grey;
    public override void ReturnToCachePosition()
    {
        SetParent(inventory.itemsRoot);
        inventory.AddBackIfHeld(itemData, this);
        UpdateSolBind();
        isAnimating = true;
        followCursor = false;
        moveSequence?.Kill(true);
        moveSequence = DOTween.Sequence();
        moveSequence.Append(canvasGroup.transform.DOMove(CachePos, 0.15f))
            .OnComplete(() => { OnMoveComplete(); });
        moveSequence.Play();
    }
    public void ReturnToCacheInstant()
    {
        SetParent(inventory.itemsRoot);
        moveSequence?.Kill(true);
        base.ReturnToCachePosition();
    }
    public void ReturnToCacheFancy()
    {
        SetParent(inventory.itemsRoot);
        UpdateSolBind();
        moveSequence?.Kill();
        isAnimating = true;
        followCursor = false;
        moveSequence?.Kill(true);
        moveSequence = DOTween.Sequence();
        moveSequence.Append(canvasGroup.transform.DOMove(CachePos, 0.15f))
            .OnComplete(() => { OnMoveComplete(); })
            ;//.SetDelay(1);
        moveSequence.Play();
        moveSequence.Play();
    }
    public void ReturnToCacheInvalid()
    {
        dropInvalidSFX.Play();
        followCursor = false;
        UpdateSolBind();
        moveSequence?.Kill();
        isAnimating = true;
        float duration = 0.125f;
        moveSequence = DOTween.Sequence();
        moveSequence
            .Append(canvasGroup.transform.DOPunchPosition(Vector3.right * 10, 0.5f, 20))
            .AppendCallback(() => { UpdateRotation(true); })
            .Append(canvasGroup.transform.DOMove(CachePos, duration))
            .OnComplete(() => { SetParent(inventory.itemsRoot); OnMoveComplete(); dropSFX.Play(); });

        FlashColor(invalidInteractionColor);
        moveSequence.Play();
    }
    void OnMoveComplete()
    {
        UpdateSolBind();
        isAnimating = false;
        canvasGroup.blocksRaycasts = true;
        base.ReturnToCachePosition();
        UpdateRotation(false);
        Publisher.Raise(new InventoryChangedEvent(inventory));
    }
    public void OnGive()
    {
        /*
        if (itemData.item.isConsumable)
        {
            itemData.qty--;
            inventory.UpdateInventoryUI();
            if (itemData.qty <= 0)
            {
                DestroyUI();
            }
        }
        else
        {
            DestroyUI();
        }*/

    }
    public void DestroyUIAfterSequence(Sequence afterSequence)
    {
        afterSequence.Play()
            .OnComplete(() => { DestroyUI(); });
    }
    public void DestroyUI()
    {
        inventory.ReleaseIfHeld(itemData);
        inventory.Remove(itemData, this, false);
        Destroy(root);
    }
    public Sequence FlashColor(Color c, int numFlashes = 2, bool autoPlay = true)
    {
        return statusImageColor.DOFlash(new Color(0, 0, 0, 0),
            c, numLoops: numFlashes, autoPlay: autoPlay);
    }
    public void UpdateSolBind()
    {
        if (isDisabled) return;
        if (inventory == null) return;
        //bool isSolBound = inventory.IsSoulBound(ItemData);
        //Color c = isSolBound ? Global10MH.Instance10MH.solBoundColor : Color.white;
        //SetGridColor(c);
        //SetSolArrows(isSolBound);
        //solBoundImage.DOColor(c1, 0.25f);
        //solBoundImage2.DOColor(c2, 0.25f);
    }
    public override void OnDoubleClick()
    {
        base.OnDoubleClick();
        TryAutoChangeInventories();
    }
    public void TryAutoChangeInventories()
    {
        List<Inventory> others = InventoryManager.Instance.GetOtherInventory(inventory);

        foreach (Inventory nextInventory in others)
        {
            InventoryDropArea availPos = null;
            if (previousPositions.ContainsKey(nextInventory))
            {
                availPos = nextInventory.IsFree(
                    (int)previousPositions[nextInventory].x,
                    (int)previousPositions[nextInventory].y,
                    ItemData);
            }
            if (availPos == null)
                availPos = nextInventory.TryFindFreePos(this);
            if (availPos && availPos.CanResolve(this))
            {
                followCursor = false;
                ChangeInventory(nextInventory, availPos);
                dropSFX.Play();
                SetInteractable(true);
                return;
            }
        }

        OnCancel(CancelSource.RESOLUTION_FAILED);
    }
    public override void PlaceOn(DropArea area)
    {
        if (area is InventoryDropArea)
        {
            cacheInvPosition = area as InventoryDropArea;
            cacheOrientation = itemData.orientation;
        }
        base.PlaceOn(area);
        UpdateRotation(false);
        UpdatePreviousPositions();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (InventoryManager.IsItemHeld()) return;
        if (isDisabled) return;
        Publisher.Raise(new ItemHoverEvent(this));
        //solBoundImage2.DOFade(0.5f, 0.25f);
        Vector3 og = new Vector3(0, 0, -Item.imageRotation);
        Vector3 rot1 = new Vector3(0, 0, -Item.imageRotation + 3);
        Vector3 rot2 = new Vector3(0, 0, -Item.imageRotation - 3);
        float delta = 0.05f;
        Sequence s = DOTween.Sequence();
        s.Append(itemImage.transform.DOLocalRotate(rot1, delta).SetRelative(false))
            .Append(itemImage.transform.DOLocalRotate(rot2, delta * 2).SetRelative(false))
            .Append(itemImage.transform.DOLocalRotate(og, delta).SetRelative(false));
        hoverSFX.Play();
        //if (!BattleController.IsInBattle)
        //    durabilityUI.Show();
        if (Item is CombatItem && (Item as CombatItem).IsAttack())
        {
            powerText.transform.parent.gameObject.SetActive(true);
            defenseText.transform.parent.gameObject.SetActive(true);
        }
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        if (InventoryManager.IsItemHeld()) return;
        Publisher.Raise(new ItemHoverEvent(null));
        // if (!BattleController.IsInBattle)
        //    durabilityUI.Hide();
        powerText.transform.parent.gameObject.SetActive(false);
        defenseText.transform.parent.gameObject.SetActive(CycleManager.PlayerCombatUnit.blockingItem == this);
    }
    public void SetIsBlock(bool val)
    {
        atbUI.SetScale(val ? 1.25f : 1);
        defenseText.transform.parent.localScale = Vector3.one * (val ? 1.25f : 1);
    }
    public void SetGridColor(Color c)
    {
        outlineShapeImage.DOColor(c, 0.25f);
    }
    public void SetSolArrows(bool bound, bool inQueue = false)
    {
        foreach (Image item in solArrows)
        {
            if (isDisabled)
            {
                item.DOColor(inQueue ? Color.yellow : disabledColor, 0.25f);
                item.rectTransform.DOSizeDelta(new Vector2(10, 10), 0.15f);
            }
            else if (bound)
            {
                item.DOColor(Global10MH.Instance10MH.solBoundColor, 0.25f);
                item.rectTransform.DOSizeDelta(new Vector2(20, 20), 0.15f);
            }
            else
            {
                item.DOColor(Color.white, 0.25f);
                item.rectTransform.DOSizeDelta(new Vector2(10, 10), 0.15f);
            }
        }
    }
    public void SetItemColor(Color c)
    {
        itemImage.color = c;
    }

    public void AddCharge()
    {
        charge++;
        if (charge > maxCharge)
            charge = maxCharge;
        chargeUI.UpdateCharge(this);
    }
}
public struct GridSlot
{
    public Image bg;

    public GridSlot(Image bg)
    {
        this.bg = bg;
    }

    public void SetOutline(Color c)
    {
        bg.DOColor(c, 0.25f);
    }
    public void SetActive(bool val)
    {
        bg.gameObject.SetActive(val);
    }
    public void Enable(bool val)
    {
        bg.enabled = (val);
    }
}
