﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using System;

[System.Serializable]
public class InventoryCollection : MonoBehaviour
{
    public InventoryPropertyData invData;
    public string key;
    public bool isPermaInv = false;
    public bool canAddItems = false;

    [ListDrawerSettings(Expanded = true)]
    public List<ItemPlacementData> initialItems = new List<ItemPlacementData>();

    public Action OnInventoryChanged;
    public Inventory GetInventory()
    {
        return InventoryManager.SpawnOtherInventory(key, isPermaInv, invData, canAddItems, null, GetInvItems().ToArray());
    }
    public List<ItemPlacementData> GetInvItems()
    {
        return CycleManager.GetInventory(key, initialItems);
    }
    public void CacheAndDestory(Inventory i)
    {
        CycleManager.CacheInventory(key, i.inventoryItems, isPermaInv);
        InventoryManager.DestoryInventory(i);
        OnInventoryChanged?.Invoke();
    }
    public bool Contains(Item i)
    {
        foreach (ItemPlacementData ipd in GetInvItems())
        {
            if (ipd.item == i)
                return true;
        }
        return false;
    }
}
