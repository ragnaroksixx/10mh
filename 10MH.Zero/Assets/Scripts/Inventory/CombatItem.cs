﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using NHG.Dialogue;
using System;
using TMPro;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Combat Item")]
public class CombatItem : Item
{
    [FoldoutGroup("Cast")]
    [Range(0, 10)]
    public int actionPoints = 1;

    public GameObject hitFXPrefab;

    [FoldoutGroup("Usage")]
    public Item replacementItem;

    public string[] onUseQwips = new string[0];

    public int attack;
    public int defense;

    public ItemEffect basicAction = new ItemEffect();
    public ItemEffect selfAction = new ItemEffect();
    public ItemEffect specialAction = new ItemEffect();
    public override int AP => GetCastTime();
    public bool IsAttack()
    {
        return attack > 0;
    }
    public bool IsBlock()
    {
        return defense > 0;
    }
    public bool CanTargetSelf()
    {
        return IsBlock() || selfAction.HasEffect();
    }
    public bool CanTargetEnemy()
    {
        return IsAttack();
    }
    public void PerformActions(BattleUnit user, BattleUnit target)
    {
        if (user == target)
        {
            if (IsBlock())
            {
                BlockCombatAction.BasicBlockAction(defense).PerformAction(user, target);
            }
            selfAction.PerformActions(user, target);
        }
        else
        {
            if (attack > 0)
            {
                DamageCombatAction.BasicDamageAction(attack).PerformAction(user, target);
            }
            basicAction.PerformActions(user, target);
        }
        //TODO: Implement special attacks
    }
    //public int CalcDamage(BattleUnit user, BattleUnit target)
    //{
    //    return CombatInteractionAdapter.CalcDamage(power, 0,
    //        user, target);
    //}
    //public int CalcBlock(CharacterData user)
    //{
    //    return Mathf.Max(1, (int)((power * user.stats.DEF) / 3.0f));
    //}
    //public int CalcHeal()
    //{
    //    return power;
    //}
    public int GetCastTime()
    {
        return actionPoints;
    }
    public virtual void OnUse(CombatInfo c)
    {
        if (IsBlock() && c.TargetsSelf)
        {
            Transform t = GameObject.Instantiate(GlobalData10MH.Instance.defaultShieldFX, c.target.unitController.UI.hpRoot).transform;
            t.localPosition = Vector3.zero;
            t.localScale = Vector3.one;
        }
        else if (hitFXPrefab)
        {
            Transform t = GameObject.Instantiate(hitFXPrefab, c.target.unitController.UI.hpRoot).transform;
            t.localPosition = Vector3.zero;
            t.localScale = Vector3.one;
        }

    }

    //public string GetLogText(int value, bool perfectExecution)
    //{
    //    string result = "";
    //    string colorTag = "";// "<color=#" + ColorUtility.ToHtmlStringRGB(GetColor()) + "> ";
    //    string colorEndTag = "";// "</color>";
    //    if (IsAttack())
    //    {
    //        result = "You dealt " + colorTag + value + colorEndTag + " damage";
    //    }
    //    else if (IsBlock())
    //    {
    //        result = "You gained " + colorTag + value + colorEndTag + " block";
    //    }
    //    else if (IsHeal())
    //    {
    //        result = "You gained " + colorTag + value + colorEndTag + " HP";
    //    }
    //    else if (IsEnergy())
    //    {
    //        if (perfectExecution)
    //            result = "You Feel A Great Surge Of Energy";
    //        else
    //            result = "You Feel Invigorated";
    //    }
    //    return result;
    //}

    public string GetQwip()
    {
        int value = CycleManager.PlayerCombatUnit.data.Range(0, onUseQwips.Length * 2);

        if (value < onUseQwips.Length)
            return onUseQwips[value];
        else
            return "";
    }
}
[System.Serializable]
public class ItemEffect
{
    [TextArea(5, 20)]
    [OnValueChanged("OnValueChanged")]
    public string effectRaw;

    [SerializeField]
    [ReadOnly]
    List<CombatActionEvent> combatActions = new List<CombatActionEvent>();

    public bool HasEffect()
    {
        return combatActions.Count > 0;
    }
    public void PerformActions(BattleUnit user, BattleUnit target)
    {
        foreach (CombatActionEvent item in combatActions)
        {
            item.PerformAction(user, target);
        }
    }
    public void OnValueChanged()
    {
        if(string.IsNullOrEmpty(effectRaw))
        {
            combatActions = new List<CombatActionEvent>();
            return;
        }
        string[] lines = effectRaw.Split('\n');
        List<CombatActionEvent> temp = new List<CombatActionEvent>();
        foreach (string line in lines)
        {
            if (line.StartsWith("//")) continue;
            string[] tagSplit = line.SplitFirst('<');
            string header = tagSplit[0];
            header = header.Trim();
            string[] headerSplit = header.SplitFirst('_');
            string type = headerSplit[0];
            string tags = tagSplit[1];

            CombatActionEvent cae = Activator.CreateInstance(CombatEventLibrary.eventTypes[type.AsKey()]) as CombatActionEvent;
            if (headerSplit.Length > 1)
                cae.Tags.AddTag("amt", headerSplit[1].Replace("_", ""));
            cae.Tags.AddTags(tags);
            temp.Add(cae);
            cae.BasicLog();
        }
        combatActions = temp;

    }
}
