﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using UnityEngine.EventSystems;
using DG.Tweening;
using TMPro;
using System.Collections.Generic;

public class InventoryManager : MonoBehaviour
{
    public PlayerInventory rightPrefab;
    public Inventory leftPrefab;
    public static InventoryManager Instance;
    public Transform heldItemRoot;
    public Transform npcInvRoot, playerInvRoot;
    public Image divider;
    public List<Inventory> nonPlayerOpenInventories;

    public LinearOnScreenAnimator anim;
    public TMP_Text locationText;
    public AudioVariable openSFX, closeSFX;
    public bool IsHidden { get => !anim.IsOnScreen; }
    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
        if (!IsItemHeld() && Input.GetKeyDown(KeyCode.Tab) && !lockInventoryInput.Value)
        {
            ToggleInventory();
        }
    }
    public static void SetInventoryUIVisibility(bool value)
    {
        Instance.transform.GetChild(0).gameObject.SetActive(value);
    }
    public void ToggleInventory()
    {
        if (anim.IsOnScreen)
        {
            CloseInventory();
            HUDController.Instance.bagQuickUI.OnUseEnd();
            closeSFX.Play();
        }
        else
        {
            OpenInventory();
            HUDController.Instance.bagQuickUI.OnUseStart();
            openSFX.Play();
        }
    }
    public void SetInventoryView(bool val)
    {
        if (val)
            OpenInventory();
        else
            CloseInventory();
    }
    public void OpenInventory()
    {
        if (!BattleController.IsInBattle)
        {
            InteractionHandler.LockInteraction(this);
            WalkController10MH.RemoveUserInput(this);
            CameraController10MH.RemoveUserInput(this);
            CursorController.HideCursor(this);
            //locationText.transform.parent.gameObject.SetActive(true);
        }
        UIController.Instance.ShowCinemaBars(this);
        UIController.Instance.ShowPlayer(this);
        Global10MH.ShowCursor(this);
        OnInventoryOpen();
        anim.AnimateOnScreen();
    }
    public void CloseInventory()
    {
        if (!BattleController.IsInBattle)
        {
            Publisher.Raise(new ClearNotificationEvent());
            InteractionHandler.RemoveInteractionLock(this);
            WalkController10MH.RestoreUserInput(this);
            CameraController10MH.RestoreUserInput(this);
            CursorController.ShowCursor(this);
        }
        //locationText.transform.parent.gameObject.SetActive(false);
        UIController.Instance.HideCinemaBars(this);
        UIController.Instance.HidePlayer(this);
        Global10MH.HideCursor(this);
        OnInventoryClose();
        anim.AnimateOffScreen();

    }

    private void Start()
    {
        SpawnPlayerInventory();
    }
    public static Inventory SpawnPlayerInventory()
    {
        ScriptableObjectDictionary<InventoryPropertyData> invs 
            = GlobalData10MH.Instance.invPropDictionary;
        InventoryPropertyData data = invs.GetItem(SaveData.currentSave.currentInventoryDataID);
        return SpawnPlayerInventory(data, CycleManager.GetInventory("player").ToArray());
    }
    public static Inventory SpawnPlayerInventory(InventoryPropertyData data, params ItemPlacementData[] initialItems)
    {
        if (PlayerInventory.Instance != null)
        {
            //PlayerInventory.Instance.CloseInventory();
            Destroy(PlayerInventory.Instance.gameObject);
            PlayerInventory.Instance = null;
        }
        PlayerInventory.Instance = Instance.SpawnInventory(null, true, Instance.playerInvRoot, true, data, initialItems, Instance.rightPrefab) as PlayerInventory;
        return PlayerInventory.Instance;
    }
    public static Inventory SpawnOtherInventory(string cacheID, bool isPerma, Vector3 worldPos, InventoryPropertyData data, bool canAddItems, Inventory prefab, params ItemPlacementData[] items)
    {
        if (prefab == null)
            prefab = Instance.leftPrefab;
        Inventory i = Instance.SpawnInventory(cacheID, isPerma, Instance.npcInvRoot, canAddItems, data, items, prefab);
        Vector2 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            (RectTransform)Instance.npcInvRoot,
            screenPos,
            null,
            out screenPos
            );

        ((RectTransform)i.transform).anchoredPosition = screenPos;
        Instance.nonPlayerOpenInventories.Add(i);
        return i;
    }

    public static Inventory SpawnOtherInventory(string cacheID, bool isPerma, InventoryPropertyData data, bool canAddItems, Inventory prefab, params ItemPlacementData[] items)
    {
        if (prefab == null)
            prefab = Instance.leftPrefab;
        Inventory i = Instance.SpawnInventory(cacheID, isPerma, Instance.npcInvRoot, canAddItems, data, items, prefab);
        Instance.nonPlayerOpenInventories.Add(i);
        float x = -(i.grid.transform as RectTransform).sizeDelta.x;
        float y = -(i.grid.transform as RectTransform).sizeDelta.y;
        x = -data.width * 40;
        y = -data.height * 40 / 2;
        (i.transform as RectTransform).anchoredPosition = new Vector2(x, y);
        return i;
    }
    Inventory SpawnInventory(string cacheID, bool isPerma, Transform root, bool canAddItems, InventoryPropertyData data, ItemPlacementData[] items, Inventory prefab)
    {
        Inventory inv =
            GameObject.Instantiate(prefab, root);
        inv.transform.SetAsFirstSibling();
        inv.canPlaceItems = canAddItems;
        inv.heldItemRoot = heldItemRoot;
        inv.data = data;
        StartCoroutine(inv.Init(cacheID, isPerma, items));
        return inv;
    }
    public static void DestoryInventory(Inventory inv)
    {
        Destroy(inv.gameObject);
    }
    public static void DestroyPlayerInventory()
    {
        //PlayerInventory.Instance.CloseInventory();
        Destroy(PlayerInventory.Instance.gameObject);
        PlayerInventory.Instance = null;
    }
    public static bool IsItemHeld()
    {
        return Instance.heldItemRoot.childCount != 0;
    }
    private void OnInventoryOpen()
    {
        if (!BattleController.IsInBattle)
            HUDController.Instance.FadeIn();
        Publisher.Raise(new InventoryOpenCloseEvent(true));
    }
    private void OnInventoryClose()
    {
        HUDController.Instance.FadeOut();
        Publisher.Raise(new InventoryOpenCloseEvent(false));
    }
    public List<Inventory> GetOtherInventory(Inventory i)
    {
        if (Instance.nonPlayerOpenInventories.Count == 0 || PlayerInventory.Instance == null)
            return new List<Inventory>();
        if (Instance.nonPlayerOpenInventories.Contains(i))
            return new List<Inventory>() { PlayerInventory.Instance };

        return Instance.nonPlayerOpenInventories;
    }
    public static void LockInput(object source)
    {
        Instance.lockInventoryInput.AddLock(source);
    }
    public static void UnlockInput(object source)
    {
        Instance.lockInventoryInput.RemoveLock(source);
    }

    MultiBoolLock lockInventoryInput = new MultiBoolLock();
}
public class InventoryChangedEvent : PublisherEvent
{
    public Inventory i;

    public InventoryChangedEvent(Inventory i)
    {
        this.i = i;
    }
}
public class InventoryOpenCloseEvent : PublisherEvent
{
    public bool isOpen;

    public InventoryOpenCloseEvent(bool open)
    {
        isOpen = open;
    }
}

