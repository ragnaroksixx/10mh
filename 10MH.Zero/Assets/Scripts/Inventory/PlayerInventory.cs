﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class PlayerInventory : Inventory
{
    public static PlayerInventory Instance;

    public bool HasPurchaseItems()
    {
        foreach (ItemPlacementData ipd in inventoryItems)
        {
            if (ipd.item is PurchaseItem)
            {
                return true;
            }
        }
        return false;
    }
    public int MaxAttackItem()
    {
throw new NotImplementedException();
    }
    public void PurchaseItems()
    {
        ConvertItems(true, true);
    }
    public void BreakItems()
    {
        ConvertItems(false, false);
    }
    void ConvertItems(bool buy, bool spendMoney)
    {/*
        int total = 0;
        List<ItemPlacementData> toadd = new List<ItemPlacementData>();
        foreach (ItemPlacementData ipd in inventoryItems)
        {
            if (ipd.item is PurchaseItem)
            {
                PurchaseItem pItem = ipd.item as PurchaseItem;
                ItemPlacementData replacementItem = new ItemPlacementData();
                replacementItem.Copy(ipd);
                if (buy || spendMoney)
                    replacementItem.item = pItem.purchasedItem;
                else
                    replacementItem.item = pItem.brokenItem;
                toadd.Add(replacementItem);
                total += pItem.price;
            }
        }
        RemoveAllPurchasables();
        if (spendMoney)
        {
            for (int i = inventoryItems.Count - 1; i >= 0; i--)
            {
                if (total == 0)
                    break;
                ItemPlacementData ipd = inventoryItems[i];
                if (ipd.item.Identifier == "money")
                {
                    if (ipd.qty > total)
                    {
                        ipd.qty -= total;
                        total = 0;
                    }
                    else
                    {
                        total -= ipd.qty;
                        ipd.qty = 0;
                        ipd.runtimeUI.UpdateUI();
                    }
                }
            }
        }
        TrySpawnItems(toadd.ToArray());
    */}

    public void AddSolArm()
    {
        foreach (ItemPlacementData ipd in inventoryItems)
        {
            if (ipd.item.Identifier == "hand")
            {
                ipd.item = GlobalData10MH.Instance.itemDictionary.GetItem("solarm");
                break;
            }
        }
    }

    public List<ItemPlacementData> GetSaveableItems()
    {
        return inventoryItems;
        //List<ItemPlacementData> savedItems = new List<ItemPlacementData>();
        //UpdateSaveLocations();

        //foreach (ItemPlacementData i in inventoryItems)
        //{
        //    if (i.IncludesLocation(solPositions.ToArray()))
        //        savedItems.Add(i);
        //}
        //if (HasHeldItem)
        //{
        //    if (HeldItem.IncludesLocation(solPositions.ToArray()))
        //        savedItems.Add(HeldItem);
        //}
        //return savedItems;
    }
    public override ItemPlacementData Find(Item i)
    {
        if (HasHeldItem && HeldItem.item == i)
            return HeldItem;
        return base.Find(i);
    }

    public override void UpdateInventoryUI()
    {
        UpdateSaveLocations();
        base.UpdateInventoryUI();
    }
    public void UpdateSaveLocations()
    {
        solPositions.Clear();
        bool breakInf = true;
        List<ItemPlacementData> evaluated = new List<ItemPlacementData>();
        if (inventoryItems.Count == 0)
            return;
        while (breakInf)
        {
            foreach (ItemPlacementData ipd in inventoryItems)
            {
                if (evaluated.Contains(ipd))
                {
                    if (ipd == inventoryItems[inventoryItems.Count - 1])
                        breakInf = false;
                    continue;
                }

                if (ipd.item.isSoulBound)
                {
                    AddSavePostion(ipd.pivot);
                    AddSolPositions(ipd);
                    break;
                }
                else if (ipd.IncludesLocation(solPositions.ToArray()))
                {
                    AddSolPositions(ipd);
                    break;
                }

                if (ipd == inventoryItems[inventoryItems.Count - 1])
                    breakInf = false;
            }
        }

        void AddSolPositions(ItemPlacementData val)
        {
            PositionData pd = val.Bounds();
            foreach (Vector2Int pos in pd.soulSpaces)
            {
                Vector2Int solPos = pos + val.pivot;
                AddSavePostion(solPos);
            }
            evaluated.Add(val);
        }
    }
    public void AddSavePostion(Vector2Int val)
    {
        if (!solPositions.Contains(val))
        {
            solPositions.Add(val);
        }
    }


    public override bool IsSoulBound(ItemPlacementData itemData)
    {
        return base.IsSoulBound(itemData) || itemData.IncludesLocation(solPositions.ToArray());
    }
    public void RemoveAllPurchasables()
    {
        List<ItemPlacementData> toRemove = GetAllPurchaseables();
        foreach (ItemPlacementData ipd in toRemove)
        {
            (ipd.item as PurchaseItem).RemoveGrabEvent();
            ipd.runtimeUI.DestroyUI();
        }
        RefreshOccupiedCache();
        UpdateInventoryUI();
    }
    public List<ItemPlacementData> GetAllPurchaseables()
    {
        return inventoryItems.FindAll(IsPurchasable);
    }
    static bool IsPurchasable(ItemPlacementData i)
    {
        return i.item is PurchaseItem;
    }
    public int GetMoneyCount()
    {
        return 0;
    }
    public override bool ContainsAny(List<Item> items, int amount = 1)
    {
        List<ItemPlacementData> allItems = new List<ItemPlacementData>(inventoryItems);
        allItems.AddRange(GetPermanentInvItems());
        int found = 0;
        foreach (ItemPlacementData ipd in allItems)
        {
            if (items.Contains(ipd.item))
                found++;
            if (found >= amount)
                return true;
        }
        return false;
    }
    public static List<ItemPlacementData> GetPermanentInvItems()
    {
        return CycleManager.GetInventory("playerConst", new List<ItemPlacementData>());
    }
    public static void AddPermanentInvItem(ItemPlacementData ipd)
    {
        List<ItemPlacementData> allItems = new List<ItemPlacementData>(GetPermanentInvItems());
        if (HasPermanentInvItem(ipd.item))
            return;
        allItems.Add(ipd);
        CycleManager.CacheInventory("playerConst", allItems, true);
        HUDController.Instance.UpdatePermanentItems();
    }
    public static bool HasPermanentInvItem(Item i)
    {
        List<ItemPlacementData> allItems = new List<ItemPlacementData>(GetPermanentInvItems());
        foreach (ItemPlacementData item in allItems)
        {
            if (i == item.item)
                return true;
        }
        return false;
    }
    public static bool HasPermanentInvItem(string id)
    {
        List<ItemPlacementData> allItems = new List<ItemPlacementData>(GetPermanentInvItems());
        foreach (ItemPlacementData item in allItems)
        {
            if (id.ToLower() == item.item.Identifier.ToLower())
                return true;
        }
        return false;
    }

    public static ItemPlacementData GetHand()
    {
        return Instance.Find(GlobalData10MH.Instance.itemDictionary.GetItem(SaveData.currentSave.hand));
    }

}
