﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using Sirenix.OdinInspector;
using System.Globalization;
using System.Runtime.InteropServices;
using TMPro;

public class Inventory : MonoBehaviour
{
    //[HideInInspector]
    public InventoryPropertyData data;
    public GameObject itemPrefab;
    public List<ItemPlacementData> inventoryItems = new List<ItemPlacementData>();
    //public Transform gridRoot;
    public Transform itemsRoot;
    InventoryDropArea[] slotPositions;
    protected List<Vector2Int> occupiedSlotsCache = new List<Vector2Int>();
    CanvasGroup canvasGroup;
    public CanvasGroup gridGroup;
    public Action onInventoryOpenClose;
    public Action updateInventoryUI;
    protected InventoryItemUI heldItem = null;
    [HideInInspector]
    public bool canPlaceItems = false;
    [HideInInspector]
    public Transform heldItemRoot;
    public GridLayoutGroup grid;
    public GameObject slotPrefab;
    public CustomAudioClip rotateA, rotateB;

    protected List<Vector2Int> solPositions = new List<Vector2Int>();

    public ItemPlacementData HeldItem { get => HasHeldItem ? heldItem.ItemData : null; }
    public bool HasHeldItem { get => heldItem != null; }
    public InventoryDropArea[] SlotPositions { get => slotPositions; }
    public List<Vector2Int> SolPositions { get => solPositions; }
    string cacheKey = null;
    bool isPerma;

    // Use this for initialization
    public virtual IEnumerator Init(string cacheID, bool isPerma, ItemPlacementData[] initialItems)
    {
        canvasGroup = GetComponent<CanvasGroup>();
        cacheKey = cacheID;
        this.isPerma = isPerma;
        slotPositions = new InventoryDropArea[data.Size];
        grid.constraintCount = data.width;
        for (int i = 0; i < slotPositions.Length; i++)
        {
            //slotPositions[i] = gridRoot.GetChild(i).GetComponentInChildren<InventoryDropArea>();
            slotPositions[i] = GameObject.Instantiate(slotPrefab, grid.transform).GetComponentInChildren<InventoryDropArea>();
            slotPositions[i].Init(this, i);
        }
        yield return null;
        TrySpawnItems(initialItems);
    }
    float nextRotTime;
    public virtual void Update()
    {
        if (HasHeldItem && !BattleController.IsInBattle)
        {

            if (Input.GetKeyDown(KeyCode.E) || Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                if (Time.time > nextRotTime)
                {
                    heldItem.Rotate(true);
                    rotateA?.Play();
                    nextRotTime = Time.time + 0.1f;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Q) || Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                if (Time.time > nextRotTime)
                {
                    heldItem.Rotate(false);
                    rotateB?.Play();
                    nextRotTime = Time.time + 0.1f;
                }
            }

        }
    }
    public virtual bool IsSoulBound(ItemPlacementData itemData)
    {
        return itemData.item.isSoulBound;
    }

    public void TrySpawnItems(params ItemPlacementData[] items)
    {
        foreach (ItemPlacementData i in items)
        {
            bool hasItem = Contains(i.item);
            Add(i, false, null);

            SpawnItem(i);

        }
        UpdateInventoryUI();
    }

    public virtual void OnOpenInventory()
    {
        onInventoryOpenClose?.Invoke();
        canvasGroup.blocksRaycasts = true;
        //animator.AnimateOnScreen();
    }
    public virtual void OnCloseInventory()
    {
        canvasGroup.blocksRaycasts = false;
        onInventoryOpenClose?.Invoke();
        //animator.AnimateOffScreen();
    }
    public virtual void UpdateInventoryUI()
    {
        if (!string.IsNullOrEmpty(cacheKey))
            CycleManager.CacheInventory(cacheKey, inventoryItems, isPerma);
        updateInventoryUI?.Invoke();
        Publisher.Raise(new InventoryChangedEvent(this));
    }

    public void DiscardHeldItem()
    {
        if (HasHeldItem)
        {
            SetHeldItem(null);
            CursorController.CancelInteraction(CancelSource.DESTORYED);
        }
    }

    public void SpawnItem(ItemPlacementData item)
    {
        int gridPos = GetGridPos(item.x, item.y);
        DropArea grid = slotPositions[gridPos];
        InventoryItemUI itemUI = GameObject.Instantiate(itemPrefab, itemsRoot).GetComponentInChildren<InventoryItemUI>();
        itemUI.SetItem(item, this);
        itemUI.PlaceOn(grid);
    }

    public int GetGridPos(int x, int y)
    {
        return (data.width * y) + x;
    }
    public string InventoryToString()
    {
        string result = "";
        foreach (ItemPlacementData item in inventoryItems)
        {
            result += item.item.Identifier + " ";
            //result += item.durability + " ";
            result += item.x + " ";
            result += item.y;
            result += "\n";
        }
        return result;
    }

    public bool IsOccupied(Vector2Int i)
    {
        foreach (Vector2Int item in occupiedSlotsCache)
        {
            if (i.x == item.x && i.y == item.y)
                return true;
        }
        return false;
    }
    public bool IsOccupied(Vector2Int i, out bool isSoulBound)
    {
        isSoulBound = false;
        foreach (Vector2Int item in occupiedSlotsCache)
        {
            if (i.x == item.x && i.y == item.y)
            {
                isSoulBound = IsSoulBoundPos(i);
                return true;
            }
        }
        return false;
    }
    public bool IsSoulBoundPos(Vector2Int i)
    {
        return solPositions.Contains(i);
    }
    public virtual void dwfwf()
    {
        UpdateInventoryUI();
    }
    public void ReleaseIfHeld(ItemPlacementData i)
    {
        if (i == HeldItem)
        {
            SetHeldItem(null);
        }
    }
    public void TryReturnHeld()
    {
        if (heldItem != null)
        {
            AddBackIfHeld(HeldItem, heldItem);
        }
    }
    public virtual void SetHeldItem(InventoryItemUI ui)
    {
        heldItem = ui;
    }
    public void AddBackIfHeld(ItemPlacementData i, InventoryItemUI ui)
    {
        if (i == HeldItem)
        {
            Add(i, true, ui);
        }
    }
    public void RemoveAll()
    {
        List<ItemPlacementData> tempItems = new List<ItemPlacementData>();
        tempItems.AddRange(inventoryItems);
        foreach (ItemPlacementData item in tempItems)
        {
            Remove(item, item.runtimeUI, false);
            item.runtimeUI.DestroyUI();
        }
    }
    public virtual void Remove(ItemPlacementData i, InventoryItemUI ui, bool hold)
    {
        if (hold)
            SetHeldItem(ui);
        inventoryItems.Remove(i);
        RefreshOccupiedCache();
        UpdateInventoryUI();
    }
    public void TryRemove(Item item)
    {
        ItemPlacementData i = Find(item);
        if (i != null)
        {
            Remove(i, i.runtimeUI, false);
            i.runtimeUI.DestroyUI();
        }
    }
    public virtual void Add(ItemPlacementData i, bool release, InventoryItemUI ui)
    {
        if (release)
            SetHeldItem(null);

        inventoryItems.Add(i);

        RefreshOccupiedCache();
        UpdateInventoryUI();
    }
    public bool Contains(Item i)
    {
        foreach (ItemPlacementData ipd in inventoryItems)
        {
            if (ipd.item == i)
                return true;
        }
        return false;
    }

    public virtual bool ContainsAny(List<Item> items, int amount = 1)
    {
        int found = 0;
        foreach (ItemPlacementData ipd in inventoryItems)
        {
            if (items.Contains(ipd.item))
                found++;
            if (found >= amount)
                return true;
        }
        return false;
    }
    public virtual ItemPlacementData Find(Item i)
    {
        foreach (ItemPlacementData ipd in inventoryItems)
        {
            if (ipd.item == i)
                return ipd;
        }
        return null;
    }

    public InventoryDropArea IsFree(int x, int y, ItemPlacementData ipd)
    {
        int gridPos = GetGridPos(x, y);
        if (slotPositions[gridPos].CanPlace(ipd))
            return slotPositions[gridPos];
        else
            return null;
    }
    public InventoryDropArea TryFindFreePos(InventoryItemUI itemUI)
    {
        return TryFindFreePos(itemUI.ItemData);
    }
    public InventoryDropArea TryFindFreePos(ItemPlacementData ipd)
    {
        foreach (InventoryDropArea area in slotPositions)
        {
            if (area.CanPlace(ipd))
            {
                return area;
            }
        }
        return null;
    }
    public void RefreshOccupiedCache()
    {
        occupiedSlotsCache.Clear();
        AddToCache(inventoryItems.ToArray());
    }
    public void AddToCache(params ItemPlacementData[] items)
    {
        foreach (ItemPlacementData ipd in items)
        {
            PositionData position = ipd.Bounds();
            for (int x = 0; x < position.width; x++)
            {
                for (int y = 0; y < position.height; y++)
                {
                    Vector2Int vec = position.topLeft + new Vector2Int(x, y);
                    if (position.IsIgnoreLocal(new Vector2Int(x, y)))
                        continue;
                    occupiedSlotsCache.Add(vec);
                }
            }
        }
    }
    public void RemoveFromCache(params ItemPlacementData[] items)
    {
        foreach (ItemPlacementData ipd in items)
        {
            PositionData position = ipd.Bounds();
            for (int x = 0; x < position.width; x++)
            {
                for (int y = 0; y < position.height; y++)
                {
                    Vector2Int vec = position.topLeft + new Vector2Int(x, y);
                    if (position.IsIgnoreLocal(new Vector2Int(x, y)))
                        continue;
                    occupiedSlotsCache.Remove(vec);
                }
            }
        }
    }

    protected virtual void OnDestroy()
    {
        if (!string.IsNullOrEmpty(cacheKey))
            CycleManager.CacheInventory(cacheKey, inventoryItems, isPerma);
        InventoryManager.Instance?.nonPlayerOpenInventories.Remove(this);
    }

    public List<ItemPlacementData> GetChargeableItems()
    {
        List<ItemPlacementData> items = new List<ItemPlacementData>();
        foreach (ItemPlacementData item in inventoryItems)
        {
            if (!(item.item is CombatItem))
                continue;
            if (item.runtimeUI.maxCharge <= 0)
                continue;
            if(item.runtimeUI.charge<item.runtimeUI.maxCharge)
                items.Add(item);
        }
        return items;
    }
}

[System.Serializable]
public class ItemPlacementData
{
    public int x, y;
    public Vector2Int pivot => new Vector2Int(x, y);
    public Item item;
    public ItemOrientationX orientation;

    const string xKey = "x";
    const string yKey = "y";
    const string idKey = "id";
    const string useKey = "use";
    const string oriKey = "ori";

    [HideInInspector]
    public InventoryItemUI runtimeUI;
    public Hashtable ToHashtable()
    {
        Hashtable result = new Hashtable();
        result.Add(xKey, x);
        result.Add(yKey, y);
        result.Add(idKey, item.Identifier);
        //result.Add(useKey, durability);
        result.Add(oriKey, (int)orientation);

        return result;
    }
    public ItemPlacementData()
    {

    }
    public void Copy(ItemPlacementData ipd)
    {
        x = ipd.x;
        y = ipd.y;
        item = ipd.item;
        //durability = ipd.durability;
        runtimeUI = ipd.runtimeUI;
        orientation = ipd.orientation;
    }
    public ItemPlacementData Copy()
    {
        ItemPlacementData ipd = new ItemPlacementData();
        ipd.Copy(this);
        return ipd;
    }
    public ItemPlacementData(Hashtable h)
    {
        x = h.GetInteger(xKey);
        y = h.GetInteger(yKey);

        string id = h.GetString(idKey);
        item = GlobalData10MH.Instance.itemDictionary.GetItem(id);

        //durability = h.GetInteger(useKey);
        orientation = (ItemOrientationX)h.GetInteger(oriKey);
    }
    public bool IncludesLocation(params Vector2Int[] positions)
    {
        return IncludesLocation(item, new Vector2Int(x, y), orientation, positions);
    }
    public PositionData Bounds()
    {
        return GetBounds(item, new Vector2Int(x, y), orientation);
    }
    public PositionData BoundsAtPosition(Vector2Int pos)
    {
        return GetBounds(item, pos, orientation);
    }
    public static bool IncludesLocation(Item i, Vector2Int pos, ItemOrientationX ori, params Vector2Int[] positions)
    {

        PositionData position = GetBounds(i, pos, ori);
        foreach (Vector2Int p in positions)
        {
            if (position.IsIgnore(p)) continue;
            if ((p.x >= position.bounds.x && p.x <= position.bounds.z)
                && (p.y >= position.bounds.y && p.y <= position.bounds.w))
                return true;
        }
        return false;
    }

    public static PositionData GetBounds(Item i, Vector2Int pos, ItemOrientationX ori)
    {
        int minX;
        int minY;
        int maxX;
        int maxY;
        List<Vector2Int> ignoreSpaces = new List<Vector2Int>();

        switch (ori)
        {
            case ItemOrientationX.RIGHT:
                minX = pos.x;
                maxX = pos.x + i.size.x - 1;

                minY = pos.y;
                maxY = pos.y + i.size.y - 1;

                break;
            case ItemOrientationX.DOWN:
                minX = pos.x - i.size.y + 1;
                maxX = pos.x;

                minY = pos.y;
                maxY = pos.y + i.size.x - 1;

                break;
            case ItemOrientationX.LEFT:
                minX = pos.x - i.size.x + 1;
                maxX = pos.x;

                minY = pos.y - i.size.y + 1;
                maxY = pos.y;
                break;
            case ItemOrientationX.UP:
                minX = pos.x;
                maxX = pos.x + i.size.y - 1;

                maxY = pos.y;
                minY = pos.y - i.size.x + 1;
                break;
            default:
                throw new Exception("Invalid Item Orientation");
        }
        return new PositionData(minX, minY, maxX, maxY, i.ignoreSpaces.ToArray(), ori, i);
    }
}
public struct PositionData
{
    public Vector4Int bounds;
    public int width, height;
    public Vector2Int topLeft;
    public Vector2Int bottomRight;
    public List<Vector2Int> ignoreSpaces;
    public List<Vector3Int> soulSpaces;
    public PositionData(int minX, int minY, int maxX, int maxY, Vector2Int[] ignoreSpace, ItemOrientationX ori, Item item)
    {
        bounds = new Vector4Int(minX, minY, maxX, maxY);
        width = maxX - minX + 1;
        height = maxY - minY + 1;
        topLeft = new Vector2Int(minX, minY);
        bottomRight = new Vector2Int(maxX, maxY);
        int[,] ignoreMatrix = new int[item.size.x, item.size.y];
        soulSpaces = new List<Vector3Int>(item.soulSpaces);
        foreach (Vector2Int space in ignoreSpace)
        {
            ignoreMatrix[space.x, space.y] = 1;
        }
        ignoreSpaces = new List<Vector2Int>();
        switch (ori)
        {
            case ItemOrientationX.RIGHT:
                break;
            case ItemOrientationX.DOWN:
                ignoreMatrix = ArrayExtensions.RotateArrayClockwise(ignoreMatrix);

                for (int i = 0; i < soulSpaces.Count; i++)
                    soulSpaces[i] = new Vector3Int(-soulSpaces[i].y, soulSpaces[i].x, 0);
                break;
            case ItemOrientationX.LEFT:
                ignoreMatrix = ArrayExtensions.RotateArrayClockwise(ignoreMatrix);
                ignoreMatrix = ArrayExtensions.RotateArrayClockwise(ignoreMatrix);

                for (int i = 0; i < soulSpaces.Count; i++)
                    soulSpaces[i] = new Vector3Int(-soulSpaces[i].x, -soulSpaces[i].y, 0);
                break;
            case ItemOrientationX.UP:
                ignoreMatrix = ArrayExtensions.RotateArrayClockwise(ignoreMatrix);
                ignoreMatrix = ArrayExtensions.RotateArrayClockwise(ignoreMatrix);
                ignoreMatrix = ArrayExtensions.RotateArrayClockwise(ignoreMatrix);

                for (int i = 0; i < soulSpaces.Count; i++)
                    soulSpaces[i] = new Vector3Int(soulSpaces[i].y, -soulSpaces[i].x, 0);
                break;
            default:
                break;
        }
        for (int i = 0; i < ignoreMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < ignoreMatrix.GetLength(1); j++)
            {
                if (ignoreMatrix[i, j] == 1)
                    ignoreSpaces.Add(new Vector2Int(i, j));
            }
        }
    }

    public bool IsIgnoreLocal(Vector2Int vector2Int)
    {
        return ignoreSpaces.Contains(vector2Int);
    }
    public bool IsIgnore(Vector2Int vector2Int)
    {
        return ignoreSpaces.Contains(vector2Int - topLeft);
    }
}

public static class ArrayExtensions
{
    public static int[,] RotateArrayClockwise(int[,] src)
    {
        int width;
        int height;
        int[,] dst;

        width = src.GetUpperBound(0) + 1;
        height = src.GetUpperBound(1) + 1;
        dst = new int[height, width];

        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                int newRow;
                int newCol;

                newRow = col;
                newCol = height - (row + 1);

                dst[newCol, newRow] = src[col, row];
            }
        }

        return dst;
    }

    public static int[,] RotateArrayCounterClockwise(int[,] src)
    {
        int width;
        int height;
        int[,] dst;

        width = src.GetUpperBound(0) + 1;
        height = src.GetUpperBound(1) + 1;
        dst = new int[height, width];

        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                int newRow;
                int newCol;

                newRow = width - (col + 1);
                newCol = row;

                dst[newCol, newRow] = src[col, row];
            }
        }

        return dst;
    }

}
namespace UnityEngine
{
    [System.Serializable]
    [StructLayout(LayoutKind.Sequential)]
    public struct Vector4Int : IEquatable<Vector4Int>, IFormattable
    {
        public int x { get { return m_X; } set { m_X = value; } }
        public int y { get { return m_Y; } set { m_Y = value; } }
        public int z { get { return m_Z; } set { m_Z = value; } }
        public int w { get { return m_W; } set { m_W = value; } }

        private int m_X;
        private int m_Y;
        private int m_Z;
        private int m_W;

        public Vector4Int(int x, int y, int z, int w)
        {
            m_X = x;
            m_Y = y;
            m_Z = z;
            m_W = w;
        }

        // Set x, y and z components of an existing Vector.
        public void Set(int x, int y, int z, int w)
        {
            m_X = x;
            m_Y = y;
            m_Z = z;
            m_W = w;
        }

        // Access the /x/, /y/ or /z/ component using [0], [1] or [2] respectively.
        public int this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0: return x;
                    case 1: return y;
                    case 2: return z;
                    case 3: return w;
                    default:
                        throw new IndexOutOfRangeException(string.Format("Invalid Vector4Int index addressed: {0}!", index));
                }
            }

            set
            {
                switch (index)
                {
                    case 0: x = value; break;
                    case 1: y = value; break;
                    case 2: z = value; break;
                    case 3: w = value; break;
                    default:
                        throw new IndexOutOfRangeException(string.Format("Invalid Vector4Int index addressed: {0}!", index));
                }
            }
        }

        // Returns the length of this vector (RO).
        public float magnitude { get { return Mathf.Sqrt((float)(x * x + y * y + z * z + w * w)); } }

        // Returns the squared length of this vector (RO).
        public int sqrMagnitude { get { return x * x + y * y + z * z + w * w; } }

        // Returns the distance between /a/ and /b/.
        public static float Distance(Vector4Int a, Vector4Int b) { return (a - b).magnitude; }

        // Returns a vector that is made from the smallest components of two vectors.
        public static Vector4Int Min(Vector4Int lhs, Vector4Int rhs) { return new Vector4Int(Mathf.Min(lhs.x, rhs.x), Mathf.Min(lhs.y, rhs.y), Mathf.Min(lhs.z, rhs.z), Mathf.Min(lhs.w, rhs.w)); }

        // Returns a vector that is made from the largest components of two vectors.
        public static Vector4Int Max(Vector4Int lhs, Vector4Int rhs) { return new Vector4Int(Mathf.Max(lhs.x, rhs.x), Mathf.Max(lhs.y, rhs.y), Mathf.Max(lhs.z, rhs.z), Mathf.Max(lhs.w, rhs.w)); }

        // Multiplies two vectors component-wise.
        public static Vector4Int Scale(Vector4Int a, Vector4Int b) { return new Vector4Int(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w); }

        // Multiplies every component of this vector by the same component of /scale/.
        public void Scale(Vector4Int scale) { x *= scale.x; y *= scale.y; z *= scale.z; w *= scale.w; }

        public void Clamp(Vector4Int min, Vector4Int max)
        {
            x = Math.Max(min.x, x);
            x = Math.Min(max.x, x);
            y = Math.Max(min.y, y);
            y = Math.Min(max.y, y);
            z = Math.Max(min.z, z);
            z = Math.Min(max.z, z);
            w = Math.Max(min.w, w);
            w = Math.Min(max.w, w);
        }

        // Converts a Vector4Int to a [[Vector4]].
        public static implicit operator Vector4(Vector4Int v)
        {
            return new Vector4(v.x, v.y, v.z, v.w);
        }

        // Converts a Vector4Int to a [[Vector2Int]].
        public static explicit operator Vector3Int(Vector4Int v)
        {
            return new Vector3Int(v.x, v.y, v.z);
        }

        // Converts a Vector4Int to a [[Vector2Int]].
        public static explicit operator Vector2Int(Vector4Int v)
        {
            return new Vector2Int(v.x, v.y);
        }

        public static Vector4Int FloorToInt(Vector4 v)
        {
            return new Vector4Int(
                Mathf.FloorToInt(v.x),
                Mathf.FloorToInt(v.y),
                Mathf.FloorToInt(v.z),
                Mathf.FloorToInt(v.w)
            );
        }

        public static Vector4Int CeilToInt(Vector4 v)
        {
            return new Vector4Int(
                Mathf.CeilToInt(v.x),
                Mathf.CeilToInt(v.y),
                Mathf.CeilToInt(v.z),
                Mathf.CeilToInt(v.w)
            );
        }

        public static Vector4Int RoundToInt(Vector4 v)
        {
            return new Vector4Int(
                Mathf.RoundToInt(v.x),
                Mathf.RoundToInt(v.y),
                Mathf.RoundToInt(v.z),
                Mathf.RoundToInt(v.w)
            );
        }

        public static Vector4Int operator +(Vector4Int a, Vector4Int b)
        {
            return new Vector4Int(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
        }

        public static Vector4Int operator -(Vector4Int a, Vector4Int b)
        {
            return new Vector4Int(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
        }

        public static Vector4Int operator *(Vector4Int a, Vector4Int b)
        {
            return new Vector4Int(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w);
        }

        public static Vector4Int operator -(Vector4Int a)
        {
            return new Vector4Int(-a.x, -a.y, -a.z, -a.w);
        }

        public static Vector4Int operator *(Vector4Int a, int b)
        {
            return new Vector4Int(a.x * b, a.y * b, a.z * b, a.w * b);
        }

        public static Vector4Int operator *(int a, Vector4Int b)
        {
            return new Vector4Int(a * b.x, a * b.y, a * b.z, a * b.w);
        }

        public static Vector4Int operator /(Vector4Int a, int b)
        {
            return new Vector4Int(a.x / b, a.y / b, a.z / b, a.w / b);
        }

        public static bool operator ==(Vector4Int lhs, Vector4Int rhs)
        {
            return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z && lhs.w == rhs.w;
        }

        public static bool operator !=(Vector4Int lhs, Vector4Int rhs)
        {
            return !(lhs == rhs);
        }

        public override bool Equals(object other)
        {
            if (!(other is Vector4Int)) return false;

            return Equals((Vector4Int)other);
        }

        public bool Equals(Vector4Int other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            var yHash = y.GetHashCode();
            var zHash = z.GetHashCode();
            var wHash = w.GetHashCode();
            return x.GetHashCode() ^ (yHash << 8) ^ (yHash >> 24) ^ (zHash << 16) ^ (zHash >> 16) ^ (wHash << 24) ^ (wHash >> 8);
        }

        public override string ToString()
        {
            return ToString(null, CultureInfo.InvariantCulture.NumberFormat);
        }

        public string ToString(string format)
        {
            return ToString(format, CultureInfo.InvariantCulture.NumberFormat);
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            return string.Format("({0}, {1}, {2}, {3})", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider), w.ToString(format, formatProvider));
        }

        public static Vector4Int zero { get { return s_Zero; } }
        public static Vector4Int one { get { return s_One; } }

        private static readonly Vector4Int s_Zero = new Vector4Int(0, 0, 0, 0);
        private static readonly Vector4Int s_One = new Vector4Int(1, 1, 1, 1);
    }
}