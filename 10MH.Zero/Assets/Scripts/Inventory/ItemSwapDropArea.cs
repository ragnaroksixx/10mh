﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ItemSwapDropArea : DropArea
{
    public InventoryItemUI self;
    public override bool CanResolve(IClickableSource source)
    {
        return false;
        if (!(source is InventoryItemUI))
            return false;
        return CanSwap(source as InventoryItemUI);
    }
    public override void Resolve(IClickableSource source)
    {
        Swap(source as InventoryItemUI);
    }
    bool CanSwap(InventoryItemUI toSwap)
    {
        if (ExcessInventory.overExerted)
            return false;
        if (!self.Inventory.canPlaceItems || !toSwap.Inventory.canPlaceItems)
            return false;

        bool canSwap = true;
        self.Inventory.RemoveFromCache(self.ItemData);
        toSwap.Inventory.RemoveFromCache(toSwap.ItemData);
        canSwap = canSwap && self.CacheInvPosition.CanResolve(toSwap);
        /*
        if (!canSwap)
        {
            self.Inventory.RefreshOccupiedCache();
            toSwap.Inventory.RefreshOccupiedCache();
            return false;
        }

        if (self.Inventory == toSwap.Inventory)
        {
            ItemPlacementData fakeIPD = toSwap.ItemData.Copy();
            fakeIPD.x = self.ItemData.x;
            fakeIPD.y = self.ItemData.y;
            self.Inventory.AddToCache(fakeIPD);
        }
        canSwap = canSwap && toSwap.CacheInvPosition.CanResolve(self);
        */
        self.Inventory.RefreshOccupiedCache();
        toSwap.Inventory.RefreshOccupiedCache();
        return canSwap;
    }
    void Swap(InventoryItemUI toSwap)
    {
        //InventoryDropArea selfLoc = self.CacheInvPosition;
        //self.Drop(toSwap.CacheInvPosition);
        toSwap.Drop(self.CacheInvPosition);
        CursorController.Instance.lastClickTime = Mathf.Infinity;
        CursorController.Instance.SetSource(self);
        self.CacheInvPosition = null;
    }
}
