﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NHG.Dialogue;

public class AutoScenePlayer : MonoBehaviour
{
    public TimeRef time;
    public string memory;
    public TextAsset scene;
    private void Start()
    {
        if (TimeController.IsPast(time))
        {
            Destroy(this);
            return;
        }
        TimeController.AddTimeEvent(time, PlayScene);
    }
    void PlayScene()
    {
        DialogueRenderer.Instance.PlayScene(scene, null);
    }
}
