﻿using UnityEngine;
using System.Collections;
using CMF;

public class CharacterInput10MH : CharacterKeyboardInput
{
    public WalkController10MH walker;

    public override float GetHorizontalMovementInput()
    {
        if (walker.IsRunning && walker.SprintMoveData.moveType == SprintMoveData.MOVE_TYPE.FORWARD)
            return 0;
        return base.GetHorizontalMovementInput();
    }
    public override float GetVerticalMovementInput()
    {
        if (walker.SprintMoveData.moveType == SprintMoveData.MOVE_TYPE.FORWARD)
        {
            if (walker.IsRunning)
                return 1;
            else if (walker.IsRunLerp)
            {
                return Mathf.Lerp(base.GetVerticalMovementInput(), 1, walker.RunLerp);
            }
        }
        return base.GetVerticalMovementInput();
    }
}
