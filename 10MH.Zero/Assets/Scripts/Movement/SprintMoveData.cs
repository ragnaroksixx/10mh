﻿using System.Collections;
using UnityEngine;


[System.Serializable]
public class SprintMoveData
{
    public float duration;
    public float rechargeRate = 1;
    public float MaxSpeed;
    public MOVE_TYPE moveType;
    public float boostDuration;
    public AnimationCurve boostCurve;
    public float falloffDuration;
    public AnimationCurve falloffCurve;
    public bool useFootsteps;
    public AudioVariable runStartSFX, runLoopSFX;
    public AudioVariable runJumpSFX, runLandSFX;

    float lastSpeedValue;
    float startTime;
    float speedBoost;
    float baseSpeed;

    public float LastSpeedValue { get => lastSpeedValue; }

    public enum MOVE_TYPE
    {
        FORWARD,
        OMNIDIRECTIONAL
    }
    public void Start(float baseSPD)
    {
        startTime = Time.time;
        baseSpeed = baseSPD;
        speedBoost = MaxSpeed - baseSpeed;
    }
    public void End()
    {

    }
    public float GetSpeed()
    {
        float t = Time.time - startTime;
        lastSpeedValue = boostCurve.Evaluate(t / boostDuration) * speedBoost;
        lastSpeedValue += baseSpeed;
        lastSpeedValue *= falloffCurve.Evaluate(t / falloffDuration);
        //Debug.Log(lastSpeedValue+"|  b:"+ boostCurve.Evaluate(t / boostDuration) +"|  f: "+ falloffCurve.Evaluate(t / falloffDuration));

        return lastSpeedValue;
    }
}
