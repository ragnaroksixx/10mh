﻿using UnityEngine;
using System.Collections;
using CMF;
using DG.Tweening;

public class AudioControl10MH : MonoBehaviour
{

    //References to components;
    WalkController10MH controller;
    Mover mover;
    Transform tr;

    //Footsteps will be played every time the traveled distance reaches this value (if 'useAnimationBasedFootsteps' is set to 'true');
    public float footstepDistance = 0.2f;
    float currentFootstepDistance = 0f;

    public CustomAudioClip jumpSFX, landSFX;
    public CustomAudioClip[] footstepSFXs;
    //public CustomAudioClip runStartSFX, runLoopSFX;
    public AudioSource audioSource;
    //Volume of all audio clips;
    [Range(0f, 1f)]
    public float audioClipVolume = 0.1f;
    //Range of random volume deviation used for footsteps;
    //Footstep audio clips will be played at different volumes for a more "natural sounding" result;
    public float RelativeRandomizedVolumeRange = 0.2f;

    Vector3 pos { get => transform.position; }
    //Setup;
    void Start()
    {
        //Get component references;
        controller = GetComponent<WalkController10MH>();
        mover = GetComponent<Mover>();
        tr = transform;

        //Connecting events to controller events;
        controller.OnLand += OnLand;
        controller.OnJump += OnJump;
        controller.OnRunStart += OnRunStart;
        controller.OnRunEnd += OnRunEnd;
    }

    //Update;
    void Update()
    {

        //Get controller velocity;
        Vector3 _velocity = controller.GetVelocity();

        //Calculate horizontal velocity;
        Vector3 _horizontalVelocity = VectorMath.RemoveDotVector(_velocity, tr.up);

        FootStepUpdate(_horizontalVelocity.magnitude);
    }

    void FootStepUpdate(float _movementSpeed)
    {
        float _speedThreshold = 0.05f;

        currentFootstepDistance += Time.deltaTime * _movementSpeed;
        //Play foot step audio clip if a certain distance has been traveled;
        if (currentFootstepDistance > footstepDistance)
        {
            //Only play footstep sound if mover is grounded and movement speed is above the threshold;
            if (mover.IsGrounded() && _movementSpeed > _speedThreshold)
                PlayFootstepSound(_movementSpeed);
            currentFootstepDistance = 0f;
        }

    }

    void PlayFootstepSound(float _movementSpeed)
    {
        if (controller.IsRunning && !controller.userInputLock.Value && !controller.SprintMoveData.useFootsteps)
            return;
        int _footStepClipIndex = Random.Range(0, footstepSFXs.Length);
        audioSource.PlayOneShot(footstepSFXs[_footStepClipIndex].clip, audioClipVolume + audioClipVolume * Random.Range(-RelativeRandomizedVolumeRange, RelativeRandomizedVolumeRange));
    }

    void OnLand(Vector3 _v)
    {
        if (controller.userInputLock.Value)
            return;

        if (controller.IsRunning)
        {
            controller.SprintMoveData.runLandSFX.Play(pos);
        }
        else
        {
            landSFX.Play(pos, 0);
        }
    }

    void OnJump(Vector3 _v)
    {
        if (controller.userInputLock.Value)
            return;

        if (controller.IsRunning)
        {
            controller.SprintMoveData.runJumpSFX.Play(pos);
        }
        else
        {
            jumpSFX.Play(pos, 0);
        }
    }
    void OnRunStart(SprintMoveData smd)
    {
        if (controller.userInputLock.Value)
            return;

        if (smd.runLoopSFX.AudioInfo != null)
        {
            smd.runLoopSFX.Play(pos)
                .audioObject.volumeFunc = RunVolume;
        }

        if (smd.runStartSFX.AudioInfo != null)
        {
            smd.runStartSFX.Play(pos);
        }
    }

    void OnRunEnd(SprintMoveData smd)
    {
        AudioSystem.Stop(smd.runLoopSFX, true, 0);
    }

    float RunVolume()
    {
        if (controller.userInputLock.Value)
            return 0;
        Vector3 _velocity = controller.GetVelocity();
        Vector3 _horizontalVelocity = VectorMath.RemoveDotVector(_velocity, tr.up);

        if (_horizontalVelocity.magnitude > 0.1f && controller.IsGrounded())
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}
