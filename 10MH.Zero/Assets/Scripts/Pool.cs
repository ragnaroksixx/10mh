﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pool<T> where T : Behaviour
{
    protected List<T> activeItems;
    GameObject blueprint;
    Transform poolLocation;
    int size;
    protected List<T> inactiveItems;

    //public List<T> InactiveItems { get => inactiveItems; }
    public List<T> ActiveItems { get => activeItems; }

    public Pool(GameObject b, int initialSize)
    {
        GameObject g = GameObject.Find("Pool");
        if (g == null)
        {
            g = new GameObject("Pool");
            Object.DontDestroyOnLoad(g);
        }
        Transform p = g.transform.Find(b.name + " Pool");
        if (p == null)
        {
            GameObject parent = new GameObject(b.name + " Pool");
            parent.transform.SetParent(g.transform);
            p = parent.transform;
        }
        Init(b, initialSize, p);
    }
    public Pool(GameObject b, int initialSize, Transform root)
    {
        Init(b, initialSize, root);
    }
    public void Init(GameObject b, int initialSize, Transform root)
    {
        inactiveItems = new List<T>();
        activeItems = new List<T>();
        blueprint = b;
        poolLocation = root;
        size = initialSize;
        Populate(size);
    }
    private void Populate(int count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject g = GameObject.Instantiate(blueprint, poolLocation);
            Init(g.GetComponent<T>());
            Push(g.GetComponent<T>());
        }
    }
    public T Pop()
    {
        if (inactiveItems.Count == 0)
            Populate(size);
        T obj = inactiveItems[0];
        inactiveItems.RemoveAt(0);
        activeItems.Add(obj);
        OnPop(obj);
        return obj;
    }

    public void Push(T obj)
    {
        if (inactiveItems.Contains(obj))
            return;
        inactiveItems.Add(obj);
        activeItems.Remove(obj);
        OnReturn(obj);
    }

    protected virtual void OnPop(T obj)
    {
        obj.gameObject.SetActive(true);
    }

    protected virtual void OnReturn(T obj)
    {
        if (obj.gameObject != null)//This can be null for some reason
            obj.gameObject.SetActive(false);
    }
    protected virtual void Init(T obj)
    {
        obj.gameObject.SetActive(false);
        obj.transform.localEulerAngles = Vector3.zero;
        obj.transform.localPosition = Vector3.zero;
    }
}
