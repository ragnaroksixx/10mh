using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DialogueProximityTrigger : MonoBehaviour, IMemorable
{
    public DialogueVariable scene;
    public Character c;
    public string memoryCategory => "ProximityDialogue";

    public string memoryID => c.characterData.displayName + scene.scene.name;

    private void Start()
    {
        if (CycleManager.Instance.HasEvent(this))
        {
            Destroy(this);
            return;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.root == Player.Instance.transform)
        {
            DialogueRenderer.Instance.PlayScene(scene, null);
            Destroy(this);
            CycleManager.Instance.AddTempEvent(this);
        }
    }
}
