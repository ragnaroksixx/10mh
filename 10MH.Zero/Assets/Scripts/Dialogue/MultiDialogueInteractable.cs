﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;
public class MultiDialogueInteractable : DialogueInteractable
{
    public DialogueVariable repeatScene;
    public override void OnInteract()
    {
        base.OnInteract();
        scene = repeatScene;
    }
}

