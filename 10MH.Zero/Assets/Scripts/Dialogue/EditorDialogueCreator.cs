﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using Sirenix.OdinInspector;
using TMPro;

namespace NHG.Dialogue
{
    public class EditorDialogueCreator : MonoBehaviour
    {
#if UNITY_EDITOR
        public CharacterTextElement debugElement = null;
        public TextStyle style;
        [TextArea(5, 20)]
        public string textWords;
        [FoldoutGroup("Defaults")]
        public TMP_Text text;
        [FoldoutGroup("Defaults")]
        public GameObject prefab;
        [FoldoutGroup("Defaults")]
        public WorldElementEffect defaultEffect;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        private void OnValidate()
        {
            text.text = textWords;
        }
        [Button(Expanded = true)]
        public void SetTextElement(CharacterTextElement te)
        {
            if (te != null)
                debugElement = te;
            if (debugElement == null) return;
            textWords = te.text;
            style = te.style;
            Style();
            OnValidate();

            text.horizontalAlignment = debugElement.horzAligment;
            text.verticalAlignment = debugElement.vertAligment;

            (transform as RectTransform).anchoredPosition = debugElement.position;
            (transform as RectTransform).localEulerAngles = debugElement.rotation;

        }
        [Button]
        void Style()
        {
            if (style)
            {
                style.ApplyStyle(text);
            }
            EditorUtility.SetDirty(this.gameObject);
        }
        [Button]
        void UpdateTextElement()
        {
            /*
            if (debugElement == null)
            {
                string assetName = "Dialogue_" + text.text;
                assetName = assetName.Substring(0, Mathf.Min(30, assetName.Length));
                debugElement = ScriptableObjectUtility.CreateAsset<TextElement>(assetName);
                debugElement.effect = defaultEffect;
            }
            else
            {
                if (!EditorUtility.DisplayDialog("Overwrite Dialogue " + debugElement.name,
                    "Are you sure you want to overwrite this, stupid",
                    "yes", "no"))
                    return;
            }

            if (!string.IsNullOrEmpty(text.text))
                debugElement.text = text.text;
            if (style)
                debugElement.style = style;

            debugElement.horzAligment = text.horizontalAlignment;
            debugElement.vertAligment = text.verticalAlignment;

            debugElement.position = (transform as RectTransform).anchoredPosition;
            debugElement.rotation = (transform as RectTransform).localEulerAngles;

            EditorUtility.SetDirty(debugElement);*/
        }

        [Button(ButtonSizes.Large)]
        void CreateNewTextElement()
        {
            debugElement = null;
            UpdateTextElement();
        }
        [Button(ButtonSizes.Gigantic)]
        [GUIColor(1f, 0.5f, 0.5f, 1)]
        void Clear()
        {
            debugElement = null;
            textWords = "";
            text.text = "";
            (transform as RectTransform).anchoredPosition = Vector3.zero;
            (transform as RectTransform).localEulerAngles = Vector3.zero;

            text.horizontalAlignment = HorizontalAlignmentOptions.Left;
            text.verticalAlignment = VerticalAlignmentOptions.Middle;

            EditorUtility.SetDirty(this.gameObject);
        }

#endif
    }
}
