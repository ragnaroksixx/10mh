﻿using UnityEngine;
using System.Collections;
using TMPro;
using NHG.Dialogue;
using UnityEngine.UI;
using RedBlueGames.Tools.TextTyper;
using System.Collections.Generic;
using DG.Tweening;
using System;
using Febucci.UI;

public class ScreenDialogueMenu : MonoBehaviour
{
    public TMP_Text text, nameText;
    public Image nameBox;
    public Image textBox;
    public OnScreenAnimatorBASE animator;
    List<string> textQueue = new List<string>();
    CharacterData currentCharacter;
    public Outline colorOutline;//, colorOutlineCharacter;
    public Color defaultTextColor;
    public CharacterData narrator;
    public TextAnimatorPlayer typer;
    LinkProcessor linkProcessor;
    public CustomAudioClip proceedSFX;
    bool show;
    public RectTransform dialogueBox;
    float normalSize = 212;
    float phoneSize = 255;

    private void Awake()
    {
        linkProcessor = text.GetComponent<LinkProcessor>();
        text.text = "";
        colorOutline.enabled = false;
        normalSize = dialogueBox.offsetMin.x;
    }
    private void Start()
    {
        Publisher.Subscribe<InventoryOpenCloseEvent>(OnInventoryShowHide);
        Publisher.Subscribe<PhoneOpenCloseEvent>(OnPhoneShowHide);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<InventoryOpenCloseEvent>(OnInventoryShowHide);
        Publisher.Unsubscribe<PhoneOpenCloseEvent>(OnPhoneShowHide);
    }
    private void OnInventoryShowHide(InventoryOpenCloseEvent e)
    {
        if (show)
        {
            if (e.isOpen)
                animator.AnimateOffScreen();
            else
                animator.AnimateOnScreen();
        }
    }
    private void OnPhoneShowHide(PhoneOpenCloseEvent e)
    {
        SetSizeForPhone(e.isOpen);
    }
    void SetSizeForPhone(bool isPhoneVisible, bool instant = false)
    {
        Vector2 size = dialogueBox.offsetMin;

        if (show)
        {
            if (isPhoneVisible)
                size.x = phoneSize;
            else
                size.x = normalSize;

            if (instant)
                dialogueBox.offsetMin = size;
            else
                dialogueBox.DOOffsetMin(size, 0.25f);
        }
    }
    public void SetUp(ScreenTextAction e)
    {
        SetUp(e.character, e.text, e.hiddenName, "");
    }
    public void SetUp(CharacterTextElement e)
    {
        SetUp(e.character, e.text, e.useHiddenName, e.hiddenName);
    }
    public void SetUp(CharacterData character, string t, bool useHiddenName, string hiddenName)
    {
        text.text = "";
        textQueue = new List<string>(t.Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries));
        //textQueue.Add(e.text);
        textQueue = Global10MH.ParseLines(textQueue);
        if (Player.Instance != null)
        {
            nameBox.gameObject.SetActive(character != narrator && character != Player.Instance.characterData);
            textBox.gameObject.SetActive(character != narrator && character != Player.Instance.characterData);
            if (character == narrator || character == Player.Instance.characterData)
            {
                text.alignment = TMPro.TextAlignmentOptions.Center;
            }
            else
            {
                text.alignment = TMPro.TextAlignmentOptions.TopLeft;
            }
        }
        else
        {
            nameBox.gameObject.SetActive(false);
            textBox.gameObject.SetActive(false);
            text.alignment = TMPro.TextAlignmentOptions.Center;
        }
        if (character != narrator)
        {
            nameBox.color = character.characterColor;
            if (useHiddenName)
            {
                nameText.text = string.IsNullOrEmpty(hiddenName) ? character.hiddenDisplayName : hiddenName;
            }
            else
            {
                nameText.text = character.displayName;
            }
        }
        LayoutRebuilder.ForceRebuildLayoutImmediate(nameBox.rectTransform);
        //linkProcessor.SetEvents(e.linkEvents);
        colorOutline.effectColor = character.characterColor;
        DialogueRenderer.Instance.SetProceedColor(character.characterColor);
        //colorOutlineCharacter.enabled = e.character != narrator;
        if (character == narrator)
            text.color = character.characterColor;
        else
            text.color = defaultTextColor;

        currentCharacter = character;
    }
    public IEnumerator Show()
    {
        show = true;
        SetSizeForPhone(Phone.Instance.IsOpen, true);
        text.gameObject.SetActive(true);
        animator.AnimateOnScreen();
        yield return new WaitForSeconds(animator.transitionSpeed);
        colorOutline.enabled = true;
        int index = 0;
        foreach (string line in textQueue)
        {
            string textLine = line;
            DialogueRenderer.Instance.log.AddLog(currentCharacter, textLine);
            float skipTime = 0.25f;
            NextLine(index);
            while (typer.IsInsideRoutine)
            {
                yield return null;
                if (DialogueRenderer.SkipProceed)
                {
                    skipTime -= Time.deltaTime;
                    if (skipTime <= 0)
                        typer.SkipTypewriter();
                }
                if (DialogueRenderer.FinishType)
                    typer.SkipTypewriter();
            }
            yield return DialogueRenderer.Instance.WaitForPlayerInput();
            if (!DialogueRenderer.SkipProceed)
                proceedSFX.Play();
            index++;
            yield return null;
        }
        text.text = "";
    }
    public IEnumerator Hide()
    {
        show = false;
        colorOutline.enabled = false;
        yield return null;
        text.text = "";
        text.gameObject.SetActive(false);
        animator.AnimateOffScreen();
    }
    void NextLine(int index)
    {
        string text = textQueue[index];
        typer.ShowText(text);
    }

}
