﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using NHG.Dialogue;

public class MoveCharacterSceneTrigger : SceneTrigger
{
    public CharacterData character;
    protected override void Trigger(SceneTriggerEvent e)
    {
        base.Trigger(e);
        Character.FindCharacter(character).MoveTo(transform.position);
        Destroy(this);
    }
}

