﻿using UnityEngine;
using System.Collections;
using TMPro;

[CreateAssetMenu(fileName = "New Text Style", menuName = "Dialogue/Text Style")]
public class TextStyle : ScriptableObject, IUnquieScriptableObject
{
    public string id;
    public TMP_FontAsset font;
    public float size;

    public string Identifier => id;
    public string[] Alias => new string[] { };

    public void ApplyStyle(TMP_Text text)
    {
        text.font = font;
        text.fontSize = size;
    }

    public void Init()
    {
    }
}
