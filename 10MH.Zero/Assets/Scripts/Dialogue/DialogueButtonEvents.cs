﻿using System.Collections;
using UnityEngine;
using NHG.Dialogue;

public class DialogueButtonEvents : MonoBehaviour
{
    public void ContinueFromWait()
    {
        WaitForEventAction.Continue();
    }

    public void JumpTo(string jumpID)
    {
        JumpToAction.JumpTo(jumpID);
    }

    public void Destroy(GameObject g)
    {
        GameObject.Destroy(g);
    }
}
