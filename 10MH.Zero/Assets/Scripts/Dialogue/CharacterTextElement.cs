﻿using UnityEngine;
using System.Collections;
using TMPro;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using RedBlueGames.Tools.TextTyper;
using DG.Tweening;

namespace NHG.Dialogue
{
    public class CharacterTextElement : WorldElement
    {
        public bool waitForInput => !tags.GetBool("auto");
        public bool newLayer => tags.GetBool("layer");
        //public bool keep => tags.GetBool("+");
        public bool stop => tags.GetBool("stop") || tags.GetBool("-");
        public bool instant => tags.GetBool("instant");
        public bool caps => tags.GetBool("caps");
        public bool outline => tags.GetBool("outline");
        public string emotes => tags.GetString("emote", "");
        public bool displayName => tags.GetBool("display") || useHiddenName;
        public string hiddenName => tags.GetString("hidden", "");
        public bool useHiddenName => tags.HasTag("hidden");
        public bool enter => tags.GetBool("enter");
        public string text => tags.GetText();
        public TextStyle style => tags.GetTextStyle();
        public Vector2 size => sizes[tags.GetString("size", "default")];
        public string sprite => tags.GetString("sprite", null);
        public string[] itemLinkEvents => tags.GetStrings("items");
        public string itemLinkJump => tags.GetString("itemjump", null);
        public bool takeItem => tags.GetBool("takeitem");
        public bool selectRandomLine => tags.GetBool("selectrandom") || tags.GetBool("randomselect");

        public HorizontalAlignmentOptions horzAligment = HorizontalAlignmentOptions.Left;
        public VerticalAlignmentOptions vertAligment = VerticalAlignmentOptions.Top;

        public TextBox runtimeSpawnObj;
        public CharacterData character
        {
            get => GlobalData10MH.Instance.characterDictionary
                    .GetItem(tags.GetString("character", null));
        }

        public override string eventName => "BLAHRAWRULALA";

        public static Dictionary<string, Vector2> sizes = new Dictionary<string, Vector2>()
        {
            {"default",new Vector2(150,150) },
            {"small",new Vector2(120,75) },
            {"dot",new Vector2(51,51) },
            {"smed",new Vector2(200,100) },
            {"med",new Vector2(250,115) },
            {"large",new Vector2(300,250) },

            {"short",new Vector2(150,100) },
            {"small long",new Vector2(150,75) },
            {"narrow",new Vector2(150,75) },
            {"medium",new Vector2(250,115) },
        };

        public static Dictionary<string, Vector2> characterPositions = new Dictionary<string, Vector2>()
        {
            {"default",new Vector2(0,0) },
            {"topleft",new Vector2(-160,90) },
            {"topright",new Vector2(160,90) },
        };
        //public List<SceneEvent> linkEvents;
        //public List<ItemEvents> itemEvents;

        public IEnumerator Process(Layer layer, Vector3 pos, Vector3 rot)
        {
            CharacterData cacheChar = character;
            if (enter)
            {
                CharacterEnterData enterData = new CharacterEnterData()
                {
                    character = cacheChar,
                    sprite = sprite ?? "normal",
                    transitionTime = CharacterEffectData.defaultTransitionSpeed,
                };
                yield return DialogueRenderer.Instance.EnterCharacterAndWait(enterData);
            }
            else if (sprite != null)
            {
                CharacterEffectData eData = new CharacterEffectData()
                {
                    character = cacheChar,
                    sprite = sprite,
                    transitionTime = CharacterEffectData.defaultTransitionSpeed,
                };
                yield return DialogueRenderer.Instance.SpriteCharacterAndWait(eData);
            }

            if (CurrentScene.IsDraft)
            {
                //yield return ProcessDraft(layer);
                //yield break;
            }
            if (cacheChar == Player.Instance.characterData)
            {
                yield return ProcessDraft(layer);
                yield break;
            }
            GameObject prefab = GlobalData10MH.Instance.textBoxPrefab;

            if (newLayer)
                layer = NewLayerAction.ProcessNewLayerUp(layer);

            if (TextBox.CharacterBoxes.ContainsKey(cacheChar))
            {
                runtimeSpawnObj = TextBox.CharacterBoxes[cacheChar];
            }
            else
            {
                Transform obj = GameObject.Instantiate(prefab, layer.transform).transform;

                runtimeSpawnObj = obj.GetComponent<TextBox>();
                obj.SetParent(layer.transform);
            }

            style.ApplyStyle(runtimeSpawnObj.TextComponent);
            yield return DialogueRenderer.Instance.SetActiveCharacter(character);
            DialogueRenderer.Instance.SetCharacterItemEvents(GetItemEvents());
            yield return runtimeSpawnObj.Set(this);
            yield return runtimeSpawnObj.TypeQueue(this);

        }

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            yield return Process(layer, position, rotation);
            yield return new WaitForSeconds(endDelay);
            if (waitForInput && !CurrentScene.IsDraft)
                yield return WaitForInputAction.ProcessWaitForInput(layer);
            runtimeSpawnObj?.OnProcessComplete();

        }

        public IEnumerator ProcessDraft(Layer layer)
        {
            yield return DialogueRenderer.Instance.SetActiveCharacter(character);
            DialogueRenderer.Instance.SetCharacterItemEvents(GetItemEvents());
            yield return DialogueRenderer.ShowScreenText(this);
            yield return DialogueRenderer.HideScreenText();
        }
        public void DoEffect(WorldElementEffect effect, EffectsData ed, MonoBehaviour mb)
        {
            mb.StartCoroutine(effect.Play(ed));
        }

        public override Vector3 GetPosition()
        {
            string pos = tags.GetString("pos", null);
            if ((!string.IsNullOrEmpty(pos)) && characterPositions.ContainsKey(pos))
            {
                Vector2 localPos = characterPositions[pos];
            }
            return base.GetPosition();
        }
        public List<ItemEvents> GetItemEvents()
        {
            List<ItemEvents> result = new List<ItemEvents>();
            foreach (string item in itemLinkEvents)
            {
                result.Add(new ItemEvents(item, itemLinkJump, takeItem));
            }
            return result;
        }

#if UNITY_EDITOR
        [Button]
        void EditorDraw()
        {
            EditorDialogueCreator temp = GameObject.FindObjectOfType<EditorDialogueCreator>();
            temp.SetTextElement(this);
        }
#endif
    }
    [System.Serializable]
    public class ItemEvents
    {
        public Item item;
        public string jumpID;
        public bool takeItem;

        public ItemEvents(string itemID, string jumpID, bool takeItem)
        {
            this.item = GlobalData10MH.Instance.itemDictionary
                    .GetItem(itemID);
            this.jumpID = jumpID;
            this.takeItem = takeItem;
        }
    }

}