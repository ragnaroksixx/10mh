﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;

namespace NHG.Dialogue
{
    public static class DialogueUtils
    {
        static DialogueUtils()
        {
            DialogueEventLibrary.Init();
            LogicLibary.Init();
            CombatEventLibrary.Init();
        }
        public static Scene GetSceneFrom(IDialogueScene ids)
        {
            return GetScene(ids.GetText());
        }
        public static Scene GetSceneFromFile(string fileName)
        {
            TextAsset ta = Resources.Load<TextAsset>(fileName);
            return GetSceneFromTextAsset(ta);
        }
        public static Scene GetSceneFromTextAsset(TextAsset ta)
        {
            return GetScene(ta.text);
        }
        public static Scene GetSceneFromCharacter(string keyName, CharacterData cd)
        {
            TextAsset ta = cd.GetScript(keyName);
            return GetScene(ta.text);
        }
        public static Scene CreateSimpleTextScene(params string[] texts)
        {
            Scene scene = new Scene();
            scene.Init(new Tags());
            string output = "";
            foreach (string text in texts)
            {
                output += text;
                output += "\t";
            }
            ScreenTextAction e = new ScreenTextAction();
            e.Init(new Tags());
            e.Tags.SetText(output);
            scene.events.Add(e);
            return scene;
        }
        public static Scene CreateBattleScene(BattleEncounter e)
        {
            Scene scene = new Scene();
            scene.Init(new Tags());
            CombatDialogueAction cda = new CombatDialogueAction();
            cda.Init(new Tags());
            cda.Tags.AddTag("express", e.Identifier);
            scene.events.Add(cda);
            return scene;
        }
        public static Dictionary<string, Scene> GetScenesFromTextAsset(TextAsset ta)
        {
            return GetScenes(ta.text);
        }
        public static Dictionary<string, Scene> GetScenes(string fileText)
        {
            string pattern = @"\{((.|\n)*?)\}";
            Dictionary<string, Scene> result = new Dictionary<string, Scene>();
            Regex r = new Regex(pattern);
            foreach (Match match in r.Matches(fileText))
            {
                string sceneText = match.Value;
                Scene scene = GetScene(sceneText);
                result.Add(scene.sceneName.ToLower(), scene);
            }

            return result;
        }
        public static Scene GetScene(string fileText)
        {
            fileText = fileText.Replace("{", "");
            fileText = fileText.Replace("}", "");

            string[] headerSplit = fileText.SplitFirst('[', '#');
            string header = headerSplit[0];
            fileText = headerSplit[1];

            Tags headerTags = new Tags(header);
            Scene scene = new Scene();
            scene.Init(headerTags);

            string[] lines = fileText.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> finalLines = new List<string>();
            string prevLine = null;
            foreach (string line in lines)
            {
                if (line.Trim() == "")
                {
                    continue;
                }
                if (line[0] == '/') // a comment
                {
                    continue;
                }
                if (line[0] == '[' || line[0] == '#')
                {
                    if (!string.IsNullOrEmpty(prevLine))
                        finalLines.Add(prevLine);
                    prevLine = line;
                }
                else
                {
                    prevLine += line;
                }
            }

            if (!string.IsNullOrEmpty(prevLine))
                finalLines.Add(prevLine);

            foreach (string line in finalLines)
            {
                Tags tags;
                DialogueEvent de = GetAction(line, out tags);
                if (de == null) continue;
                de.Init(tags);
                scene.events.Add(de);
            }

            return scene;
        }

        public static DialogueEvent GetAction(string line, out Tags initialTags)
        {
            if (String.IsNullOrEmpty(line.Trim()))
            {
                initialTags = null;
                return null;
            }
            DialogueEvent e;
            initialTags = new Tags(line);
            string lineData;
            string typeText = GetTypeString(line, out lineData);
            string[] tagSplit = lineData.SplitFirst('<');
            string body = tagSplit[0];
            body = body.Trim();
            string tags = tagSplit[1];

            if (IsAction(typeText))//Action
            {
                e = GetAction(typeText);
                initialTags.AddTag("body", body);
                return e;
            }
            else if (IsLogic(typeText))//Logic Statement
            {
                e = GetLogic(typeText);
                initialTags.AddTag("body", body);
                return e;
            }


            if (typeText == "Box")//Narration
            {
                e = new NarrationTextElement();
                initialTags.AddTag("text", body);
            }
            else if (typeText == "")
            {
                e = new ScreenTextAction();
                initialTags.AddTag("text", body);
            }
            else if (typeText.ToLower() == "narrator")
            {
                e = new ScreenTextAction();
                initialTags.AddTag("text", body);
                initialTags.AddTag("character", "narrator");
            }
            else if (IsCharacter(typeText))
            {
                if (initialTags.HasTag("position"))
                {
                    e = new CharacterTextElement();
                }
                else
                {
                    e = new CharacterTextElement();
                }
                initialTags.AddTag("character", typeText);
                initialTags.AddTag("text", body);
            }
            else
            {
                e = null;
                Debug.LogError("Keyword " + typeText + " was not recognized");
            }

            return e;
        }

        private static bool IsCharacter(string typeText)
        {
            return GlobalData10MH.Instance.characterDictionary.HasItem(typeText);
        }

        public static string GetTypeString(string line, out string remainderText)
        {
            if (line[0] == '#')
                return GetActionTypeString(line, out remainderText);
            string pattern = @"\[(.*?)\]";
            string typeText = new Regex(pattern).Match(line).Value;
            typeText = typeText.Substring(1, typeText.Length - 2).ToLower().Trim();
            remainderText = line.SplitFirst(']')[1].Trim(']');
            return typeText.Trim();
        }
        private static string GetActionTypeString(string line, out string remainderText)
        {
            string[] split = line.SplitFirst(' ');
            remainderText = split.Length > 0 ? split[1] : "";
            return split[0].Replace("#", "");
        }
        private static bool IsAction(string typeText)
        {
            return DialogueEventLibrary.eventTypes.ContainsKey(typeText.AsKey());
        }
        private static DialogueAction GetAction(string typeText)
        {
            return Activator.CreateInstance(DialogueEventLibrary.eventTypes[typeText.AsKey()]) as DialogueAction;
        }

        private static bool IsLogic(string typeText)
        {
            return LogicLibary.logicTypes.ContainsKey(typeText.AsKey());
        }
        private static DialogueLogic GetLogic(string typeText)
        {
            return Activator.CreateInstance(LogicLibary.logicTypes[typeText.AsKey()]) as DialogueLogic;
        }
    }
}
public class Tags
{
    Dictionary<string, Tag> tags;
    const char listDelimiter = ' ';
    public Tags()
    {
        tags = new Dictionary<string, Tag>();
    }
    public Tags(string text) : this()
    {
        AddTags(text);
    }
    public void AddTags(string text)
    {
        string expressPattern = @"<<(.*?)>>";
        Regex r = new Regex(expressPattern);
        foreach (Match match in r.Matches(text))
        {
            Tag tag = new Tag("express", match.Value.Trim('<', '>'));
            tags.Add(tag.name, tag);
        }

        text = Regex.Replace(text, expressPattern, "");

        string pattern = @"<(.*?)>";
        r = new Regex(pattern);
        foreach (Match match in r.Matches(text))
        {
            Tag tag = new Tag(match.Value);
            tags.Add(tag.name, tag);
        }
    }
    public void AddTag(string name)
    {
        AddTag(name, "");
    }
    public void AddTag(string name, string value)
    {
        Tag tag = new Tag(name, value);
        if (HasTag(name))
            tags[name] = tag;
        else
            tags.Add(tag.name, tag);
    }
    public void RemoveTag(string name)
    {
        tags.Remove(name);
    }
    public bool HasTag(string tagName)
    {
        return tags.ContainsKey(tagName);
    }
    public string GetExpressString(string defaultValue = null)
    {
        return GetString("express", defaultValue);
    }
    public int GetExpressInt(int defaultValue = -1)
    {
        return GetInt("express", defaultValue);
    }
    public float GetExpressFloat(float defaultValue = -1)
    {
        return GetFloat("express", defaultValue);
    }
    public string GetString(string tagName, string defaultValue)
    {
        if (tags.ContainsKey(tagName))
        {
            return tags[tagName].value;
        }
        return defaultValue;
    }
    public string[] GetStrings(string tagName, char customDelimiter)
    {
        string[] split = new string[0];
        if (tags.ContainsKey(tagName))
        {
            split = tags[tagName].value.Split(customDelimiter);
        }
        return split;
    }
    public string[] GetStrings(string tagName)
    {
        return GetStrings(tagName, listDelimiter);
    }
    public bool GetBool(string tagName)
    {
        return tags.ContainsKey(tagName);
    }
    public bool GetBool(string tagName, bool defaultBool)
    {
        return tags.ContainsKey(tagName) || defaultBool;
    }
    public int GetInt(string tagName, int defaultValue)
    {
        if (tags.ContainsKey(tagName))
        {
            return Convert.ToInt32(tags[tagName].value);
        }
        return defaultValue;
    }
    public float GetFloat(string tagName, float defaultValue)
    {
        if (tags.ContainsKey(tagName))
        {
            return (float)Convert.ToDouble(tags[tagName].value);
        }
        return defaultValue;
    }
    public float[] GetFloats(string tagName)
    {
        string[] split = new string[0];
        if (tags.ContainsKey(tagName))
        {
            split = tags[tagName].value.Split(listDelimiter);
        }
        float[] floats = new float[split.Length];
        for (int i = 0; i < split.Length; i++)
        {
            floats[i] = (float)Convert.ToDouble(split[i]);
        }
        return floats;
    }
    public CustomAudioClip GetAudio(string key = "audio")
    {
        if (tags.ContainsKey(key))
        {
            string audioName = tags[key].value;
            return GlobalData10MH.Instance.audioDictionary.GetItem(audioName.Trim());
        }
        return null;
    }
    public CustomAudioClip GetExpressAudio()
    {
        string audioName = GetExpressString(null);
        if (audioName == null)
            return null;
        return GlobalData10MH.Instance.audioDictionary.GetItem(audioName.Trim());
    }
    public CharacterData GetExpressCharacter()
    {
        return GlobalData10MH.Instance.characterDictionary
                .GetItem(GetExpressString());
    }
    public CharacterData GetCharacter()
    {
        if (tags.ContainsKey("character"))
        {
            return GlobalData10MH.Instance.characterDictionary
                .GetItem(GetString("character", null));
        }
        else
            return null;
    }
    public void SetCharacter(CharacterData cd)
    {
        AddTag("character", cd.Identifier);
    }
    public AreaData GetArea(string key = "area")
    {
        if (tags.ContainsKey(key))
        {
            return GlobalData10MH.Instance.areaDictionary
                .GetItem(GetString(key, null));
        }
        else
            return null;
    }
    public AreaData GetExpressArea()
    {
        return GetArea("express");
    }
    public string GetText()
    {
        return GetString("text", null);
    }
    public void SetText(string text)
    {
        AddTag("text", text);
    }
    public void SetExpress(string text)
    {
        AddTag("express", text);
    }
    public TextStyle GetTextStyle(string defaultValue = "standard")
    {
        string s = GetString("style", defaultValue);
        return GlobalData10MH.Instance.textStyleDictionary.GetItem(s);
    }
    public BattleEncounter GetBattle()
    {
        if (tags.ContainsKey("battle"))
        {
            return GlobalData10MH.Instance.battleDictionary
                .GetItem(GetString("battle", null));
        }
        else
            return null;
    }
    public BattleEncounter GetExpressBattle()
    {
        return GlobalData10MH.Instance.battleDictionary
                .GetItem(GetExpressString());
    }
    public Item GetExpressItem()
    {
        return GetItem(GetExpressString());
    }
    public Item GetItem(string key)
    {
        return GlobalData10MH.Instance.itemDictionary
                .GetItem(key);
    }
    public GameObject GetPrefab(string key)
    {
        return Resources.Load<GameObject>(GetString(key, null));
    }
    public GameObject GetExpressPrefab()
    {
        return Resources.Load<GameObject>(GetExpressString(null));
    }
    public Vector3 GetVector3(string tagName, Vector3 defaultVector)
    {
        if (tags.ContainsKey(tagName))
        {
            float[] val = GetFloats(tagName);
            return new Vector3(val[0], val[1], val[2]);
        }
        return Vector3.zero;
    }
    public Vector3 GetVector2(string tagName, Vector2 defaultVector)
    {
        if (tags.ContainsKey(tagName))
        {
            float[] val = GetFloats(tagName);
            return new Vector3(val[0], val[1]);
        }
        return defaultVector;
    }
    public Vector3 GetPosition()
    {
        return GetVector3("position", Vector3.zero);
    }
    public Vector3 GetRotation()
    {
        return GetVector3("rotation", Vector3.zero);
    }

    public TimeRef GetTime(string tagName, TimeRef defaultTime)
    {
        if (tags.ContainsKey(tagName))
        {
            Vector2 time = GetVector2(tagName, Vector2.zero);
            return new TimeRef((int)time[0], (int)time[1]);
        }
        return defaultTime;
    }
}
public class Tag
{
    public string name, value;

    public Tag(string rawText)
    {
        rawText = rawText.Substring(1, rawText.Length - 2);
        string[] split = rawText.SplitFirst(' ');
        this.name = split[0].ToLower().Trim();
        this.value = split[1].ToLower().Trim();
    }

    public Tag(string name, string value)
    {
        this.name = name;
        this.value = value;
    }
}

