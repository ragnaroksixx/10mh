﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;

namespace NHG.Dialogue
{
    public class Layer : MonoBehaviour
    {
        public CanvasGroup canvasGroup;
        CoroutineHandler coroutine;
        Tween fadeTween, scaleTween;
        float fadeOutDuration = 0.5f;
        public static List<Layer> Layers = new List<Layer>();
        bool exitLock;
        //public GameObject EditorTextElementPrefab;
        public static Layer ActiveLayer
        {
            get => Layers.Count > 0 ? Layers[0] : null;
        }
        public void Init()
        {
            coroutine = new CoroutineHandler(this);
        }
        public void OnFocus()
        {

        }
        public void OnFocusExit()
        {
            if (exitLock) return;
            coroutine.StartCoroutine(FocusLossCoroutine());
        }
        public void OnExit(bool exitUp = false)
        {
            if (exitLock) return;
            exitLock = true;
            fadeTween.Kill();
            scaleTween.Kill();
            coroutine.StartCoroutine(ExitCoroutine(exitUp));
        }
        IEnumerator FocusLossCoroutine()
        {
            //fadeTween = canvasGroup.DOFade(0.2f, fadeOutDuration);
            //fadeTween.SetAutoKill(false);
            //scaleTween = transform.DOScale(transform.localScale * fadeOutScale, fadeOutDuration);
            //fade out
            //yield return new WaitForSeconds(fadeOutDuration / 3);
            //move behind player
            transform.SetAsFirstSibling();
            yield return null;
        }
        IEnumerator ExitCoroutine(bool exitUp)
        {
            fadeTween = canvasGroup.DOFade(0f, fadeOutDuration);
            fadeTween.SetAutoKill(false);
            //scaleTween = transform.DOLocalMoveY(transform.localPosition.y + 100, fadeOutDuration);
            //fade out
            yield return new WaitForSeconds(fadeOutDuration);
            Destroy(this.gameObject);
        }
        private void OnDestroy()
        {
            Layers.Remove(this);
        }
        public static Layer PushNewLayer(Transform parent)
        {
            if (ActiveLayer)
            {
                ActiveLayer.OnFocusExit();
            }
            Layer newLayer = GameObject.Instantiate(Global10MH.Instance10MH.dialogueLayerPrefab, parent, false);
            newLayer.Init();
            Push(newLayer);
            ActiveLayer.OnFocus();
            return newLayer;
        }
        public static Layer PushNewLayerExitUp(Transform parent)
        {
            if (ActiveLayer)
            {
                ActiveLayer.OnExit(true);
            }
            Layer newLayer = GameObject.Instantiate(Global10MH.Instance10MH.dialogueLayerPrefab, parent, false);
            newLayer.Init();
            Push(newLayer);
            ActiveLayer.OnFocus();
            return newLayer;
        }
        static int maxLayers = 2;
        private static void Push(Layer l)
        {
            Layers.Insert(0, l);
            for (int i = 0; i < Layers.Count; i++)
            {
                if (i >= maxLayers)
                    Layers[i].OnExit();
            }
        }
        public static void PopLast()
        {
            if (Layers.Count <= 0) return;
            Layers[Layers.Count - 1].OnExit();
        }
        public static void ExitAll()
        {
            for (int i = 0; i < Layers.Count; i++)
            {
                Layers[i].OnExit();
            }
        }
        /*[Button("Spawn Text Element")]
        public void SpawnTextElementInEditor()
        {
            TMP_Text worldText = GameObject.Instantiate(EditorTextElementPrefab).GetComponent<TMP_Text>();
            worldText.transform.SetParent(transform);
            worldText.transform.localScale = Vector3.one;
        }*/
    }
}
