﻿using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.UI;
using NHG.Dialogue;
using System.Collections.Generic;

public class PlayerResponeMenu : MonoBehaviour
{
    public TMP_Text highlightText;
    public DialogueChoiceUI[] options;
    CanvasGroup c;
    public LinearOnScreenAnimator animator;
    public CharacterData player;
    public Color normalColor, smsColor;
    bool isSMS = false;
    // Use this for initialization
    void Start()
    {
        c = gameObject.AddComponent<CanvasGroup>();
        for (int i = 0; i < options.Length; i++)
        {
            options[i].Init(player);
        }
    }

    public void SetUp(DialogueOptionsEvent e)
    {
        highlightText.text = e.highlightText;
        isSMS = e.isSMSOptions;
        ColorBlock c = options[0].button.colors;
        c.normalColor = c.highlightedColor = e.isSMSOptions ? smsColor : normalColor;

        for (int i = 0; i < options.Length; i++)
        {
            options[i].button.colors = c;
            options[i].button.gameObject.SetActive(false);
        }
        for (int i = 0; i < options.Length; i++)
        {
            Button b = options[i].button;
            b.onClick.RemoveAllListeners();
            if (e.options.Count <= i)
            {
                b.gameObject.SetActive(false);
            }
            else
            {
                bool isEnabled = e.options[i].MeetsCriteria() || !e.options[i].disableif;
                b.interactable = isEnabled;
                b.gameObject.SetActive(true);
                b.GetComponentInChildren<TMP_Text>().text = e.options[i].text
                    + (isEnabled ? "" : e.options[i].disableTextAddon);
                DialogueOption de = e.options[i];
                b.onClick.AddListener(() => { ProcressSelection(de); });
            }
        }
    }
    public void ProcressSelection(DialogueOption e)
    {
        StartCoroutine(Process(e));
    }
    IEnumerator Process(DialogueOption e)
    {
        yield return DialogueRenderer.HideResponseMenu();
        if (!string.IsNullOrEmpty(e.smsEvent))
        {
            //Jank incoming
            MessagePage mp = GameObject.FindObjectOfType<MessagePage>();
            TextMessageAction.AddMessage(mp.chat.chatName, "Norman", e.smsEvent);
        }

        if (!string.IsNullOrEmpty(e.jumpEvent))
            JumpToAction.JumpTo(e.jumpEvent);
    }
    public IEnumerator Show()
    {
        c.interactable = false;
        animator.AnimateOnScreen();
        yield return new WaitForSeconds(animator.transitionSpeed);
        Global10MH.ShowCursor(this);
        //HUDController.Instance.SetUIState(HUDController.Instance.dialogueView);
        InventoryManager.LockInput(this);
        c.interactable = true;
    }
    public IEnumerator Hide()
    {
        Global10MH.HideCursor(this);
        InventoryManager.UnlockInput(this);
        c.interactable = false;
        animator.AnimateOffScreen();
        yield return new WaitForSeconds(animator.transitionSpeed);
    }
}
