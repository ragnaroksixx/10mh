﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace NHG.Dialogue
{
    public class PopUpEffect : WorldElementEffect
    {
        public float duration = 1;
        public override IEnumerator Play(EffectsData ed)
        {
            yield return base.Play(ed);
            SetFinal(ed, scale: false);
            Vector3 startScale = ed.finalScale;
            startScale.y = 0;
            ed.rect.transform.localScale = startScale;
            ed.rect.DOScaleY(1, duration).SetEase(Ease.OutBounce);
            yield return new WaitForSeconds(duration);
        }
    }

    public class StretchUpEffect : WorldElementEffect
    {
        public float duration = 1;
        public float scalePercentage = 0.25f;

        public StretchUpEffect(float duration, float scalePercentage)
        {
            this.duration = duration;
            this.scalePercentage = scalePercentage;
        }

        public override IEnumerator Play(EffectsData ed)
        {
            yield return base.Play(ed);
            SetFinal(ed);
            Vector3 maxScale = ed.finalScale * 0.5f;
            ed.rect.DOScaleY(ed.finalScale.y * (1 + scalePercentage), duration).SetEase(Ease.OutBounce);
            yield return new WaitForSeconds(duration);
            ed.rect.DOScaleY(ed.finalScale.y, 0.5f).SetEase(Ease.OutBounce);
            yield return new WaitForSeconds(0.5f);
        }
    }
}
