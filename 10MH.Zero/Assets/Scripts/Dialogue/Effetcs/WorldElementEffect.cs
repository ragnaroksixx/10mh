﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace NHG.Dialogue
{
    public class WorldElementEffect
    {
        public virtual IEnumerator Play(EffectsData ed)
        {
            yield return null;
        }
        protected void SetFinal(EffectsData ed, bool scale = true, bool pos = true, bool rot = true, bool alpha = true)
        {
            if (pos)
                ed.rect.anchoredPosition = ed.finalPos;
            if (rot)
                ed.rect.localEulerAngles = ed.finalRot;
            if (scale)
                ed.rect.localScale = new Vector3(1, 1, 1);
            else
                ed.rect.localScale = new Vector3(0, 0, 0);
            if (alpha)
                ed.obj.alpha = 1;
            else
                ed.obj.alpha = 0;
        }

        public void Init()
        {

        }
    }

    public struct EffectsData
    {
        public Vector3 finalPos;
        public Vector3 finalRot;
        public Vector3 finalScale;
        public CanvasGroup obj;
        public RectTransform rect
        {
            get => obj.transform as RectTransform;
        }
        public EffectsData(Vector3 finalPos, Vector3 finalRot, Vector3 finalScale, CanvasGroup obj)
        {
            this.finalPos = finalPos;
            this.finalRot = finalRot;
            this.finalScale = finalScale;
            this.obj = obj;
        }
    }
}
