﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace NHG.Dialogue
{
    public class ShakeEffect : WorldElementEffect
    {
        public float intensity = 90;
        public int vibrato = 10;
        public float duration = 1;

        public ShakeEffect()
        {
        }

        public ShakeEffect(float intensity)
        {
            this.intensity = intensity;
        }

        public ShakeEffect(float intensity, int vibrato, float duration)
        {
            this.intensity = intensity;
            this.vibrato = vibrato;
            this.duration = duration;
        }

        public override IEnumerator Play(EffectsData ed)
        {
            yield return base.Play(ed);
            SetFinal(ed);
            Vector3 strength = new Vector3(intensity, 0, 0);
            yield return null;
            ed.rect.DOShakePosition(duration, strength, vibrato);
        }
    }

    public class DelayedShakeEffect : ShakeEffect
    {
        public float delay = 0;

        public DelayedShakeEffect(float delay, float intensity, int vibrato, float duration) : base(intensity, vibrato, duration)
        {
            this.intensity = intensity;
            this.vibrato = vibrato;
            this.duration = duration;
            this.delay = delay;
        }

        public override IEnumerator Play(EffectsData ed)
        {
            SetFinal(ed);
            yield return new WaitForSeconds(delay);
            yield return base.Play(ed);
        }
    }
}
