﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace NHG.Dialogue
{
    public class PopInEffect : WorldElementEffect
    {
        public float duration = 1;
        public override IEnumerator Play(EffectsData ed)
        {
            yield return base.Play(ed);
            SetFinal(ed, scale: false);

            ed.rect.DOScale(ed.finalScale, duration).SetEase(Ease.OutBounce);
        }
    }
}