﻿using UnityEngine;
using System.Collections;

namespace NHG.Dialogue
{
    public class NoEffect : WorldElementEffect
    {
        public override IEnumerator Play(EffectsData ed)
        {
            yield return base.Play(ed);
            SetFinal(ed);
        }
    }
}
