﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace NHG.Dialogue
{
    public class DropInEffect : WorldElementEffect
    {
        public float fallPos = 0;
        public float duration = 1;

        public DropInEffect(float fallPos, float duration)
        {
            this.fallPos = fallPos;
            this.duration = duration;
        }

        public override IEnumerator Play(EffectsData ed)
        {
            yield return base.Play(ed);
            SetFinal(ed, pos: false, scale: false);

            Vector3 startPos = ed.finalPos;
            startPos.y += fallPos;
            ed.rect.anchoredPosition = startPos;

            yield return null;

            ed.rect.localScale = new Vector3(1, 1, 1);
            ed.rect.DOAnchorPos(ed.finalPos, duration).SetEase(Ease.OutBounce);
        }
    }

    public class InvertRotate : WorldElementEffect
    {
        public override IEnumerator Play(EffectsData ed)
        {
            yield return base.Play(ed);
            SetFinal(ed, rot: false);

            ed.rect.transform.localEulerAngles = -ed.finalRot;

            yield return new WaitForSeconds(0.25f);

            ed.rect.transform.localEulerAngles = ed.finalRot;
        }
    }
}
