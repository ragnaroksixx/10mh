﻿using UnityEngine;
using System.Collections;
using RedBlueGames.Tools.TextTyper;
using NHG.Dialogue;
using TMPro;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;
using Febucci.UI;
using System;

public class TextBox : MonoBehaviour
{
    public static Dictionary<CharacterData, TextBox> CharacterBoxes = new Dictionary<CharacterData, TextBox>();
    public TextAnimatorPlayer typer;
    public Transform excalmation, question;
    public CanvasGroup cg;
    public TMP_Text nameText;
    CharacterData c;
    public CustomAudioClip onOpenSFX;
    bool keep = true;
    public LayoutGroup lGroup;
    public Image top, bottom;
    public Image topOutline, bottomOutline;
    public GameObject[] outlinesAndShadowsObjects;
    List<Shadow> outlinesAndShadows;
    public TMP_Text TextComponent => typer.textAnimator.tmproText;
    RectTransform rectTransform => transform as RectTransform;
    private void Awake()
    {
        outlinesAndShadows = new List<Shadow>();
        foreach (GameObject game in outlinesAndShadowsObjects)
        {
            outlinesAndShadows.AddRange(game.GetComponentsInChildren<Shadow>());
        }
    }
    public IEnumerator Set(CharacterTextElement te)
    {
        c = te.character;

        if (te.stop)
            keep = false;

        if (!TextBox.CharacterBoxes.ContainsKey(c))
        {
            CharacterBoxes.Add(c, this);

            if (DialogueRenderer.Instance.IsOffScreen(c))
            {
                OffScreenInfo offData = DialogueRenderer.Instance.GetOffScreenInfo(c);
                nameText.text = offData.displayName;
            }
            else if (te.useHiddenName)
            {
                nameText.text = string.IsNullOrEmpty(te.hiddenName) ? c.hiddenDisplayName : te.hiddenName;
            }
            else
            {
                nameText.text = c.displayName;
            }

            nameText.color = c.characterColor;
            nameText.gameObject.SetActive(te.displayName || DialogueRenderer.Instance.IsOffScreen(c));

        }
        SetOutlineColor(c.characterColor);
        yield return null;

    }
    void HandleSizeAndPosition(CharacterTextElement te)
    {
        float minWidth = nameText.gameObject.activeSelf ? 190 : 90;
        float width = Mathf.Clamp(typer.textAnimator.tmproText.preferredWidth, minWidth, 250);

        top.gameObject.SetActive(width > 180);
        topOutline.gameObject.SetActive(top.gameObject.activeSelf);

        bottom.gameObject.SetActive(width > 120);
        bottomOutline.gameObject.SetActive(bottom.gameObject.activeSelf);

        (transform as RectTransform).sizeDelta = new Vector2(width, (transform as RectTransform).sizeDelta.y);

        Vector2 anchoredPos = Vector2.zero;
        if (DialogueRenderer.Instance.GetCharacterRenderer(c) != null)
        {
            float charPos = (DialogueRenderer.Instance.GetCharacterRenderer(c).transform as RectTransform).anchoredPosition.x;

            anchoredPos.x = (Mathf.Abs(charPos) + 100 + (width / 2) + UnityEngine.Random.Range(0, 40)) * Mathf.Sign(charPos);

            anchoredPos.y = UnityEngine.Random.Range(60, 90);
        }
        else
        {
            anchoredPos.x = DialogueRenderer.Instance.offScreenCharacters[c].postion.x + UnityEngine.Random.Range(0, 25);
            anchoredPos.y = DialogueRenderer.Instance.offScreenCharacters[c].postion.y;
        }


        float maxAnchorPosX = (DialogueRenderer.Instance.mainCanvas.rect.width * 0.45f) - (width / 2);
        anchoredPos.x = Mathf.Clamp(anchoredPos.x, -maxAnchorPosX, maxAnchorPosX);

        EffectsData ed = new EffectsData(anchoredPos, te.rotation, new Vector3(1, 1, 1), te.runtimeSpawnObj.cg);
        te.DoEffect(te.effect, ed, te.runtimeSpawnObj);

        rectTransform.anchoredPosition = anchoredPos;
    }
    public IEnumerator TypeQueue(CharacterTextElement te)
    {
        List<string> textQueue = new List<string>();
        textQueue = new List<string>(te.text.Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries));
        textQueue = Global10MH.ParseLines(textQueue);

        if(te.selectRandomLine)
        {
            string randomline = textQueue[UnityEngine.Random.Range(0, textQueue.Count)];
            textQueue.Clear();
            textQueue.Add(randomline);
        }
        int index = 0;
        foreach (string line in textQueue)
        {
            string textLine = line;
            DialogueRenderer.Instance.log.AddLog(c, textLine);
            float skipTime = 0.25f;

            string t = textQueue[index];

            excalmation.gameObject.SetActive(te.emotes.Contains("!") || t.Contains("!"));
            question.gameObject.SetActive(te.emotes.Contains("?") || te.text.Contains("?"));

            t = t.Replace("!", "");
            t = t.Replace("?", "");

            if (te.caps)
                t = t.ToUpper();


            typer.ShowText(t);

            yield return null;

            HandleSizeAndPosition(te);

            yield return new WaitForSeconds(0.25f);

            typer.StartShowingText(true);

            if (te.instant)
            {
                typer.SkipTypewriter();
            }

            while (typer.IsInsideRoutine)
            {
                yield return null;
                if (DialogueRenderer.SkipProceed)
                {
                    skipTime -= Time.deltaTime;
                    if (skipTime <= 0)
                        typer.SkipTypewriter();
                }
                if (DialogueRenderer.FinishType)
                    typer.SkipTypewriter();
            }
            yield return DialogueRenderer.Instance.WaitForPlayerInput();
            if (!DialogueRenderer.SkipProceed)
                onOpenSFX?.Play();
            index++;
            yield return null;
        }
    }

    public void OnProcessComplete()
    {
        if (!keep)
        {
            CharacterBoxes.Remove(c);
            transform.DOScaleY(0.15f, 0.1f)
                .OnComplete(() => { Destroy(gameObject); });
        }
    }
    private void OnDestroy()
    {
        if (c)
        {
            CharacterBoxes.Remove(c);
        }
    }
    public void Set(string text)
    {
        excalmation.gameObject.SetActive(text.Contains("!"));
        question.gameObject.SetActive(text.Contains("?"));

        text = text.Replace("!", "");
        text = text.Replace("?", "");

        typer.ShowText(text);

        onOpenSFX?.Play();
    }
    void OnCharacterPrinted(string character)
    {
        if (string.IsNullOrEmpty(character))
            return;
        if (char.IsLetterOrDigit(character[0]))
            onOpenSFX.Play();// c?.dialogueSFX?.Play();
    }

    public void SetOutlineColor(Color c)
    {
        foreach (Shadow item in outlinesAndShadows)
        {
            item.effectColor = c;
        }
    }
}
