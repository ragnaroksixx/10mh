﻿using UnityEngine;
using System.Collections;
using System;

namespace NHG.Dialogue
{
    public abstract class DialogueLogic : DialogueEvent
    {
        public abstract string logicID { get; }

        protected DialogueCondition CreateCondition(string conditionID, Tags tags)
        {
            conditionID = conditionID.ToLower();
            if (!LogicLibary.conditionTypes.ContainsKey(conditionID))
                throw new Exception("Condition [" + conditionID + "] not found");
            DialogueCondition c = Activator.CreateInstance(LogicLibary.conditionTypes[conditionID]) as DialogueCondition;
            c.tags = tags;
            return c;
        }
    }
}