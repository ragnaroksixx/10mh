﻿using System.Collections;
using UnityEngine;

namespace NHG.Dialogue
{
    public class Condition_HasMemory : DialogueCondition
    {
        public override string conditionID => "HASMEMORY";
        public string memory => tags.GetExpressString("");
        public string catergory => tags.GetString("catergory", "");
        public override bool Evaluate()
        {
            return CycleManager.Instance.HasEvent(catergory, memory);
        }
    }
}