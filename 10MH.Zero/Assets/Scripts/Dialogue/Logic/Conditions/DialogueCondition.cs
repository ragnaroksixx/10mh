﻿using System.Collections;
using UnityEngine;

namespace NHG.Dialogue
{
    public abstract class DialogueCondition
    {
        public abstract string conditionID { get; }
        public Tags tags;
        public abstract bool Evaluate();
    }
}