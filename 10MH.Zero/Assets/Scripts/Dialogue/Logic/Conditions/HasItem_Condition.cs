﻿using System.Collections;
using UnityEngine;

namespace NHG.Dialogue
{
    public class HasItem_Condition : DialogueCondition
    {
        public override string conditionID => "HASITEM";
        public Item item => tags.GetExpressItem();
        public override bool Evaluate()
        {
            return PlayerInventory.Instance.Contains(item);
        }
    }
}