﻿using UnityEngine;
using System.Collections;

namespace NHG.Dialogue
{
    public class JumpLocationAction : DialogueLogic
    {
        public override string logicID => "Jump";
        public override string JumpIndex => tags.GetString("body", "");
    }

    public class JumpToAction : DialogueLogic
    {
        public override string logicID => "JumpTo";
        public string jumpID => tags.GetExpressString(null);
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            JumpTo(jumpID);
        }
        public static void JumpTo(string id)
        {
            DialogueRenderer.SetJumpSceneIDS(id);
        }
    }

    //public class JumpIfAction : Dialogue_IF
    //{
    //    public override string logicID => "JumpIf";
    //    public string jumpID => tags.GetExpressString(null);
    //    public bool isCalamity => tags.GetBool("calamity");
    //    public TimeRef pastTime => tags.GetTime("ispast", null);
    //    public string hasMemory => tags.GetString("memory", null);
    //    public override IEnumerator Process(Layer layer)
    //    {
    //        yield return base.Process(layer);

    //        if (isCalamity && !TimeController.IsCalamity())
    //            yield break;

    //        if (pastTime != null && !TimeController.IsPast(pastTime))
    //            yield break;

    //        if (hasMemory != null && !CycleManager.Instance.HasEvent("", hasMemory))
    //            yield break;

    //        JumpToAction.JumpTo(jumpID);
    //    }
    //}
}