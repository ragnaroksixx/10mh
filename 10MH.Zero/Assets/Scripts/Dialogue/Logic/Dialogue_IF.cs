﻿using System.Collections;
using UnityEngine;

namespace NHG.Dialogue
{
    public class Dialogue_IF : DialogueLogic
    {
        public override string logicID => "IF";
        public string dialogueConditionName => tags.GetString("body", null);

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            DialogueCondition conditional = CreateCondition(dialogueConditionName, tags);
            if (conditional.Evaluate() == false)
            {
                DialogueRenderer.SetJumpSceneIDS("else", "elseif", "endif");
            }

        }
    }
    public class Dialogue_ELSEIF : Dialogue_IF
    {
        public override string logicID => "ELSEIF";
        public override string JumpIndex => "elseif";
    }
    public class Dialogue_ELSE : DialogueLogic
    {
        public override string logicID => "ELSE";
        public override string JumpIndex => "else";
    }
    public class Dialogue_ENDIF : DialogueLogic
    {
        public override string logicID => "ENDIF";
        public override string JumpIndex => "endif";
    }
}