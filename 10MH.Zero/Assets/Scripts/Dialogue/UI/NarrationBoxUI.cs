﻿using UnityEngine;
using System.Collections;
using TMPro;
using NHG.Dialogue;
using System;

public class NarrationBoxUI : MonoBehaviour
{
    public RectTransform outlineRect;
    public TMP_Text text;

    public void Set(NarrationText nText)
    {
        (transform as RectTransform).anchoredPosition = nText.postion;
        (transform as RectTransform).sizeDelta = nText.size;
    }
}
