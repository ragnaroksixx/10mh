﻿using DG.Tweening;
using NHG.Dialogue;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(TextMeshProUGUI))]
public class LinkProcessor : MonoBehaviour, IPointerClickHandler
{
    TextMeshProUGUI text = null;
    TextMeshProUGUI pTextMeshPro
    {
        get
        {
            if (text == null)
                text = GetComponent<TextMeshProUGUI>();
            return text;
        }
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        if (!Cursor.visible) return;
        if (eventData.button != PointerEventData.InputButton.Right) return;
        int linkIndex = TMP_TextUtilities.FindIntersectingLink(pTextMeshPro, Input.mousePosition, null);
        if (linkIndex != -1)
        { // was a link clicked?
            CursorController.Instance.rippleEffect.Ripple(CursorController.CursorPosition,
                Color.yellow);
            TMP_LinkInfo linkInfo = pTextMeshPro.textInfo.linkInfo[linkIndex];
            //int id = Convert.ToInt32(linkInfo.GetLinkID());
            Debug.Log("Selected Link " + linkInfo.GetLinkID());
            //string e = events[id];
            pTextMeshPro.textInfo.textComponent.text = "";
            DialogueRenderer.ProcessSelection(linkInfo.GetLinkID());
            InputSystem10MH.proceedDialogue.Enable(this);
        }
    }
    string hoverText = null;
    private void Update()
    {
        if (!Cursor.visible) return;
        int linkIndex = TMP_TextUtilities.FindIntersectingLink(pTextMeshPro, Input.mousePosition, null);
        if (linkIndex != -1)
        {
            TMP_LinkInfo linkInfo = pTextMeshPro.textInfo.linkInfo[linkIndex];
            if (linkInfo.GetLinkText() != hoverText)
            {
                Debug.Log("Enter Link " + linkInfo.GetLinkID());
                HighlightText(linkInfo.GetLinkText());
                Publisher.Raise(new NotificationEvent("???"));
                hoverText = linkInfo.GetLinkText();
            }
        }
        else
        {
            if (hoverText != null)
            {
                RemoveHighlights();
                Publisher.Raise(new ClearNotificationEvent());
            }
            hoverText = null;
        }
    }

    void HighlightText(string toHighlight)
    {
        //string startTag = "<font=CaviarDreams SDF HIGHLIGHT>";// "<mark=#F3E4970a>";
        //string endTag = "</font>";// "</mark>";
        string startTag = "<u>";
        string endTag = "</u>";
        RemoveHighlights();
        text.text = text.text.Replace(toHighlight, startTag + toHighlight + endTag);
        InputSystem10MH.proceedDialogue.Disable(this);
    }
    void RemoveHighlights()
    {
        //string startTag = "<font=CaviarDreams SDF HIGHLIGHT>";// "<mark=#F3E4970a>";
        //string endTag = "</font>";// "</mark>";
        string startTag = "<u>";
        string endTag = "</u>";

        text.text = text.text.Replace(startTag, "");
        text.text = text.text.Replace(endTag, "");
        InputSystem10MH.proceedDialogue.Enable(this);
    }
    private void OnDestroy()
    {
        InputSystem10MH.proceedDialogue.Enable(this);
    }
}