﻿using DG.Tweening;
using NHG.Dialogue;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogueCharacterRenderer : MonoBehaviour
{
    public Image characterImage;
    public Image outline;
    public CharacterDropArea characterDropArea;
    public CanvasGroup cg;
    CharacterData character;
    public float baseScale = 1.25f;
    public CharacterData CharacterData { get => character; }

    public void Init()
    {
        Color color = characterImage.color;
        color.a = 0;
        characterImage.color = color;
        outline.color = color;

        outline.rectTransform.DOAnchorPosX(0, 0);
        outline.raycastTarget = false;
        characterImage.raycastTarget = false;

    }
    public IEnumerator OnSetActive()
    {

        yield return null;

        outline.DOKill();
        outline.rectTransform.DOAnchorPosX(0, 0);
        outline.enabled = true;
        outline.rectTransform.DOAnchorPosX(5, 0.15f);

        //SetFocus(CharacterEffectData.FocusLevel.FOCUS, false);
    }
    public void OnClearActive()
    {
        outline.DOKill();
        outline.rectTransform.DOAnchorPosX(0, 0.15f)
            .OnComplete(() => { outline.enabled = false; });

        //SetFocus(CharacterEffectData.FocusLevel.DEFAULT, false);

    }
    public void SetSprite(CharacterEffectData data)
    {
        SetSprite(character.GetImage(data.sprite));
        if (data.changeSpriteInWorld)
            Character.FindCharacter(character).SetSprite(characterImage.sprite);
    }
    public void SetSprite(Sprite s)
    {
        characterImage.sprite = s;
        outline.sprite = characterImage.sprite;
    }
    public IEnumerator SetSpriteAndWait(CharacterEffectData data)
    {
        characterImage.DOFade(0, 0.25f);
        yield return new WaitForSeconds(0.25f);
        SetSprite(data);
        characterImage.DOFade(1, 0.25f);
        yield return new WaitForSeconds(0.25f);
    }
    public IEnumerator SetSpriteAndWait(Sprite sprite)
    {
        characterImage.DOFade(0, 0.25f);
        yield return new WaitForSeconds(0.25f);
        SetSprite(sprite);
        characterImage.DOFade(1, 0.25f);
        yield return new WaitForSeconds(0.25f);
    }
    public void EnterCharacter(CharacterEnterData data)
    {
        character = data.character;

        if (Character.IsCharacterInArea(character))
            character.GetCharacter().OnDialogueEnter();

        characterDropArea.SetCharacter(character);
        outline.enabled = false;

        Color outlineColor = character.characterColor;
        outlineColor.a = 1;
        outline.color = outlineColor;
        float duration = data.transitionTime;

        SetSprite(data);
        Color color = characterImage.color;
        color.a = 1;
        characterImage.color = color;

        transform.SetAsLastSibling();
        float y = 0;

        SetFocus(data.focusLevel, true);
        if (data.focusLevel == CharacterEffectData.FocusLevel.FOCUS)
            y = -110;

        switch (data.animType)
        {
            case CharacterEnterData.EnterAnimationType.DEFAULT:
            case CharacterEnterData.EnterAnimationType.SCALE:
                (transform as RectTransform).anchoredPosition = new Vector2(data.xPos, y);
                outline.rectTransform.DOAnchorPosX(0, 0);

                Vector3 startScale = transform.localScale;
                startScale.y = 0;
                transform.localScale = startScale;
                //outline.transform.localScale = startScale;

                transform.DOScaleY(baseScale, duration).SetEase(Ease.OutBounce);
                //outline.transform.DOScaleY(baseScale, duration).SetEase(Ease.OutBounce);
                break;
            case CharacterEnterData.EnterAnimationType.RIGHT:
                (transform as RectTransform).anchoredPosition = new Vector2(800, y);
                outline.rectTransform.DOAnchorPosX(0, 0);
                MoveCharacter(data);
                break;
            case CharacterEnterData.EnterAnimationType.LEFT:
                (transform as RectTransform).anchoredPosition = new Vector2(-800, y);
                outline.rectTransform.DOAnchorPosX(0, 0);
                MoveCharacter(data);
                break;
            case CharacterEnterData.EnterAnimationType.FADE:
                Color startColor = Color.white;
                startColor.a = 0;
                characterImage.color = startColor;
                outline.color = startColor;

                (transform as RectTransform).anchoredPosition = new Vector2(data.xPos, y);
                outline.rectTransform.DOAnchorPosX(0, 0);

                characterImage.DOFade(1, data.transitionTime);
                outline.DOFade(1, data.transitionTime);

                break;
            case CharacterEnterData.EnterAnimationType.HIDDEN:
                Vector3 ss = characterImage.transform.localScale;
                startScale.y = 0;
                characterImage.transform.localScale = ss;
                outline.transform.localScale = ss;
                break;
            default:
                break;
        }

        characterImage.raycastTarget = true;
    }
    public IEnumerator EnterCharacterAndWait(CharacterEnterData data)
    {
        EnterCharacter(data);
        yield return new WaitForSeconds(data.transitionTime);
    }
    public IEnumerator MoveCharacterAndWait(CharacterEffectData data)
    {
        MoveCharacter(data);
        yield return new WaitForSeconds(data.transitionTime);
    }
    public void MoveCharacter(CharacterEffectData data)
    {
        (transform as RectTransform).DOAnchorPosX(data.xPos, data.transitionTime);
    }
    public IEnumerator ExitCharacterAndWait()
    {
        ExitCharacter();
        yield return new WaitForSeconds(0.5f);
    }
    public void ExitCharacter()
    {
        if (Character.IsCharacterInArea(character))
            character.GetCharacter().OnDialogueExit();
        float duration = 0.5f;
        characterImage.DOFade(0, duration);
        outline.DOFade(0, duration);
        character = null;
        characterImage.raycastTarget = false;

        Destroy(this.gameObject, duration * 1.25f);
    }
    public void SetCharacterItemEvents(List<ItemEvents> itemEvents)
    {
        if (itemEvents == null || itemEvents.Count == 0)
        {
            characterDropArea.ClearEvents();
        }
        else
        {
            characterDropArea.SetEvents(itemEvents);
        }
    }

    public void SetFocus(CharacterEffectData.FocusLevel ft, bool instant)
    {
        switch (ft)
        {
            case CharacterEffectData.FocusLevel.DEFAULT:
                if (instant)
                    transform.localScale = Vector3.one * baseScale;
                else
                    transform.DOScale(baseScale, 0.2f).SetEase(Ease.OutBounce);
                break;
            case CharacterEffectData.FocusLevel.FOCUS:
                if (instant)
                    transform.localScale = Vector3.one * 1.1f * baseScale;
                else
                    transform.DOScale(1.1f * baseScale, 0.2f).SetEase(Ease.OutBounce);
                break;
            default:
                break;
        }
    }
}