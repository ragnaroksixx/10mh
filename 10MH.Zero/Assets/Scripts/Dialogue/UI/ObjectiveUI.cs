﻿using UnityEngine;
using System.Collections;
using TMPro;
using NHG.Dialogue;
using DG.Tweening;
using UnityEngine.Playables;
using System;

public class ObjectiveUI : MonoBehaviour
{
    public TMP_Text headerText, objText;
    public OnScreenAnimatorBASE anim;
    public CanvasGroup cg;
    public CustomAudioClip sfx;
    public SubObjective[] subObjectives;
    public 
    // Use this for initialization
    void Start()
    {
        Publisher.Subscribe<ObjectiveEvent>(DoTheThing);
        gameObject.SetActive(false);
    }

    private void OnInventoryOpenClose(InventoryOpenCloseEvent e)
    {
        cg.alpha = e.isOpen ? 0 : 1;
    }

    private void OnDestroy()
    {
        Publisher.Unsubscribe<ObjectiveEvent>(DoTheThing);
    }
    void DoTheThing(ObjectiveEvent e)
    {
        ObjectiveObject obj = e.objective;
        //objText.text = "";
        objText.text = obj.title;

        foreach (SubObjective subObjective in subObjectives)
            subObjective.Hide();

        for (int i = 0; i < obj.objectives.Length; i++)
        {
            string text = obj.objectives[i].isHidden ? "???" : obj.objectives[i].text;
            subObjectives[i].Set(text);
            subObjectives[i].Show();
        }

        gameObject.SetActive(true);
        StartCoroutine(TheThing());
    }
    IEnumerator TheThing()
    {
        Publisher.Subscribe<InventoryOpenCloseEvent>(OnInventoryOpenClose);
        yield return null;

        yield return new WaitForSeconds(0.15f);

        anim.AnimateOnScreen();
        sfx.Play();

        yield return new WaitForSeconds(4);

        bool areSubComplete = false;
        while (!areSubComplete)
        {
            foreach (SubObjective subObjective in subObjectives)
            {
                areSubComplete = subObjective.IsComplete();
                if (!areSubComplete)
                    break;
            }
            yield return null;
        }

        yield return new WaitForSeconds(4);

        anim.AnimateOffScreen();

        yield return new WaitForSeconds(0.5f);

        CursorController.ShowCursor(this);

        foreach (SubObjective subObjective in subObjectives)
            subObjective.Hide();

        Publisher.Unsubscribe<InventoryOpenCloseEvent>(OnInventoryOpenClose);
        gameObject.SetActive(false);
    }
    public void CompleteObjective(int i, Objective obj)
    {
        subObjectives[i].Set(obj.text);
        subObjectives[i].Complete();
    }
    public void UncompleteObjective(int i ,Objective obj)
    {
        subObjectives[i].UnComplete(obj.text);
    }
    public void ShowObjective(int i, Objective obj)
    {
        subObjectives[i].Set(obj.text);
    }
}

public class ObjectiveEvent : PublisherEvent
{
    public ObjectiveObject objective;

    public ObjectiveEvent(ObjectiveObject objective)
    {
        this.objective = objective;
    }
}
public class ObjectivePopUp :MonoBehaviour
{
    public TMP_Text header, body;

    public void Show()
    {

    }
    public void Hide()
    {

    }
}

public class ObjectiveAction : DialogueAction
{
    public override string eventName => "objective";
    public string id => tags.GetExpressString(null);
    public override IEnumerator Process(Layer layer)
    {
        yield return base.Process(layer);
        Publisher.Raise(new ObjectiveEvent(GlobalData10MH.Instance.objectives.GetItem(id)));
    }
}


