﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PlayerEmoteUI : MonoBehaviour
{
    public Image bubbleImage, buubleImageOutline, emote1, emote2;

    public Sprite normalBubble, spikyBubble;
    public Sprite exclamationMark, questionMark;

    public OnScreenAnimatorBASE anim;
    CoroutineHandler emoteRoutine;
    public AudioVariable sfx;
    public enum EmoteType
    {
        NONE,
        EXCLAIM,
        EXCLAIM_EXCLAIM,
        QUESTION,
        EXCLAIM_QUESTION
    }
    // Use this for initialization
    void Start()
    {
        anim.AnimateOffScreen(null, true);
        emoteRoutine = new CoroutineHandler(this);
    }
    private void Update()
    {
    }
    public void Show(EmoteType e, float duration)
    {
        switch (e)
        {
            case EmoteType.NONE:
                break;
            case EmoteType.EXCLAIM:
                bubbleImage.sprite = spikyBubble;
                emote1.sprite = exclamationMark;
                emote2.sprite = null;
                break;
            case EmoteType.EXCLAIM_EXCLAIM:
                bubbleImage.sprite = spikyBubble;
                emote1.sprite = exclamationMark;
                emote2.sprite = exclamationMark;
                break;
            case EmoteType.QUESTION:
                bubbleImage.sprite = normalBubble;
                emote1.sprite = questionMark;
                emote2.sprite = null;
                break;
            case EmoteType.EXCLAIM_QUESTION:
                bubbleImage.sprite = normalBubble;
                emote1.sprite = exclamationMark;
                emote2.sprite = questionMark;
                break;
            default:
                break;
        }
        buubleImageOutline.sprite = bubbleImage.sprite;
        anim.AnimateOnScreen();
        transform.DOKill(true);
        (transform as RectTransform).DOPunchAnchorPos((transform as RectTransform).up * 20, 0.5f, 10, 0);
        if (duration > 0)
        {
            emoteRoutine.StartCoroutine(HideIn(duration));
        }
        else
        {
            emoteRoutine.StopCoroutine();
        }
        sfx.Play();
    }
    public IEnumerator ShowFor(EmoteType e, float duration)
    {
        Show(e, duration);
        yield return new WaitForSeconds(duration);
    }
    public IEnumerator HideIn(float x)
    {
        yield return new WaitForSeconds(x);
        Hide();
    }
    public void Hide()
    {
        anim.AnimateOffScreen();
    }
}