﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using Sirenix.OdinInspector;

public class PlayerInfoMenu : MonoBehaviour
{
    public static PlayerInfoMenu Instance;
    public TMP_Text actText;
    public TMP_Text healthText;
    public Image healthBar;
    public Image playerImage;
    public CanvasGroup playerCG;
    public Image playerBox;
    public GameObject statusImagesRoot;
    Image[] statusImages;

    public TextBox normanSpeechBox;
    public CanvasGroup apGroup;
    public PlayerEmoteUI emoteUI;
    CoroutineHandler normanSpeechRoutine;
    public GameObject blockUI;
    public TMP_Text blockText;

    private void Start()
    {
        normanSpeechRoutine = new CoroutineHandler(this);
        statusImages = statusImagesRoot.GetComponentsInChildren<Image>();
        normanSpeechBox.gameObject.SetActive(false);
        statusImages.ForEach(i => i.gameObject.SetActive(false));
        Publisher.Subscribe<InventoryOpenCloseEvent>(OnInventoryToggle);
        UpdateBlockUI(0);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<InventoryOpenCloseEvent>(OnInventoryToggle);
    }
    private void Awake()
    {
        Instance = this;
    }
    public void OnInventoryToggle(InventoryOpenCloseEvent e)
    {
        if (!e.isOpen) return;
        healthText.text = CycleManager.PlayerCombatUnit.hp.ToString();// + "/" + CycleManager.Player.maxHp.ToString();
        healthBar.fillAmount = CycleManager.PlayerCombatUnit.HPRatio();
        List<CombatEffect> keys = new List<CombatEffect>(CycleManager.PlayerCombatUnit.statusFXs.Keys);
        for (int i = 0; i < statusImages.Length; i++)
        {
            if (i >= keys.Count)
            {
                statusImages[i].gameObject.SetActive(false);
            }
            else
            {
                statusImages[i].sprite = keys[i].image;
                statusImages[i].gameObject.SetActive(true);
            }
        }
    }
    public void UpdateBlockUI(int block)
    {
        blockText.text = block.ToString();
        blockUI.gameObject.SetActive(block > 0);
    }
    public void ActButton()
    {
        // BattleController.Instance?.ActButton();
    }
    public void CancelButton()
    {
        // BattleController.Instance?.CancelButton();
    }

    public Coroutine Say(string text)
    {
        return normanSpeechRoutine.StartCoroutine(SayRoutine(text), OnInterrupt);
    }

    public IEnumerator SayRoutine(string text)
    {
        normanSpeechBox.gameObject.SetActive(true);
        normanSpeechBox.Set(text);
        yield return new WaitUntil(() => !normanSpeechBox.typer.IsInsideRoutine);
        yield return new WaitForSeconds(0.5f);
        normanSpeechBox.gameObject.SetActive(false);
    }
    public void OnInterrupt()
    {
        normanSpeechBox.typer.SkipTypewriter();
        normanSpeechBox.gameObject.SetActive(false);
    }
    public void ChangeSprite(string sprite)
    {
        playerImage.sprite = Player.Instance.characterData.GetImage(sprite);
    }

    public void ChangeSprite(CharacterEffectData eData)
    {
        playerImage.sprite = Player.Instance.characterData.GetImage(eData.sprite);
    }

    public IEnumerator ChangeSpriteAndWait(CharacterEffectData data)
    {
        playerImage.DOFade(0, 0.25f);
        yield return new WaitForSeconds(0.25f);
        ChangeSprite(data);
        playerImage.DOFade(1, 0.25f);
        yield return new WaitForSeconds(0.25f);
    }


}

