﻿using UnityEngine;
using System.Collections;
using TMPro;
using NHG.Dialogue;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine.UI;

public class TutorialUI : MonoBehaviour
{
    public FullScreenTutorialUI fullScreenUI;
    public MiniTutorialUI miniUI;

    public List<string> texts = new List<string>();
    public CanvasGroup proceedGroup;

    public static bool eventWait;
    // Use this for initialization
    void Start()
    {
        fullScreenUI.anim.AnimateOffScreen(instant: true);
        miniUI.anim.AnimateOffScreen(instant: true);
        fullScreenUI.cg.alpha = 0;
        miniUI.cg.alpha = 0;
        Publisher.Subscribe<TutorialEvent>(DoTheThing);
        Publisher.Subscribe<TutorialProceedEvent>(OnTutorialTrigger);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<TutorialEvent>(DoTheThing);
        Publisher.Unsubscribe<TutorialProceedEvent>(OnTutorialTrigger);
    }
    void DoTheThing(TutorialEvent e)
    {
        TutorialUIStuff ui = null;
        if (e.isFullScreen)
            ui = fullScreenUI;
        else
            ui = miniUI;

        ui.headerText.text = e.title;
        texts = new List<string>(e.texts);

        SetTopText(ui);

        StartCoroutine(TheThing(e, ui));
    }
    void SetTopText(TutorialUIStuff ui)
    {
        string t = texts[0];
        ui.objText.text = t;
        texts.RemoveAt(0);
    }
    IEnumerator TheThing(TutorialEvent e, TutorialUIStuff ui)
    {
        yield return null;
        bool lockPlayer = e.isPlayerInScene && e.isFullScreen;
        if (lockPlayer)
        {
            TimeController.Instance.IsPaused.AddLock(this);
            CursorController.HideCursor(this);
            Global10MH.ShowCursor(this);
            CameraController10MH.RemoveUserInput(this);
            InteractionHandler.LockInteraction(this);
            WalkController10MH.RemoveUserInput(this);
            InventoryManager.LockInput(this);
        }
        ui.cg.DOKill();
        ui.cg.DOFade(1, 0.35f);

        yield return new WaitForSeconds(0.15f);

        ui.anim.AnimateOnScreen();

        yield return new WaitForSeconds(ui.anim.transitionSpeed);

        yield return Wait(e);

        while (texts.Count > 0)
        {
            SetTopText(ui);
            yield return Wait(e);
        }

        ui.anim.AnimateOffScreen();
        ui.cg.DOFade(0, 1);

        if (lockPlayer)
        {
            Global10MH.HideCursor(this);
            CameraController10MH.RestoreUserInput(this);
            WalkController10MH.RestoreUserInput(this);
            TimeController.Instance.IsPaused.RemoveLock(this);
            InteractionHandler.RemoveInteractionLock(this);
            InventoryManager.UnlockInput(this);
        }
        yield return new WaitForSeconds(0.5f);

        ui.cg.alpha = 0;
        if (lockPlayer)
            CursorController.ShowCursor(this);

        TutorialAction.waitforTutorialUI = false;
        Publisher.Raise(new TutorialEndEvent());
    }


    IEnumerator Wait(TutorialEvent e)
    {
        if (e.isFullScreen)
        {
            proceedGroup.DoFadeFlash(0, 1, 0.1f, 0.75f, -1);

            while (!Input.GetKeyDown(KeyCode.Space))
                yield return null;

            proceedGroup.DOKill();
            proceedGroup.alpha = 0;

            yield return null;
        }
        else if (e.time > 0)
        {
            yield return new WaitForSeconds(e.time);
        }
        else
        {
            eventWait = true;
            while (eventWait)
            {
                yield return null;
            }
        }
    }
    void OnTutorialTrigger(TutorialProceedEvent e)
    {
        eventWait = false;
    }
}

[System.Serializable]
public class FullScreenTutorialUI : TutorialUIStuff
{
}

[System.Serializable]
public class MiniTutorialUI : TutorialUIStuff
{

}

[System.Serializable]
public class TutorialUIStuff
{
    public TMP_Text headerText, objText;
    public OnScreenAnimatorBASE anim;
    public CanvasGroup cg;
}

public class TutorialEvent : PublisherEvent
{
    public string title;
    public string[] texts;
    public bool isPlayerInScene;
    public bool isFullScreen;
    public float time;
    public Sprite sprite;
    public TutorialEvent(string title, bool isPlayerInScene, bool isFullScreen, string imageName, float time, params string[] texts)
    {
        this.title = title;
        this.texts = texts;
        this.time = time;
        this.isFullScreen = isFullScreen;
        this.sprite = string.IsNullOrEmpty(imageName) ? null : Resources.Load<Sprite>("TutorialImages/" + imageName);
        this.isPlayerInScene = isPlayerInScene;
    }
}
public class TutorialEndEvent : PublisherEvent
{
}
public class TutorialProceedEvent : PublisherEvent
{
}
public class TutorialAction : DialogueAction
{
    public static bool waitforTutorialUI = false;
    public override string eventName => "tutorial";
    public string text => tags.GetString("text", "");
    public bool isPlayerInScene => !tags.GetBool("noplayer");
    public bool fullScreen => tags.GetBool("fullscreen");
    public bool wait => tags.GetBool("wait");
    public float time => tags.GetFloat("time", -1);
    public string imageName => tags.GetString("image", "");
    public override IEnumerator Process(Layer layer)
    {
        waitforTutorialUI = wait || fullScreen;
        yield return base.Process(layer);
        List<string> texts = new List<string>();
        texts.Add(Global10MH.ParseLine(text));
        for (int i = 1; i < Mathf.Infinity; i++)
        {
            string tag = "text" + i;
            if (tags.HasTag(tag))
            {
                texts.Add(Global10MH.ParseLine(tags.GetString(tag, null)));
            }
            else
            {
                break;
            }
        }
        Publisher.Raise(new TutorialEvent(ExpressTag, isPlayerInScene, fullScreen, imageName, time, texts.ToArray()));
        while (waitforTutorialUI)
        {
            yield return null;
        }
    }
}

public class TutrialProceedAction : DialogueAction
{
    public override string eventName => "tutorial next";

    public override IEnumerator Process(Layer layer)
    {
        yield return base.Process(layer);
        Publisher.Raise(new TutorialProceedEvent());
    }
}

public class RegisterCombatTutorial : DialogueAction
{
    public override string eventName => "combat tutorial";
    public override string[] eventAlias => new string[1] { "battle tutorial" };

    public string type => tags.GetExpressString("").ToLower();
    public override IEnumerator Process(Layer layer)
    {
        yield return base.Process(layer);
        switch (type)
        {
            case "basic":
            case "basics":
                new TBCTutorialBasics().RegisterTutorial(BattleController.Instance);
                break;
            case "defense":
                new TBCTutorialDefense().RegisterTutorial(BattleController.Instance);
                break;
            default:
                Debug.LogError("Tutorial: " + type + " not found");
                break;
        }
    }
}


