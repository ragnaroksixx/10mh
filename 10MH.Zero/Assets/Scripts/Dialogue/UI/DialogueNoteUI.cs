﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogueNoteUI : MonoBehaviour
{
    public OnScreenAnimatorBASE anim;
    public TMP_Text text;
    public Image image;
    private void Start()
    {
        text.text = "";
        anim.AnimateOffScreen(null, true);
    }

    public void ShowItem(Item i,string t)
    {
        image.sprite = i.image;
        text.text = t;
        anim.AnimateOnScreen();
    }

    public void Hide()
    {
        text.text = "";
        anim.AnimateOffScreen();
    }
}
