﻿using System.Collections;
using TMPro;
using UnityEngine;


public class SubObjective : MonoBehaviour
{
    public TMP_Text text;
    public CanvasGroup cg;
    bool isChecked;

    public bool IsComplete()
    {
        return !gameObject.activeInHierarchy || isChecked;
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
    public void Show()
    {
        gameObject.SetActive(true);
    }
    private void OnEnable()
    {
        //checkedObj.SetActive(false);
        isChecked = false;
        cg.alpha = 1.0f;
    }
    public void Set(string t)
    {
        text.text = t;
    }

    public void Complete()
    {
        text.text = "<s>" + text.text;
        isChecked = true;
        cg.alpha = 0.25f;
    }
    public void UnComplete(string t)
    {
        text.text = t;
        isChecked = false;
        cg.alpha = 1.0f;
    }
}
