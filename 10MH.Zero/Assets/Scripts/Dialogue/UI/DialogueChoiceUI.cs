﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class DialogueChoiceUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public TMP_Text text;
    public Button button;
    public Outline outline;
    public Image outline2;
    Color accentColor;
    public void Init(CharacterData c)
    {
        accentColor = c.characterColor;
        Unhighlight();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!button.interactable) return;
        transform.localScale = Vector3.one * 1.025f;
        text.color = accentColor;
        text.fontSize = 26;
        outline.effectColor = accentColor;
        outline2.color = accentColor;
        outline.effectDistance = new Vector2(2.25f, 2);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!button.interactable) return;
        Unhighlight();
    }
    void Unhighlight()
    {
        transform.localScale = Vector3.one;
        text.color = Color.white;
        text.fontSize = 22;
        outline.effectColor = Color.white;
        outline2.color = Color.white;
        outline.effectDistance = new Vector2(1.25f, 1);
    }
}
