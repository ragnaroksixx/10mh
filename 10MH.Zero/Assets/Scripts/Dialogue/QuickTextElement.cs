﻿using UnityEngine;
using System.Collections;
using TMPro;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using RedBlueGames.Tools.TextTyper;
using Febucci.UI;
namespace NHG.Dialogue
{
    /*public class QuickTextElement : WorldElement
    {
        public bool waitForInput = true;
        public string text => tags.GetString("text", null);
        public TextStyle style => tags.GetTextStyle();

        public override string eventName => "QText";

        //public string[] linkEvents;
        //public List<ItemEvents> itemEvents;


        public override IEnumerator Process(Layer layer)
        {
            GameObject prefab = GlobalData10MH.Instance.textBoxPrefab;
            layer = NewLayerAction.ProcessNewLayerUp(layer);
            yield return base.Process(layer);
            Transform obj = GameObject.Instantiate(prefab).transform;
            TextAnimatorPlayer typer = obj.GetComponentInChildren<TextAnimatorPlayer>();

            obj.SetParent(layer.transform);


            style.ApplyStyle(typer.textAnimator.tmproText);
            string t = Global10MH.ParseLine(text);
            EffectsData ed = new EffectsData(position, rotation, new Vector3(1, 1, 1), typer.GetComponentInParent<CanvasGroup>());
            typer.StartCoroutine(new NoEffect().Play(ed));
            typer.ShowText(t);
            float skipTime = 0.25f;
            while (typer.IsInsideRoutine)
            {
                yield return null;
                if (DialogueRenderer.SkipProceed)
                {
                    skipTime -= Time.deltaTime;
                    if (skipTime <= 0)
                        typer.SkipTypewriter();
                }
                else if (DialogueRenderer.FinishType)
                    typer.SkipTypewriter();
            }
            if (waitForInput)
                yield return WaitForInputAction.ProcessWaitForInput(layer);

        }

    }*/
}
