﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using NHG.Dialogue;

public class SceneTrigger : MonoBehaviour
{
    public string trigger;
    public UnityEvent onTrigger;
    private void OnEnable()
    {
        Publisher.Subscribe<SceneTriggerEvent>(Trigger);
    }
    private void OnDisable()
    {
        Publisher.Unsubscribe<SceneTriggerEvent>(Trigger);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<SceneTriggerEvent>(Trigger);
    }
    protected virtual void Trigger(SceneTriggerEvent e)
    {
        if (e.action != trigger.ToLower().Trim())
            return;
        onTrigger.Invoke();
    }
    public void MovePlayer()
    {
        SceneTransitionEffect.waitTime = 0.15f;
        SceneController.LoadAreaTransition();
        WalkController10MH.Instance.transform.position = transform.position;
    }
}

public class SceneTriggerEvent : PublisherEvent
{
    public string action;

    public SceneTriggerEvent(string action)
    {
        this.action = action;
    }
}