﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

namespace NHG.Dialogue
{
    // [CreateAssetMenu(fileName = "New Dialogue Element", menuName = "Dialogue")]
    public abstract class DialogueEvent
    {
        public float startDelay => tags.GetFloat("start", 0);
        public float endDelay { get => tags.GetFloat("end", 0); set => tags.AddTag("end", value.ToString()); }
        public CustomAudioClip audio => tags.GetAudio();
        bool renderOnNewLayer => tags.GetBool("newlayer", false);
        protected Tags tags;

        public Tags Tags { get => tags; }
        public virtual string JumpIndex { get => tags.GetString("jump", ""); }

        public Scene CurrentScene => DialogueRenderer.ActiveScene;
        public virtual IEnumerator Process(Layer layer)
        {
            if (!DialogueRenderer.SkipProceed)
                yield return new WaitForSeconds(startDelay);
            if (renderOnNewLayer)
                layer = NewLayerAction.ProcessNewLayer(layer);
            audio?.Play();
            yield return null;
        }

        public void Init(Tags tags)
        {
            OnInit(tags);
        }
        public void OnInit(Tags t)
        {
            tags = t;
        }
    }

}
