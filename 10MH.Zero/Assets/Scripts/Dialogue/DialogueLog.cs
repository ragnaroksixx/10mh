﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DialogueLog : MonoBehaviour
{
    public OnScreenAnimatorBASE animator;
    public TMP_Text text;
    CharacterData lastCharacter;
    public CharacterData narrator;
    public ScrollRect scrollView;
    public bool IsOpen => animator.IsOnScreen;

    public void ToggleLog()
    {
        if (animator.IsOnScreen)
            CloseLog();
        else
            OpenLog();
    }
    public void OpenLog()
    {
        animator.AnimateOnScreen();
        scrollView.DOVerticalNormalizedPos(0, 0.5f);
    }

    public void CloseLog()
    {
        animator.AnimateOffScreen();
        scrollView.DOKill();
    }

    public void ClearLog()
    {
        text.text = "";
        lastCharacter = null;
    }

    public void AddLog(CharacterData c, string text)
    {
        if (c == narrator)
        {
            text = text.Colorize(narrator.characterColor);
        }

        if (lastCharacter != c)
        {
            if (lastCharacter != null)
            {
                AddNewLine();
                AddNewLine();
            }
            if (c != narrator)
            {
                AddLine(c.displayName.ToUpper().Colorize(c.characterColor).Bold());
                AddNewLine();
            }
        }
        AddLine(text + " ");
        lastCharacter = c;
    }


    void AddLine(string t)
    {
        text.text = text.text + t;
    }
    void AddNewLine()
    {
        text.text = text.text + "\n";
    }
}
