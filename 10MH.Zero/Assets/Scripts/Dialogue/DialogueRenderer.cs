﻿using DG.Tweening;
using NHG.Dialogue;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogueRenderer : MonoBehaviour
{
    public static DialogueRenderer Instance;
    public RectTransform mainCanvas;
    static bool skip = false;
    static bool auto = false;
    const float AUTO_WAIT_TIME = 1.5f;
    CoroutineHandler playCoroutine;

    //public CinemaBars cinemaBars;

    float transitionDuration = 0.25f;

    public Image backgroundImage;
    public CanvasGroup proceedButton;
    public Image proceedImage_1;//, proceedImage_2;
    //Color defaultProceedColor;

    public PlayerResponeMenu responseMenu;
    public ScreenDialogueMenu screenDialogueMenu;

    public TMP_Text activeCharacterName;

    DialogueCharacterRenderer activeCharacter;
    private Dictionary<CharacterData, DialogueCharacterRenderer> characters;
    public Dictionary<CharacterData, OffScreenInfo> offScreenCharacters;
    public GameObject defaultCharacterRenderer;
    public Transform characterRoot;

    public GameObject debugGridLayout;
    public Image lightsUI;
    public CustomAudioClip proceedSFX;
    public bool isInDialogue = false;
    public OnScreenAnimatorBASE dialogueCommandsAnim;

    public Toggle skipToogle, autoToogle;
    public Button logButton;

    public DialogueLog log;
    public DialogueNoteUI noteUI;
    private void Awake()
    {
        //defaultProceedColor = proceedImage_1.color;
        Instance = this;
        playCoroutine = new CoroutineHandler(this);
        offScreenCharacters = new Dictionary<CharacterData, OffScreenInfo>();
        characters = new Dictionary<CharacterData, DialogueCharacterRenderer>();
        activeCharacterName.text = "";
    }
    private void Start()
    {
        dialogueCommandsAnim.AnimateOffScreen();
        proceedButton.alpha = 0;
        Publisher.Subscribe<InventoryOpenCloseEvent>(OnInventoryOpenClose);
        skipToogle.onValueChanged.AddListener(Skip);
        autoToogle.onValueChanged.AddListener(Auto);
        logButton.onClick.AddListener(Log);
        skipToogle.isOn = skip;
        autoToogle.isOn = auto;
        //mainCanvas.root.gameObject.SetActive(false);
    }

    private void OnInventoryOpenClose(InventoryOpenCloseEvent e)
    {
        if (isInDialogue && !BattleController.IsInBattle)
        {
            if (e.isOpen)
                dialogueCommandsAnim.AnimateOffScreen();
            else
                dialogueCommandsAnim.AnimateOnScreen();
        }
    }

    private void OnDestroy()
    {
        Publisher.Unsubscribe<InventoryOpenCloseEvent>(OnInventoryOpenClose);
    }
    public Coroutine PlayScene(DialogueVariable scene, Action onSceneComplete)
    {
        Scene s = DialogueUtils.GetSceneFrom(scene);
        return PlayScene(s, onSceneComplete);
    }
    public Coroutine PlayScene(string fileName, Action onSceneComplete)
    {
        Scene s = DialogueUtils.GetSceneFromFile(fileName);
        return PlayScene(s, onSceneComplete);
    }
    public Coroutine PlayScene(TextAsset file, Action onSceneComplete)
    {
        Scene s = DialogueUtils.GetSceneFromTextAsset(file);
        return PlayScene(s, onSceneComplete);
    }
    public Coroutine PlaySceneInMap(TextAsset file, Action onSceneComplete)
    {
        Scene s = DialogueUtils.GetSceneFromTextAsset(file);
        return PlaySceneInMap(s, onSceneComplete);
    }
    public Coroutine PlaySceneInMap(Scene s, Action onSceneComplete)
    {
        s.nonPlayerScene = true;
        return PlayScene(s, onSceneComplete);
    }
    public Coroutine PlayScene(Scene s, Action onSceneComplete)
    {
        //mainCanvas.root.gameObject.SetActive(true);
        return playCoroutine.StartCoroutine(Play(s), () => { onSceneComplete?.Invoke(); });
    }
    public Coroutine PlayPhoneScene(DialogueVariable scene, Action onSceneComplete)
    {
        Scene s = DialogueUtils.GetSceneFrom(scene);
        s.Tags.AddTag("phone");
        return PlayScene(s, onSceneComplete);
    }
    public void ForceInterruptScene(Action onInterrupt)
    {
        playCoroutine.StopCoroutine();
        onInterrupt?.Invoke();
    }
    IEnumerator Play(Scene s)
    {
        log.ClearLog();
        AddTimeAction.AddTime(s.time);
        skipToogle.isOn = false;
        onSceneFinished = null;
        //remove camera input
        activeCharacterName.text = "";
        isInDialogue = true;
        ActiveScene = s;
        if (!s.nonPlayerScene)
        {
            InventoryManager.Instance.CloseInventory();
            TimeController.Instance.IsPaused.AddLock(this);
            Global10MH.ShowCursor(this);
            CursorController.HideCursor(this);
            InteractionHandler.LockInteraction(this);
            Player.Instance.OnDialogueStart();
            HUDController.Instance.FadeIn(val: 0.3f);
            if (s.hideInv)
            {
                InventoryManager.LockInput(this);
                HUDController.Instance.SetHUDVisibility(false);
            }

            while (CameraController10MH.Instance == null || WalkController10MH.Instance == null)
                yield return null;
            WalkController10MH.RemoveUserInput(this);
            if (WalkController10MH.Instance.IsCrouched)
                WalkController10MH.Instance.CrouchToggle(false);
        }

        SetBackground(null, new Color(0, 0, 0, 0));

        if (s == null)
        {
            Debug.LogError("Scene can not be null");
        }
        else
        {
            if (s.bgmAudio)
                AudioSystem.PlayBGM(s.bgmAudio, Vector3.zero, 0.5f);

            if (!s.isSilent)
                yield return OpenDialogueUI();

            if (s.hidePlayer)
                UIController.Instance.HidePlayer(this);
            else
                UIController.Instance.ShowPlayer(this);

            if (!s.nonPlayerScene)
                CameraController10MH.RemoveUserInput(this);
            //show character
            Layer.PushNewLayer(mainCanvas.transform);
            yield return new WaitForSeconds(0.1f);

            yield return ProcessSceneData(s);
        }

        Layer.ExitAll();
        yield return ExitCharacters();
        yield return CloseDialogueUI();
        if (!s.nonPlayerScene)
        {
            Player.Instance.OnDialogueEnd();
            HUDController.Instance.FadeOut();
            Global10MH.HideCursor(this);
            CursorController.ShowCursor(this);
            CameraController10MH.RestoreUserInput(this);
            WalkController10MH.RestoreUserInput(this);
            TimeController.Instance.IsPaused.RemoveLock(this);
            InteractionHandler.RemoveInteractionLock(this);
            InventoryManager.UnlockInput(this);
        }
        isInDialogue = false;
        skipToogle.isOn = false;
        log.CloseLog();
        dialogueCommandsAnim.AnimateOffScreen();
        onSceneFinished?.Invoke();
        ActiveScene = null;
    }

    public void OnBattleStart()
    {
        dialogueCommandsAnim.AnimateOffScreen();
        skipToogle.isOn = false;
        log.CloseLog();
    }

    public static Action onSceneFinished;
    public static Scene interruptScene = null;
    public static Scene ActiveScene { get; private set; }
    private static List<string> nextJumpSceneIDs = null;
    public static void SetJumpSceneIDS(params string[] ids)
    {
        nextJumpSceneIDs = new List<string>();
        foreach (string id in ids)
        {
            nextJumpSceneIDs.Add(id.ToLower());
        }
    }
    IEnumerator ProcessSceneData(Scene start)
    {
        nextJumpSceneIDs = null;
        interruptScene = null;
        ActiveScene = start;
        int startIndex = 0;
        List<DialogueEvent> events = new List<DialogueEvent>(start.events);

        if (CycleManager.Instance.HasEvent(start))
        {
            for (int i = startIndex; i < events.Count; i++)
            {
                if (events[i].JumpIndex.ToLower() == "repeat")
                {
                    startIndex = i;
                    break;
                }
            }
        }
        else
        {
#if UNITY_EDITOR
            int index = 0;
            foreach (DialogueEvent e in events)
            {
                if (e.JumpIndex.ToLower() == "skip")
                {
                    startIndex = index;
                    break;
                }
                index++;
            }
#endif
        }

        CycleManager.Instance.AddTempEvent(start);
        do
        {
            yield return ActiveScene.Process(null);
            interruptScene = null;
            for (int i = startIndex; i < events.Count; i++)
            {
                DialogueEvent item = events[i];
                if (interruptScene != null)
                {
                    events = events.GetRange(i, events.Count - i);
                    break;
                }
                else if (nextJumpSceneIDs != null)
                {
                    while (i < events.Count)
                    {
                        item = events[i];
                        if (nextJumpSceneIDs.Contains(item.JumpIndex.ToLower()))
                            break;
                        i++;
                    }
                    if (i >= events.Count)
                    {
                        item = null;
                        throw new Exception("Jump Index " + nextJumpSceneIDs[0] + " not Found");
                    }
                    nextJumpSceneIDs = null;
                }

                if (item is EndAction)
                {
                    if ((item as EndAction).End())
                    {
                        yield return item.Process(Layer.ActiveLayer);
                        break;
                    }
                }

                if (Input.GetKey(KeyCode.LeftControl))
                {
                    break;
                }

                yield return item.Process(Layer.ActiveLayer);
            }

            if (interruptScene != null)
            {
                if (interruptScene.insertInsteadOfReplace)
                {
                    events.InsertRange(0, interruptScene.events);
                }
                else
                {
                    events = new List<DialogueEvent>(interruptScene.events);
                }
                ActiveScene = interruptScene;
            }

        } while (interruptScene != null);
    }
    IEnumerator OpenDialogueUI()
    {
        dialogueCommandsAnim.AnimateOnScreen();
        backgroundImage.DOFade(1, transitionDuration);
        UIController.Instance.ShowCinemaBars(this);
        EffectsManager.Instance.Blur(0.5f);
        yield return null;
    }
    public IEnumerator CloseDialogueUI()
    {
        Publisher.Raise(new ClearNotificationEvent());
        ClearFocusItem();
        activeCharacterName.DOText("", 0.75f, true, ScrambleMode.All);
        backgroundImage.DOFade(0, transitionDuration);
        UIController.Instance.HideCinemaBars(this);
        UIController.Instance.HidePlayer(this);
        EffectsManager.Instance.Blur(0);
        offScreenCharacters.Clear();
        yield return null;
        //mainCanvas.root.gameObject.SetActive(false);
    }
    public void EnterCharacter(CharacterEnterData enterData)
    {
        offScreenCharacters.Remove(enterData.character);
        DialogueCharacterRenderer cRend = SpawnCharacterRenderer(enterData.character);
        cRend.EnterCharacter(enterData);
    }
    private DialogueCharacterRenderer SpawnCharacterRenderer(CharacterData cd)
    {
        DialogueCharacterRenderer cRend = GameObject.Instantiate(
            cd.dialogueCharacterPrefab == null ?
            defaultCharacterRenderer : cd.dialogueCharacterPrefab,
            characterRoot)
            .GetComponentInChildren<DialogueCharacterRenderer>();

        characters.Add(cd, cRend);

        return cRend;
    }
    public void EnterOffScreenCharacter(CharacterData cData, OffScreenInfo info)
    {
        if (!offScreenCharacters.ContainsKey(cData))
            offScreenCharacters.Add(cData, null);
        offScreenCharacters[cData] = info;
    }
    public IEnumerator EnterCharacterAndWait(CharacterEnterData enterData)
    {
        offScreenCharacters.Remove(enterData.character);
        DialogueCharacterRenderer cRend = SpawnCharacterRenderer(enterData.character);
        yield return cRend.EnterCharacterAndWait(enterData);
    }
    public IEnumerator MoveCharacterAndWait(CharacterEffectData data)
    {
        DialogueCharacterRenderer cRend = GetCharacterRenderer(data.character);
        yield return cRend.MoveCharacterAndWait(data);
    }
    public void MoveCharacter(CharacterEffectData data)
    {
        DialogueCharacterRenderer cRend = GetCharacterRenderer(data.character);
        cRend.MoveCharacter(data);
    }
    public void SpriteCharacter(CharacterEffectData data)
    {
        DialogueCharacterRenderer cRend = GetCharacterRenderer(data.character);
        cRend.SetSprite(data);
    }
    public IEnumerator SpriteCharacterAndWait(CharacterEffectData data)
    {
        DialogueCharacterRenderer cRend = GetCharacterRenderer(data.character);
        yield return cRend.SetSpriteAndWait(data);
    }
    public IEnumerator SpriteCharacterAndWait(CharacterData cd, Sprite s)
    {
        DialogueCharacterRenderer cRend = GetCharacterRenderer(cd);
        yield return cRend.SetSpriteAndWait(s);
    }
    public IEnumerator ExitCharacterAndWait(CharacterData cd)
    {
        //activeCharacterName.DOText("", 0.75f, true, ScrambleMode.All);
        DialogueCharacterRenderer cRend = GetCharacterRenderer(cd);
        characters.Remove(cd);
        yield return cRend?.ExitCharacterAndWait();
        if (cRend == activeCharacter)
            activeCharacter = null;
    }
    public void ExitCharacter(CharacterData cd)
    {
        //activeCharacterName.DOText("", 0.75f, true, ScrambleMode.All);
        DialogueCharacterRenderer cRend = GetCharacterRenderer(cd);
        characters.Remove(cd);
        cRend?.ExitCharacter();
        if (cRend == activeCharacter)
            activeCharacter = null;
    }
    public IEnumerator ExitCharacters()
    {
        activeCharacterName.DOText("", 0.75f, true, ScrambleMode.All);
        activeCharacter = null;
        offScreenCharacters.Clear();
        List<CharacterData> temp = new List<CharacterData>(characters.Keys);
        foreach (CharacterData c in temp)
        {
            ExitCharacter(c);
        }
        yield return new WaitForSeconds(0.5f);

    }
    public bool IsOffScreen(CharacterData cd)
    {
        return offScreenCharacters.ContainsKey(cd);
    }

    public OffScreenInfo GetOffScreenInfo(CharacterData cd)
    {
        return offScreenCharacters[cd];
    }
    public DialogueCharacterRenderer GetCharacterRenderer(CharacterData cd)
    {
        if (characters.ContainsKey(cd))
            return characters[cd];
        else return null;
    }
    public IEnumerator SetActiveCharacter(CharacterData cd)
    {
        if (activeCharacter != null && activeCharacter.CharacterData == cd)
            yield break;
        activeCharacter?.OnClearActive();

        activeCharacter = GetCharacterRenderer(cd);

        if (activeCharacter != null)
        {
            yield return activeCharacter.OnSetActive();
            activeCharacterName.DOText(cd.displayName, 0.75f, true, ScrambleMode.All);
            SetProceedColor(cd.characterColor);
        }
    }
    public void SetCharacterItemEvents(List<ItemEvents> itemEvents)
    {
        activeCharacter?.SetCharacterItemEvents(itemEvents);
    }
    public IEnumerator WaitForPlayerInput()
    {
        proceedButton.DOKill();

        float t = 0;
        while (auto && !skip && !log.IsOpen)
        {
            if (InventoryManager.Instance.IsHidden)
            {
                t += Time.deltaTime;
                if (t >= AUTO_WAIT_TIME)
                    yield break;
                else
                    yield return null;
            }
            else
            {
                yield return null;
            }
        }

        proceedButton.DOFade(0, 0);

        yield return null;
        proceedButton.DOFade(0.7f, 0.25f);

        bool wait = !SkipProceed;

        while (wait)
        {
            yield return null;
            if (Phone.IsOnPhone)
                continue;
            if (log.IsOpen)
                continue;
            else if (interruptScene != null || nextJumpSceneIDs != null)
                yield break;
            else if (InputSystem10MH.proceedDialogue.GetUp() && (BattleController.IsInBattle || InventoryManager.Instance.IsHidden))
            {
                wait = false;
            }
            else if (SkipProceed)
            {
                yield return new WaitForSeconds(0.25f);
                wait = false;
            }
        }

        do
            yield return null;
        while (log.IsOpen);

        wait = !SkipProceed;

        proceedButton.DOKill();

        proceedButton.DOFade(0, 0);

        if (!SkipProceed)
            proceedSFX.Play();
    }
    public static bool SkipProceed
    {
        get => skip && InventoryManager.Instance.IsHidden;
    }
    public static bool FinishType
    {
        get => InputSystem10MH.proceedDialogue.GetUp();
    }
    private void Update()
    {
        // if (debugGridLayout.activeSelf != Input.GetKey(KeyCode.LeftControl))
        //    debugGridLayout.SetActive(!debugGridLayout.activeSelf);
    }
    public static bool showingResponses = false;
    public static IEnumerator ShowResponseMenu(DialogueOptionsEvent e)
    {
        showingResponses = true;
        Instance.responseMenu.SetUp(e);
        yield return Instance.responseMenu.Show();
    }
    public static IEnumerator HideResponseMenu()
    {
        yield return Instance.responseMenu.Hide();
        showingResponses = false;
    }
    public static IEnumerator ShowScreenText(ScreenTextAction e)
    {
        Instance.screenDialogueMenu.SetUp(e);
        yield return Instance.screenDialogueMenu.Show();
    }
    public static IEnumerator ShowScreenText(CharacterTextElement e)
    {
        Instance.screenDialogueMenu.SetUp(e);
        yield return Instance.screenDialogueMenu.Show();
    }
    public static IEnumerator HideScreenText()
    {
        yield return Instance.screenDialogueMenu.Hide();
    }
    public static void SetBackground(Sprite s, Color c)
    {
        if (s == null)
            s = GlobalData10MH.Instance.transparentImage;
        Instance.backgroundImage.sprite = s;
        Instance.backgroundImage.color = c;
    }
    public static void ProcessSelection(string e)
    {
        Instance.ProcressSelection(e);
    }
    public void ProcressSelection(string e)
    {
        StartCoroutine(Process(e));
    }
    IEnumerator Process(string e)
    {
        if (!string.IsNullOrEmpty(e))
        {
            yield return null;
            JumpToAction.JumpTo(e);
        }
    }
    public static void SetLocation(AreaData ad)
    {
        if (CycleManager.Instance.HasAreaUnlocked(ad))
            return;
        CycleManager.Instance.UnlockArea(ad);
        Debug.LogWarning("We broke this");
        //yield return NewLocationUI.Instance.Show(newLocation);
        //newLocation = ad.displayName;
    }
    public static void ShowFocusItem(Item i, string text)
    {
        Instance.noteUI.ShowItem(i, text);
    }
    public static void ClearFocusItem()
    {
        Instance.noteUI.Hide();
    }

    public void Lighting(float alpha, float time, bool blackout)
    {
        lightsUI.DOKill();
        lightsUI.transform.SetSiblingIndex(blackout ? 2 : 0);
        lightsUI.DOFade(alpha, time);
    }
    public void SetProceedColor(Color c)
    {
        proceedImage_1.color = c;
        //proceedImage_2.color = c;
    }
    public void RevertColor()
    {
        SetProceedColor(Color.white);
    }

    public void Skip(bool val)
    {
        skip = val;
        skipToogle.GetComponentInChildren<TMP_Text>().color = skip ? skipToogle.colors.disabledColor : skipToogle.colors.normalColor;
        Time.timeScale = val ? 2 : 1;
    }

    public void Auto(bool val)
    {
        auto = val;
        autoToogle.GetComponentInChildren<TMP_Text>().color = auto ? autoToogle.colors.disabledColor : autoToogle.colors.normalColor;
    }

    public void Log()
    {
        if (isInDialogue)
        {
            skipToogle.isOn = false;
            log.OpenLog();
        }
    }
}

public class CharacterEffectData
{
    public const float defaultTransitionSpeed = 0.5f;

    public float xPos = 0;
    public string sprite = "";
    public float transitionTime = defaultTransitionSpeed;
    public CharacterData character;
    public bool changeSpriteInWorld = false;
    public enum FocusLevel
    {
        DEFAULT,
        FOCUS
    }
}
public class CharacterEnterData : CharacterEffectData
{
    public FocusLevel focusLevel;
    public EnterAnimationType animType;

    public enum EnterAnimationType
    {
        DEFAULT,
        SCALE,
        RIGHT,
        LEFT,
        FADE,
        HIDDEN
    }
}


