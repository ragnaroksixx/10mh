﻿using UnityEngine;
using System.Collections;
using RedBlueGames.Tools.TextTyper;
using Sirenix.OdinInspector;
using Febucci.UI;

namespace NHG.Dialogue
{
    public class NarrationTextElement : DialogueEvent
    {
        public Vector2 size;
        public bool waitForInput => !tags.GetBool("auto");
        public float printDelayOverride => tags.GetFloat("printDelay", -1.0f);

        public NarrationText nText;

        public override IEnumerator Process(Layer layer)
        {
            nText.postion = tags.GetPosition();
            nText.text = tags.GetString("text", null);
            nText.size = tags.GetVector2("size", Vector2.one);

            yield return base.Process(layer);


            NarrationBoxUI nbUI = GameObject.Instantiate(GlobalData10MH.Instance.narratorBoxUI);
            nbUI.Set(nText);
            TextAnimatorPlayer typer = nbUI.GetComponentInChildren<TextAnimatorPlayer>();
            nbUI.transform.SetParent(layer.transform);

            string t = Global10MH.ParseLine(nText.text);
            EffectsData ed = new EffectsData(nText.postion, Vector3.zero, new Vector3(1, 1, 1), nbUI.GetComponent<CanvasGroup>());
            nbUI.StartCoroutine(new NoEffect().Play(ed));
            typer.ShowText(t);
            float skipTime = 0.25f;
            while (typer.IsInsideRoutine)
            {
                yield return null;
                if (DialogueRenderer.SkipProceed)
                {
                    skipTime -= Time.deltaTime;
                    if (skipTime <= 0)
                        typer.SkipTypewriter();
                }
                else if (DialogueRenderer.FinishType)
                    typer.SkipTypewriter();
            }
            if (waitForInput)
                yield return WaitForInputAction.ProcessWaitForInput(layer);
        }

        [Button("Editor Draw")]
        void Draw()
        {
            EditorNarrationViewer env = GameObject.FindObjectOfType<EditorNarrationViewer>();
            if (env)
                env.Draw(this);
            else
                Debug.LogError("Narration Viewer Not Found");
        }
    }
    [System.Serializable]
    public struct NarrationText
    {
        public Vector3 postion;
        public Vector2 size;
        [TextArea(5, 20)]
        public string text;
    }
}
