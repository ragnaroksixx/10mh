﻿using UnityEngine;
using System.Collections;
using TMPro;
using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;

namespace NHG.Dialogue
{
    public abstract class WorldElement : DialogueAction
    {
        //public static readonly Vector3 topLeft = new Vector3(-400, -150, 0);
        public static readonly Vector3 size = new Vector3(400 * 2, 150 * 2, 0);
        public const int divisons = 5;
        public Vector3 position => GetPosition();
        public Vector3 rotation => GetRotation();


        public bool waitForEnterTransition => tags.GetBool("waitForTransition");

        public virtual bool HasPositionData()
        {
            return tags.HasTag("position") || tags.HasTag("pos");
        }
        public virtual Vector3 GetPosition()
        {
            if (tags.HasTag("position"))
            {
                return tags.GetPosition();
            }
            else if (tags.HasTag("pos"))
            {
                string pos = tags.GetString("pos", null);
                Vector3 vec = new Vector3(CalcX(pos), CalcY(pos), 0);
                return vec;
            }
            return Vector3.zero;
        }

        public virtual Vector3 GetRotation()
        {
            if (tags.HasTag("rotation"))
            {
                return tags.GetRotation();
            }
            else if (tags.HasTag("rot"))
            {
                return new Vector3(0, 0, tags.GetFloat("rot", 0));
            }
            return Vector3.zero;
        }

        public static float CalcX(string value)
        {
            int index = 0;
            float charIndex = char.ToUpper(value[index++]) - 'A';
            charIndex -= 3;//-3 because we always want D to be our center
            if (value.Length > index && value[index] == '#')
            {
                charIndex += 0.5f;
            }
            else if (value.Length > index && value[index] == '\'')
            {
                charIndex += 0.25f;
            }
            float x = (size.x / divisons) * charIndex;
            return x;
        }

        public static float CalcY(string value)
        {
            int index = 1;

            if (value[index] == '#')
                index++;

            float charIndex = char.ToUpper(value[index++]) - '0';
            charIndex -= 3;
            if (value.Length > index && value[index] == '#')
            {
                charIndex += 0.5f;
            }
            else if (value.Length > index && value[index] == '\'')
            {
                charIndex += 0.25f;
            }
            float y = (size.y / divisons) * charIndex;
            return y;
        }

        public WorldElementEffect effect => effects[tags.GetString("effect", "default")];
        public static Dictionary<string, WorldElementEffect> effects = new Dictionary<string, WorldElementEffect>()
        {
            {"default",new DropInEffect(20,0.5f) },
            {"drop100",new DropInEffect(100,1) },
            {"drop10",new DropInEffect(10,1) },
            {"invert",new InvertRotate() },
            {"scream",new ShakeEffect(45,50,0.5f) },
            {"delayshake_1/4",new DelayedShakeEffect(0.25f,45,50,0.5f) },
            {"yawn",new StretchUpEffect(1,0.1f) },
        };
    }
}
