﻿using NHG.Dialogue;
using System.Collections;
using UnityEngine;


public class TextMessageAction : DialogueAction
{
    public override string eventName => "sms";
    public string sender => tags.GetExpressString("Norman");
    public string message => tags.GetString("msg", null);
    public string groupId => tags.GetString("group", sender);

    public bool waitForInput => !tags.GetBool("auto");

    public override IEnumerator Process(Layer layer)
    {
        yield return base.Process(layer);

        AddMessage(groupId, sender, message);
        if (waitForInput)
        {
            yield return WaitForInputAction.ProcessWaitForInput(layer);
            yield return new WaitForSeconds(0.5f);
        }
    }

    public static void AddMessage(string groupId, string sender, string message)
    {
        TextChain tc = Phone.Instance.GetTextChain(groupId);
        tc.AddMessage(sender, message);
    }
}


public class OpenTextChainAction : DialogueAction
{
    public override string eventName => "Open SMS";
    public string chatName => tags.GetExpressString(null);

    public override IEnumerator Process(Layer layer)
    {
        yield return base.Process(layer);
        Phone.Instance.OpenTextChain(chatName);
    }
}


