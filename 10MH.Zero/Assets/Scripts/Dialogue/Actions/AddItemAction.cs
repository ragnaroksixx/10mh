﻿using UnityEngine;
using System.Collections;

namespace NHG.Dialogue
{
    public class AddItemAction : DialogueAction
    {
        public override string eventName => "add item";
        public Item item => tags.GetExpressItem();
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            ItemPlacementData ipd = new ItemPlacementData();
            ipd.item = item;
            ipd.x = 0;
            ipd.y = 0;
            ipd.orientation = item.defaultOrientation;

            InventoryDropArea availPos = PlayerInventory.Instance.TryFindFreePos(ipd);

            if (availPos == null)
                yield break;

            ipd.x = availPos.pos.x;
            ipd.y = availPos.pos.y;

            PlayerInventory.Instance.TrySpawnItems(ipd);
        }
    }

    public class AddPermanItem : DialogueAction
    {
        public override string eventName => "add perma item";
        public Item item => tags.GetExpressItem();

        public override IEnumerator Process(Layer layer)
        {
            yield return null;
            ItemPlacementData ipd = new ItemPlacementData();
            ipd.item = item;
            PlayerInventory.AddPermanentInvItem(ipd);
        }
    }

    public class RemoveItem:DialogueAction
    {
        public override string eventName => "remove item";
        public Item item => tags.GetExpressItem();

        public override IEnumerator Process(Layer layer)
        {
            yield return null;
            PlayerInventory.Instance.TryRemove(item);
        }
    }
}
