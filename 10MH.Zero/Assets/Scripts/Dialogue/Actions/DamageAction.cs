﻿using UnityEngine;
using System.Collections;

namespace NHG.Dialogue
{
    public class DamageAction : DialogueAction
    {
        public override string eventName => "Damage";
        public int damage => tags.GetExpressInt(0);
        public string causeOfDeath => tags.GetString("reason", null);
        public bool lethal => tags.GetBool("lethal", false);
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            int damageUse = damage;

            if (!lethal)
            {
                if (CycleManager.PlayerCombatUnit.hp != 1)
                    damageUse = Mathf.Clamp(damageUse, 0, CycleManager.PlayerCombatUnit.hp - 1);
            }

            CycleManager.PlayerCombatUnit.LoseHP(damageUse);

            if (CycleManager.PlayerCombatUnit.hp <= 0)
                yield return GameOver.EndGame(causeOfDeath);
        }
    }
}
