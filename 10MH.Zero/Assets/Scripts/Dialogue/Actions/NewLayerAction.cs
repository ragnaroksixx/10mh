﻿using UnityEngine;
using System.Collections;

namespace NHG.Dialogue
{
    public class NewLayerAction : DialogueAction
    {
        public override string eventName => "push";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            ProcessNewLayer(layer);
        }
        public static Layer ProcessNewLayer(Layer layer)
        {
            return Layer.PushNewLayer(DialogueRenderer.Instance.mainCanvas.transform);
        }
        public static Layer ProcessNewLayerUp(Layer layer)
        {
            return Layer.PushNewLayer(DialogueRenderer.Instance.mainCanvas.transform);
        }
    }

    public class PopLayerAction : DialogueAction
    {
        public override string eventName => "pop";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            Layer.PopLast();
        }
    }
}
