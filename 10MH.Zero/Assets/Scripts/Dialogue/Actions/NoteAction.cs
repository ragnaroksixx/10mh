﻿using System.Collections;
using UnityEngine;

namespace NHG.Dialogue
{
    public class NoteAction : DialogueAction
    {
        public override string eventName => "note";
        public override string[] eventAlias => new string[] { "comment","todo" };
        string log => tags.GetExpressString("");
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            Debug.Log(log);
        }
    }
}
