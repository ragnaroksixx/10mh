﻿using UnityEngine;
using System.Collections;
namespace NHG.Dialogue
{
    public class SceneTriggerAction : DialogueAction
    {
        public string trigger { get => tags.GetExpressString(null); }

        public override string eventName => "trigger";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            Publisher.Raise(new SceneTriggerEvent(trigger));
            if (!DialogueRenderer.SkipProceed)
                yield return new WaitForSeconds(endDelay);
            yield return null;
        }

    }

    public class EventAction { }
}
