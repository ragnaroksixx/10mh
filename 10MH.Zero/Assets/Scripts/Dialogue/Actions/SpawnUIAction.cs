﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;

public class SpawnUIAction : DialogueAction
{
    public override string eventName => "spawn ui";

    public override IEnumerator Process(Layer layer)
    {
        yield return base.Process(layer);
        GameObject.Instantiate(Resources.Load<GameObject>(ExpressTag));
    }
}

public class DestoryUIAction : DialogueAction
{
    public override string eventName => "destroy ui";

    public override IEnumerator Process(Layer layer)
    {
        yield return base.Process(layer);
        GameObject.Destroy(GameObject.Find(ExpressTag));
    }
}
