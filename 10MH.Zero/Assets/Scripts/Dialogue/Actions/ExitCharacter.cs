﻿using UnityEngine;
using System.Collections;

namespace NHG.Dialogue
{
    public class ExitCharacter : CharacterEffectAction
    {
        public override string eventName => "exit";
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            if (wait)
                yield return DialogueRenderer.Instance.ExitCharacterAndWait(character);
            else
                DialogueRenderer.Instance.ExitCharacter(character);
        }
    }

    public class ExitAllCharacters : DialogueAction
    {
        public override string eventName => "exit all";
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            yield return DialogueRenderer.Instance.ExitCharacters();

        }
    }
}
