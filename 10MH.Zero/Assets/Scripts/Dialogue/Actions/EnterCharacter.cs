﻿using UnityEngine;
using System.Collections;
using System;

namespace NHG.Dialogue
{
    public abstract class CharacterEffectAction : WorldElement
    {
        public CharacterData character => tags.GetExpressCharacter();
        protected bool wait { get => !tags.GetBool("nowait"); }
        protected string Sprite { get => tags.GetString("sprite", "normal"); }
        protected float transitionTime { get => tags.GetFloat("time", CharacterEffectData.defaultTransitionSpeed); }

        public override Vector3 GetPosition()
        {
            if (tags.HasTag("pos"))
            {
                string pos = tags.GetString("pos", null);
                Vector3 vec = new Vector3(CalcX(pos), 0, 0);
                return vec;
            }
            return Vector3.zero;
        }
    }
    public class EnterCharacter : CharacterEffectAction
    {
        bool isUnknown { get => tags.GetBool("unknown"); }

        CharacterEnterData.EnterAnimationType animationType
        {
            get
            {
                return tags.GetString("anim", "default").ToUpper().ToEnum<CharacterEnterData.EnterAnimationType>
                    (typeof(CharacterEnterData.EnterAnimationType));
            }
        }

        CharacterEnterData.FocusLevel focusLevel
        {
            get
            {
                return tags.GetString("focus", "default").ToUpper().ToEnum<CharacterEnterData.FocusLevel>
                    (typeof(CharacterEnterData.FocusLevel));
            }
        }
        public override string eventName => "enter";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            CharacterEnterData eData = new CharacterEnterData()
            {
                character = character,
                xPos = GetPosition().x,
                sprite = Sprite,
                animType = animationType,
                transitionTime = transitionTime,
                focusLevel = focusLevel
            };
            if (wait)
                yield return DialogueRenderer.Instance.EnterCharacterAndWait(eData);
            else
                DialogueRenderer.Instance.EnterCharacter(eData);
        }

    }

    public class MoverCharacter : CharacterEffectAction
    {

        public override string eventName => "move character";
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            CharacterEffectData eData = new CharacterEffectData()
            {
                character = character,
                xPos = GetPosition().x,
                sprite = "",
                transitionTime = transitionTime
            };
            if (wait)
                yield return DialogueRenderer.Instance.MoveCharacterAndWait(eData);
            else
                DialogueRenderer.Instance.MoveCharacter(eData);
        }
    }

    public class ChangeCharacterSprite : CharacterEffectAction
    {
        public override string eventName => "sprite";
        public bool changeSpriteInWorld => tags.GetBool("world");

        public override IEnumerator Process(Layer layer)
        {
            CharacterEffectData eData = new CharacterEffectData()
            {
                character = character,
                sprite = Sprite,
                transitionTime = transitionTime,
                changeSpriteInWorld = changeSpriteInWorld
            };

            yield return base.Process(layer);

            if (eData.character == Player.Instance.characterData)
            {
                if (wait)
                    yield return PlayerInfoMenu.Instance.ChangeSpriteAndWait(eData);
                else
                    PlayerInfoMenu.Instance.ChangeSprite(eData);
            }
            else
            {

                if (wait)
                    yield return DialogueRenderer.Instance.SpriteCharacterAndWait(eData);
                else
                    DialogueRenderer.Instance.SpriteCharacter(eData);
            }


            yield return new WaitForSeconds(endDelay);
        }
    }

    public class CharacterAnimationEffect : CharacterEffectAction
    {
        public override string eventName => "character anim";
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            DialogueCharacterRenderer dcr = DialogueRenderer.Instance.GetCharacterRenderer(character);
            RectTransform rt = dcr.transform as RectTransform;
            EffectsData ed = new EffectsData(rt.anchoredPosition, rt.localEulerAngles, new Vector3(1, 1, 1), dcr.cg);
            if (wait)
                yield return effect.Play(ed);
            else
                dcr.StartCoroutine(effect.Play(ed));
        }
    }

    public class NormanAnimationEffect : CharacterEffectAction
    {
        public override string eventName => "norman anim";
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            RectTransform rt = PlayerInfoMenu.Instance.playerImage.transform as RectTransform;
            EffectsData ed = new EffectsData(rt.anchoredPosition, rt.localEulerAngles, new Vector3(1, 1, 1), PlayerInfoMenu.Instance.playerCG);
            if (wait)
                yield return effect.Play(ed);
            else
                PlayerInfoMenu.Instance.StartCoroutine(effect.Play(ed));
        }
    }

    public class ShowPlayerAction : DialogueAction
    {
        public override string eventName => "norman show";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            UIController.Instance.ShowPlayer(this);
        }
    }

    public class EnterOffScreenCharacter : WorldElement
    {
        public CharacterData character => tags.GetExpressCharacter();

        public override string eventName => "enter offscreen";
        public override string[] eventAlias => new string[2] { "enter hidden", "offscreen" };

        public string nameOverwrite => tags.GetString("name", "");
        public bool useHiddenName => tags.HasTag("usehiddenname");
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);

            OffScreenInfo info = new OffScreenInfo();
            info.postion = GetPosition();
            info.displayName = nameOverwrite;
            if (useHiddenName)
                info.displayName = character.hiddenDisplayName;
            else
                info.displayName = character.displayName;

            DialogueRenderer.Instance.EnterOffScreenCharacter(character, info);
        }
    }

    public class OffScreenInfo
    {
        public Vector2 postion;
        public string displayName;

        public OffScreenInfo()
        {
            postion = Vector3.zero;
            displayName = null;
        }
    }
}
