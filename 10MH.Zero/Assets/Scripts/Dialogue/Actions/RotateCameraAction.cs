﻿using UnityEngine;
using System.Collections;
namespace NHG.Dialogue
{
    public class RotateCameraAction : DialogueAction
    {
        public float rotationAmount { get => tags.GetFloat("angle", 0); }

        public override string eventName => "setCameraRot";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            CameraController10MH.Instance.z = rotationAmount;
        }
    }
}
