﻿using UnityEngine;
using TMPro;
using DG.Tweening;
using System.Collections;

namespace NHG.Dialogue
{
    public class TextAction : WorldElement
    {
        public override string eventName => "text";
        public virtual bool waitForInput => !tags.GetBool("auto");
        public bool keep => tags.GetBool("+");
        public bool newLayer => tags.GetBool("layer");
        public virtual TextStyle style => tags.GetTextStyle("world default");
        public string text => tags.GetExpressString();


        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            if (newLayer)
                layer = NewLayerAction.ProcessNewLayerUp(layer);

            GameObject prefab = GlobalData10MH.Instance.worldTextPrefab;

            Transform obj = GameObject.Instantiate(prefab).transform;
            TMP_Text textObj = obj.GetComponentInChildren<TMP_Text>();

            textObj.text = Global10MH.ParseLine(text);

            obj.SetParent(layer.transform);
            style.ApplyStyle(textObj);

            EffectsData ed = new EffectsData(position, rotation, new Vector3(1, 1, 1), textObj.GetComponent<CanvasGroup>());

            DoEffect(effect, ed, textObj);
            audio?.Play();

            yield return new WaitForSeconds(endDelay);
            if (waitForInput)
                yield return WaitForInputAction.ProcessWaitForInput(layer);
            if (!keep)
            {
                textObj.DOFade(0.15f, 0.1f)
                    .OnComplete(() => { GameObject.Destroy(obj.gameObject); });
            }

        }


        void DoEffect(WorldElementEffect effect, EffectsData ed, MonoBehaviour mb)
        {
            mb.StartCoroutine(effect.Play(ed));
        }

    }
}
