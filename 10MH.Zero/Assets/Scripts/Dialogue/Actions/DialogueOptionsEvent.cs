﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;
using System.Collections.Generic;

namespace NHG.Dialogue
{
    public class DialogueOptionsEvent : DialogueAction
    {
        public virtual string highlightText => tags.GetExpressString();
        public bool isSMSOptions => tags.GetBool("sms");
        public List<DialogueOption> options = new List<DialogueOption>();

        public override string eventName => "Options";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            options.Clear();
            for (int i = 0; i < 4; i++)
            {
                DialogueOption option = new DialogueOption(this, i + 1);
                bool show = !string.IsNullOrEmpty(option.text);
                show = show && (option.MeetsCriteria() || !option.hideif);
                if (show)
                    options.Add(option);
            }
            yield return DialogueRenderer.ShowResponseMenu(this);
            while (DialogueRenderer.showingResponses)
            {
                yield return null;
                if (DialogueRenderer.interruptScene != null)
                {
                    yield return DialogueRenderer.HideResponseMenu();
                    yield break;
                }
            }
        }
    }

    public struct DialogueOption
    {
        public string text => tags.GetString("option" + index, "");
        public string jumpEvent => tags.GetString("result" + index, "");
        public string smsEvent => tags.GetString("sms" + index, "");
        public bool hideif => tags.GetBool("hideif" + index);
        public bool disableif => tags.GetBool("disableif" + index);
        public string disableTextAddon => " " + tags.GetString("disableif" + index, "");
        public bool attackCriteria
        {
            get
            {
                if (tags.HasTag("atk" + index))
                {
                    return PlayerInventory.Instance.MaxAttackItem() >= tags.GetInt("atk" + index, 0);
                }
                else if (tags.HasTag("atkless" + index))
                {
                    return PlayerInventory.Instance.MaxAttackItem() < tags.GetInt("atkless" + index, 0);
                }
                return true;
            }
        }
        public bool moneyCriteria
        {
            get
            {
                if (tags.HasTag("money" + index))
                {
                    return PlayerInventory.Instance.GetMoneyCount() >= tags.GetInt("money" + index, 0);
                }
                else if (tags.HasTag("moneyless" + index))
                {
                    return PlayerInventory.Instance.GetMoneyCount() < tags.GetInt("moneyless" + index, 0);
                }
                return true;
            }
        }
        DialogueOptionsEvent doe;
        int index;
        Tags tags => doe.Tags;

        public DialogueOption(DialogueOptionsEvent doe, int index)
        {
            this.doe = doe;
            this.index = index;
        }

        public bool MeetsCriteria()
        {
            return attackCriteria && moneyCriteria;
        }
    }
}
