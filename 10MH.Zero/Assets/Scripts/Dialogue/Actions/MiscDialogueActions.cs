﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;

public class SolArmAction : DialogueAction
{
    public override string eventName => "Sol Arm";

    public override IEnumerator Process(Layer layer)
    {
        yield return base.Process(layer);
        SaveData.currentSave.GetSolArm();
    }
}
