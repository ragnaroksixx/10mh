﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace NHG.Dialogue
{
    public class ScreenTextAction : DialogueEvent
    {
        public string text => tags.GetString("text", null).Trim(' ');
        public bool center => tags.GetBool("center");
        public bool hiddenName => tags.GetBool("hidden");
        public CharacterData character
        {
            get => GlobalData10MH.Instance.characterDictionary
                    .GetItem(tags.GetString("character", "norman"));
        }
        public string[] itemLinkEvents => tags.GetStrings("items");
        public string itemLinkJump => tags.GetString("itemjump", null);
        public bool takeItem => tags.GetBool("takeitem");

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            yield return DialogueRenderer.Instance.SetActiveCharacter(character);
            DialogueRenderer.Instance.SetCharacterItemEvents(GetItemEvents());
            yield return DialogueRenderer.ShowScreenText(this);
            yield return DialogueRenderer.HideScreenText();
        }

        public List<ItemEvents> GetItemEvents()
        {
            List<ItemEvents> result = new List<ItemEvents>();
            foreach (string item in itemLinkEvents)
            {
                result.Add(new ItemEvents(item, itemLinkJump, takeItem));
            }
            return result;
        }
    }
}
