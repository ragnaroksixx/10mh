﻿using UnityEngine;
using System.Collections;

namespace NHG.Dialogue
{
    public class CameraControlAction : DialogueAction
    {
        public float angle { get => tags.GetFloat("angle", 0); }
        public float duration { get => tags.GetFloat("duration", 0); }

        public override string eventName => "smoothCameraRot";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            yield return CameraController10MH.Instance.LookAtY(angle, duration);

        }
    }

    public class LookAtAction : DialogueAction
    {
        public string gameobjectName { get => tags.GetExpressString(); }
        public CharacterData character { get => tags.GetCharacter(); }
        public override string eventName => "look at";
        public float duration { get => tags.GetFloat("duration", 0); }
        public bool lockX { get => tags.HasTag("lockX") || tags.HasTag("Yonly"); }
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            Vector3 pos;
            if (character)
            {
                pos = Character.FindCharacter(character).transform.position;
                pos.y += 1.5f;
            }
            else
                pos = GameObject.Find(gameobjectName).transform.position;
            if (lockX)
                yield return CameraController10MH.Instance.LookAtY(pos, duration);
            else
                yield return CameraController10MH.Instance.LookAt(pos, duration);

        }

    }
}
