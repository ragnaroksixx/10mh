﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

namespace NHG.Dialogue
{
    /*
    [CreateAssetMenu(fileName = "New Text Element", menuName = "Dialogue/Text Element/Fill Screen Text")]
    public class FillScreenWithText : DialogueEvent
    {
        public TextElement[] dialogues;
        [ListDrawerSettings(NumberOfItemsPerPage = 30)]
        public Vector3[] positions;
        public float delay;
        public bool waitForInput;
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            int index = 0;
            foreach (Vector3 pos in positions)
            {
                yield return dialogues[index].Process(layer, pos, dialogues[index].rotation);
                if (DialogueRenderer.AutoProceed)
                    yield return new WaitForSeconds(delay / 3);
                else
                    yield return new WaitForSeconds(delay);
                index++;
                if (index >= dialogues.Length)
                    index = 0;
            }
            if (waitForInput)
                yield return WaitForInputAction.ProcessWaitForInput(layer);
        }

    }*/
}
