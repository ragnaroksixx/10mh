﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;

public class WaitForEventAction : DialogueAction
{
    public static bool wait = false;

    public override string eventName => "wait event";

    public override IEnumerator Process(Layer layer)
    {
        yield return base.Process(layer);
        wait = true;
        while (wait)
        {
            yield return null;
        }
    }

    public static void Continue()
    {
        wait = false;
    }
}
public class DialogueWaitEvent : PublisherEvent
{

    public DialogueWaitEvent()
    {
        WaitForEventAction.Continue();
    }
}

public class WaitForPhoneAction : DialogueAction
{
    public override string eventName => "wait phone open";
    public float time => tags.GetExpressFloat(0);
    public bool lockAfter => tags.GetBool("lock");

    public override IEnumerator Process(Layer layer)
    {
        yield return base.Process(layer);
        Phone.Instance.SetSystemButtons(false);
        Phone.Instance.LockScreen();
        while (!Phone.Instance.peekAnim.IsOnScreen)
        {
            yield return null;
        }
        if (lockAfter)
        {
            Phone.Instance.SetSystemButtons(false);
            InputSystem10MH.phoneOpenClose.Disable("dialogue");
        }

    }
}

public class WaitForPhoneUnlock : DialogueAction
{
    public override string eventName => "wait phone unlock";
    public override IEnumerator Process(Layer layer)
    {
        yield return base.Process(layer);
        while (Phone.Instance.IsOnLockScreen())
        {
            yield return null;
        }
    }
}
public class OpenPhone : DialogueAction
{
    public override string eventName => "open phone";
    public bool lockSystem => tags.GetBool("locksystem");
    public bool lockInput => tags.GetBool("lockinput");
    public override IEnumerator Process(Layer layer)
    {
        if (lockSystem)
            Phone.Instance.SetSystemButtons(false);
        if (lockInput)
        {
            InputSystem10MH.phoneOpenClose.Disable("dialogue");
        }
        yield return base.Process(layer);
        if (Phone.Instance.IsOpen)
        {
            if (lockInput)
                Phone.Instance.LockInput();
        }
        else
        {
            Phone.Instance.LockScreen();
            Phone.Instance.Open(lockInput);
            yield return new WaitForSeconds(1);
        }


    }
}
public class ClosePhone : DialogueAction
{
    public override string eventName => "close phone";

    public override IEnumerator Process(Layer layer)
    {
        yield return base.Process(layer);
        Phone.Instance.Home();
        Phone.Instance.Close();
        Phone.Instance.SetSystemButtons(true);
        InputSystem10MH.phoneOpenClose.Enable("dialogue");
        yield return new WaitForSeconds(1);

    }
}

public class AddApp : DialogueAction
{
    public override string eventName => "add app";
    public string AppName => tags.GetExpressString();

    public override IEnumerator Process(Layer layer)
    {
        if (Phone.Instance.IsOnLockScreen())
        {
            Phone.Instance.Home();
            yield return new WaitForSeconds(1);
        }
        yield return base.Process(layer);
        Phone.Instance.FindApp(AppName).gameObject.SetActive(true);
        yield return new WaitForSeconds(1);
    }
}
public class OpenApp : DialogueAction
{
    public override string eventName => "open app";
    public string AppName => tags.GetExpressString();

    public override IEnumerator Process(Layer layer)
    {
        if (Phone.Instance.IsOnLockScreen())
        {
            Phone.Instance.Home();
            yield return new WaitForSeconds(1);
        }
        yield return base.Process(layer);
        Phone.Instance.Open(AppName);
        yield return new WaitForSeconds(1);
    }
}
