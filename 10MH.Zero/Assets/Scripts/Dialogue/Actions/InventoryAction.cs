﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace NHG.Dialogue
{
    public class InventoryAction : DialogueAction
    {
        //public ItemPlacementData[] items;
        public string onItemsTakenJump => tags.GetString("taken", null);
        public string onItemsNotTakenJump => tags.GetString("nottaken", null);
        public string cache => tags.GetString("cache", null);
        public bool add => tags.GetBool("add", false);
        public bool perma => tags.GetBool("permanent", false);
        public InventoryPropertyData invData => GlobalData10MH.Instance.invPropDictionary
            .GetItem(tags.GetExpressString());

        public override string eventName => "Inventory";

        public override IEnumerator Process(Layer layer)
        {
            bool exit = false;
            yield return base.Process(layer);
            Inventory invCache;
            List<ItemPlacementData> itemsTemp = CycleManager.GetInventory(cache);

            if (itemsTemp == null)
            {
                itemsTemp = new List<ItemPlacementData>();
                for (int i = 1; i < Mathf.Infinity; i++)
                {
                    if (!tags.HasTag("item" + i)) break;
                    string[] splitText = tags.GetString("item" + i, null).Split(' ');
                    Item item = GlobalData10MH.Instance.itemDictionary.GetItem(splitText[0]);
                    int x = Convert.ToInt32(splitText[1]);
                    int y = Convert.ToInt32(splitText[2]);
                    ItemOrientationX ori = (ItemOrientationX)Convert.ToInt32(splitText[3]);
                    ItemPlacementData ipd = new ItemPlacementData()
                    {
                        item = item,
                        x = x,
                        y = y,
                        orientation = ori
                    };
                    itemsTemp.Add(ipd);
                }
            }
            invCache = InventoryManager.SpawnOtherInventory(cache, perma, invData, add, null, itemsTemp.ToArray());
            //invCache.OpenInventory();
            InventoryManager.Instance.OpenInventory();
            HUDController.Instance.ShowInventoryHelp();
            while (!exit)
            {
                yield return null;
                if (InventoryManager.IsItemHeld())
                    continue;
                if (Input.GetKeyDown(KeyCode.Tab))
                {
                    exit = true;
                }
            }
            CycleManager.CacheInventory(cache, invCache.inventoryItems, perma);
            string jumpPoint = null;

            if (invCache.inventoryItems.Count <= 0)
                jumpPoint = onItemsTakenJump;
            else
                jumpPoint = onItemsNotTakenJump;

            InventoryManager.DestoryInventory(invCache);
            InventoryManager.Instance.CloseInventory();

            if (jumpPoint != null)
                JumpToAction.JumpTo(jumpPoint);
        }
    }

}
