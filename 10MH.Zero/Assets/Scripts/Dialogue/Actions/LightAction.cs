﻿using UnityEngine;
using System.Collections;

namespace NHG.Dialogue
{
    public class LightAction : DialogueAction
    {
        public override string eventName => "lights";
        bool on { get => tags.GetBool("on", false); }

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            //DialogueRenderer.Instance.Lighting(alpha, time, blackout);
            Area.activeArea.SetAllLightSources(on);
        }
    }
}
