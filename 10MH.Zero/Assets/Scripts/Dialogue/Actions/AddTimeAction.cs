﻿using UnityEngine;
using System.Collections;
namespace NHG.Dialogue
{
    public class AddTimeAction : DialogueAction
    {
        public override string eventName => "add time";
        public override string[] eventAlias => new string[1] { "time" };
        public Vector2 cap => tags.GetVector2("cap", Vector2.zero);
        public int time => tags.GetExpressInt(0);
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            int t = time;
            if (cap != Vector2.zero)
            {
                if (new TimeRef(cap) > TimeController.Instance.CurrentTime)
                {
                    TimeRef dif = new TimeRef(cap) - TimeController.Instance.CurrentTime;
                    if (time > dif.TotalSeconds())
                        t = dif.TotalSeconds();
                }
            }
            yield return AddTimeAndWait(t);
        }

        public static void AddTime(int seconds)
        {
            if (seconds == 0) return;

            TimeController.Instance.AddSeconds(seconds);
            string timeText = "+" + seconds + "sec";
            FloatingTextController.PopUpText(timeText, Color.yellow, TimeController.Instance.timePopUpRoot.transform.position, FloatingText.TextType.TIME);
        }

        public static IEnumerator AddTimeAndWait(int seconds)
        {
            if (seconds == 0) yield break;


            string timeText = "+" + seconds + "sec";
            FloatingTextController.PopUpText(timeText, Color.yellow, TimeController.Instance.timePopUpRoot.transform.position, FloatingText.TextType.TIME);

            yield return TimeController.Instance.SkipSeconds(seconds);
        }
    }

    public class TimeTo : DialogueAction
    {
        public override string eventName => "time to";
        public Vector2 time => tags.GetVector2("express", Vector2.zero);
        public bool instant => tags.HasTag("instant");
        public bool realtime => tags.HasTag("realtime");
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            if (instant)
            {
                TimeController.Instance.AddSecondsTo(new TimeRef(time.x, time.y));
            }
            else if (realtime)
            {
                yield return TimeController.Instance.WaitForTimeInGame(new TimeRef(time.x, time.y));
            }
            else
            {
                yield return TimeController.Instance.SkipTo(new TimeRef(time.x, time.y));
            }
        }
    }
    public class SetHelperTimeAction : DialogueAction
    {
        public override string eventName => "max time";

        public Vector2 time => tags.GetVector2("express", Vector2.zero);

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            TimeController.Instance.SetHelperMaxTime(new TimeRef(time.x, time.y));
        }
    }


    public class ClearHelperTimeAction : DialogueAction
    {
        public override string eventName => "clear max time";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            TimeController.Instance.ClearHelperTiime();
        }
    }
}
