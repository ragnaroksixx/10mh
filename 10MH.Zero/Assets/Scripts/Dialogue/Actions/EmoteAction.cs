﻿using UnityEngine;
using System.Collections;
using System;

namespace NHG.Dialogue
{
    public class EmoteAction : TextAction
    {
        public override string eventName => "Emote";
        public override TextStyle style => tags.GetTextStyle("emotemid");
        public PlayerEmoteUI.EmoteType emote => ParseExpressEmote();
        public CharacterData charaterData => tags.GetCharacter();
        public override bool waitForInput => false;
        public override IEnumerator Process(Layer layer)
        {
            if (charaterData == null)
                PlayerInfoMenu.Instance.emoteUI.Show(emote, 1.0f);
            else
            {
                endDelay = 0.5f;
                yield return base.Process(layer);
            }
        }
        public PlayerEmoteUI.EmoteType ParseExpressEmote()
        {
            PlayerEmoteUI.EmoteType e = PlayerEmoteUI.EmoteType.NONE;
            switch (tags.GetExpressString(""))
            {
                case "!":
                    e = PlayerEmoteUI.EmoteType.EXCLAIM;
                    break;
                case "!!":
                    e = PlayerEmoteUI.EmoteType.EXCLAIM_EXCLAIM;
                    break;
                case "?":
                    e = PlayerEmoteUI.EmoteType.QUESTION;
                    break;
                case "!?":
                    e = PlayerEmoteUI.EmoteType.EXCLAIM_QUESTION;
                    break;
            }
            return e;
        }
        public override Vector3 GetRotation()
        {
            if (charaterData == null)
                return base.GetRotation();
            else
                return new Vector3(0, 0, 15);
        }
        public override Vector3 GetPosition()
        {
            if (charaterData == null)
                return base.GetPosition();
            else
            {
                Vector3 pos;
                DialogueCharacterRenderer dcr = DialogueRenderer.Instance.GetCharacterRenderer(charaterData);
                pos = (dcr.transform as RectTransform).anchoredPosition;
                pos.y = 100;
                pos.x -= 100;
                return pos;
            }

        }
    }
}

