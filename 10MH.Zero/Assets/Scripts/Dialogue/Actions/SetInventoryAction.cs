﻿using System.Collections;
using UnityEngine;

namespace NHG.Dialogue
{
    public class SetInventoryAction : DialogueAction
    {
        public override string eventName => "set inventory";
        public string inventoryId => tags.GetExpressString(null);
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            InventorySet i = GlobalData10MH.Instance.inventorySets.GetItem(inventoryId);
            i.LoadInventory();
        }
    }

    public class SetInventoryFromSaveData : DialogueAction
    {
        public override string eventName => "restore inventory";
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            PlayerInventory.Instance.RemoveAll();
            InventoryManager.SpawnPlayerInventory();
        }
    }
}
