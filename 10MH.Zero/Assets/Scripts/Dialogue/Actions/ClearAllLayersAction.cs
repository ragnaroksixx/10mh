﻿using UnityEngine;
using System.Collections;

namespace NHG.Dialogue
{
    public class ClearAllLayersAction : DialogueAction
    {
        public override string eventName => "clear";

        public override IEnumerator Process(Layer layer)
        {
            Layer.ExitAll();
            yield return new WaitForSeconds(1);
            layer = NewLayerAction.ProcessNewLayer(layer);
            yield return base.Process(layer);
        }
    }
}
