﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace NHG.Dialogue
{
    public class SFXAction : DialogueAction
    {
        CustomAudioClip clip { get => tags.GetExpressAudio(); }

        public override string eventName => "sfx";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            if (clip == null)
                AudioSystem.PlaySFX(null, Vector3.zero);
            else
                clip.Play();
        }
    }
    public class StopAudioAction : DialogueAction
    {
        CustomAudioClip clip { get => tags.GetExpressAudio(); }

        public override string eventName => "stop audio";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            AudioSystem.Stop(clip, false);

        }
    }

    public class BGMAction : DialogueAction
    {
        CustomAudioClip clip { get => tags.GetExpressAudio(); }
        public int bgmAudioSection { get => tags.GetInt("section", -1); }
        public float transitionTime { get => tags.GetFloat("time", -1); }


        public override string eventName => "bgm";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            if (clip == null)
                MusicPlayer.Instance.Stop(transitionTime);
            else
            {
                AudioVariable av = new AudioVariable(clip);
                MusicPlayer.Instance.Interrupt(av, transitionTime, bgmAudioSection);
            }
        }
    }
}
