﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;

public class SetBackgroundAction : DialogueEvent
{
    public Sprite background;
    public Color color = Color.white;
    public override IEnumerator Process(Layer layer)
    {
        DialogueRenderer.SetBackground(background, color);
        yield return base.Process(layer);
    }
}
