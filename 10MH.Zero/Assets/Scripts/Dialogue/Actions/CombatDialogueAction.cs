﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace NHG.Dialogue
{
    public class CombatDialogueAction : DialogueAction
    {
        public override string eventName => "Battle";
        public bool jumpWin => tags.GetBool("jumpwin");
        public bool jumpLose => tags.GetBool("jumplose");
        public BattleEncounter encounter { get => tags.GetExpressBattle(); }

        public override IEnumerator Process(Layer layer)
        {
            Layer.ExitAll();
            yield return new WaitForSeconds(1);
            layer = NewLayerAction.ProcessNewLayer(layer);

            yield return base.Process(layer);

            yield return DoBattle();
            yield return HandleBattleEnd();
        }
        protected IEnumerator DoBattle()
        {
            yield return DialogueRenderer.Instance.ExitCharacters();

            TimeController.Instance.IsPaused.RemoveLock(DialogueRenderer.Instance);
            yield return BattleController.Instance.StartBattleScene(encounter);
            TimeController.Instance.IsPaused.AddLock(DialogueRenderer.Instance);
        }
        protected IEnumerator HandleBattleEnd()
        {
            yield return new WaitForSeconds(1);

            if (BattleController.Player.hp > 0)//We winin bois
            {
                if (jumpWin)
                    JumpToAction.JumpTo("win");
                MusicPlayer.Instance.PlayNext();
            }
            else
            {
                if (jumpLose)
                    JumpToAction.JumpTo("lose");
                else
                    GameOver.EndGame(encounter.name);
            }
        }
    }

    public class BattleWin : DialogueAction
    {
        public override string eventName => "win";
        public override string JumpIndex => "win";
    }

    public class BattleLose : DialogueAction
    {
        public override string eventName => "lose";
        public override string JumpIndex => "lose";

    }
    public class GameOver : EndAction
    {
        public override string eventName => "gameover";
        public override string JumpIndex => "gameover";
        public string reason => tags.GetString("reason", "");

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            EndGame(reason);
        }
        public static Coroutine EndGame(string reason)
        {
            return ResultsScreen.Instance.StartCoroutine(EndGameCo(reason));
        }
        static IEnumerator EndGameCo(string reason)
        {
            InteractionHandler.LockInteraction("end");
            InventoryManager.LockInput("end");
            WalkController10MH.RemoveUserInput("end");
            CursorController.HideCursor("end");

            InventoryManager.Instance.CloseInventory();
            yield return null;
            HUDController.Instance.SetTimeVisibility(false);
            if (CycleManager.IsFirstCycle())
                yield return ResultsScreen.Instance.FirstCycleGameOver();
            else
                yield return ResultsScreen.Instance.ShowGameOver(reason);

            InteractionHandler.RemoveInteractionLock("end");
            InventoryManager.UnlockInput("end");
            WalkController10MH.RestoreUserInput("end");
            CursorController.ShowCursor("end");
        }
    }
}
