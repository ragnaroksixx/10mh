﻿using System.Collections;
using UnityEngine;

namespace NHG.Dialogue
{
    public class ReadAction : DialogueAction
    {
        public override string eventName => "read";
        public Item item => tags.GetExpressItem();
        string text => tags.GetString("body", "");
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            DialogueRenderer.ShowFocusItem(item, text);
        }
    }

    public class CloseReadAction : DialogueAction
    {
        public override string eventName => "endread";
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            DialogueRenderer.ClearFocusItem();
        }
    }
}