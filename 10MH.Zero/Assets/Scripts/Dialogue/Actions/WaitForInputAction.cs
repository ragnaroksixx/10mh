﻿using UnityEngine;
using System.Collections;

namespace NHG.Dialogue
{
    public class WaitForInputAction : DialogueAction
    {
        public override string eventName => "wait";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            yield return ProcessWaitForInput(layer);
        }
        public static IEnumerator ProcessWaitForInput(Layer layer)
        {
            yield return DialogueRenderer.Instance.WaitForPlayerInput();
        }
    }

    public class WaitForSecondsAction : DialogueAction
    {
        public override string eventName => "wait time";
        public override string[] eventAlias => new string[] { "waitfor" };
        public float seconds => tags.GetExpressFloat(0);

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            yield return new WaitForSeconds(seconds);
        }

    }
}