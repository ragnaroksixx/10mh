﻿using UnityEngine;
using System.Collections;

namespace NHG.Dialogue
{
    public class MoveControlAction : DialogueAction
    {
        public CharacterData moveTo { get => tags.GetCharacter(); }
        public Vector3 velocity { get => tags.GetVector3("velocity", Vector3.zero); }
        public float duration { get => tags.GetFloat("duration", 0); }
        public bool ignorewait { get => tags.GetBool("ignorewait"); }
        public bool reverse { get => tags.GetBool("reverse"); }

        public override string eventName => "move";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);

            Vector3 direction = velocity;
            if (moveTo != null)
            {
                direction = Character.FindCharacter(moveTo).transform.position;
                direction -= WalkController10MH.Instance.transform.position;
                direction.y = 0;
            }
            if (reverse)
                direction *= -1;
            if (ignorewait)
            {
                layer.StartCoroutine(WalkController10MH.Instance.WalkInDirection(direction.normalized, duration));
            }
            else
            {
                yield return WalkController10MH.Instance.WalkInDirection(direction.normalized, duration);
            }
        }
    }

    public class WarpTo : DialogueAction
    {
        public string gameobjectName { get => tags.GetExpressString(); }
        public float endValue { get => tags.GetFloat("end", -1); }

        public override string eventName => "warp";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            HUDController.Instance.FadeIn(0.5f, 1);
            yield return new WaitForSeconds(1f);
            Transform obj = GameObject.Find(gameobjectName).transform;

            Pose p = new Pose(obj.position, obj.rotation);
            p.position.y = Player.GetPose().position.y;

            Player.SetPose(p);
            yield return new WaitForSeconds(0.5f);
            HUDController.Instance.FadeIn(0.1f, endValue);
            yield return new WaitForSeconds(0.5f);
        }
    }

    public class HiddenWarp : WarpTo
    {
        public override string eventName => "hidden warp";
        public float delay { get => tags.GetFloat("wait", 0); }

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            Player.Instance.Invoke("Move", delay);

        }
        void Move()
        {
            Transform obj = GameObject.Find(gameobjectName).transform;
            Pose p = new Pose(obj.position, obj.rotation);
            p.position.y = Player.GetPose().position.y;
            Player.SetPose(p);
        }
    }
}
