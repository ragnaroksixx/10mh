﻿using System.Collections;
using UnityEngine;


namespace NHG.Dialogue
{
    public class NPCCombatDialogueAction : CombatDialogueAction
    {
        public override string eventName => "NPC Battle";
        public CharacterData npc => tags.GetCharacter();
        public override IEnumerator Process(Layer layer)
        {
            CycleManager.Instance.SetPlayerCharacter(npc);
            Layer.ExitAll();
            yield return new WaitForSeconds(1);
            layer = NewLayerAction.ProcessNewLayer(layer);

            //yield return base.Process(layer);

            yield return DoBattle();
            yield return new WaitForSeconds(1);
            yield return HandleBattleEnd();
            yield return DialogueRenderer.Instance.ExitCharacters();
        }
    }

    public class SetPlayerCharacter : DialogueAction
    {
        public override string eventName => "Set Player";

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            CycleManager.Instance.SetPlayerCharacter(tags.GetExpressCharacter());
        }
    }
}
