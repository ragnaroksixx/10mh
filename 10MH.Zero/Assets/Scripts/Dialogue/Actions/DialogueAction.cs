﻿using UnityEngine;
using System.Collections;
namespace NHG.Dialogue
{
    //event called using []
    public abstract class DialogueAction : DialogueEvent
    {
        public abstract string eventName { get; }
        public virtual string[] eventAlias { get => new string[0] { }; }
        public string ExpressTag => tags.GetExpressString(null);
        public DialogueAction()
        {

        }
    }
}
