﻿using UnityEngine;
using System.Collections;
namespace NHG.Dialogue
{
    public class ExitCharacterWorld : ExitCharacter
    {
        public override string eventName => base.eventName+" World";
        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            yield return DialogueRenderer.Instance.ExitCharacterAndWait(character);
            Character.FindCharacter(character).Leave(false);
        }
    }
}
