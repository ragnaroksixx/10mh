﻿using Febucci.UI;
using System.Collections;
using UnityEngine;


public class TextTyperSettings : MonoBehaviour
{
    public TextAnimatorPlayer player;

    public static float TextSpeed
    {
        get
        {
            return PlayerPrefs.GetFloat("TEXT_SPEED", 1);
        }
        private set
        {
            PlayerPrefs.SetFloat("TEXT_SPEED", value);
        }
    }


    // Use this for initialization
    void Start()
    {
        OnTextSpeedChanged(null);
        Publisher.Subscribe<TextSpeedChangedEvent>(OnTextSpeedChanged);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<TextSpeedChangedEvent>(OnTextSpeedChanged);
    }
    void OnTextSpeedChanged(TextSpeedChangedEvent e)
    {
        player.SetTypewriterSpeed(TextSpeed);
    }
    public static void SetTextSpeed(float val)
    {
        TextSpeed = val;
        Publisher.Raise(new TextSpeedChangedEvent());
    }
}

public class TextSpeedChangedEvent : PublisherEvent
{

}
