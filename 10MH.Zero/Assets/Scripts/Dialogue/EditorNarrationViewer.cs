﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace NHG.Dialogue
{
    public class EditorNarrationViewer : MonoBehaviour
    {
        public void Draw(NarrationTextElement nte)
        {
#if UNITY_EDITOR
            Transform[] tArray = transform.GetComponentsInChildren<Transform>(true);
            foreach (Transform child in tArray)
            {
                if (child == transform) continue;
                if (child == null) continue;
                GameObject.DestroyImmediate(child.gameObject);
            }
            NarrationText nText = nte.nText;
            {
                Object o = PrefabUtility.InstantiatePrefab(GlobalData10MH.Instance.narratorBoxUI.gameObject);
                NarrationBoxUI nbUI = (o as GameObject).GetComponent<NarrationBoxUI>();
                nbUI.transform.SetParent(transform, true);
                nbUI.transform.localScale = Vector3.one;
                nbUI.text.text = nText.text;
                nbUI.Set(nText);
            }
#endif
        }
    }
}
