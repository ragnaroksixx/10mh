﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using UnityEngine.Search;

public class DialogueSceneMB : MonoBehaviour, IDialogueScene
{
    [TextArea(5, 100)]
    public string text;

    string IDialogueScene.GetText()
    {
        return text;
    }
#if UNITY_EDITOR
    [Button]
    public void CopyTextAsset(TextAsset ta)
    {
        if (ta == null) return;
        if(!string.IsNullOrEmpty(text))
        {
            if (!UnityEditor.EditorUtility.DisplayDialog("Are you sure about that?",
                "This will overwrite the current text",
                "heck yeah",
                "no shot"))
            {
                return;
            }
        }
        text = ta.text;
        UnityEditor.EditorUtility.SetDirty(this);
    }
#endif
}
