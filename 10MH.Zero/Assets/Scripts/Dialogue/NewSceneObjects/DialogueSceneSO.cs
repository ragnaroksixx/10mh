﻿using System.Collections;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using Sirenix.OdinInspector;
[CreateAssetMenu]
public class DialogueSceneSO : ScriptableObject, IDialogueScene
{
    TextAsset textAsset;
    public TextAsset Script
    {
        get
        {
            if (textAsset == null)
            {
                //CreateTextAsset();
            }
            return textAsset;
        }
    }
#if UNITY_EDITOR
    [Button]
    void CreateTextAsset()
    {
        string path = AssetDatabase.GetAssetPath(this);
        path = path.Replace(name + ".asset", "");
        string fileName = "script" + GetInstanceID();
        string fullPath = path + "/" + fileName + ".txt";

        TextAsset text = new TextAsset("TEST");
        text.name = "script";
       // AssetDatabase.CreateAsset(text, fullPath);

        AssetDatabase.AddObjectToAsset(text, this);
        AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(text));

        AssetDatabase.AddObjectToAsset(new Texture2D(16, 16), this);
    }
#endif

    string IDialogueScene.GetText()
    {
        return Script.text;
    }
}
