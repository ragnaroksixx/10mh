﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;

[System.Serializable]
[InlineProperty]
public class DialogueVariable : IDialogueScene
{
    [ValidateInput("TypeCheck", "Invalid Scene type!")]
    [HideLabel]
    [InlineButton("Open")]
    public Object scene;
    string IDialogueScene.GetText()
    {
        if (scene is TextAsset)
            return (scene as TextAsset).text;

        return (scene as IDialogueScene).GetText();
    }

    bool TypeCheck(Object o)
    {
        return o is TextAsset || o is IDialogueScene;
    }

    void Open()
    {
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.OpenAsset(scene);
#endif
    }
}