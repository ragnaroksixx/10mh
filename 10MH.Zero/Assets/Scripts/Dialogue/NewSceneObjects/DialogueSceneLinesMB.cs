﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using UnityEngine.Search;

public class DialogueSceneLinesMB : MonoBehaviour, IDialogueScene
{
    [TextArea(5, 100)]
    public string[] texts;

    string IDialogueScene.GetText()
    {
        string output = "[] ";
        foreach (string text in texts)
        {
            output += text;
            output += "\t";
        }
        return output;
    }
}
