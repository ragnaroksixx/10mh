﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System;

public class WorldTrigger : MonoBehaviour
{
    [SerializeField]
    protected UnityEvent onTriggerEnter;
    [SerializeField]
    protected UnityEvent onTriggerExit;
    [SerializeField]
    protected UnityEvent onDestroy;
    public bool triggerOnce;
    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Player") return;
        if (enabled)
        {
            onTriggerEnter?.Invoke();
            if (triggerOnce)
                enabled = false;
        }
    }
    protected virtual void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag != "Player") return;
        if (enabled)
        {
            onTriggerExit?.Invoke();
            if (triggerOnce)
                enabled = false;
        }
    }
    protected virtual void OnDestroy()
    {
        onDestroy?.Invoke();
    }
    public void RegisterTriggerEnterEvent(UnityAction e)
    {
        onTriggerEnter.AddListener(e);
    }
}
