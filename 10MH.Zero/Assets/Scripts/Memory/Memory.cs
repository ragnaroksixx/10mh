﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;

public class Memory : DialogueAction, IMemorable
{
    public string identifier;
    public bool isCyleMemory { get => tags.GetBool("cycle"); }

    public override string eventName => "memory";

    public string memoryCategory => tags.GetString("category", "");

    public string memoryID => ExpressTag;

    public override IEnumerator Process(Layer layer)
    {
        identifier = tags.GetExpressString();
        yield return base.Process(layer);
        if (isCyleMemory)
            CycleManager.Instance.AddTempEvent(this);
        else
            CycleManager.Instance.AddPermanentEvent(this);
    }
}

public class RemoveMemory : DialogueAction
{
    public string identifier;

    public override string eventName => "remove memory";
    public bool isPermanentMemory { get => tags.GetBool("perma"); }

    public override IEnumerator Process(Layer layer)
    {
        identifier = tags.GetExpressString();
        yield return base.Process(layer);
       // if (!isPermanentMemory)
       //     CycleManager.Instance.RemoveTempEvent(ExpressTag);
      //  else
        //    CycleManager.Instance.RemovePermanentEvent(ExpressTag);
    }
}
