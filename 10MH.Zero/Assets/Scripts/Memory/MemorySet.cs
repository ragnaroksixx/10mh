﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MemorySet
{
    const string memKey = "memKey";
    public const string FIRST_TIME = "cherry";

    Dictionary<string, List<string>> memories;
    Dictionary<string, List<ItemPlacementData>> cacheInventories;
    Dictionary<string, int> eventValues;//Temp only
    const string cacheInvKey = "cacheinvKey";

    public int Count => memories.Count;
    public MemorySet()
    {
        memories = new Dictionary<string, List<string>>();
        cacheInventories = new Dictionary<string, List<ItemPlacementData>>();
        eventValues = new Dictionary<string, int>();
    }

    public void Clear()
    {
        memories.Clear();
    }

    public void Add(IMemorable mem)
    {
        Add(mem.memoryCategory, mem.memoryID);
    }
    public void Remove(IMemorable mem)
    {
        Remove(mem.memoryCategory, mem.memoryID);
    }
    public void Add(string category, string memory)
    {
        category = category.ToLower();
        memory = memory.ToLower();

        List<string> set;
        if (!memories.ContainsKey(category))
        {
            set = new List<string>();
            memories.Add(category, set);
        }
        else
        {
            set = memories[category];
        }
        if (!set.Contains(memory))
        {
            set.Add(memory);
            memories[category] = set;
            Publisher.Raise(new OnMemoryGainedEvent(memory));
        }
    }

    public void Add(string key, int value)
    {
        if (eventValues.ContainsKey(key))
        {
            eventValues[key] = value;
        }
        else
            eventValues.Add(key, value);
    }

    public void Remove(string category, string memory)
    {
        category = category.ToLower();
        memory = memory.ToLower();

        if (!memories.ContainsKey(category))
            return;
        List<string> set = memories[category];
        set.Remove(memory);
        memories[category] = set;
    }
    public bool HasEvent(string category, string memory)
    {
        category = category.ToLower();
        memory = memory.ToLower();

        if (!memories.ContainsKey(category))
            return false;
        return memories[category].Contains(memory);
    }
    public bool HasEvent(IMemorable mem)
    {
        return HasEvent(mem.memoryCategory, mem.memoryID);
    }
    public int GetEventValue(string key,int defaultValue)
    {
        return eventValues.ContainsKey(key) ? eventValues[key] : defaultValue;
    }

    public bool HasInventory(string key)
    {
        return cacheInventories.ContainsKey(key);
    }
    public void UpdateInventory(string key, List<ItemPlacementData> value)
    {
        cacheInventories[key] = value;
    }
    public List<ItemPlacementData> GetInventory(string key)
    {
        return GetInventory(key, new ItemPlacementData[0]);
    }
    public List<ItemPlacementData> GetInventory(string key, params ItemPlacementData[] defaultItems)
    {
        if (!cacheInventories.ContainsKey(key))
            cacheInventories.Add(key, new List<ItemPlacementData>(defaultItems));
        return cacheInventories[key];
    }

    public static void ToHastable(MemorySet m, Hashtable mainHashTable)
    {
        Hashtable h = new Hashtable();
        foreach (KeyValuePair<string, List<string>> item in m.memories)
        {
            ArrayList list = new ArrayList(item.Value);
            h.Add(item.Key, list);
        }

        Hashtable cacheInventoryList = new Hashtable();
        foreach (KeyValuePair<string, List<ItemPlacementData>> kvp in m.cacheInventories)
        {
            ArrayList cacheInv = new ArrayList();
            foreach (ItemPlacementData ipd in kvp.Value)
            {
                cacheInv.Add(ipd.ToHashtable());
            }
            cacheInventoryList.Add(kvp.Key, cacheInv);
        }

        mainHashTable.Add(cacheInvKey, cacheInventoryList);
        mainHashTable.Add(memKey, h);
    }

    public static MemorySet FromHashtable(Hashtable mainHastable)
    {
        Hashtable memoryHash = mainHastable.GetHashTable(memKey);

        MemorySet result = new MemorySet();
        foreach (object key in memoryHash.Keys)
        {
            string value = key as string;
            result.memories.Add(value, new List<string>());
            foreach (object obj in memoryHash.GetArrayList(value))
            {
                result.memories[value].Add(obj as string);
            }
        }

        result.cacheInventories = new Dictionary<string, List<ItemPlacementData>>();
        Hashtable chestInvHashTables = mainHastable.GetHashTable(cacheInvKey);
        foreach (object key in chestInvHashTables.Keys)
        {
            string value = key as string;
            result.cacheInventories.Add(value, new List<ItemPlacementData>());
            foreach (object item in chestInvHashTables.GetArrayList(value))
            {
                result.cacheInventories[value].Add(new ItemPlacementData(item as Hashtable));
            }
        }

        return result;
    }
}

public interface IMemorable
{
    string memoryCategory { get; }
    string memoryID { get; }
}