﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class BasicMemoryListener : MemoryListener
{
    public string category = "";
    public string memory;
    public UnityEvent onMemoryGainCallback;

    public override void OnMemoryGained()
    {
        if (CycleManager.Instance.HasEvent(category, memory))
        {
            onMemoryGainCallback?.Invoke();
            Destroy(this);
        }
    }
}
