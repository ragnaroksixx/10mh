﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public abstract class MemoryListener : MonoBehaviour
{

    protected virtual void Awake()
    {
        Publisher.Subscribe<OnMemoryGainedEvent>(OnMemoryGained);
    }
    protected virtual void Start()
    {
        OnMemoryGained();
    }
    protected virtual void OnDestroy()
    {
        Publisher.Unsubscribe<OnMemoryGainedEvent>(OnMemoryGained);
    }
    protected virtual void OnDisable()
    {
        Publisher.Unsubscribe<OnMemoryGainedEvent>(OnMemoryGained);
    }
    private void OnMemoryGained(OnMemoryGainedEvent e)
    {
        OnMemoryGained();
    }
    public abstract void OnMemoryGained();
}

public class OnMemoryGainedEvent : PublisherEvent
{
    public string memory;

    public OnMemoryGainedEvent(string memory)
    {
        this.memory = memory;
    }
}

