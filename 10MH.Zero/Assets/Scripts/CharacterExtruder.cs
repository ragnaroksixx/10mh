﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

public class CharacterExtruder : Extruder
{
    [Button("Create/Update Mesh")]
    public void UpdateMeshEditor()
    {
        Init();
    }

    public void UpdateMeshPoints()
    {
        UpdateMesh();
    }
}
