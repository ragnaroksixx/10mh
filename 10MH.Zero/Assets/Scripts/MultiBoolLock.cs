﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class MultiBoolLock
{
    List<Object> locks;
    public bool Value
    {
        get => locks.Count > 0;
    }
    public MultiBoolLock()
    {
        locks = new List<object>();
    }
    public void AddLock(object o)
    {
        if (!locks.Contains(o))
            locks.Add(o);
    }
    public bool HasLock(object o)
    {
        return locks.Contains(o);
    }
    public void RemoveLock(object o)
    {
        locks.Remove(o);
    }
    public void ClearLocks()
    {
        locks.Clear();
    }
}

