﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneTransitionEffect : MonoBehaviour
{
    public CanvasGroup image;
    protected float transitionTime = 1;
    public static SceneTransitionEffect Instance;
    public static bool skipPlayerSpawn;

    public static void OnPlayerSpawned()
    {
        Instance.PlayerSpawned();
    }

    public static float waitTime = 0;
    // Start is called before the first frame update
    protected virtual void Awake()
    {
        Time.timeScale = 0;
        Instance = this;
        if(skipPlayerSpawn)
        {
            skipPlayerSpawn = false;
            OnPlayerSpawned();
        }

    }
    private void OnDestroy()
    {
        Instance = null;
    }
    protected virtual void PlayerSpawned()
    {
        WalkController10MH.RemoveUserInput(this);
        CameraController10MH.RemoveUserInput(this);
        Sequence s = DOTween.Sequence()
            .AppendInterval(waitTime)
            .AppendCallback(StartScene)
            .Append(image.DOFade(0, transitionTime))
            .OnComplete(OnComplete)
            .SetUpdate(true);

        s.Play();

    }
    protected virtual void StartScene()
    {
        Time.timeScale = 1;
        waitTime = 0;
        WalkController10MH.RestoreUserInput(this);
        CameraController10MH.RestoreUserInput(this);
    }
    protected void OnComplete()
    {
        SceneController.UnloadAreaTransition();
    }
}
