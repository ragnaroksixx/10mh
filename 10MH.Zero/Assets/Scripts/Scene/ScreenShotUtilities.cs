﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Dreamteck.Splines.SplineSampleModifier;


public static class ScreenShotUtilities
{
    public static Dictionary<int, List<Texture2D>> cache = new Dictionary<int, List<Texture2D>>();
    public static float lastCapTime = 0;
    const int MAX_CACHE = 20;
    public static void CacheScreen()
    {
        CycleManager.Instance?.CacheScreen();
    }
    public static void CacheScreen(int key, Texture2D texture)
    {
        return;
        lastCapTime = Time.time;
        if (!cache.ContainsKey(key))
        {
            cache.Add(key, new List<Texture2D>());
        }
        cache[key].Add(texture);
        if (cache[key].Count > MAX_CACHE)
        {
            cache[key].RemoveAt(0);
        }
    }
    public static Sprite Last(int key)
    {
        if (!cache.ContainsKey(key))
            return null;
        if (cache[key].Count == 0)
            return null;
        return MakeSprite(cache[key][cache[key].Count - 1]);
    }
    public static List<Texture2D> FlatList()
    {
        List<Texture2D> result = new List<Texture2D>();
        for (int i = 0; i < 10; i++)
        {
            if (cache.ContainsKey(i))
                result.AddRange(cache[i]);
        }
        return result;
    }
    public static Sprite MakeSprite(Texture2D texture)
    {
        return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);

    }
    public static void ClearCache()
    {
        cache.Clear();
        lastCapTime = 0;
    }
    public static Texture2D ScreenShotTexture(Camera camera, int resWidth, int resHeight)
    {
        if (resHeight == -1)
            resHeight = Screen.height;
        if (resWidth == -1)
            resWidth = Screen.width;

        RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
        camera.targetTexture = rt;

        Texture2D texture = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
        camera.Render();
        RenderTexture.active = rt;

        texture.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);

        camera.targetTexture = null;
        RenderTexture.active = null; // JC: added to avoid errors
        if (Application.isPlaying)
            MonoBehaviour.Destroy(rt);
        else
            MonoBehaviour.DestroyImmediate(rt);

        return texture;
    }

    public static Sprite ScreenShotSprite(Camera camera, int resWidth, int resHeight)
    {
        Texture2D texture = ScreenShotTexture(camera, resWidth, resHeight);

        return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
    }
}
