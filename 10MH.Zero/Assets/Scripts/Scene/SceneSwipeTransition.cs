﻿using DG.Tweening;
using Febucci.UI;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SceneSwipeTransition : SceneTransitionEffect
{
    public Image black, screenShotImage;
    public LinearOnScreenAnimator blackAnim, screenShotAnim;
    public TextAnimatorPlayer typer;
    Coroutine loading;

    protected override void Awake()
    {
        base.Awake();        
        screenShotImage.sprite = ScreenShotUtilities.Last(TimeController.Instance.CurrentTime.minute);
        blackAnim.AnimateOnScreenCustomPos(0);
        loading = StartCoroutine(ShowLoading());
    }
    private IEnumerator ShowLoading()
    {
        yield return new WaitForSeconds(2);
        typer.SetTypewriterSpeed(0.4f);
        typer.ShowText("LOADING.");
    }
    protected override void PlayerSpawned()
    {
        WalkController10MH.RemoveUserInput(this);
        CameraController10MH.RemoveUserInput(this);
        //base.PlayerSpawned();
        Sequence s = DOTween.Sequence()
            .AppendInterval(Mathf.Max(blackAnim.transitionSpeed, waitTime))
            .AppendCallback(StartScene)
            .AppendInterval(screenShotAnim.transitionSpeed)
            .OnComplete(OnComplete)
            .SetUpdate(true);

        s.Play();
    }
    protected override void StartScene()
    {
        base.StartScene();
        StopCoroutine(loading);
        typer.textAnimator.tmproText.DOFade(0, 0.1f);
        screenShotAnim.AnimateOnScreen();
        blackAnim.AnimateOnScreen();
    }
}
