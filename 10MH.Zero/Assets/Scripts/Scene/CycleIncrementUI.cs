﻿using System.Collections;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.Playables;

public class CycleIncrementUI : MonoBehaviour
{
    public TMP_Text currentCycle, nextCycle;
    public Animator anim;
    public AnimationClip clip;
    public CanvasGroup cg;
    public IEnumerator Play()
    {
        currentCycle.text = SaveData.currentSave.numCycles.ToString();
        nextCycle.text = (SaveData.currentSave.numCycles + 1).ToString();
        PlayableGraph pg;
        AnimationPlayableUtilities.PlayClip(anim, clip, out pg);
        yield return new WaitForSecondsRealtime(clip.length);
        anim.enabled = false;
    }

    public void Hide(float transitionTime)
    {
        cg.DOFade(0, transitionTime);
    }
}
