﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewindTransition : SceneTransitionEffect
{
    public RectTransform listRoot;
    public Image firstImage;
    public CanvasGroup fadeToWhite;
    public AnimationCurve curve;
    public float minSpeed, maxSpeed;
    public float maxTimeRebase;
    public Image prefab;
    int sample = 20;
    List<Texture2D> texs;
    public CustomAudioClip clocktickSFX;
    public CycleIncrementUI incrementUI;
    protected override void Awake()
    {
        base.Awake();
        texs = new List<Texture2D>();
        List<Texture2D> flatList = ScreenShotUtilities.FlatList();
        int increment = flatList.Count / sample;
        if (increment >= 1)
        {
            for (int i = 0; i < flatList.Count; i += increment)
            {
                texs.Add(flatList[i]);
            }
        }
        else
        {
            texs = flatList;
        }
        firstImage.sprite = ScreenShotUtilities.MakeSprite(texs[texs.Count - 1]);


    }
    private IEnumerator Start()
    {
        float delay = 0.75f;
        AudioReference aRef = clocktickSFX.Play();
        for (int i = texs.Count - 2; i >= 0; i--)
        {
            yield return new WaitForSecondsRealtime(delay);
            delay = Mathf.Max(0.2f, delay - 0.1f);
            Image img = Instantiate(prefab, listRoot);
            img.sprite = ScreenShotUtilities.MakeSprite(texs[i]);
            SpinOut(img, 0);
        }

        aRef.Stop(1);
        fadeToWhite.DOFade(1, 1).SetUpdate(true);
        yield return new WaitForSecondsRealtime(1);

        listRoot.gameObject.SetActive(false);

        while (!isPlayerSpawned)
            yield return null;

        if (SaveData.currentSave.numCycles > 2)
            yield return incrementUI.Play();
        StartScene();
        fadeToWhite.DOFade(0, transitionTime);
        incrementUI.Hide(transitionTime);
        yield return new WaitForSeconds(transitionTime);
        OnComplete();

    }
    private void OnDestroy()
    {
        ScreenShotUtilities.ClearCache();
    }
    private void Update()
    {
    }

    public void SpinOut(Image i, float delay)
    {
        //i.rectTransform.DORotate(new Vector3(0, 0, 360), 1.25f).SetOptions(true)
        //    .SetRelative(true).SetLoops(100).SetDelay(delay).SetUpdate(true);
        i.rectTransform.DOScale(Vector3.one, 1).SetUpdate(true);
        // Destroy(i.gameObject, 1 + delay);
    }
    bool isPlayerSpawned = false;
    protected override void PlayerSpawned()
    {
        WalkController10MH.RemoveUserInput(this);
        CameraController10MH.RemoveUserInput(this);
        isPlayerSpawned = true;
    }
}
