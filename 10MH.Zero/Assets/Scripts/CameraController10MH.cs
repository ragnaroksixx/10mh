﻿using UnityEngine;
using System.Collections;
using CMF;
using Cinemachine;
using DG.Tweening;

public class CameraController10MH : CameraController
{
    public static CameraController10MH Instance;
    static CameraInput defaultUserInput;
    public ForwardCameraInput forwardCamInput;
    public MultiBoolLock cameraLocked;
    public CinemachineVirtualCamera vCam;
    public Camera Cam
    {
        get => cam;
    }
    protected override void Setup()
    {
        base.Setup();
        Instance = this;
        forwardCamInput.enabled = false;
        defaultUserInput = cameraInput;
        cameraLocked = new MultiBoolLock();
    }
    public static IEnumerator RemoveUserInputWRecenter(Object o)
    {
        if (!Instance.cameraLocked.Value)//is not already locked
        {
            yield return Instance.UseForwardCamera(true);
        }
        else
            yield return null;
        Instance.cameraLocked.AddLock(o);
    }
    public static void RemoveUserInput(Object o)
    {
        if (!Instance.cameraLocked.Value)//is not already locked
        {
            Instance.UseForwardCameraInstant();
        }
        Instance.StopZoom();
        Instance.cameraLocked.AddLock(o);
    }
    public static void RestoreUserInput(Object o)
    {
        if (!Instance.cameraLocked.Value)//is already unlocked
            return;
        Instance.cameraLocked.RemoveLock(o);
        if (!Instance.cameraLocked.Value)
            SetInput(defaultUserInput);
    }
    static void SetInput(CameraInput i)
    {
        Instance.cameraInput.enabled = false;
        Instance.cameraInput = i;
        Instance.cameraInput.enabled = true;
    }

    IEnumerator UseForwardCamera(bool recenter = true)
    {
        UseForwardCameraInstant();
        if (recenter)
        {
            //move camera to proper locattion in 1 second
            float cameraMoveDuration = 0.35f;
            float startX = GetCurrentXAngle();
            float startY = GetCurrentYAngle();
            float timeTrack = cameraMoveDuration;
            float x, y;
            while (timeTrack > 0)
            {
                timeTrack -= Time.deltaTime;
                x = Mathf.Lerp(startX, 0, (cameraMoveDuration - timeTrack) / cameraMoveDuration);
                y = Mathf.Lerp(startY, 0, (cameraMoveDuration - timeTrack) / cameraMoveDuration);
                SetRotationAngles(x, startY);
                yield return null;
            }
            SetRotationAngles(0, startY);
        }
    }
    void UseForwardCameraInstant()
    {
        SetInput(forwardCamInput);
    }
    public IEnumerator LookAtY(Vector3 objPos, float cameraMoveDuration = 0.35f)
    {
        objPos.y = transform.position.y;
        Vector3 toObj = objPos - transform.position;
        Vector3 fwd = transform.forward;
        float endY = Vector3.Angle(fwd, toObj);
        yield return LookAtY(endY, cameraMoveDuration);
    }
    public IEnumerator LookAtY(float endY, float cameraMoveDuration)
    {
        yield return LookAtXY(0, endY, cameraMoveDuration);
    }

    public IEnumerator LookAt(Vector3 objPos, float cameraMoveDuration = 0.35f)
    {
        float endX, endY;
        Vector3 toObj = objPos - transform.position;
        RotateTowardDirection(toObj, out endX, out endY);
        yield return LookAtXY(endX, endY, cameraMoveDuration);
    }

    public IEnumerator LookAtXY(float endX, float endY, float cameraMoveDuration = 0.35f)
    {
        SetInput(forwardCamInput);
        //move camera to proper locattion in 1 second
        float startX = GetCurrentXAngle();
        float startY = GetCurrentYAngle();

        float timeTrack = cameraMoveDuration;
        float x, y;
        while (timeTrack > 0)
        {
            timeTrack -= Time.deltaTime;
            x = Mathf.Lerp(startX, endX, (cameraMoveDuration - timeTrack) / cameraMoveDuration);
            y = Mathf.Lerp(startY, endY, (cameraMoveDuration - timeTrack) / cameraMoveDuration);
            SetRotationAngles(x, y);
            yield return null;
        }
        SetRotationAngles(endX, endY);
    }

    protected override void Update()
    {
        base.Update();
        if (zoom)
        {
            if (!Input.GetKey(KeyCode.Mouse1))
            {
                StopZoom();
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                Zoom();
            }
        }
    }
    bool zoom;
    Tween zoomTween;
    public void Zoom()
    {
        if (zoom) return;
        if (cameraLocked.Value) return; 
        zoomTween.Kill(false);
        zoomTween = DOTween.To(() => vCam.m_Lens.FieldOfView,
            x => vCam.m_Lens.FieldOfView = x,
            45,
            0.25f);
        zoom = true;
    }
    public void StopZoom()
    {
        if (!zoom) return;
        zoomTween.Kill(false);
        zoomTween = DOTween.To(() => vCam.m_Lens.FieldOfView,
            x => vCam.m_Lens.FieldOfView = x,
            90,
            0.25f);
        zoom = false;
    }
}
