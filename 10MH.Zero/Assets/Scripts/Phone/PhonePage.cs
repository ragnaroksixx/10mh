﻿using UnityEngine;
using System.Collections;

namespace NHG.Phone10MH
{
    public class PhonePage : MonoBehaviour
    {
        OnScreenAnimatorBASE anim;
        CoroutineHandler updateLoop;
        public virtual void Awake()
        {
            anim = GetComponent<OnScreenAnimatorBASE>();
            updateLoop = new CoroutineHandler(this);
        }
        public virtual void Start()
        {

        }
        public void Show(bool instant)
        {
            anim.AnimateOnScreen(null,instant);
            updateLoop.StartCoroutine(ActiveUpdate());
        }
        public void Hide(bool instant)
        {
            updateLoop.StopCoroutine();
            anim.AnimateOffScreen(null, instant);
            OnInactive();
        }
        IEnumerator ActiveUpdate()
        {
            yield return OnStartImpl();
            while (true)
            {
                ActiveUpdateImpl();
                yield return null;
            }
        }
        protected virtual IEnumerator OnStartImpl()
        {
            yield return null;
        }
        protected virtual void ActiveUpdateImpl()
        {
        }
        protected virtual void OnInactive()
        {

        }
    }
}
