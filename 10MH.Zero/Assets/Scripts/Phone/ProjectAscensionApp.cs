﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace NHG.Phone10MH
{
    public class ProjectAscensionApp : PhonePage
    {
        public TMP_Text notificationText;

        public override void Start()
        {
            base.Start();
            TimeController.AddTimeEvent(TimeController.Instance.calamityTime - new TimeRef(3),
                () => ShowMessage("Power Malfunction Detected..."));

            TimeController.AddTimeEvent(TimeController.Instance.calamityTime + new TimeRef(1),
                () => ShowMessage("Restoring Power..."));

            TimeController.AddTimeEvent(TimeController.Instance.calamityTime + new TimeRef(5),
                () => ShowMessage("Power Restored"));
            ShowMessage("");
        }
        public void ShowMessage(string message)
        {
            notificationText.text = message;
        }

    }
}
