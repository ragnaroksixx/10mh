﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NHG.Phone10MH
{
    public class MusicAppPage : PhonePage
    {
        public Button playButton,
            prevButton,
            nextButton;
        public TMP_Text songName,
            artistName;

        public Image scrubberImage;
        public Image playImage;
        public Sprite playSprite, pauseSprite;
        public RectTransform cdTransform;
        public override void Start()
        {
            base.Start();
            playButton.onClick.AddListener(PlayStop);
            nextButton.onClick.AddListener(MusicPlayer.Instance.PlayNext);
            prevButton.onClick.AddListener(MusicPlayer.Instance.PlayPrev);
            Publisher.Subscribe<MusicChangeEvent>(OnSongChanged);
        }
        private void OnDestroy()
        {
            Publisher.Unsubscribe<MusicChangeEvent>(OnSongChanged);
        }
        protected override void ActiveUpdateImpl()
        {
            base.ActiveUpdateImpl();

            if (MusicPlayer.Instance.AudioRef != null)
            {
                scrubberImage.fillAmount =
                    MusicPlayer.Instance.AudioRef.audioObject.Source.time
                    / MusicPlayer.Instance.AudioRef.clip.AudioInfo.clip.length;
                if(!MusicPlayer.Instance.AudioRef.audioObject.IsPaused)
                {
                    cdTransform.transform.localEulerAngles = new Vector3(0,0,
                        Mathf.Repeat(Time.time*180,360));
                }
            }
        }
        public void PlayStop()
        {
            AudioReference audioRef = MusicPlayer.Instance.AudioRef;
            if (audioRef == null) return;
            if (audioRef.audioObject.IsPaused)
            {
                MusicPlayer.Instance.Resume();
                playImage.sprite = pauseSprite;
            }
            else
            {
                MusicPlayer.Instance.Pause();
                playImage.sprite = playSprite;
            }

        }
        public void OnSongChanged(MusicChangeEvent e)
        {
            AudioReference audioRef = MusicPlayer.Instance.AudioRef;
            if (audioRef != null)
            {

                playButton.interactable = true;

                prevButton.interactable =
                nextButton.interactable = true;// MusicPlayer.Instance.TrackCount > 1;

                songName.text = "";// cac!=null&& string.IsNullOrEmpty(cac.displayName) ? audioRef.clip.name : audioRef.clip.displayName;
                artistName.text = "";// cac != null && string.IsNullOrEmpty(cac.displayArtist) ? "---" : audioRef.clip.displayArtist;

                playImage.sprite = pauseSprite;
            }
            else
            {
                playImage.sprite = playSprite;

                playButton.interactable =
                prevButton.interactable =
                nextButton.interactable = false;
                songName.text = "";
                artistName.text = "";
                scrubberImage.fillAmount = 0;
            }
        }
    }
}
