﻿using UnityEngine;
using System.Collections;
using System;

public class HomeScreenAnimator : OnScreenAnimatorBASE
{
    public CanvasGroup group;
    protected override void OffScreenImpl(Action onComplete, bool instant)
    {
        group.blocksRaycasts = false;
    }

    protected override void OffScreenImplEDITOR()
    {
        group.blocksRaycasts = false;
    }

    protected override void OnScreenImpl(Action onComplete, bool instant)
    {
        group.blocksRaycasts = true;
    }

    protected override void OnScreenImplEDITOR()
    {
        group.blocksRaycasts = true;
    }
}
