﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine.UI;
namespace NHG.Phone10MH
{
    public class PhoneApp : MonoBehaviour, IMemorable
    {
        public Phone phone;

        public string appName;
        public Sprite sprite;
        public Color appColor = Color.white;

        public TMP_Text nameText;
        public Image bgImage, iconImage;

        public PhonePage toPage;
        public AudioVariable openSFX;

        public string memoryCategory => "phone";

        public string memoryID => appName;

        public void OnClick()
        {
            if (!Application.isPlaying)
                return;
            if (toPage == null) return;
            phone.NextPage(toPage);
            openSFX.Play();
        }
        [Button]
        public void Set()
        {
            if (!string.IsNullOrEmpty(appName))
            {
                nameText.text = appName;
                name = appName + " App";
            }
            bgImage.color = appColor;
            if (sprite)
                iconImage.sprite = sprite;

#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                UnityEditor.EditorUtility.SetDirty(gameObject);
                UnityEditor.EditorUtility.SetDirty(nameText);
                UnityEditor.EditorUtility.SetDirty(bgImage);
                UnityEditor.EditorUtility.SetDirty(iconImage);
            }
#endif
        }
    }
}
