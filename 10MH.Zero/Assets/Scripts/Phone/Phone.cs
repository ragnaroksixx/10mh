﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NHG.Phone10MH;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class Phone : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public static Phone Instance;
    bool isOnPhone = false;
    public static bool IsOnPhone
    {
        get => Instance.isOnPhone;
        set => Instance.isOnPhone = value;
    }
    public RotationOnScreenAnimator rotAnim;
    public LinearOnScreenAnimator peekAnim;
    Stack<PhonePage> pageStack;
    public PhonePage homePage, lockScreen;
    public AudioVariable homeSFX, backSFX;
    PhonePage currentPage => pageStack.Count == 0 ? null : pageStack.Peek();
    public TMP_Text timeText, homeTime, lockScreenTimeText;
    public TMP_Text batteryText;
    public Image peekImage;
    public GameObject powerButton;
    public CanvasGroup systemButtons;
    public List<TextChain> messages;
    public AppSpashScreen splashScreen;
    public bool IsOpen => peekAnim.IsOnScreen;
    private void Awake()
    {
        Instance = this;
    }
    // Use this for initialization
    void Start()
    {
        rotAnim.AnimateOffScreen(null, true);
        peekAnim.AnimateOffScreen(null, true);

        pageStack = new Stack<PhonePage>();
        Home(false);
        LockScreen();
        Publisher.Subscribe<InventoryOpenCloseEvent>(OnInventoryOpenClose);
        rotAnim.gameObject.SetActive(false);
    }

    public void LockScreen()
    {
        if (currentPage == lockScreen)
            return;
        NextPage(lockScreen, true);
    }

    public bool IsOnLockScreen()
    {
        return currentPage == lockScreen;
    }

    private void OnDestroy()
    {
        IsOnPhone = false;
        Publisher.Unsubscribe<InventoryOpenCloseEvent>(OnInventoryOpenClose);
    }

    public void SetSystemButtons(bool v)
    {
        systemButtons.interactable = v;
        powerButton.gameObject.SetActive(v);
    }

    // Update is called once per frame
    void Update()
    {
        if (!PlayerInventory.HasPermanentInvItem("phone"))
            return;
        if (rotAnim.IsOnScreen)
        {
            timeText.text = TimeController.Instance.CurrentTime.FormatString(TimeController.START_HOUR, timeText);
            homeTime.text = TimeController.Instance.CurrentTime.FormatString(TimeController.START_HOUR, homeTime);
            lockScreenTimeText.text = TimeController.Instance.CurrentTime.FormatString(TimeController.START_HOUR, lockScreenTimeText);
            batteryText.text = (CycleManager.PlayerCombatUnit.HPRatio() * 100).ToString("00") + "%";
        }/*
        if (InputSystem10MH.phoneOpenClose.GetDown())
        {
        ToggleOpen();
        }*/
    }
    public void ToggleOpen()
    {
        if (rotAnim.IsOnScreen)
        {
            Close();
        }
        else
        {
            Open();
        }
    }
    public void Open(bool blockPlayerInput = false)
    {
        if (peekAnim.IsOnScreen) return;
        HUDController.Instance.phoneQuickUI.OnUseStart();
        peekImage.raycastTarget = blockPlayerInput;
        rotAnim.gameObject.SetActive(true);
        if (!blockPlayerInput)
        {
            peekImage.DOFade(0, 0.25f);
        }
        powerButton.SetActive(true);
        rotAnim.AnimateOnScreen();
        peekAnim.AnimateOnScreen();
        currentPage.Show(true);

        CursorController.HideCursor(this);
        InteractionHandler.LockInteraction(this);
        Global10MH.ShowCursor(this);
        WalkController10MH.RemoveUserInput(this);
        CameraController10MH.RemoveUserInput(this);
        Publisher.Raise(new PhoneOpenCloseEvent(true));
    }
    public void LockInput()
    {
        peekImage.raycastTarget = true;
    }

    public void Open(PhonePage page)
    {
        NextPage(page, true);
        Open();
    }
    public void Open(string appName)
    {
        FindApp(appName).OnClick();
    }
    public PhoneApp FindApp(string appName)
    {
        PhoneApp[] apps = GameObject.FindObjectsOfType<PhoneApp>(true);
        foreach (PhoneApp app in apps)
        {
            if (app.appName.ToLower() == appName.ToLower())
            {
                return app;
            }
        }
        return null;
    }
    public void Close()
    {
        if (!peekAnim.IsOnScreen) return;

        HUDController.Instance.phoneQuickUI.OnUseEnd();
        peekImage.DOFade(0.25f, 0.25f);
        peekImage.raycastTarget = true;

        rotAnim.AnimateOffScreen(OnCloseComplete);
        peekAnim.AnimateOffScreen();

        CursorController.ShowCursor(this);
        Global10MH.HideCursor(this);
        InteractionHandler.RemoveInteractionLock(this);
        WalkController10MH.RestoreUserInput(this);
        CameraController10MH.RestoreUserInput(this);
        Publisher.Raise(new PhoneOpenCloseEvent(false));
    }

    void OnCloseComplete()
    {
        rotAnim.gameObject.SetActive(false);
    }
    public void OnInventoryOpenClose(InventoryOpenCloseEvent e)
    {
        if (!e.isOpen)
        {
            if (DialogueRenderer.Instance.isInDialogue &&
                DialogueRenderer.ActiveScene != null &&
                DialogueRenderer.ActiveScene.isPhoneScene)
                return;
            Close();
        }
    }
    public void Back()
    {
        if (currentPage == homePage)
            return;
        backSFX.Play();
        PhonePage prev = pageStack.Pop();
        SetPage(prev, currentPage);
    }
    public void Home(bool playAudio = true)
    {
        if (currentPage == homePage)
            return;
        if (playAudio)
            homeSFX.Play();
        PhonePage prev = currentPage;
        pageStack.Clear();
        pageStack.Push(homePage);
        SetPage(prev, homePage);
    }
    public void NextPage(PhonePage next, bool instant = false)
    {
        PhonePage prev = currentPage;
        pageStack.Push(next);
        SetPage(prev, next);
    }
    void SetPage(PhonePage prevPage, PhonePage nextPage, bool instant = false)
    {
        prevPage?.Hide(instant);
        nextPage.Show(instant);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        IsOnPhone = true;

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        IsOnPhone = false;
    }

    public void MainMenu()
    {
        SceneController.LoadStartMenu();
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void OpenTextChain(string name)
    {
        TextChain tc = GetTextChain(name);
        Publisher.Raise(new OpenTextChainEvent(tc));
    }
    public void OpenTextChain(TextChain tc)
    {
        OpenTextChain(tc.chatName);
    }

    public TextChain GetTextChain(string name)
    {
        foreach (TextChain textChain in messages)
        {
            if (textChain.chatName.ToLower() == name.ToLower())
            {
                return textChain;
            }
        }
        return null;
    }
}

public class PhoneOpenCloseEvent : PublisherEvent
{
    public bool isOpen;

    public PhoneOpenCloseEvent(bool open)
    {
        isOpen = open;
    }
}