﻿using DG.Tweening;
using NHG.Phone10MH;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MessagePage : PhonePage
{
    public TextChain chat;
    public Transform listRoot;
    public TextMessage prefab, replyPrefab;
    public ScrollRect scrollView;
    public TMP_Text chatName;
    public Button replyButton;
    public TMP_Text[] smsNotificationUIs;
    List<TextChain> unreadMessages;
    public override void Awake()
    {
        base.Awake();
        Publisher.Subscribe<OpenTextChainEvent>(OpenTextChain);
        Publisher.Subscribe<TextChainUpdateEvent>(OnTextUpdated);
        replyButton.onClick.AddListener(Reply);
        unreadMessages = new List<TextChain>();
        SetNotificationText(0);
    }
    public override void Start()
    {
        base.Start();
        if (CycleManager.IsFirstCycle())
            AddNotications(Phone.Instance.messages[0]);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<OpenTextChainEvent>(OpenTextChain);
        Publisher.Unsubscribe<TextChainUpdateEvent>(OnTextUpdated);
    }
    public void AddNotications(TextChain tc)
    {
        if (!unreadMessages.Contains(tc))
            unreadMessages.Add(tc);

        SetNotificationText(unreadMessages.Count);
    }
    public void RemoveNotication(TextChain tc)
    {
        unreadMessages.Remove(tc);
        SetNotificationText(unreadMessages.Count);
    }
    void SetNotificationText(int count)
    {
        foreach (TMP_Text smsNotificationUI in smsNotificationUIs)
        {
            smsNotificationUI.transform.parent.gameObject.SetActive(count > 0);
            smsNotificationUI.text = count.ToString();
        }
    }
    private void OpenTextChain(OpenTextChainEvent e)
    {
        chat = e.tChain;
        RemoveNotication(chat);
        if (Phone.Instance.IsOpen)
            Phone.Instance.NextPage(this);
        else
            Phone.Instance.Open(this);
    }
    protected override IEnumerator OnStartImpl()
    {
        chatName.text = chat.chatName;
        replyButton.interactable = chat.AwaitingReply() && !DialogueRenderer.Instance.isInDialogue;
        yield return base.OnStartImpl();
        foreach (Transform child in listRoot)
        {
            GameObject.Destroy(child.gameObject);
        }
        yield return null;


        yield return SpawnMessages(chat.messages.ToArray(), true);

    }
    protected override void ActiveUpdateImpl()
    {
        replyButton.interactable = chat.AwaitingReply() && !DialogueRenderer.Instance.isInDialogue;
        base.ActiveUpdateImpl();
    }
    public void Reply()
    {
        if (chat.AwaitingReply() && !DialogueRenderer.Instance.isInDialogue)
        {
            Phone.Instance.SetSystemButtons(false);
            chat.Reply();
        }
    }
    protected override void OnInactive()
    {
        base.OnInactive();
        chat = null;
    }
    private void OnTextUpdated(TextChainUpdateEvent e)
    {
        if (e.tChain != chat)
        {
            AddNotications(e.tChain);
            return;
        }
        StartCoroutine(SpawnMessage(e.sms, false));
    }

    IEnumerator SpawnMessage(TextMessageData tmd, bool instant)
    {
        TextMessage prefabUse = tmd.sender == Player.Instance.characterData ? replyPrefab : prefab;
        TextMessage tm = GameObject.Instantiate(prefabUse, listRoot);
        tm.Init(tmd);
        yield return tm.Write(instant);

        scrollView.DOVerticalNormalizedPos(0, 1.0f);
    }

    IEnumerator SpawnMessages(TextMessageData[] tmds, bool instant)
    {
        foreach (TextMessageData tmd in tmds)
        {
            TextMessage prefabUse = tmd.sender == Player.Instance.characterData ? replyPrefab : prefab;
            TextMessage tm = GameObject.Instantiate(prefabUse, listRoot);
            tm.Init(tmd);
            yield return tm.Write(instant);
        }

        scrollView.DOVerticalNormalizedPos(0, 0.5f);
    }
}
