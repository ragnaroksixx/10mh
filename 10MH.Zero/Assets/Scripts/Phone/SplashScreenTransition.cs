﻿using NHG.Phone10MH;
using System;
using System.Collections;
using UnityEngine;


public class SplashScreenTransition : ScaleOnScreenAnimator
{
    public PhoneApp app;
    CoroutineHandler coroutine;
    protected override void Awake()
    {
        base.Awake();
        coroutine = new CoroutineHandler(this);
    }
    protected override void OffScreenImpl(Action onComplete, bool instant)
    {
        base.OffScreenImpl(onComplete, instant);
    }


    protected override void OnScreenImpl(Action onComplete, bool instant)
    {
        if (app != null && !CycleManager.Instance.HasEvent(app))
        {
            CycleManager.Instance.AddTempEvent(app);
            coroutine.StartCoroutine(app.phone.splashScreen.Show(app),
                () => base.OnScreenImpl(onComplete, true));
        }
        else
        {
            base.OnScreenImpl(onComplete, instant);
        }
    }
}
