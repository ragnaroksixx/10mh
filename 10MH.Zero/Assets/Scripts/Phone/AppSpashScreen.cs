﻿using DG.Tweening;
using NHG.Phone10MH;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AppSpashScreen : MonoBehaviour
{
    public Image appImage;
    public Image pageBG, appBG;
    public CanvasGroup cg;
    private void Awake()
    {
        cg.blocksRaycasts = cg.interactable = false;
    }
    public IEnumerator Show(PhoneApp app)
    {
        cg.blocksRaycasts = cg.interactable = true;
        appImage.sprite = app.sprite;
        appBG.color = app.appColor;
        cg.alpha = 0;
        transform.GetChild(0).localScale = Vector3.one * 0.5f;
        cg.DOFade(1, 0.25f);
        transform.GetChild(0).DOScale(Vector3.one, 0.25f);
        yield return new WaitForSeconds(.6f);
        cg.DOFade(0, 0.25f);
        cg.blocksRaycasts = cg.interactable = false;
    }
}
