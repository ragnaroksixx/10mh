﻿using NHG.Phone10MH;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MessagesHomePage : PhonePage
{
    public TextGroupUI messgaeGroupPrefab;
    public Transform listRoot;
    protected override IEnumerator OnStartImpl()
    {
        yield return base.OnStartImpl();
        SpawnMessages();
    }
    public void SpawnMessages()
    {
        foreach (Transform child in listRoot)
        {
            GameObject.Destroy(child.gameObject);
        }

        foreach (TextChain tc in Phone.Instance.messages)
        {
            TextGroupUI tm = GameObject.Instantiate(messgaeGroupPrefab, listRoot);
            tm.Set(tc);
        }
    }

}
