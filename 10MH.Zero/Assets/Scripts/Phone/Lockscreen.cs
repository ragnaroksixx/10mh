﻿using DG.Tweening;
using NHG.Phone10MH;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Lockscreen : PhonePage
{
    public Image unlockEffect;
    public Sequence unlockSequence;
    public CustomAudioClip unlockSFX;
    private void OnEnable()
    {
        base.ActiveUpdateImpl();
        unlockSequence = DOTween.Sequence();
        unlockSequence.AppendInterval(4)
            .Append(unlockEffect.rectTransform.DOScale(Vector3.one, 0.25f))
            .Append(unlockEffect.transform.parent.GetChild(0).DOShakeRotation(1.0f, 15, 5))
            .AppendInterval(4f)
            .Append(unlockEffect.rectTransform.DOScale(Vector3.zero, 1))
            .SetLoops(-1);
        unlockSequence.Play();
    }
    private void OnDisable()
    {
        unlockSequence.Kill(true);
    }
    protected override IEnumerator OnStartImpl()
    {
        yield return base.OnStartImpl();
    }
    protected override void OnInactive()
    {
        base.OnInactive();
    }
}
