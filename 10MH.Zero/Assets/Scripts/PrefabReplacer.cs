﻿using Sirenix.OdinInspector;
using System.Collections;

using UnityEngine;

public class PrefabReplacer : MonoBehaviour
{
    public GameObject prefabToReplaceWith;
    

    [Button]
    public void Replace()
    {
#if UNITY_EDITOR
        GameObject obj = UnityEditor.PrefabUtility.InstantiatePrefab(prefabToReplaceWith) as GameObject;
        obj.transform.SetParent(transform.parent);
        obj.transform.localPosition = transform.localPosition;
        obj.transform.localRotation = transform.localRotation;
        obj.transform.localScale = transform.localScale;
#endif
    }
}
