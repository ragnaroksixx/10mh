﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboarding : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Player.Instance == null)
            return;
        Vector3 target = Player.Instance.transform.position;
        target.y = transform.position.y;
        transform.LookAt(target);
    }
}
