﻿using UnityEngine;
using System.Collections;

public class ItemPickUpInteraction : Interaction
{
    public TextAsset preDialogueScene;
    public TextAsset postDialogueScene;
    public string onItemsTakenMemory;
    public ItemPlacementData item;
    public InventoryPropertyData invData;
    bool exit = false;
    CoroutineHandler loop;
    public Inventory customInventoryPrefab = null;
    protected Inventory invCache;
    public GameObject onDestroy;
    public bool isPerma;

    public override void Awake()
    {
        base.Awake();
        loop = new CoroutineHandler(this);
        if(onDestroy==null)
        {
            onDestroy = gameObject;
        }
        if (CycleManager.Instance.HasEvent("", ID()))
        {
            DestoryThis();
        }
    }
    public override void OnInteract()
    {
        loop.StartCoroutine(InteractLoop());
    }
    public virtual string ID()
    {
        return "PickedUp" + gameObject.name + item.item.Identifier;
    }
    public virtual ItemPlacementData GetItem()
    {
        return item;
    }
    public IEnumerator InteractLoop()
    {
        exit = false;
        InteractionHandler.LockInteraction(this);
        WalkController10MH.RemoveUserInput(this);
        CameraController10MH.RemoveUserInput(this);

        if (preDialogueScene != null)
            yield return DialogueRenderer.Instance.PlayScene(preDialogueScene, null);

        invCache = GetInventory();
        InventoryManager.Instance.OpenInventory();
        HUDController.Instance.ShowInventoryHelp();
        while (MainLoop(invCache))
        {
            yield return null;
        }
        OnClose();
        InventoryManager.DestoryInventory(invCache);
        InventoryManager.Instance.CloseInventory();

        if (postDialogueScene != null)
            yield return DialogueRenderer.Instance.PlayScene(postDialogueScene, null);

        CameraController10MH.RestoreUserInput(this);
        WalkController10MH.RestoreUserInput(this);
        InteractionHandler.RemoveInteractionLock(this);
    }
    protected virtual void OnClose()
    {
        if (invCache.inventoryItems.Count <= 0)
        {
            if (isPerma)
            {
                CycleManager.Instance.AddPermanentEvent("", ID());
                CycleManager.Instance.AddPermanentEvent("", onItemsTakenMemory);
            }
            else
            {
                CycleManager.Instance.AddTempEvent("", ID());
                CycleManager.Instance.AddTempEvent("", onItemsTakenMemory);
            }
            DestoryThis();
        }
    }
    public virtual void DestoryThis()
    {
        Destroy(onDestroy);
    }

    public virtual bool MainLoop(Inventory inv)
    {
        if (InventoryManager.IsItemHeld())
            return true;
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            return false;
        }
        return true;
    }
    public virtual Inventory GetInventory()
    {
        return InventoryManager.SpawnOtherInventory("", false, invData, true, null, GetItem());
    }
}
