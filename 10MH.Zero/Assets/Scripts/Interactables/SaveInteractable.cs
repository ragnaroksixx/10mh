﻿using UnityEngine;
using System.Collections;

public class SaveInteractable : Interaction
{
    public override void Awake()
    {
        base.Awake();
        if (CycleManager.IsIntroCycles())
            Destroy(this);
    }
    public override void OnInteract()
    {
        SaveData.SaveGame();
        CycleManager.Instance.RestartCycle();
    }
}
