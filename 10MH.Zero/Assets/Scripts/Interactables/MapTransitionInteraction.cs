﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;

public class MapTransitionInteraction : Interaction
{
    public AreaTransitionData mapTransitionInfo;
    public override void Awake()
    {
        base.Awake();
    }
    public override void OnInteract()
    {
        if (mapTransitionInfo == null)
            throw new System.Exception("Map transition isn't set you lazy f*ck. How so very dare you.");

        Scene pathScene = null;
        if (!PathManager.CanUse(mapTransitionInfo, out pathScene))
        {
            PlayScene(pathScene, null, false);
            return;
        }

        interactable.doOnce = false;
        Global10MH.SetCachePose(Player.GetPose());
        InteractionHandler.Instance.ClearInteract();
        SceneController.StartMapScene();
    }
}
