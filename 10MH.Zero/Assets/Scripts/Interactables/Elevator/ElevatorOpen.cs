﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ElevatorOpen : InspectInteraction
{
    public ElevatorFloor elevator;
    public override void OnInteract()
    {
        if (TimeController.IsCalamity())
        {
            base.OnInteract();
        }
        else
        {
            elevator.CallElevator();
        }
    }
}
