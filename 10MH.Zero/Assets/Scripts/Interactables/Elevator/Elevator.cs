﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;
using System.Collections.Generic;

public class Elevator : MonoBehaviour
{
    public string evelatorKeyName;
    public TextAsset calamityLockedText;
    public ElevatorDoor left, right;

    public LightPulse elevatorLight;
    public CustomAudioClip arriveBeep, doorSlideOpen, doorSlideClose, invalidSFX;
    public AudioReference doorSlide;
    public int currentFloorIndex => CycleManager.Instance.GetEventValue(evelatorKeyName, startingFloor);
    public ElevatorFloor currentFloor => isCurrentFloorInScene ? floors[currentFloorIndex] : null;
    public bool isCurrentFloorInScene => floors.ContainsKey(currentFloorIndex);

    public bool IsOpen { get => isOpen; set => isOpen = value; }
    public bool IsMoving { get => isSmoving; set => isSmoving = value; }

    public int startingFloor = 1;
    public Transform movableBit;
    bool isOpen;
    public Dictionary<int, ElevatorFloor> floors;

    public void SetFloor(int value)
    {
        CycleManager.Instance.AddTempEvent(evelatorKeyName, value);
    }
    private void Awake()
    {
        floors = new Dictionary<int, ElevatorFloor>();
    }
    // Use this for initialization
    IEnumerator Start()
    {
        //elevatorLight.intensity = 0;
        TimeController.TriggerOrAddCalamity(OnCalamity);
        left.Init(false);
        right.Init(true);

        yield return null;

        //int delta = currentFloor - floor;
        //Vector3 targetPos = movableBit.transform.position;
        //targetPos.y += (delta * 10);
        //movableBit.transform.position = targetPos;
    }
    private void OnDestroy()
    {
        TimeController.RemoveTimeEventCalamity(OnCalamity);
    }

    void OnCalamity()
    {
        AreaTransitionInteraction[] transitions
            = gameObject.GetComponentsInChildren<AreaTransitionInteraction>();
        foreach (AreaTransitionInteraction item in transitions)
        {
            item.forceLocked = true;
            item.lockedScene = calamityLockedText;
        }
    }

    public void CloseDoor()
    {
        arriveBeep.Play(transform.position);
        if (!isOpen) return;
        isOpen = false;

        left.Close(0);
        right.Close(0);
        currentFloor?.left.Close(0.5f);
        currentFloor?.right.Close(0.5f);
        doorSlide?.Stop();
        doorSlide = doorSlideClose.Play(transform.position);
    }

    public void OpenDoor()
    {
        arriveBeep.Play(transform.position);
        if (isOpen) return;
        isOpen = true;

        left.Open(0);
        right.Open(0);
        currentFloor?.left.Open(0.5f);
        currentFloor?.right.Open(0.5f);

        doorSlide?.Stop();
        doorSlide = doorSlideOpen.Play(transform.position);
    }

    public void Alarm()
    {
        elevatorLight.DOKill();
        elevatorLight.enabled = true;
    }
    public void StopAlarm()
    {
        elevatorLight.DOKill();
        elevatorLight.enabled = false;
        elevatorLight.light.DOIntensity(0, 0.5f);
    }
    bool isSmoving = false;
    public void GOOOOO(bool up, Action onComplete)
    {
        if (isSmoving) return;
        isSmoving = true;
        StartCoroutine(GoRoutine(up, onComplete));
    }
    public void GOOOOO(ElevatorFloor floor, Action onComplete = null)
    {
        if (isSmoving) return;
        isSmoving = true;
        StartCoroutine(GOFloor(floor, onComplete));
    }
    public void GOOOOO(int floor, Action onComplete = null)
    {
        if (isSmoving) return;
        isSmoving = true;
        StartCoroutine(GOFloor(floors[floor], onComplete));
    }
    public IEnumerator GoRoutine(bool up, Action onComplete)
    {
        TimeController.Instance.IsPaused.AddLock(this);
        CloseDoor();
        yield return new WaitForSeconds(1);
        float targetPos = 1.755f;
        if (up)
            targetPos += 10;
        else
            targetPos -= 10;
        movableBit.DOLocalMoveY(targetPos, 10);
        yield return new WaitForSeconds(3);
        TimeController.Instance.IsPaused.RemoveLock(this);
        onComplete?.Invoke();
        isSmoving = false;
    }

    public IEnumerator GOFloor(ElevatorFloor floor, Action onComplete)
    {
        TimeController.Instance.IsPaused.AddLock(this);
        CloseDoor();
        yield return new WaitForSeconds(2);
        floor.CallElevator(true);
        yield return new WaitForSeconds(3);
        TimeController.Instance.IsPaused.RemoveLock(this);
        onComplete?.Invoke();
        isSmoving = false;
        yield return new WaitForSeconds(6);
        CloseDoor();
    }
}

[System.Serializable]
public struct ElevatorDoor
{
    public Transform door;
    Vector3 startPos, endPos;

    public void Init(bool right)
    {
        startPos = endPos = door.localPosition;
        if (right)
            endPos.x += 1;
        else
            endPos.x -= 1;
    }
    public void Open(float delay)
    {
        door.DOLocalMove(endPos, 2)
    .SetEase(Ease.InOutCirc)
    .SetDelay(delay);
    }
    public void Close(float delay)
    {
        door.DOLocalMove(startPos, 2)
    .SetEase(Ease.InOutCirc)
    .SetDelay(delay);
    }
}