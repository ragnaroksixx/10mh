﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ElevatorPanel : MonoBehaviour
{
    public AreaTransitionData f1, f2, f3, FoN;
    public Elevator elevator;

    string code = "";
    bool codeMode = false;

    public AreaTransitionData r111, r112, r121, r122;

    public void F1Button()
    {
        if (codeMode)
        {
            AddCode("1");
        }
        else
        {
            if (elevator.currentFloorIndex == 1)
            {
                elevator.invalidSFX.Play(transform.position);
                return;
            }

            if (f1 == null)
            {
                elevator.GOOOOO(1);
            }
            else
            {
                elevator.SetFloor(1);
                Go(f1, false);
            }
        }
    }

    public void F2Button()
    {
        if (codeMode)
        {
            AddCode("2");
        }
        else
        {
            if (elevator.currentFloorIndex == 2)
            {
                elevator.invalidSFX.Play(transform.position);
                return;
            }
            bool isUp = elevator.currentFloorIndex < 2;

            if (f2 == null)
            {
                elevator.GOOOOO(2);
            }
            else
            {
                elevator.SetFloor(2);
                Go(f2, isUp);
            }
        }
    }

    public void RoofButton()
    {
        if (codeMode)
        {
            AddCode("r");
        }
        else
        {
            if (elevator.currentFloorIndex == 3)
            {
                elevator.invalidSFX.Play(transform.position);
                return;
            }

            if (f3 == null)
            {
                elevator.GOOOOO(3);
            }
            else
            {
                elevator.SetFloor(3);
                Go(f3, true);
            }

        }
    }

    public void FoNButton()
    {
        if (codeMode)
        {
            AddCode("n");
        }
        else
        {
            Go(FoN, true);
        }
    }

    public void OpenButton()
    {
        if (codeMode)
        {
            ClearCode();
        }
        else
        {
            elevator.OpenDoor();
        }
    }

    public void AlarmButton()
    {
        ClearCode();
        codeMode = !codeMode;

        if (codeMode)
            elevator.Alarm();
        else
            elevator.StopAlarm();
    }

    public void CloseButton()
    {
        if (codeMode)
        {
            TryCode();
            ClearCode();
        }
        else
        {
            elevator.CloseDoor();
        }
    }

    public void AddCode(string value)
    {
        code += value;
    }
    public void ClearCode()
    {
        code = "";
    }
    public void Go(AreaTransitionData atd, bool up = true)
    {
        NHG.Dialogue.Scene pathScene = null;
        if (!PathManager.CanUse(atd, out pathScene))
        {
            DialogueRenderer.Instance.PlayScene(pathScene,null);
            return;
        }
        elevator.GOOOOO(up, () => { ActuallyGO(atd); });
        //Close();
    }
    void ActuallyGO(AreaTransitionData atd)
    {
        InteractionHandler.Instance.ClearInteract();
        SceneController.SetActiveEnvironment(atd);
    }

    public void GoFloor(int floor)
    {

    }
    public IEnumerator GoFloorCO(int floor)
    {
        elevator.CloseDoor();
        yield return new WaitForSeconds(1);
    }
    public void TryCode()
    {
        switch (code)
        {
            case "r111":
                Go(r111);
                break;
            case "r112":
                Go(r112);
                break;
            case "r121":
                Go(r121);
                break;
            case "r122":
                Go(r122);
                break;
            default:
                break;
        }
    }
}
