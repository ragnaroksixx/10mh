﻿using DG.Tweening;
using System.Collections;
using UnityEngine;


public class ElevatorFloor : MonoBehaviour
{
    public Elevator elevator;
    public Transform moveRoot;
    public ElevatorDoor left, right;
    public int floor = -1;

    // Use this for initialization
    void Start()
    {
        if (floor == -1)
            return;
        left.Init(false);
        right.Init(true);
        elevator.floors.Add(floor, this);

        if (elevator.currentFloor == this)
            SetElevator();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetElevator()
    {
        Vector3 targetPos = elevator.movableBit.transform.position;
        targetPos.y = moveRoot.transform.position.y;
        elevator.movableBit.transform.position = targetPos;
    }


    public void CallElevator(bool ignoreMoveCriteria = false)
    {
        if (elevator.IsMoving && !ignoreMoveCriteria)
            return;
        if (elevator.currentFloor == this)
        {
            if (!elevator.IsOpen)
                elevator.OpenDoor();
            return;
        }
        elevator.SetFloor(floor);
        StartCoroutine(CallElevatorRoutine());
    }
    public IEnumerator CallElevatorRoutine()
    {
        float time = 4;
        elevator.movableBit.DOMoveY(moveRoot.transform.position.y, time);

        yield return new WaitForSeconds(time);
        elevator.OpenDoor();
        //currentFloor = floor;
    }
}
