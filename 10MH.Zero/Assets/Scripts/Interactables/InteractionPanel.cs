﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using NHG.Dialogue;
using Cinemachine;
using System.Collections.Generic;

public class InteractionPanel : MonoBehaviour
{
    public const int focusPriority = 5;
    public CinemachineVirtualCamera cam;
    CoroutineHandler waitRoutine;
    public bool inTransition;
    protected virtual void Awake()
    {
        waitRoutine = new CoroutineHandler(this);
        if (cam)
            cam.enabled = false;
    }
    public void Open()
    {
        inTransition = true;
        if (cam)
        {
            cam.Priority = focusPriority;
            cam.enabled = true;
            waitRoutine.StartCoroutine(waitForSeconds());
        }
        else
        {
            OnCameraTransitionComplete();
        }
    }
    IEnumerator waitForSeconds()
    {
        yield return new WaitForSeconds(Global10MH.Instance10MH.cameraBrain.m_DefaultBlend.BlendTime);
        OnCameraTransitionComplete();
        inTransition = false;
    }
    public virtual void OnCameraTransitionComplete()
    {

    }

    public virtual void Close()
    {
        waitRoutine.StopCoroutine();
        //Publisher.Raise(new InteractionPanelClosedEvent)
        if (cam)
        {
            cam.Priority = 0;
            cam.enabled = false;
        }
    }

    public Inventory SpawnInventory(InventoryPropertyData data, bool canAddItems, string inventoryKey, List<ItemPlacementData> initialItems = null)
    {
        if (initialItems == null)
            initialItems = new List<ItemPlacementData>();
        List<ItemPlacementData> items = CycleManager.GetInventory(inventoryKey, initialItems);
        return InventoryManager.SpawnOtherInventory(inventoryKey, false, data, canAddItems, null, items.ToArray());
    }
    public Inventory SpawnInventory(Transform root, InventoryPropertyData data, bool canAddItems, string inventoryKey)
    {
        List<ItemPlacementData> items = CycleManager.GetInventory(inventoryKey, new List<ItemPlacementData>());
        return InventoryManager.SpawnOtherInventory(inventoryKey, false, root.position, data, canAddItems, null, items.ToArray());
    }
    public Inventory SpawnInventory(InteractionDoorLock idl)
    {
        return SpawnInventory(idl.worldPos, idl.data, true, idl.key);
    }

    public void CacheAndDestory(InteractionDoorLock idl)
    {
        CacheAndDestory(idl.key, idl.inv,false);
    }
    public void CacheAndDestory(string key,Inventory i,bool isPerma)
    {
        CycleManager.CacheInventory(key, i.inventoryItems, isPerma);
        InventoryManager.DestoryInventory(i);
    }
}

[System.Serializable]
public struct InteractionDoorLock
{
    public Transform worldPos;
    public string key;
    public InventoryPropertyData data;
    public Inventory inv;
}

public class InteractionUIAction : DialogueAction
{
    public static bool waitforInteractUI = false;
    public override string eventName => "interactUI";
    public GameObject prefab => tags.GetExpressPrefab();

    public override IEnumerator Process(Layer layer)
    {
        waitforInteractUI = true;
        yield return base.Process(layer);
        GameObject go = GameObject.Instantiate(prefab, layer.transform);

        while (waitforInteractUI)
        {
            yield return null;
        }

        GameObject.Destroy(go);
    }

    public static Scene InteractionUIScene(string prefabName)
    {
        Scene scene = new Scene();
        scene.Init(new Tags());

        InteractionUIAction e = new InteractionUIAction();
        e.Init(new Tags());
        e.Tags.SetExpress(prefabName);
        scene.events.Add(e);
        return scene;
    }
}

public class InteractionPanelClosedEvent
{
    public InteractionPanel interactionPanel;

    public InteractionPanelClosedEvent(InteractionPanel interactionPanel)
    {
        this.interactionPanel = interactionPanel;
    }
}