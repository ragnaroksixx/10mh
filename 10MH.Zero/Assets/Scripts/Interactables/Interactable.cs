﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

public class Interactable : MonoBehaviour
{
    public bool crouchInteractable;
    public bool canUseInDark;
    public string displayText;
    [HideInInspector]
    public bool doOnce = true;
    Interaction[] interactOptions;
    public bool autoInteract;
    [ShowIf("autoInteract")]
    public Interaction autoInteraction;
    public InteractionPanel panel;
    public Interaction[] InteractOptions { get => interactOptions; }

    [FoldoutGroup("Callbacks")]
    public UnityEvent onMenuOpen,onMenuClose;
    public bool IsAutoInteraction { get => autoInteract; set => autoInteract = value; }
    public void Awake()
    {
        List<Interaction> ints = new List<Interaction>();
        interactOptions = GetComponentsInChildren<Interaction>();
        foreach (Interaction i in interactOptions)
        {
            i.Init(this);
            if (!i.hiddenInMenu)
                ints.Add(i);
        }
        interactOptions = ints.ToArray();
    }
    public virtual void Start()
    {

    }
    public virtual bool IsInteractable()
    {
        if (crouchInteractable && !WalkController10MH.Instance.IsCrouched)
            return false;
        return enabled && doOnce;
    }
    public virtual void AutoInteract()
    {
        Interaction i;
        if (autoInteraction)
            i = autoInteraction;
        else
            i = interactOptions[0];
        i.Interact();
    }

    public bool HasInteraction<T>()
    {
        foreach (Interaction i in interactOptions)
        {
            if (i is T)
                return true;
        }
        return false;
    }

    public void MenuOpen()
    {
        panel?.Open();
        onMenuOpen?.Invoke();
    }

    public void MenuClose()
    {
        panel?.Close();
        onMenuClose?.Invoke();
    }
}


