﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;

public class PanelInteraction : Interaction
{
    public GameObject prefab;

    public override void OnInteract()
    {
        Scene s = InteractionUIAction.InteractionUIScene(prefab.name);
        PlayScene(s, null, false);
    }
}
