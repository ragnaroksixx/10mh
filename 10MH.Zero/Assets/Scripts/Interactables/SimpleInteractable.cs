﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class SimpleInteractable : Interactable
{
    public UnityEvent action;
    public override void Start()
    {
        base.Start();
        autoInteract = true;
    }
    public override void AutoInteract()
    {
        action?.Invoke();
    }
}
