﻿using UnityEngine;
using System.Collections;
using TMPro;
using DG.Tweening;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InteractionButton : MonoBehaviour
{
    public Button button;
    public TMP_Text text;
    public CanvasGroup canvasGroup;
    //public GameObject subMenu;
    public Image image;
    public RectTransform root;
    private void Start()
    {
        EventTrigger b = gameObject.AddComponent<EventTrigger>();
        if (b)
        {

            EventTrigger.Entry onHighlight = new EventTrigger.Entry();
            onHighlight.eventID = EventTriggerType.PointerEnter;
            onHighlight.callback.AddListener((BaseEventData e) => { OnPointerEnter(); });
            b.triggers.Add(onHighlight);

            EventTrigger.Entry onUnhighlight = new EventTrigger.Entry();
            onUnhighlight.eventID = EventTriggerType.PointerExit;
            onUnhighlight.callback.AddListener((BaseEventData e) => { OnPointerExit(); });
            b.triggers.Add(onUnhighlight);
        }
    }
    void Set(string intName, int usageTime, int i, Sprite sprite)
    {
        canvasGroup.alpha = 0;
        root.anchoredPosition = new Vector2(0, -40);
        root.transform.localScale=Vector3.one;
        root.DOKill();
        gameObject.SetActive(true);

        root.DOAnchorPosY(0,0.25f).SetEase(Ease.OutBounce);
        canvasGroup.DOFade(1, 0.25f)
            .SetDelay((0.2f * (i)));

        //shortcutText.text = (i + 1).ToString();
        text.SetText(intName);
        button.onClick.RemoveAllListeners();
        image.sprite = sprite;

        //timeText.text = usageTime.ToString() + Environment.NewLine + "<size=50%>SEC";
        //timeText.transform.parent.gameObject.SetActive(usageTime > 0);



    }

    public void SetInteraction(InteractionSelector selector, Interaction interaction, int i)
    {
        Set(interaction.displayName, interaction.usageTime, i, interaction.icon);
        //subMenu.gameObject.SetActive(interaction.HasSubMenu);
        if (interaction.HasSubMenu)
        {
            button.onClick.AddListener(() => { selector.OpenSubMenu(this, interaction); });
        }
        else
        {
            button.onClick.AddListener(() => { interaction.Interact(); });
            button.onClick.AddListener(selector.CancelButton);
            //button.onClick.AddListener(() => { UseTime(interaction.usageTime); });
        }
    }

    public void SetSubInteraction(SubInteractionSelector selector, Interaction interaction, SubInteraction si, int i)
    {
        Set(si.name, si.usageTime, i, interaction.icon);
        //subMenu.gameObject.SetActive(false);
        button.onClick.AddListener(selector.CancelButton);
        button.onClick.AddListener(selector.selector.CancelButton);
        button.onClick.AddListener(() => { interaction.Interact(); });
        button.onClick.AddListener(() => { UseTime(si.usageTime); });
    }

    void UseTime(int seconds)
    {
        NHG.Dialogue.AddTimeAction.AddTime(seconds);
    }

    public void OnPointerEnter()
    {
        text.transform.parent.gameObject.SetActive(true);
        root.transform.DOScale(1.2f, 0.1f);
    }

    public void OnPointerExit()
    {
        text.transform.parent.gameObject.SetActive(false);
        root.transform.DOScale(1.0f, 0.2f);
    }
}
