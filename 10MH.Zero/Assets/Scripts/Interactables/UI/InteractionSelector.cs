﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System;

public class InteractionSelector : MonoBehaviour
{
    public InteractionButton[] buttons;
    public TMPButton exitButton;
    Interactable interactable;
    public OnScreenAnimatorBASE anim;
    public SubInteractionSelector subMenu;
    public CanvasGroup cg;
    // Use this for initialization
    void Start()
    {
        Publisher.Subscribe<StartInteractionEvent>(OnStartInteraction);
        Publisher.Subscribe<CloseInteractionEvent>(EndInteraction);
        exitButton.onClick.AddListener(CancelButton);
    }
    private void Update()
    {
        if (!anim.IsOnScreen || anim.IsTransitioning || subMenu.anim.IsOnScreen) return;
        if (interactable.panel && interactable.panel.inTransition) return;
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            CancelButton();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
        {
            TryClick(0);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
        {
            TryClick(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3))
        {
            TryClick(2);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Keypad4))
        {
            TryClick(3);
        }
    }

    public void OpenSubMenu(InteractionButton ib, Interaction interaction)
    {
        cg.blocksRaycasts = cg.interactable = false;
        (subMenu.transform as RectTransform).anchoredPosition =
            new Vector2((subMenu.transform as RectTransform).anchoredPosition.x, (ib.transform as RectTransform).anchoredPosition.y + 10);
        subMenu.Show(interaction);
    }

    private void TryClick(int val)
    {
        if (buttons[val].gameObject.activeInHierarchy)
        {
            buttons[val]?.button.onClick.Invoke();
        }
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<StartInteractionEvent>(OnStartInteraction);
    }
    void Set(Interactable inter)
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].gameObject.SetActive(false);
            buttons[i].canvasGroup.DOKill();
        }
        interactable = inter;
        int buttonNumber = 0;
        for (int i = 0; i < interactable.InteractOptions.Length; i++)
        {
            if (buttonNumber >= buttons.Length)
                break;
            InteractionButton b = buttons[buttonNumber];
            Interaction interaction = i < interactable.InteractOptions.Length
                ? interactable.InteractOptions[i] : null;
            if (interaction && !interaction.CanUse())
                interaction = null;
            else
                buttonNumber++;

            if (interaction != null)
            {
                b.SetInteraction(this, interaction, i);
            }
        }
    }
    void Show()
    {
        interactable.MenuOpen();
        float val = -1;
        if (interactable.panel)
            val = 0.2f;
        HUDController.Instance.FadeIn(0.5f, val);
        anim.AnimateOnScreen();
        InputSystem10MH.phoneOpenClose.Disable(this);
    }
    void Hide()
    {
        interactable.MenuClose();
        HUDController.Instance.FadeOut(0.5f);
        anim.AnimateOffScreen();
        InputSystem10MH.phoneOpenClose.Enable(this);
    }
    public void CancelButton()
    {
        Hide();
        Publisher.Raise(new ClearNotificationEvent());
        Global10MH.HideCursor(this);
        CameraController10MH.RestoreUserInput(this);
        WalkController10MH.RestoreUserInput(this);
        TimeController.Instance.IsPaused.RemoveLock(this);
        InteractionHandler.RemoveInteractionLock(this);
        InventoryManager.UnlockInput(this);
        CursorController.ShowCursor(this);
    }
    void OnStartInteraction(StartInteractionEvent e)
    {
        if (e.interactable.autoInteract)
        {
            e.interactable.AutoInteract();
            return;
        }
        Set(e.interactable);
        Show();
        TimeController.Instance.IsPaused.AddLock(this);
        Global10MH.ShowCursor(this);
        CameraController10MH.RemoveUserInput(this);
        InteractionHandler.LockInteraction(this);
        WalkController10MH.RemoveUserInput(this);
        InventoryManager.LockInput(this);
        CursorController.HideCursor(this);
        Publisher.Raise(new NotificationEvent(e.interactable.displayText));
    }
    void EndInteraction(CloseInteractionEvent e)
    {
        CancelButton();
    }
}

public class StartInteractionEvent : PublisherEvent
{
    public Interactable interactable;

    public StartInteractionEvent(Interactable i)
    {
        interactable = i;
    }
}

public class CloseInteractionEvent : PublisherEvent
{
}
