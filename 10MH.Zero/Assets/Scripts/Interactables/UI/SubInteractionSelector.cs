﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SubInteractionSelector : MonoBehaviour
{
    public InteractionButton[] buttons;
    public TMPButton exitButton;
    public OnScreenAnimatorBASE anim;
    public InteractionSelector selector;

    void Start()
    {
        exitButton.onClick.AddListener(CancelButton);
    }

    private void Update()
    {
        if (!anim.IsOnScreen || anim.IsTransitioning) return;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CancelButton();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
        {
            TryClick(0);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
        {
            TryClick(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3))
        {
            TryClick(2);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Keypad4))
        {
            TryClick(3);
        }
    }

    private void TryClick(int val)
    {
        if (buttons[val].gameObject.activeInHierarchy)
        {
            buttons[val].button?.onClick.Invoke();
        }
    }

    public void Show(Interaction i)
    {
        Set(i);
        anim.AnimateOnScreen();
    }
    void Hide()
    {
        anim.AnimateOffScreen();
    }

    public void CancelButton()
    {
        Hide();
        selector.cg.blocksRaycasts = selector.cg.interactable = true;
    }

    void Set(Interaction inte)
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].gameObject.SetActive(false);
            buttons[i].canvasGroup.DOKill();
        }
        SubInteraction[] subInteractions = inte.GetSubInteractions();
        int buttonNumber = 0;
        for (int i = 0; i < subInteractions.Length; i++)
        {
            InteractionButton b = buttons[buttonNumber];
            SubInteraction subInt = subInteractions[i];
            b.SetSubInteraction(this, inte, subInt, i);

        }
        exitButton.Group.alpha = 0;
        exitButton.Group.DOFade(1, 0.25f)
            .SetDelay((0.2f * (subInteractions.Length)));
    }
}

public struct SubInteraction
{
    public string name;
    public int usageTime;

    public SubInteraction(string name, int usageTime)
    {
        this.name = name;
        this.usageTime = usageTime;
    }
}
