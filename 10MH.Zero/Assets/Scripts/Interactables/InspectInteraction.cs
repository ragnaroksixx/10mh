﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;

public class InspectInteraction : Interaction
{
    public bool dontOpenMenuOnEnd = false;
    [TextArea(5, 20)]
    public string[] texts;
    public override void Init(Interactable i)
    {
        if (string.IsNullOrEmpty(displayName))
            displayName = "Look";
        base.Init(i);
    }
    public override void OnInteract()
    {
        Scene s = DialogueUtils.CreateSimpleTextScene(texts);
        PlayScene(s);
    }
    public void PlayScene(Scene scene)
    {
        DialogueRenderer.Instance.PlayScene(scene, () => { OnSceneComplete(scene); });
    }
    protected virtual void OnSceneComplete(Scene s)
    {
        if (!interactable.autoInteract && !dontOpenMenuOnEnd)
            OpenInteractionMenu();
    }
}
