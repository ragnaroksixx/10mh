﻿using System.Collections.Generic;
using NHG.Dialogue;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AreaTransitionInteraction : Interaction
{
    public AreaTransitionData nextArea;
    public bool forceLocked = false;
    public CustomAudioClip lockedSFX, onUseSFX;
    public TextAsset lockedScene;

    AreaTransitionLock[] locks;

    public AreaTransitionLock[] Locks
    {
        get
        {
            if (locks == null)
                locks = GetComponentsInChildren<AreaTransitionLock>();
            return locks;
        }
    }

    public override void Awake()
    {
        base.Awake();
    }
    public override void Init(Interactable i)
    {
        base.Init(i);
        if (nextArea && string.IsNullOrEmpty(i.displayText))
            i.displayText = "Door to " + nextArea.GetArea().displayName;
    }
    public override void OnInteract()
    {
        if (nextArea == null)
        {
            lockedSFX?.Play();
            if (lockedScene)
                PlayScene(lockedScene, null, false);
            return;
        }
        NHG.Dialogue.Scene pathScene = null;
        if (!PathManager.CanUse(nextArea, out pathScene))
        {
            PlayScene(pathScene, null, false);
            return;
        }
        if (!IsUnLocked(out pathScene))
        {
            lockedSFX?.Play();
            if (pathScene != null)
                PlayScene(pathScene, null, false);
            if (lockedScene)
                PlayScene(lockedScene, null, false);
            return;
        }
        if (EscortSystem.IsEscorting)
        {
            if (!EscortSystem.escortData.validLocations.Contains(nextArea))
            {
                PlayScene(EscortSystem.escortData.defaultWrongWayScene, null, false);
                return;
            }
        }
        interactable.doOnce = false;
        if (onUseSFX)
            SceneTransitionEffect.waitTime = onUseSFX.clip.length;
        onUseSFX?.Play();
        GoTo(nextArea);
    }
    public static void GoTo(AreaTransitionData at)
    {
        InteractionHandler.Instance.ClearInteract();
        SceneController.SetActiveEnvironment(at);
    }
    public virtual bool IsUnLocked()
    {
        if (forceLocked)
            return false;
        foreach (AreaTransitionLock l in Locks)
        {
            if (l.IsLocked())
                return false;
        }
        return true;
    }
    public virtual bool IsUnLocked(out NHG.Dialogue.Scene s)
    {
        s = null;
        if (forceLocked)
            return false;
        foreach (AreaTransitionLock l in Locks)
        {
            if (l.IsLocked())
            {
                if (l.lockedScene!=null)
                    s = DialogueUtils.GetSceneFrom(l.lockedScene);
                l.onLocked?.Invoke();
                return false;
            }
        }
        return true;
    }
#if UNITY_EDITOR
    //[Button]
    public void ReplaceDoor(GameObject newPrefab)
    {
        if (newPrefab == null) return;

        GameObject go = UnityEditor.PrefabUtility.InstantiatePrefab(newPrefab) as GameObject;
        go.transform.parent = transform.parent;
        go.transform.localScale = transform.localScale;
        go.transform.rotation = transform.rotation;
        go.transform.position = transform.position;
        AreaTransitionInteraction ati = go.GetComponent<AreaTransitionInteraction>();

        ati.displayName = displayName;
        ati.sfx = sfx;

        ati.nextArea = nextArea;
        ati.forceLocked = forceLocked;
        ati.lockedSFX = lockedSFX;
        ati.onUseSFX = onUseSFX;
        ati.lockedScene = lockedScene;

        ati.name = name;
        ati.transform.SetSiblingIndex(transform.GetSiblingIndex());
        SceneManager.MoveGameObjectToScene(go, gameObject.scene);

        DestroyImmediate(gameObject);
    }
#endif
}
