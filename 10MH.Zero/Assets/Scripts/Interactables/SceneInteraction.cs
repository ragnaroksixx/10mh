﻿using NHG.Dialogue;
using Sirenix.OdinInspector;
using UnityEngine;

public class SceneInteraction : Interaction
{
    public bool preloadScene;
    public bool dontOpenMenuOnEnd = false;
    public DialogueVariable scene;
    Scene s;
    public override void Awake()
    {
        base.Awake();
        if (preloadScene)
            s = DialogueUtils.GetSceneFrom(scene);
    }
    public override void OnInteract()
    {
        if (s == null)
            s = DialogueUtils.GetSceneFrom(scene);
        PlayScene(s, null, !interactable.autoInteract && !dontOpenMenuOnEnd);
    }
#if UNITY_EDITOR
    bool IsFile => scene != null && scene.scene is TextAsset;


    [ShowIf("IsFile")]
    [Button(Expanded = true)]
    public void ConvertSceneToMonoBehaviour()
    {
        if (!UnityEditor.EditorUtility.DisplayDialog("Are you sure?",
    "This will overwrite the current scene",
    "yes",
    "no"))
        {
            return;
        }

        DialogueSceneMB dsmb = gameObject.AddComponent<DialogueSceneMB>();
        dsmb.CopyTextAsset(scene.scene as TextAsset);

        if (UnityEditor.EditorUtility.DisplayDialog("Delete File?",
"Yo, do you wanna delete that original file?\nTHIS WILL DELETE THE FILE, FOREVER!!!",
"heck yeah",
"no shot"))
        {
            System.IO.File.Delete(UnityEditor.AssetDatabase.GetAssetPath(scene.scene));
        }
        scene.scene = dsmb;
        UnityEditor.EditorUtility.SetDirty(gameObject);
    }
#endif


}

