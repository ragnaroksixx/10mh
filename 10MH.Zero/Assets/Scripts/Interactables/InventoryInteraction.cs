﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

public class InventoryInteraction : Interaction
{
    CoroutineHandler loop;
    public InventoryCollection inventory;
    public override void Start()
    {
        base.Start();
        loop = new CoroutineHandler(this);
    }
    public override void OnInteract()
    {
        loop.StartCoroutine(InteractLoop());
    }

    public virtual IEnumerator InteractLoop()
    {
        InteractionHandler.LockInteraction(this);
        WalkController10MH.RemoveUserInput(this);
        CameraController10MH.RemoveUserInput(this);
        Inventory inv = GetInventory();
        InventoryManager.Instance.OpenInventory();
        HUDController.Instance.ShowInventoryHelp();
        while (MainLoop(inv))
        {
            yield return null;
        }
        OnClose();
        inventory.CacheAndDestory(inv);
        InventoryManager.Instance.CloseInventory();
        CameraController10MH.RestoreUserInput(this);
        WalkController10MH.RestoreUserInput(this);
        InteractionHandler.RemoveInteractionLock(this);
    }
    public virtual void OnClose()
    {

    }
    public virtual bool MainLoop(Inventory inv)
    {
        if (InventoryManager.IsItemHeld())
            return true;
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            return false;
        }
        return true;
    }
    public virtual Inventory GetInventory()
    {
        return inventory.GetInventory();
    }
    public ItemPlacementData[] GetInvItems()
    {
        return inventory.GetInvItems().ToArray();
    }
}
