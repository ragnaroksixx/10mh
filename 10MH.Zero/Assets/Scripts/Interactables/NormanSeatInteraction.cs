﻿using System.Collections;
using UnityEngine;


public class NormanSeatInteraction : Interaction
{
    public TimeRef waitTillTime;
    CoroutineHandler loop;

    public NormanClassArea area;
    public override void Start()
    {
        base.Start();
        loop = new CoroutineHandler(this);
    }
    public override void OnInteract()
    {
        loop.StartCoroutine(InteractLoop());
    }

    public virtual IEnumerator InteractLoop()
    {
        InteractionHandler.LockInteraction(this);
        WalkController10MH.RemoveUserInput(this);
        CameraController10MH.RemoveUserInput(this);
        TimeController.Instance.IsPaused.AddLock(this);
        yield return null;

        area.WaitForClass();

        yield return new WaitForSeconds(1);
        yield return TimeController.Instance.SkipTo(waitTillTime);

        CameraController10MH.RestoreUserInput(this);
        WalkController10MH.RestoreUserInput(this);
        InteractionHandler.RemoveInteractionLock(this);
        TimeController.Instance.IsPaused.RemoveLock(this);

        yield return new WaitForSeconds(1);
    }
}
