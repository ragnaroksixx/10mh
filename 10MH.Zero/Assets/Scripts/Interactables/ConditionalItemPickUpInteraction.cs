﻿using UnityEngine;
using System.Collections;


public class ConditionalItemPickUpInteraction : ItemPickUpInteraction
{
    public string[] conditions;
    public ItemPlacementData[] items;
    public override string ID()
    {
        return "whocares";
    }
    public override bool CanUse()
    {
        bool hasOne = false;
        foreach (string item in conditions)
        {
            if (CycleManager.Instance.HasEvent("",item))
            {
                hasOne = true;
                break;
            }
        }
        return hasOne && base.CanUse();
    }
    public override ItemPlacementData GetItem()
    {
        for (int i = 0; i < conditions.Length; i++)
        {
            string item = conditions[i];
            if (CycleManager.Instance.HasEvent("",item))
            {
                return items[i];
            }
        }
        return base.GetItem();
    }
}

