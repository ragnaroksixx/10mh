﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public abstract class InteractionBlocker : MonoBehaviour
{
    public DialogueVariable blockedDialogue;
    public UnityEvent onBlockTriggered;

    public abstract bool IsBlocked();
}
