﻿using System.Collections;
using UnityEngine;

public class CharacterBlocker : InteractionBlocker
{
    public CharacterData character;

    public override bool IsBlocked()
    {
        return Area.IsCharacterInArea(character);
    }
}
