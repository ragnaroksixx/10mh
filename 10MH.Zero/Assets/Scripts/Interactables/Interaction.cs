﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;
using NHG.Dialogue;
using System;
using System.Collections.Generic;
using UnityEngine.Events;

[RequireComponent(typeof(Interactable))]
public abstract class Interaction : MonoBehaviour
{
    protected const string UI_UX_LABEL = "UI\\UX";
    protected const string INFO_LABEL = "Info";
    public string displayName;
    [HideInInspector]
    public int usageTime = 0;
    public bool HasSubMenu => GetSubInteractions().Length > 0;

    [FoldoutGroup(UI_UX_LABEL)]
    public bool hiddenInMenu = false;
    [FoldoutGroup(UI_UX_LABEL)]
    public AudioVariable sfx;
    [FoldoutGroup(UI_UX_LABEL)]
    public Sprite icon;

    protected Interactable interactable;

    [FoldoutGroup("Callbacks")]
    public UnityEvent onUseCallback;

    [FoldoutGroup("Blocks")]
    [ListDrawerSettings(Expanded = true)]
    public InteractionBlocker[] blockers;

    public bool HiddenInMenu { get => hiddenInMenu; set => hiddenInMenu = value; }

    public virtual void Init(Interactable i)
    {
        this.interactable = i;
    }
    public virtual void Awake()
    {
    }
    public virtual void Start()
    {

    }
    public virtual bool CanUse()
    {
        return true;
    }
    public virtual SubInteraction[] GetSubInteractions() { return new SubInteraction[0]; }
    public abstract void OnInteract();
    public void Interact()
    {
        foreach (InteractionBlocker blocker in blockers)
        {
            if (blocker.IsBlocked())
            {
                if (blocker.blockedDialogue.scene != null)
                    DialogueRenderer.Instance.PlayScene(blocker.blockedDialogue, null);
                return;
            }
        }
        OnInteract();
        AudioSystem.PlaySFX(sfx, transform.position);
        onUseCallback.Invoke();
    }
    protected void OpenInteractionMenu()
    {
        InteractionHandler.Instance.StartInteraction(interactable);
    }

    public void PlayScene(TextAsset scene, Action onComplete, bool reOpenMenu)
    {
        PlayScene(DialogueUtils.GetSceneFromTextAsset(scene), onComplete, reOpenMenu);
    }
    public void PlayScene(Scene scene, Action onComplete, bool reOpenMenu)
    {
        Action onSceneComplete = null;
        if (onComplete != null)
            onSceneComplete += onComplete;
        if (reOpenMenu)
            onSceneComplete += OpenInteractionMenu;
        DialogueRenderer.Instance.PlayScene(scene, onSceneComplete);
    }
}

[System.Serializable]
public struct MemorySceneInterrupts
{
    public string memory;
    public TextAsset scene;
    public bool not;
    public bool once;
    public bool PassCriteria(Interaction i)
    {
        if (once)
        {
            string key = "Memory Interrupt" + memory + i.displayName + i.gameObject.name;
            if (CycleManager.Instance.HasEvent("", key))
                return false;
        }
        if (string.IsNullOrEmpty(memory))
            return false;
        bool result = CycleManager.Instance.HasEvent("", memory);
        if (not)
            result = !result;
        return result;
    }

    public void Cycle(Interaction i)
    {
        if (!once) return;
        string key = "Memory Interrupt" + memory + i.displayName + i.gameObject.name;
        CycleManager.Instance.AddTempEvent("", key);
    }
}