﻿using UnityEngine;
using System.Collections;

public class LightsInteraction : Interaction
{
    public AreaLightSource lightSource;
    public AudioVariable lightONsfx, lightOFFsfx;
    public override void Awake()
    {
        base.Awake();
        sfx = lightSource.isOn ? lightOFFsfx : lightONsfx;
    }
    public override void OnInteract()
    {
        lightSource.Toggle();
        sfx = lightSource.isOn ? lightOFFsfx : lightONsfx;
    }

}
