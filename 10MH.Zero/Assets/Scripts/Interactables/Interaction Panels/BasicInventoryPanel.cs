﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicInventoryPanel : InteractionPanel
{
    Inventory i;
    public InventoryCollection inventory;
    public override void OnCameraTransitionComplete()
    {
        base.OnCameraTransitionComplete();
        i = inventory.GetInventory();// SpawnInventory(invData, true, key, initialItems);
        InventoryManager.Instance.OpenInventory();
    }
    public override void Close()
    {
        base.Close();
        inventory.CacheAndDestory(i);
        //CacheAndDestory(key, i, isPermaInv);
        InventoryManager.Instance.CloseInventory();
    }
}
