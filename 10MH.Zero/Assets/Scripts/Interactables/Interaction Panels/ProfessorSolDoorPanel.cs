﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProfessorSolDoorPanel : InteractionPanel
{
    public InteractionDoorLock lock1, lock2;
    public PowererdCable cable1, cable2;
    public ManualLock itemLock1, itemLock2;
    public Item key;
    protected override void Awake()
    {
        base.Awake();
    }
    public override void OnCameraTransitionComplete()
    {
        base.OnCameraTransitionComplete();
        lock1.inv = SpawnInventory(lock1);
        lock2.inv = SpawnInventory(lock2);

        lock1.inv.updateInventoryUI += OnInventoryUpdated;
        lock2.inv.updateInventoryUI += OnInventoryUpdated;

        InventoryManager.Instance.OpenInventory();
    }

    public override void Close()
    {
        base.Close();
        if (lock1.inv.inventoryItems.Count > 0)
            lock1.inv.inventoryItems[0].runtimeUI.TryAutoChangeInventories();
        if (lock2.inv.inventoryItems.Count > 0)
            lock2.inv.inventoryItems[0].runtimeUI.TryAutoChangeInventories();
        CacheAndDestory(lock1);
        CacheAndDestory(lock2);
        InventoryManager.Instance.CloseInventory();
    }

    public void OnInventoryUpdated()
    {
        itemLock1.isLocked = !lock1.inv.ContainsAny(new List<Item>() { key });
        itemLock2.isLocked = !lock2.inv.ContainsAny(new List<Item>() { key });
        cable1.SetPower(!itemLock1.IsLocked());
        cable2.SetPower(!itemLock2.IsLocked());
    }
}
