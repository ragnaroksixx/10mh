﻿using UnityEngine;
using System.Collections;

public class PermanentItemInteractable : Interaction
{
    public Item item;
    public override void Awake()
    {
        base.Awake();

    }
    public override void Start()
    {
        base.Start();
        if (PlayerInventory.HasPermanentInvItem(item))
        {
            Destroy(this.gameObject);
        }
    }
    public override void OnInteract()
    {
        ItemPlacementData ipd = new ItemPlacementData();
        ipd.item = item;
        PlayerInventory.AddPermanentInvItem(ipd);
        InteractionHandler.Instance.ClearInteract();
        Destroy(this.gameObject);
    }
}
