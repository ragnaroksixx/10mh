﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NHG.Dialogue;

public class InteractionHandler : MonoBehaviour
{
    Camera cam;
    public RectTransform cursor;

    public CanvasGroup inspectUI;

    public float interactionDist = 1;
    public LayerMask interactableLayers;
    InteractionHitData prevHit = new InteractionHitData();

    public static InteractionHandler Instance;
    public MultiBoolLock interactionLocked;
    public CustomAudioClip interactHover;
    public OnScreenAnimatorBASE uiAnimator;
    public Camera Cam
    {
        get
        {
            if (cam == null)
                cam = Camera.main;
            return cam;
        }
    }
    private void Awake()
    {
        interactionLocked = new MultiBoolLock();
        Instance = this;
        SetInteractableUI(null);
    }
    public static void LockInteraction(object o)
    {
        Instance.interactionLocked.AddLock(o);
        Instance.enabled = false;
        Instance.SetInteract(new InteractionHitData());
    }
    public static void RemoveALLInteractionLock()
    {
        Instance.interactionLocked.ClearLocks();
        Instance.enabled = true;
        Instance.SetInteract(new InteractionHitData());
    }
    public static void RemoveInteractionLock(object o)
    {
        if (!Instance.interactionLocked.HasLock(o))
            return;
        Instance.interactionLocked.RemoveLock(o);
        if (!Instance.interactionLocked.Value)
        {
            Instance.enabled = true;
            Instance.prevHit = new InteractionHitData();
            InteractionHitData currentHit = Instance.RaycastScreenSpaceUI(Instance.cursor);
            Instance.SetInteract(currentHit);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Cam == null)
            return;
        InteractionHitData currentHit = RaycastScreenSpaceUI(cursor);
        SetInteract(currentHit);
        if (currentHit.IsHit)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (!Area.activeArea.IsLightOn() && !currentHit.interactable.canUseInDark)
                {
                    Scene s = DialogueUtils.CreateSimpleTextScene(
                        "I can't see a thing.",
                        "I need to get some lights on first");
                    DialogueRenderer.Instance.PlayScene(s, null);
                    return;
                }
                StartInteraction(currentHit.interactable);
            }
        }
    }
    public void StartInteraction(Interactable i)
    {
        Publisher.Raise(new StartInteractionEvent(i));
    }

    void SetInteract(InteractionHitData currentHit)
    {
        if (!prevHit.CompareInteractabe(currentHit))
        {
            //prevHit.interactable?.OnExit();
            //currentHit.interactable?.OnEnter();
            if (currentHit.IsHit)
                interactHover.Play();
            SetInteractableUI(currentHit.interactable);
        }
        prevHit = currentHit;
    }
    public void ClearInteract()
    {
        SetInteract(new InteractionHitData());
    }
    public InteractionHitData Raycast(Transform source)
    {
        return null;
    }
    InteractionHitData RaycastScreenSpaceUI(RectTransform source)
    {
        Vector3 uiPos = source.position;
        uiPos.z = Cam.nearClipPlane;
        Vector3 pos = Cam.ScreenToWorldPoint(uiPos);
        Vector3 fwd = Cam.transform.forward;
        RaycastHit hit;


        InteractionHitData hitData = new InteractionHitData();
        if (Physics.Raycast(pos, fwd, out hit, interactionDist, interactableLayers, QueryTriggerInteraction.Collide))
        {
            Interactable i;
            i = hit.transform.GetComponentInParent<Interactable>();/*
            if (hit.transform.parent == null)
                i = hit.transform.GetComponentInChildren<Interactable>();
            else
                i = hit.transform.parent.GetComponentInChildren<Interactable>();*/
            if (i && i.IsInteractable())
                hitData.interactable = i;
        }
        else
        {

        }
        return hitData;
    }
    void SetInteractableUI(Interactable i)
    {
        float fadeDuration = 0.125f;
        if (i)
        {
            cursor.transform.DOScale(Vector3.one * 2, 0.2f);
            bool isLightLocked = !Area.activeArea.IsLightOn() && !i.HasInteraction<LightsInteraction>();
            Publisher.Raise(new NotificationEvent(isLightLocked ? "???" : i.displayText));
            if (!isLightLocked)
                uiAnimator.AnimateOnScreen();
            else
                uiAnimator.AnimateOffScreen();

            inspectUI.DOKill();
            inspectUI.DOFade(1, fadeDuration);
            Sequence shake = DOTween.Sequence();
            float duration = 0.2f;
            float rot = 5;
            shake.Append(inspectUI.transform.DOLocalRotate(new Vector3(0, 0, -rot), duration))
                .Append(inspectUI.transform.DOLocalRotate(new Vector3(0, 0, rot), duration))
                .Append(inspectUI.transform.DOLocalRotate(new Vector3(0, 0, -rot), duration))
                .Append(inspectUI.transform.DOLocalRotate(new Vector3(0, 0, rot), duration))
                .Append(inspectUI.transform.DOLocalRotate(new Vector3(0, 0, 0), duration));
            shake.Play();

            Sequence pop = DOTween.Sequence();
            pop.Append(inspectUI.transform.DOScale(1.1f, duration * 3f / 4))
                .Append(inspectUI.transform.DOScale(1, duration * 3f / 4))
                .Append(inspectUI.transform.DOScale(1.1f, duration * 3f / 4))
                .Append(inspectUI.transform.DOScale(1, duration * 3f / 4));
            pop.Play();
        }
        else
        {
            uiAnimator.AnimateOffScreen();
            cursor.transform.DOScale(Vector3.one, 0.2f);
            inspectUI.DOKill();
            inspectUI.DOFade(0, fadeDuration);
            Publisher.Raise(new ClearNotificationEvent());
        }
    }
}

public class InteractionHitData
{
    public Interactable interactable = null;
    public bool IsHit { get => interactable != null; }
    public bool CompareInteractabe(InteractionHitData other)
    {
        return other.interactable == interactable;
    }
}
