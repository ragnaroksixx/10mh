﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChestInteractable : Interaction
{
    CoroutineHandler loop;
    public InventoryPropertyData invData;
    public string key;
    public override void Start()
    {
        base.Start();
        loop = new CoroutineHandler(this);
    }
    public override void OnInteract()
    {
        loop.StartCoroutine(InteractLoop());
    }

    public virtual IEnumerator InteractLoop()
    {
        InteractionHandler.LockInteraction(this);
        WalkController10MH.RemoveUserInput(this);
        CameraController10MH.RemoveUserInput(this);
        Inventory inv = GetInventory();
        //inv.OpenInventory();
        InventoryManager.Instance.OpenInventory();
        HUDController.Instance.ShowInventoryHelp();
        while (MainLoop(inv))
        {
            yield return null;
        }
        OnClose();
        CycleManager.CacheInventory(key, inv.inventoryItems, true);
        InventoryManager.DestoryInventory(inv);
        InventoryManager.Instance.CloseInventory();
        CameraController10MH.RestoreUserInput(this);
        WalkController10MH.RestoreUserInput(this);
        InteractionHandler.RemoveInteractionLock(this);
    }
    public virtual void OnClose()
    {

    }
    public virtual bool MainLoop(Inventory inv)
    {
        if (InventoryManager.IsItemHeld())
            return true;
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            return false;
        }
        return true;
    }
    public virtual Inventory GetInventory()
    {
        return InventoryManager.SpawnOtherInventory(key, true, invData, true, null, GetInvItems());
    }
    public virtual ItemPlacementData[] GetInvItems()
    {
        return CycleManager.GetInventory(key, new List<ItemPlacementData>()).ToArray();
    }
}
