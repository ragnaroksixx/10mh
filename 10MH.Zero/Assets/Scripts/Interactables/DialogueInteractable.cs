﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;
public class DialogueInteractable : Interaction
{
    public DialogueVariable scene;
    public override void Start()
    {
        base.Start();
    }
    public override void OnInteract()
    {
        DialogueRenderer.Instance.PlayScene(scene, OnSceneComplete);
    }
    protected virtual void OnSceneComplete()
    {

    }

}
