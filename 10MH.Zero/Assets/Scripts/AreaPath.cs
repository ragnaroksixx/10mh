﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "Area/Path")]
public class AreaPath : ScriptableObject
{
    public TextAsset wrongWayScenes;
    public List<TransitionSceneLink> alternateWrongWayScenes;
    public List<AreaSceneLink> areaEnterScenes;
    public List<AreaTransitionData> validAreaTransitions;
    public AreaData destinationArea;
    public bool forceMapMovement;

    public string GetScenName(AreaTransitionData a)
    {
        foreach (TransitionSceneLink item in alternateWrongWayScenes)
        {
            if (item.transition == a)
            {
                return item.subSceneName;
            }
        }
        return "generic";
    }

    public AreaSceneLink GetEntryScene(AreaData a)
    {
        foreach (AreaSceneLink item in areaEnterScenes)
        {
            if (item.area == a)
            {
                return item;
            }
        }
        return null;
    }

    public void RemoveEntryScene(AreaSceneLink a)
    {
        areaEnterScenes.Remove(a);
    }
}
[System.Serializable]
public class TransitionSceneLink
{
    public AreaTransitionData transition;
    public string subSceneName;
}

[System.Serializable]
public class AreaSceneLink
{
    public AreaData area;
    public TextAsset scene;
}