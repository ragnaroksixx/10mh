﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;

public class AudioSystem : MonoBehaviour
{
    AudioPool audioPool;
    public static AudioSystem Instance;
    AudioObject bgm = null;

    AudioPool AudioPool { get => audioPool; }
    public AudioObject BGM { get => bgm; }

    public CustomAudioClip EmptyBGM, EmptySFX;
    public GameObject AudioSourcePrefabGameObject;
    public AudioMixer mixer { get => sfxGroup.audioMixer; }
    public AudioMixerGroup sfxGroup, bgmGroup;
    private void Awake()
    {
        if (Instance)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
            AudioDictionary.Init();
            Instance = this;
            audioPool = new AudioPool(AudioSourcePrefabGameObject, 10);
        }
    }
    public void Start()
    {
        QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("QUALITY", 3));
        if (PlayerPrefs.HasKey("mute"))
            SetMasterPercentage(0);
        else
        {
            SetBGMPercentage(GetBGM());
            SetSFXPercentage(GetSFX());
            SetMasterPercentage(GetMASTER());
        }
    }
    public static float GetBGM()
    {
        return PlayerPrefs.GetFloat("BGM", 70);
    }
    public static float GetSFX()
    {
        return PlayerPrefs.GetFloat("SFX", 70);
    }
    public static float GetMASTER()
    {
        return PlayerPrefs.GetFloat("MASTER", 70);
    }
    public static AudioReference PlayBGM(IAudio clip, Vector3 location, float fadeInTime = -1)
    {
        if (clip == null || clip.AudioInfo == null)
            clip = Instance.EmptyBGM;
        return Play(clip, location, fadeInTime, true, AudioCategory.BGM);
    }
    public static AudioReference PlaySFX(IAudio clip, Vector3 location, float fadeInTime = -1)
    {
        if (clip == null || clip.AudioInfo == null)
            return null;
        return Play(clip, location, fadeInTime, true, AudioCategory.SFX);
    }
    static AudioReference Play(IAudio clip, Vector3 location, float fadeInTime, bool overrideTrack = false, AudioCategory track = AudioCategory.SFX)
    {
        AudioObject obj = Instance.audioPool.Pop();
        AudioData aData = clip.AudioInfo;
        obj.Source.ignoreListenerPause = aData.parameters.playWhilePaused;
        obj.Source.transform.position = location;
        obj.Source.clip = aData.clip;
        obj.volumeFunc = obj.DefaultVolume;
        if (!overrideTrack)
            track = aData.parameters.category;
        switch (track)
        {
            case AudioCategory.SFX:
                obj.Source.outputAudioMixerGroup = Instance.sfxGroup;
                obj.Source.spatialBlend = aData.parameters.is3D ? 1 : 0;
                break;
            case AudioCategory.BGM:
                obj.Source.outputAudioMixerGroup = Instance.bgmGroup;
                obj.Source.spatialBlend = 0;
                break;
            default:
                break;
        }

        obj.clip = clip;


        obj.Source.loop = aData.parameters.loop;

        if (fadeInTime == -1)
        {
            obj.Source.volume = aData.parameters.volume;
            obj.Source.time = aData.parameters.startTime;
            obj.Source.Play();
        }
        else
        {
            obj.Source.volume = 0;
            obj.FadeIn(false, fadeInTime);
        }

        if (track == AudioCategory.BGM)
        {
            if (Instance.bgm != null)
                FadeOutAndStop(Instance.bgm, fadeInTime);
            Instance.bgm = obj;
        }
        AudioReference audioRef = new AudioReference(clip, obj);
        return audioRef;
    }

    public static void StopInstant(AudioObject obj)
    {
        Instance.audioPool.Push(obj);
    }
    public static void Stop(IAudio clip, bool stopAll, float time = -1)
    {
        System.Action onComplete = () => { };
        foreach (AudioObject item in Instance.audioPool.ActiveItems)
        {
            AudioObject i = item;
            if (item.clip == clip)
            {
                if (time <= 0)
                {
                    onComplete += () => { StopInstant(i); };
                }
                else
                {
                    onComplete += () => { FadeOutAndStop(i, time); };
                }
                if (!stopAll)
                    break;
            }
        }
        onComplete?.Invoke();
    }
    public static void Stop(AudioReference aRef, float time = -1)
    {
        System.Action onComplete = () => { };
        foreach (AudioObject item in Instance.audioPool.ActiveItems)
        {
            AudioObject i = item;
            if (item.clip == aRef.clip && item == aRef.audioObject)
            {
                if (time <= 0)
                {
                    onComplete += () => { StopInstant(i); };
                }
                else
                {
                    onComplete += () => { FadeOutAndStop(i, time); };
                }
            }
        }
        onComplete?.Invoke();
    }
    public static void FadeOutAndStop(AudioObject obj, float time = -1)
    {
        if (obj == Instance.bgm)
            Instance.bgm = null;
        obj.FadeOut(false, time);
    }
    public static void SetBGMPercentage(float percent)
    {
        PlayerPrefs.SetFloat("BGM", percent);
        percent = Mathf.Max(0.00001f, percent);
        percent /= 100.0f;
        percent = Mathf.Log10(percent) * 20;
        Instance.mixer.SetFloat("BGMVolume", percent);
    }
    public static void SetSFXPercentage(float percent)
    {
        PlayerPrefs.SetFloat("SFX", percent);
        percent = Mathf.Max(0.00001f, percent);
        percent /= 100.0f;
        percent = Mathf.Log10(percent) * 20;
        Instance.mixer.SetFloat("SFXVolume", percent);
    }
    public static void SetMasterPercentage(float percent)
    {
        PlayerPrefs.SetFloat("MASTER", percent);
        percent = Mathf.Max(0.00001f, percent);
        percent /= 100.0f;
        percent = Mathf.Log10(percent) * 20;
        Instance.mixer.SetFloat("MASTERVolume", percent);
    }

    public static void StopAll()
    {
        Instance.audioPool.StopAll();
    }
}
public static class AudioDictionary
{
    static Dictionary<string, CustomAudioClip> dict;
    static string AUDIO_CLIP_PATH = "AudioObjects";
    static AudioDictionary()
    {
        CustomAudioClip[] allAudio = Resources.LoadAll<CustomAudioClip>(AUDIO_CLIP_PATH);
        dict = new Dictionary<string, CustomAudioClip>();
        foreach (CustomAudioClip item in allAudio)
        {
            if (!dict.ContainsKey(item.name))
            {
                dict.Add(item.name.ToLower(), item);
                item.Init();
            }
            else
            {
                Debug.LogError("Duplicate Audio Clip: " + item.name);
            }
        }
    }
    public static CustomAudioClip GetClip(string name)
    {
        name = name.ToLower();
        if (dict.ContainsKey(name))
            return dict[name];

        Debug.LogError("Audio Clip Not Found");
        return null;
    }
    public static void Init()
    {

    }
}
public enum AudioCategory
{
    SFX,
    BGM,
}

public class AudioReference
{
    public IAudio clip;
    public AudioObject audioObject;

    public AudioReference(IAudio clip, AudioObject audioObject)
    {
        this.clip = clip;
        this.audioObject = audioObject;
    }

    public void Stop(float time = -1)
    {
        AudioSystem.Stop(this, time);
    }
    public void StopIn(float time, float transtionTime = 0)
    {
        audioObject.StartCoroutine(StopInCO(time, transtionTime));
    }
    IEnumerator StopInCO(float time, float transtionTime = 0)
    {
        yield return new WaitForSeconds(time - transtionTime);
        Stop(transtionTime);
    }

    public void Pause(float transitionTime = 1)
    {
        audioObject.SetPause(true, transitionTime);
    }
    public void UnPause(float transitionTime = 1)
    {
        audioObject.SetPause(false, transitionTime);
    }
    public void RePlay(float transitionTime = 1)
    {
        audioObject.RePlay(transitionTime);
    }
}
