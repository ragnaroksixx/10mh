﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;

[System.Serializable]
[InlineProperty]
public class AudioVariable : IAudio
{
    [ValidateInput("TypeCheck", "Invalid Audio type!")]
    [HideLabel]
    [SerializeField]
    private Object audio;

    public AudioVariable()
    {
    }

    public AudioVariable(Object audio)
    {
        this.audio = audio;
    }

    public AudioData AudioInfo { get => GetAudioData(); }

    public bool IsNULL()
    {
        return audio == null;
    }
    public AudioData GetAudioData()
    {
        if (audio == null)
            return null;

        if (audio is AudioClip)
        {
            return new AudioData(audio as AudioClip);
        }

        return (audio as IAudio).AudioInfo;

    }
    bool TypeCheck(Object o)
    {
        return o == null || o is AudioClip || o is IAudio;
    }

    public AudioReference Play()
    {
        return AudioSystem.PlaySFX(this, Vector3.zero);
    }

    public AudioReference Play(Vector3 pos)
    {
        return AudioSystem.PlaySFX(this, pos);
    }
    public AudioReference Play(float fadeInTime)
    {
        return AudioSystem.PlaySFX(this, Vector3.zero, fadeInTime);
    }
}
