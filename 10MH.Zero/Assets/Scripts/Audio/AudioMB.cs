﻿using System.Collections;
using UnityEngine;


public class AudioMB : MonoBehaviour, IAudio
{
    public AudioData audioInfo;
    public AudioData AudioInfo { get => audioInfo; set => audioInfo = value; }

    public void PlayEvent()
    {
        AudioSystem.PlaySFX(this, Vector3.zero);
    }
}
