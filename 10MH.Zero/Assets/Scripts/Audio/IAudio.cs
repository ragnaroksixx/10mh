﻿using System.Collections;
using UnityEngine;

public interface IAudio
{
    public AudioData AudioInfo { get; }
}
[System.Serializable]
public class AudioData
{
    public AudioClip clip;
    public AudioParameters parameters;
    static readonly AudioParameters DEFAULT_PARAMETERS = new AudioParameters();
    public AudioData(AudioClip clip)
    {
        this.clip = clip;
        this.parameters = DEFAULT_PARAMETERS;
    }
    public AudioData(AudioClip clip, AudioParameters parameters)
    {
        this.clip = clip;
        this.parameters = parameters;
    }
}
[System.Serializable]
public class AudioParameters
{
    public float volume = 1;
    public bool loop = false;
    public bool playWhilePaused = false;
    public AudioCategory category = AudioCategory.SFX;
    public float startTime = 0;
    public bool is3D = false;

    public AudioParameters()
    {
    }

    public AudioParameters(float volume, bool loop, bool playWhilePaused, AudioCategory category, float startTime, bool is3D)
    {
        this.volume = volume;
        this.loop = loop;
        this.playWhilePaused = playWhilePaused;
        this.category = category;
        this.startTime = startTime;
        this.is3D = is3D;
    }
}