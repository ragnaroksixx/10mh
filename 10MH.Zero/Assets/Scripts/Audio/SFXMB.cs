﻿using System.Collections;
using UnityEngine;


public class SFXMB : MonoBehaviour
{
    public AudioVariable sfx;
    AudioReference aRef;
    // Use this for initialization
    void Start()
    {
        aRef=sfx.Play(transform.position);
    }
    private void OnDestroy()
    {
        aRef?.Stop();
    }
}
