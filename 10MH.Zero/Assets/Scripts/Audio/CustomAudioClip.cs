﻿using UnityEngine;
using System.Collections;
using System;
using System.Reflection;
using Sirenix.OdinInspector;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "Audio/Audio Clip")]
public class CustomAudioClip : ScriptableObject, IUnquieScriptableObject, IAudio
{
    public AudioClip clip = null;
    public float volume = 1;
    public bool loop = false;
    public bool playWhilePaused = false;
    public AudioCategory category = AudioCategory.SFX;
    public float startTime = 0;
    public bool is3D = false;

    public string Identifier => name.ToLower().Trim();
    public string[] Alias => alias;

    public string displayName;
    public string displayArtist;
    public string[] alias = new string[] { };
    [NonSerialized]
    AudioData audioInfo = null;
    public AudioData AudioInfo
    {
        get
        {
            if (audioInfo == null)
            {
                AudioParameters aParams = new AudioParameters(
                    volume, loop, playWhilePaused, category, startTime, is3D);
                audioInfo = new AudioData(clip, aParams);
            }
            return audioInfo;
        }
    }

    private AudioClip MakeSubclip(AudioClip c, float start, float stop)
    {
        start = Mathf.Max(0, start);
        stop = Mathf.Min(c.length, stop);
        /* Create a new audio clip */
        int frequency = c.frequency;

        float timeLength = stop - start;
        int samplesLength = (int)(frequency * timeLength * c.channels);
        AudioClip newClip = AudioClip.Create(c.name + "-sub", samplesLength / c.channels, c.channels, frequency, false);
        /* Create a temporary buffer for the samples */
        float[] data = new float[samplesLength];
        /* Get the data from the original clip */
        c.GetData(data, (int)(frequency * start));
        /* Transfer the data to the new clip */
        newClip.SetData(data, 0);
        /* Return the sub clip */
        return newClip;
    }
#if UNITY_EDITOR
    private void Awake()
    {
        if (clip == null)
        {
            if (Selection.activeObject is AudioClip)
            {
                clip = Selection.activeObject as AudioClip;
                name = clip.name;
            }
        }
    }
#endif
    public void Init()
    {
        audioInfo = null;
    }
    public void PlayAt(Transform pos)
    {
        Play(pos.position, -1, -1);
    }
    public void PlayEvent()
    {
        Play(-1, -1);
    }
    public AudioReference Play(float fadeInTime = -1, int section = -1)
    {
        return Play(Vector3.zero, fadeInTime);
    }
    public AudioReference Play(Vector3 pos, float fadeInTime = -1, int section = -1)
    {
        if (category == AudioCategory.BGM)
        {
            return AudioSystem.PlayBGM(this, pos, fadeInTime);
        }
        else
        {
            return AudioSystem.PlaySFX(this, pos, fadeInTime);
        }
    }

    public void Stop(float fadeInTime = -1)
    {
        AudioSystem.Stop(this, false, fadeInTime);
    }
    public void StopAll(float fadeInTime = -1)
    {
        AudioSystem.Stop(this, true, fadeInTime);
    }
#if UNITY_EDITOR
    [Button]
    void PlayEditor()
    {
        PlayClipEditor(clip);
    }

    [Button]
    void StopEditor()
    {
        StopAllClips();
    }
    public static void PlayClipEditor(AudioClip clip, int startSample = 0, bool loop = false)
    {
        Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;

        Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
        MethodInfo method = audioUtilClass.GetMethod(
            "PlayPreviewClip",
            BindingFlags.Static | BindingFlags.Public,
            null,
            new Type[] { typeof(AudioClip), typeof(int), typeof(bool) },
            null
        );

        Debug.Log(method);
        method.Invoke(
            null,
            new object[] { clip, startSample, loop }
        );
    }

    public static void StopAllClips()
    {
        Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;

        Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
        MethodInfo method = audioUtilClass.GetMethod(
            "StopAllPreviewClips",
            BindingFlags.Static | BindingFlags.Public,
            null,
            new Type[] { },
            null
        );

        Debug.Log(method);
        method.Invoke(
            null,
            new object[] { }
        );
    }
#endif
}
