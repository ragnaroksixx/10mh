﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicPlayer : MonoBehaviour
{
    public AudioVariable[] preCalamityTracks;
    public AudioVariable[] calamityTracks;
    List<AudioVariable> activeTracks;
    public static MusicPlayer Instance;
    static AudioReference audioRef;
    int index = 0;

    public AudioReference AudioRef { get => MusicPlayer.audioRef; }
    public int TrackCount => activeTracks.Count;
    private void Awake()
    {
        Instance = this;
        activeTracks = new List<AudioVariable>(preCalamityTracks);
    }
    private void Start()
    {
        TimeController.AddTimeEventCalamity(OnCalamityStart);
    }
    void OnCalamityStart()
    {
        //if (!CycleManager.IsFirstCycle())
        //    LoadTracks(calamityTracks);
    }
    void LoadTracks(AudioVariable[] tracks)
    {
        activeTracks = new List<AudioVariable>(tracks);
        index = -1;
        PlayNext();
    }
    public void Stop(float transitionTime=1)
    {
        audioRef?.Stop(transitionTime);
    }
    public void Pause()
    {
        audioRef?.Pause();
    }
    public void Resume()
    {
        audioRef?.UnPause();
    }
    public void PlayNext()
    {
        index++;
        if (index >= activeTracks.Count || index < 0)
            index = 0;

        Play(activeTracks[index]);
    }

    public void PlayPrev()
    {
        index--;
        if (index < 0)
            index = activeTracks.Count - 1;

        Play(activeTracks[index]);
    }
    public void ReplayCurrent()
    {
        Play(activeTracks[index]);
    }
    void Play(AudioVariable cac, float fadeInTime = -1, int section = -1)
    {
        audioRef?.Stop();
        audioRef = AudioSystem.PlayBGM(cac, Vector3.zero, fadeInTime);
        audioRef.audioObject.onComplete += PlayNext;

        Publisher.Raise(new MusicChangeEvent());
    }

    public void Interrupt(AudioVariable cac, float fadeInTime = -1, int section = -1)
    {
        if (activeTracks.Contains(cac))
        {
            index = activeTracks.IndexOf(cac);
        }
        Play(cac, fadeInTime, section);
    }
}

public class MusicChangeEvent : PublisherEvent
{

}