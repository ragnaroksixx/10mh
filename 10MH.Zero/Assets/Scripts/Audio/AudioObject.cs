﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;
using System.Linq;

public class AudioObject : MonoBehaviour
{
    const float DEFAULT_AUDIO_FADE_DURATION = 1;
    AudioSource source;
    public IAudio clip;
    public Tweener fadeTween;
    public float volumeScaler = 1;
    public delegate float VolumeScaler();
    public delegate void AudioEvent();
    public VolumeScaler volumeFunc;
    public AudioEvent onComplete;
    bool isPaused;
    public AudioSource Source
    {
        get
        {
            return source;
        }

        set
        {
            source = value;
        }
    }

    public float Volume
    {
        get
        {
            return clip.AudioInfo.parameters.volume * volumeScaler;
        }
        set
        {
            if (value == volumeScaler)
                return;
            volumeScaler = value;
            if (!isTransitioning)
                source.volume = Volume;
        }
    }

    public bool IsPaused { get => isPaused; }

    public bool isTransitioning;
    private void Update()
    {
        if (isPaused)
            return;
        if (!source.isPlaying && !source.loop && !isTransitioning)
        {
            volumeFunc = null;
            onComplete?.Invoke();
            AudioSystem.StopInstant(this);
        }
        else
        {
            Volume = volumeFunc();
        }
    }
    public void SetPause(bool val, float transitionTime)
    {
        isPaused = val;
        if (isPaused)
        {
            FadeOut(true, transitionTime);
        }
        else
        {
            FadeIn(true, transitionTime);
        }
    }
    public void RePlay(float transitionTime)
    {
        isPaused = false;
        FadeIn(false, transitionTime);
    }
    public void FadeOut(bool pause, float duration = -1)
    {
        duration = duration == -1 ? DEFAULT_AUDIO_FADE_DURATION : duration;

        if (fadeTween != null)
            fadeTween.Kill(false);

        isTransitioning = true;
        fadeTween = source.DOFade(0, duration).SetUpdate(source.ignoreListenerPause).OnComplete(
            () => { PauseStop(pause); ; isTransitioning = false; });

    }
    void PauseStop(bool pause)
    {
        if (pause)
            source.Pause();
        else
            AudioSystem.StopInstant(this);
    }

    public void FadeIn(bool pause, float duration = -1)
    {
        duration = duration == -1 ? DEFAULT_AUDIO_FADE_DURATION : duration;
        source.volume = 0;
        if (fadeTween != null)
            fadeTween.Kill(false);
        if (pause)
        {
            source.UnPause();
        }
        else
        {
            source.time = clip.AudioInfo.parameters.startTime;
            source.Play();
        }

        isTransitioning = true;
        fadeTween = source.DOFade(Volume, duration).OnComplete(
            () => { isTransitioning = false; });
    }

    public float DefaultVolume()
    {
        return 1;
    }

    float[] _audioOutputArr = new float[128];
    public float GetAmplitude()
    {
        Source.GetOutputData(_audioOutputArr, 0);
        return (_audioOutputArr.Max());
    }
}
