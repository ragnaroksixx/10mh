﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Settings
{
    public class GameSettings : MonoBehaviour
    {
        public TMP_Dropdown dialogueSpeedDropdown;
        // Use this for initialization
        void Start()
        {
            List<string> dSpeeds = new List<string>() { "FAST", "NORMAL", "SLOW" };
            dialogueSpeedDropdown.AddOptions(dSpeeds);
            dialogueSpeedDropdown.onValueChanged.AddListener(OnSpeedChanged);
            dialogueSpeedDropdown.value = PlayerPrefs.GetInt("DIALOGUE_SPEED", 1);
        }

        void OnSpeedChanged(int i)
        {
            float speed = 1;
            switch (i)
            {
                case 0:
                    speed = 2;
                    break;
                case 1:
                    speed = 1;
                    break;
                case 2:
                    speed = 0.5f;
                    break;
                default:
                    break;
            }
            PlayerPrefs.SetInt("DIALOGUE_SPEED", i);
            TextTyperSettings.SetTextSpeed(speed);
        }
    }
}