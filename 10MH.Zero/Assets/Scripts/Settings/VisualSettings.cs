﻿using System.Collections;
using UnityEngine;
using TMPro;
using System.Collections.Generic;

namespace Assets.Scripts.Settings
{
    public class VisualSettings : MonoBehaviour
    {
        public TMP_Dropdown qualityDropdown, windowModeDropdown, resolutionsDropdown;
        // Use this for initialization
        void Start()
        {
            qualityDropdown.AddOptions(new List<string>(QualitySettings.names));
            qualityDropdown.onValueChanged.AddListener(OnQualityChanged);
            qualityDropdown.value = PlayerPrefs.GetInt("QUALITY", 3);

            //resolutionsDropdown.AddOptions(new List<string>());

            foreach (Resolution res in Screen.resolutions)
            {
                string name = res.width + "x" + res.height + " " + res.refreshRate;
            }

            //resolutionsDropdown.onValueChanged.AddListener(OnResolutionChanged);
            //resolutionsDropdown.value = PlayerPrefs.GetInt("RESOLUTION", 3);

            List<string> windows = new List<string>() { "Fullscreen", "Borderless", "Windowed" };
            windowModeDropdown.AddOptions(windows);
            windowModeDropdown.onValueChanged.AddListener(OnWindowChanged);
            windowModeDropdown.value = PlayerPrefs.GetInt("WINDOW_MODE", (int)Screen.fullScreenMode);
        }

        void OnQualityChanged(int i)
        {
            QualitySettings.SetQualityLevel(i, true);
            PlayerPrefs.SetInt("QUALITY", i);
        }

        void OnWindowChanged(int i)
        {
            FullScreenMode mode = FullScreenMode.Windowed;
            switch (i)
            {
                case 0:
                    mode = FullScreenMode.ExclusiveFullScreen;
                    break;
                case 1:
                    mode = FullScreenMode.FullScreenWindow;
                    break;
                case 2:
                    mode = FullScreenMode.Windowed;
                    break;
                default:
                    break;
            }
            PlayerPrefs.SetInt("WINDOW_MODE", i);
            Screen.fullScreenMode = mode;
        }
    }
}