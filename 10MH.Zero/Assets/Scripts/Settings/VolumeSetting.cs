﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSetting : MonoBehaviour
{
    public VolumeType volumeType;
    public Image slider;
    public Button incrementButton, decrementButton;

    public enum VolumeType
    {
        MASTER,
        BGM,
        SFX
    }
    // Use this for initialization
    void Start()
    {
        float val = 0;
        switch (volumeType)
        {
            case VolumeType.MASTER:
                val = AudioSystem.GetMASTER();
                break;
            case VolumeType.BGM:
                val = AudioSystem.GetBGM();
                break;
            case VolumeType.SFX:
                val = AudioSystem.GetSFX();
                break;
            default:
                break;
        }
        slider.fillAmount = (val / 100);
        incrementButton.onClick.AddListener(Increment);
        decrementButton.onClick.AddListener(Decrement);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Increment()
    {
        float val = slider.fillAmount * 100;
        val += 10;
        val = Mathf.Clamp(val, 0, 100);
        Set(val);
    }

    public void Decrement()
    {
        float val = slider.fillAmount*100;
        val -= 10;
        val = Mathf.Clamp(val, 0, 100);
        Set(val);
    }

    public void Set(float val)
    {
        switch (volumeType)
        {
            case VolumeType.MASTER:
                AudioSystem.SetMasterPercentage(val);
                break;
            case VolumeType.BGM:
                AudioSystem.SetBGMPercentage(val);
                break;
            case VolumeType.SFX:
                AudioSystem.SetSFXPercentage(val);
                break;
            default:
                break;
        }
        slider.fillAmount = (val / 100);
    }
}
