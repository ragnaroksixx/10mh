﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SaveData
{
    public string gameSeed = "testSeed";
    public MemorySet memories=new MemorySet();
    public string currentInventoryDataID = "default";
    public TimeRef startTime, endTime;
    public int numCycles = 0;
    public string hand = "hand";

    const string seedKey = "seedKey";
    const string playerInvKey = "playerinvKey";
    const string invDataKey = "invdataKey";
    const string cyclesKey = "cycleKey";
    const string timeDataKey = "timedataKey";
    const string handKey = "handKeyKey";

    public static SaveData currentSave = null;

    public static void SetSave(SaveData s)
    {
        currentSave = s;
    }
    public void UpdateSave()
    {
        memories = CycleManager.Instance.PermanentEvents;
        memories.UpdateInventory("player", PlayerInventory.Instance.GetSaveableItems());
        currentInventoryDataID = PlayerInventory.Instance.data.Identifier;
        //chestInventory is updated seperated per chest
    }
    public void SetTimes(TimeRef start, TimeRef end)
    {
        startTime = start;
        endTime = end;
    }

    public void GetSolArm()
    {
        hand = "solarm";
        PlayerInventory.Instance.AddSolArm();
    }
    public Hashtable ToHashtable()
    {
        Hashtable h = new Hashtable();
        h.Add(seedKey, gameSeed);

        h.Add(invDataKey, currentInventoryDataID);

        MemorySet.ToHastable(memories, h);

        h.Add(timeDataKey + "s", startTime.GetData());
        h.Add(timeDataKey + "e", endTime.GetData());
        h.Add(cyclesKey, numCycles);
        h.Add(handKey, hand);
        return h;
    }
    public string ToJSON()
    {
        Hashtable h = ToHashtable();
        string json = easy.JSON.JsonEncode(h);
        return json;
    }
    public void Init(Hashtable h)
    {
        gameSeed = h.GetString(seedKey);

        currentInventoryDataID = h.GetString(invDataKey, "default");

        memories = MemorySet.FromHashtable(h);

        startTime = new TimeRef(h.GetString(timeDataKey + "s"));
        endTime = new TimeRef(h.GetString(timeDataKey + "e"));
        numCycles = h.GetInteger(cyclesKey);
        hand = h.GetString(handKey);

    }
    public SaveData(string json)
    {
        Hashtable h = easy.JSON.JsonDecode(json) as Hashtable;
        Init(h);
    }
    public SaveData(MemorySet mSet)
    {
        memories = mSet;
    }

    public static void SaveGame()
    {
        currentSave.UpdateSave();
        SaveGame(currentSave);
    }
    public static void SaveGame(SaveData sd)
    {
        PlayerPrefs.SetString("save1", sd.ToJSON());
    }

    public static SaveData LoadGame()
    {
        string json = PlayerPrefs.GetString("save1");
        SaveData s = new SaveData(json);
        return s;
    }
    public static bool HasSave()
    {
        return PlayerPrefs.HasKey("save1");
    }
}
