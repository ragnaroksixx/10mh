﻿using DG.Tweening;
using System;
using UnityEngine;

public class FadeScreenAnimator : OnScreenAnimatorBASE
{
    public CanvasGroup cGroup;
    public float onAlpha = 1.0f;
    protected override void OffScreenImpl(Action onComplete, bool instant)
    {
        if (instant)
        {
            cGroup.alpha = onAlpha;
        }
        else
        {
            cGroup.DOFade(0, transitionSpeed);
            cGroup.blocksRaycasts = cGroup.interactable = false;
        }
    }

    protected override void OffScreenImplEDITOR()
    {
        cGroup.alpha = 0;
        cGroup.blocksRaycasts = cGroup.interactable = false;
    }

    protected override void OnScreenImpl(Action onComplete, bool instant)
    {
        if (instant)
        {
            cGroup.alpha = onAlpha;
        }
        else
        {
            cGroup.DOFade(onAlpha, transitionSpeed);
        }
        cGroup.blocksRaycasts = cGroup.interactable = true;
    }

    protected override void OnScreenImplEDITOR()
    {
        cGroup.alpha = onAlpha;
        cGroup.blocksRaycasts = cGroup.interactable = true;
    }
}

