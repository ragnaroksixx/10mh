﻿using UnityEngine;
using System.Collections;
using System;
using Sirenix.OdinInspector;

public abstract class OnScreenAnimatorBASE : MonoBehaviour
{
    public float transitionSpeed = 0.25f;
    public AudioVariable onSFX, offSFX;
    protected bool isOnScreen, isTransitioning;
    public RectTransform rectTransform
    {
        get => transform as RectTransform;
    }
    public bool IsOnScreen { get => isOnScreen; }
    public bool IsTransitioning { get => isTransitioning; }

    public void AnimateOnScreen(Action onComplete = null, bool instant = false)
    {
        if (!isOnScreen)
            onSFX?.Play();
        OnScreenImpl(onComplete, instant);
        isOnScreen = true;
    }
    protected abstract void OnScreenImpl(Action onComplete, bool instant);
    protected abstract void OnScreenImplEDITOR();

    public void AnimateOffScreen(Action onComplete = null, bool instant = false)
    {
        if (isOnScreen)
            offSFX?.Play();
        OffScreenImpl(onComplete, instant);
        isOnScreen = false;
    }
    protected abstract void OffScreenImpl(Action onComplete, bool instant);
    protected abstract void OffScreenImplEDITOR();

    public void Toggle(Action onComplete = null)
    {
        Animate(!isOnScreen, onComplete);
    }

    public void Animate(bool onScreen, Action onComplete = null)
    {
        if (onScreen)
        {
            AnimateOnScreen(onComplete);
        }
        else
        {
            AnimateOffScreen(onComplete);
        }
    }

    [Button("Animate On Screen")]
    public void AnimateOnScreenEditor()
    {
        OnScreenImplEDITOR();
        isOnScreen = true;
    }
    [Button("Animate Off Screen")]
    public void AnimateOffScreenEditor()
    {
        OffScreenImplEDITOR();
        isOnScreen = false;
    }
}
