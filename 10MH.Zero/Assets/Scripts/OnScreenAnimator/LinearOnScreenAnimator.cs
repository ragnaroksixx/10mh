﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using System;

public class LinearOnScreenAnimator : OnScreenAnimatorBASE
{
    public float onScreenPos;
    public float offScreenPos;
    public TransitionDirection transition;

    public enum TransitionDirection
    {
        X,
        Y
    }
    private void Start()
    {
        //gameObject.SetActive(false);
    }
    protected override void OnScreenImpl(Action onComplete, bool instant)
    {
        MoveTo(onScreenPos, instant, onComplete);
    }
    protected override void OnScreenImplEDITOR()
    {
        MoveTo(onScreenPos, true, null);
    }
    protected override void OffScreenImpl(Action onComplete, bool instant)
    {
        MoveTo(offScreenPos, instant, onComplete);
    }
    protected override void OffScreenImplEDITOR()
    {
        MoveTo(offScreenPos, true, null);
    }
    public void AnimateOffScreenCustomPos(float pos, Action onComplete = null, bool instant = false)
    {
        if (isOnScreen)
            offSFX?.Play();
        MoveTo(pos, instant, onComplete);
        isOnScreen = false;
    }
    public void AnimateOnScreenCustomPos(float pos, Action onComplete = null, bool instant = false)
    {
        if (!isOnScreen)
            onSFX?.Play();
        MoveTo(pos, instant, onComplete);
        isOnScreen = true;
    }
    protected virtual void MoveTo(float pos, bool instant, Action onComplete = null)
    {
        Vector2 vec = rectTransform.anchoredPosition;
        if (transition == TransitionDirection.Y)
            vec.y = pos;
        else
            vec.x = pos;
        if (instant)
        {
            rectTransform.anchoredPosition = vec;
            onComplete?.Invoke();
            isTransitioning = false;
        }
        else
        {
            isTransitioning = true;
            rectTransform.DOAnchorPos(vec, transitionSpeed).OnComplete
                (() => { onComplete?.Invoke(); isTransitioning = false; })
                .SetEase(Ease.Linear)
                .SetUpdate(true);
        }
    }

}
