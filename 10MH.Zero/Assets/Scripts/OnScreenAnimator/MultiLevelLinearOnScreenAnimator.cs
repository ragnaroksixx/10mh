﻿using System;
using System.Collections;
using UnityEngine;


public class MultiLevelLinearOnScreenAnimator : LinearOnScreenAnimator
{
    public float[] onScreenPositions;

    public void AnimateOnScreenTier(int index, Action onComplete = null, bool instant = false)
    {
        if (!isOnScreen)
            onSFX?.Play();
        onScreenPos = onScreenPositions[index];
        AnimateOnScreen(onComplete, instant);
    }
}
