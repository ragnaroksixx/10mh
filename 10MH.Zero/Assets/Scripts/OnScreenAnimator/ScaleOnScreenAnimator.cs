﻿using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;


public class ScaleOnScreenAnimator : FadeScreenAnimator
{
    public Vector3 onScale = Vector3.one;
    public Vector3 offScale = Vector3.zero;
    float scaleSpeed => transitionSpeed * 1.5f;
    public Transform scaleRoot;
    protected virtual void Awake()
    {
        if (scaleRoot == null)
            scaleRoot = transform;
    }
    protected override void OffScreenImpl(Action onComplete, bool instant)
    {
        base.OffScreenImpl(onComplete, instant);
        scaleRoot.DOScale(offScale, scaleSpeed);
    }

    protected override void OffScreenImplEDITOR()
    {
        base.OffScreenImplEDITOR();
        scaleRoot.localScale = offScale;
    }

    protected override void OnScreenImpl(Action onComplete, bool instant)
    {
        scaleRoot.localScale = offScale;
        base.OnScreenImpl(onComplete, instant);
        if (instant)
            scaleRoot.localScale = onScale;
        else
        {
            scaleRoot.DOScale(onScale, scaleSpeed)
                .SetEase(Ease.OutBack);
        }
    }

    protected override void OnScreenImplEDITOR()
    {
        base.OnScreenImplEDITOR();
        scaleRoot.localScale = onScale;
    }
}
