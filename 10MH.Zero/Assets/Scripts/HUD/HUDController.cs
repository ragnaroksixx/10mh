﻿using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
    public Transform controlsRoot;
    public static HUDController Instance;
    public float fadeVal = 0.75f;
    public TMP_Text notificationTextField;
    public Image backgroundFade;
    public PlayerInfoMenu pMenu;
    public InventoryManager iMenu;

    public Graphic[] hudImages;
    public HUDQuickItem phoneQuickUI, skateboardQuickUI, bagQuickUI;

    public OnScreenAnimatorBASE timeAnimator;
    public Image staminaUI, staminaImageBG;
    public Sprite staminaNormalSprite, staminaUseSprite;
    public Color staminaNormalColor, staminaExhaustedColor;

    private void Awake()
    {
        Instance = this;
        Publisher.Subscribe<NotificationEvent>(OnNotificationRecieved);
        Publisher.Subscribe<CombatNotificationEvent>(OnCombatNotificationRecieved);
        Publisher.Subscribe<ClearNotificationEvent>(OnClearNotificationRecieved);
        backgroundFade.DOFade(0, 0);
        staminaUI.color = staminaNormalColor;
    }
    private void Start()
    {
        skateboardQuickUI.Set(skateboard, "SHIFT");
        bagQuickUI.Set(bag, "TAB");
        UpdatePermanentItems();
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<NotificationEvent>(OnNotificationRecieved);
        Publisher.Unsubscribe<CombatNotificationEvent>(OnCombatNotificationRecieved);
        Publisher.Unsubscribe<ClearNotificationEvent>(OnClearNotificationRecieved);
    }
    float minSpeed = 0.25f;
    float maxSpeed = 1f;
    public void OnNotificationRecieved(NotificationEvent e)
    {
        notificationTextField.DOKill();
        if (string.IsNullOrEmpty(e.text))
        {
            notificationTextField.text = "";
        }
        else
        {
            float speed = Mathf.Clamp(e.text.Length / 8, minSpeed, maxSpeed);
            if (e.instant)
                speed = 0;
            notificationTextField.DOText(e.text, speed);
        }
    }
    public void OnClearNotificationRecieved(ClearNotificationEvent e)
    {
        notificationTextField.DOKill();
        notificationTextField.DOText("", 0.1f).SetDelay(e.delay);
    }
    public void OnCombatNotificationRecieved(CombatNotificationEvent e)
    {
        notificationTextField.DOKill();
        notificationTextField.text = e.text;
        notificationTextField.rectTransform.DOShakeScale(0.25f);
    }

    public void ShowInventoryHelp()
    {
        Publisher.Raise(new NotificationEvent("[TAB] to close", true));
    }

    public void FadeIn(float time = 0.1f, float val = -1)
    {
        if (val == -1)
            val = fadeVal;
        backgroundFade.DOKill();
        backgroundFade.DOFade(val, time);
    }
    public void FadeOut(float time = 0.1f)
    {
        backgroundFade.DOKill();
        backgroundFade.DOFade(0, time);
    }

    public void SetTimeVisibility(bool val)
    {
        if (val)
        {
            timeAnimator.AnimateOffScreen(null, true);
            timeAnimator.gameObject.SetActive(true);
            timeAnimator.AnimateOnScreen(null, false);
        }
        else
        {
            timeAnimator.AnimateOffScreen(null, true);
            timeAnimator.gameObject.SetActive(false);
        }
    }
    public void SetQuickItemVisibility(bool val)
    {
        phoneQuickUI.gameObject.SetActive(false);
        skateboardQuickUI.gameObject.SetActive(val);
        SetBagUI(val);
    }
    public void SetBagUI(bool val)
    {
        bagQuickUI.gameObject.SetActive(val);
    }
    public void SetHUDVisibility(bool val)
    {
        SetTimeVisibility(val);
        SetQuickItemVisibility(val);
    }
    [Button]
    public void SetHUDColor(Color c)
    {
        foreach (Graphic graphic in hudImages)
        {
            if (graphic is TMP_Text)
            {
                c.a = (graphic as TMP_Text).outlineColor.a;
                (graphic as TMP_Text).outlineColor = c;
            }
            //Refactor this later #copium
            else if (graphic.GetComponent<Outline>())
            {
                c.a = graphic.GetComponent<Outline>().effectColor.a;
                graphic.GetComponent<Outline>().effectColor = c;
            }
            else
            {
                c.a = graphic.color.a;
                graphic.color = c;
            }
        }
    }

    public Item phone, skateboard, bag;
    public void UpdatePermanentItems()
    {
        if (false && PlayerInventory.HasPermanentInvItem(phone))
        {
            phoneQuickUI.Set(phone, "ESC");
        }
        else
        {
            phoneQuickUI.gameObject.SetActive(false);
        }
    }

    public void UpdateStaminia(float val)
    {
        staminaUI.fillAmount = 1.0f - val;
    }
    public void SetStaminiaLock(bool isLocked)
    {
        if (isLocked)
        {
            staminaUI.color = staminaExhaustedColor;
            staminaUI.DoFadeFlash(0.25f, 1.0f, 0.25f, 0.15f, -1, true);
        }
        else
        {
            staminaUI.color = staminaNormalColor;
            staminaUI.DOKill();
            staminaUI.DOFade(1, 0);
        }
    }

    public void OnSprintStart()
    {
        skateboardQuickUI.OnUseStart();
        staminaImageBG.sprite = staminaUI.sprite = staminaUseSprite;

        staminaImageBG.transform.DOKill();
        staminaImageBG.transform.DOScale(1.5f, 0.25f);
    }

    public void OnSprintEnd()
    {
        skateboardQuickUI.OnUseEnd();
        staminaImageBG.sprite = staminaUI.sprite = staminaNormalSprite;

        staminaImageBG.transform.DOKill();
        staminaImageBG.transform.DOScale(1.0f, 0.25f);
    }
}

public class NotificationEvent : PublisherEvent
{
    public string text;
    public bool instant = false;
    public NotificationEvent(string text)
    {
        this.text = text;
    }
    public NotificationEvent(string text, bool instant)
    {
        this.text = text;
        this.instant = instant;
        Debug.Log("Notification: " + text);
    }
}

public class ClearNotificationEvent : NotificationEvent
{
    public float delay;
    public ClearNotificationEvent() : base("")
    {
        delay = 0;
    }
    public ClearNotificationEvent(float d) : base("")
    {
        delay = d;
    }
}
public class CombatNotificationEvent : PublisherEvent
{
    public string text;

    public CombatNotificationEvent(string text)
    {
        this.text = text;
        //Debug.Log("Combat Notification: " + text);
    }
}

