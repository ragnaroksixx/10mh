﻿using MoreMountains.Feedbacks;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HUDQuickItem : MonoBehaviour
{
    public Image itemImage;
    public TMP_Text inputText;
    public Image bgImage;
    Color bgStartColor;
    public Color bgUseColor;
    Item item;
    public UnityEvent onUse, onUseEnd;
    public void Awake()
    {
        itemImage.enabled = false;
        inputText.enabled = false;
        bgStartColor = bgImage.color;
    }
    public void OnUseStart()
    {
        onUse?.Invoke();
        bgImage.color = bgUseColor;
    }

    public void OnUseEnd()
    {
        onUseEnd?.Invoke();
        bgImage.color = bgStartColor;
    }
    public void Set(Item i, string input)
    {
        if (i == item) return;
        item = i;
        itemImage.enabled = true;
        inputText.enabled = true;
        itemImage.sprite = i.image;
        itemImage.transform.localEulerAngles = new Vector3(0, 0, item.imageRotation);
        inputText.text = input;
    }
}
