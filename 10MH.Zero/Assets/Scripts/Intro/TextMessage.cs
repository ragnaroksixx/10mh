﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;
using Febucci.UI;
using DG.Tweening;
public class TextMessage : MonoBehaviour
{
    public Image characterImage;
    public TextAnimatorPlayer text;
    public TMP_Text nameField;
    public Outline colorOutline;
    public CustomAudioClip notifSFX;
    VerticalLayoutGroup vlg;

    Transform imageRoot => characterImage.transform.parent.parent;
    Transform textRoot => text.transform.parent;
    TextMessageData tmd;
    public bool showNames = true;
    public bool showPortrait = true;

    public void Init(TextMessageData tmd)
    {
        this.tmd = tmd;
        vlg = GetComponentInParent<VerticalLayoutGroup>();

        Vector3 aPos = characterImage.rectTransform.anchoredPosition;
        aPos.y -= 50;
        characterImage.rectTransform.anchoredPosition = aPos;

        characterImage.sprite = tmd.sender.GetImageEditor(ImageKey.NORMAL);
        text.SetTypewriterSpeed(1.5f);

        imageRoot.localScale = new Vector3(0, 0, 1);

        nameField.text = tmd.sender.displayName;
        nameField.color = tmd.sender.characterColor;
        colorOutline.effectColor = tmd.sender.characterColor;

        vlg.enabled = false;
        vlg.enabled = true;

    }

    public IEnumerator Write(bool instant = false)
    {
        if (!instant)
        {
            notifSFX?.Play();
            imageRoot.localScale = new Vector3(0, 0, 1);
            textRoot.localScale = new Vector3(1, 0, 1);
        }

        imageRoot.gameObject.SetActive(showPortrait);
        nameField.gameObject.SetActive(showNames);

        if (!instant)
        {
            imageRoot.DOScale(Vector3.one, 0.45f).SetEase(Ease.OutBounce);
            textRoot.DOScaleY(1, 0.25f).SetEase(Ease.OutBounce);
        }else
        {
            imageRoot.localScale = Vector3.one;
            textRoot.localScale = Vector3.one;
        }

        if (!instant)
            yield return new WaitForSeconds(0.1f);

        if (!instant)
        {
            characterImage.rectTransform.DOLocalMoveY(50, 0.3f).SetRelative(true);
        }
        else
        {
            Vector3 aPos = characterImage.rectTransform.anchoredPosition;
            aPos.y += 50;
            characterImage.rectTransform.anchoredPosition = aPos;
        }

        text.ShowText(tmd.message);
        if (instant)
            text.SkipTypewriter();

        vlg.enabled = false;
        vlg.enabled = true;

        yield return null;
    }
}
