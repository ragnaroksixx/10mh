﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TextChain
{
    public string chatName;
    public List<TextMessageData> messages;
    public DialogueVariable futureMessagesScene;
    bool replied;
    public void AddMessage(string sender, string message)
    {
        TextMessageData tmd = new TextMessageData(sender, message);
        messages.Add(tmd);
        Publisher.Raise(new TextChainUpdateEvent(this, tmd));
    }
    public bool AwaitingReply()
    {
        return !replied && futureMessagesScene.scene != null;
    }
    public void Reply()
    {
        replied = true;
        DialogueRenderer.Instance.PlayPhoneScene(futureMessagesScene, null);
    }
}

public class TextChainUpdateEvent : PublisherEvent
{
    public TextChain tChain;
    public TextMessageData sms;

    public TextChainUpdateEvent(TextChain tChain, TextMessageData sms)
    {
        this.tChain = tChain;
        this.sms = sms;
    }
}

public class OpenTextChainEvent : PublisherEvent
{
    public TextChain tChain;

    public OpenTextChainEvent(TextChain tChain)
    {
        this.tChain = tChain;
    }
}
