﻿using System.Collections;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;

public class EnterNameUI : MonoBehaviour
{
    int caretPosition;
    public TMP_Text[] letters;
    Outline[] outlines;
    public AudioVariable onChangeSFX;
    string outText;
    public RectTransform upDownArrows;
    public Button enterButton;
    // Use this for initialization
    void Start()
    {
        outlines = new Outline[letters.Length];
        for (int i = 0; i < letters.Length; i++)
        {
            letters[i].maxVisibleCharacters = 1;
            outlines[i] = letters[i].GetComponentInParent<Outline>();
            letters[i].text = " ";
        }
        outlines[0].enabled = true;
        enterButton.onClick.AddListener(OnSubmit);
    }
    float holdTrack;
    private void Update()
    {
        if (Input.anyKeyDown)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                //move caret left
                DecrementCaret();
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                //move caret right
                IncrementCaret();
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                //increment letter back
                DecrementLetter();
                holdTrack = Time.time + 0.5f;
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                //increment letter forward
                IncrementLetter();
                holdTrack = Time.time + 0.5f;
            }
            else if (Input.GetKeyDown(KeyCode.Return))
            {
                OnSubmit();
            }
            else if (Input.GetKeyDown(KeyCode.Space))
            {
                SetLetter(' ');
                IncrementCaret();
            }
            else if (Input.GetKeyDown(KeyCode.Backspace))
            {
                SetLetter(' ');
                DecrementCaret();
            }
            else
            {
                for (int i = (int)KeyCode.A; i < (int)KeyCode.Z; i++)
                {
                    //Set letter
                    if (Input.GetKeyDown((KeyCode)i))
                    {
                        SetLetter((KeyCode)i);
                        IncrementCaret();
                        break;
                    }
                }
            }
        }

        if (Time.time > holdTrack)
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                //increment letter back
                DecrementLetter();
                holdTrack = Time.time + 0.5f;
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                //increment letter forward
                IncrementLetter();
                holdTrack = Time.time + 0.5f;
            }
        }
    }
    private void OnSubmit()
    {
        outText = "";
        foreach (TMP_Text item in letters)
        {
            outText += item.text;
        }
        outText = outText.Trim();

        if (outText.Length > 0)
        {
            Publisher.Raise(new DialogueWaitEvent());
            Destroy(this.gameObject);
        }
    }
    public void SetLetter(KeyCode key)
    {
        letters[caretPosition].text = key.ToString().ToUpper();
    }
    public void SetLetter(char key)
    {
        letters[caretPosition].text = key.ToString().ToUpper();
    }
    public void IncrementLetter()
    {
        char current = letters[caretPosition].text.ToLower()[0];
        if (current == ' ')
            current = 'a';
        else
            current++;
        if (current > 'z')
            current = ' ';
        letters[caretPosition].text = current.ToString().ToUpper();
    }
    public void DecrementLetter()
    {
        char current = letters[caretPosition].text.ToLower()[0];
        if (current == ' ')
            current = 'z';
        else
            current--;
        if (current < 'a')
            current = ' ';
        letters[caretPosition].text = current.ToString().ToUpper();
    }
    public void IncrementCaret()
    {
        if (caretPosition + 1 >= letters.Length)
            return;

        caretPosition++;

        for (int i = 0; i < letters.Length; i++)
        {
            outlines[i].enabled = i == caretPosition;
        }
        upDownArrows.anchoredPosition = (letters[caretPosition].transform.parent as RectTransform).anchoredPosition;

    }
    public void DecrementCaret()
    {
        if (caretPosition == 0)
            return;

        caretPosition--;

        for (int i = 0; i < letters.Length; i++)
        {
            outlines[i].enabled = i == caretPosition;
        }
        upDownArrows.anchoredPosition = (letters[caretPosition].transform.parent as RectTransform).anchoredPosition;
    }
    //void OnInputChanged(string inputString)
    //{
    //    onChangeSFX.Play();
    //    for (int i = 0; i < letters.Length; i++)
    //    {
    //        outlines[i].enabled = i == caretPosition;
    //    }
    //}
}
