﻿using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;


public class SceneScreenFader : MonoBehaviour
{
    public CanvasGroup image;
    public static SceneScreenFader Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            image.transform.GetChild(0).gameObject.SetActive(false);
            DontDestroyOnLoad(this.gameObject);
        }else
        {
            Destroy(this.gameObject);
        }

    }

    public void Play(Action onBlack = null)
    {
        image.transform.GetChild(0).gameObject.SetActive(true);
        Sequence s = DOTween.Sequence()
            .Append(image.DOFade(1, 1));

        if (onBlack != null)
            s.AppendCallback(onBlack.Invoke);

        s.AppendInterval(1.5f)
            .Append(image.DOFade(0, 1))
            .OnComplete(OnComplete)
            .SetUpdate(true);

        s.Play();
    }
    void OnComplete()
    {
        image.transform.GetChild(0).gameObject.SetActive(false);
    }
}
