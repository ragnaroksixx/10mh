﻿using System.Collections;
using TMPro;
using UnityEngine;

public class TextGroupUI : MonoBehaviour
{
    public TMP_Text groupName, lastMessage;
    TextChain chain;
    public void Set(TextChain tc)
    {
        chain = tc;
        groupName.text = tc.chatName;
        lastMessage.text = tc.messages[tc.messages.Count - 1].message;
    }

    public void OnClick()
    {
        Publisher.Raise(new OpenTextChainEvent(chain));
    }
}
