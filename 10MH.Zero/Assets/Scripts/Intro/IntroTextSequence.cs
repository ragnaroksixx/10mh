﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine.UI;

public class IntroTextSequence : MonoBehaviour
{
    public RectTransform slider;
    public TMP_Text beginText, wakeUpText;
    bool wait = true;

    public TextMessage prefab, replyPrefab;
    public Image darkImage;

    public CanvasGroup fakeLogoGroup;
    public ParticleSystem pSystem;
    public CustomAudioClip stickerSFX;

    public Transform message1Root;
    public TextMessageData[] messages_1,
        messages_2;
    public Color proceedColor;
    public OnScreenAnimatorBASE typingEffectAnimator;
    int phase = 0;
    private void Awake()
    {
        beginText.DOFade(0, 0);
        typingEffectAnimator.AnimateOffScreen(null, true);
    }
    // Use this for initialization
    IEnumerator Start()
    {
        Publisher.Subscribe<SceneTriggerEvent>(OnSceneTriggerEvent);

        yield return WaitForWait();

        DialogueRenderer.Instance.SetProceedColor(proceedColor);
        pSystem.Play();
        fakeLogoGroup.DOFade(1, 0.25f);
        stickerSFX.Play();
        wakeUpText.DOFade(0, 1);


        yield return WaitForWait();

        float max = 0.75f;
        float min = 0;
        float hold = 0.25f;
        float flash = 0.5f;

        Sequence s = beginText.DoFadeFlash(max, min, hold, flash, -1);

        while (!(Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.LeftShift)))
        {
            yield return null;
        }
        s.Kill();
        beginText.DOFade(0, 1);
        float pos = 35;
        foreach (TextMessageData data in messages_1)
        {
            typingEffectAnimator.AnimateOnScreen();
            pos += 60 + 42;
            SetPos(pos);
            yield return new WaitForSeconds(typingEffectAnimator.transitionSpeed);
            yield return DialogueRenderer.Instance.WaitForPlayerInput();

            typingEffectAnimator.AnimateOffScreen(null, true);



            TextMessage tm = GameObject.Instantiate(prefab, message1Root);

            int padding = 0;
            if (phase == 1) padding = 32;
            else if (phase == 3) padding = 32;

            phase = (phase + 1) % 4;

            tm.GetComponent<HorizontalLayoutGroup>().padding.left = padding;
            tm.Init(data);

            yield return null;

            message1Root.GetComponent<VerticalLayoutGroup>().enabled = false;
            message1Root.GetComponent<VerticalLayoutGroup>().enabled = true;

            yield return null;

            message1Root.parent.GetComponent<VerticalLayoutGroup>().enabled = false;
            message1Root.parent.GetComponent<VerticalLayoutGroup>().enabled = true;

            yield return null;

            yield return tm.Write();
        }

        s = wakeUpText.DoFadeFlash(max, min, hold, flash, -1);
        pos += 60;
        SetPos(pos);
        while (!(Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.LeftShift)))
        {
            yield return null;
        }
        s.Kill();
        wakeUpText.DOFade(0, 1);

        MusicPlayer.Instance.Stop();
        darkImage.DOFade(1, 1);

        yield return new WaitForSeconds(2);

        DialogueRenderer.Instance.RevertColor();

        Publisher.Raise(new DialogueWaitEvent());

    }
    IEnumerator WaitForWait()
    {
        while (wait)
            yield return null;
        wait = true;
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<SceneTriggerEvent>(OnSceneTriggerEvent);
    }
    private void SetPos(float val)
    {
        slider.DOAnchorPosY(val, 0.5f);
    }

    void OnSceneTriggerEvent(SceneTriggerEvent e)
    {
        wait = false;
    }
}

[System.Serializable]
public class TextMessageData
{
    public CharacterData sender;
    [TextArea(5, 20)]
    public string message;

    public TextMessageData(string s,string m)
    {
        message=m;
        sender = GlobalData10MH.Instance.characterDictionary
                    .GetItem(s);
    }
}
