﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class StarShakeEffect : MonoBehaviour
{
    // public static float waitTime = 7.5f;
    public float delta = 40;
    public float delay = 0.4f;

    Vector3 startRot;
    Vector3 leftRot;
    Vector3 rightRot;
    //public bool ignoreWait = false;
    private void Awake()
    {
        startRot = transform.localEulerAngles;
        leftRot = startRot;
        leftRot.z -= delta;
        rightRot = startRot;
        rightRot.z += delta;
    }
    private void OnEnable()
    {
        StartCoroutine(Shake(startRot, leftRot, rightRot));
    }
    IEnumerator Shake(Vector3 start, Vector3 left, Vector3 right)
    {
        //if (!ignoreWait)
        //    yield return new WaitForSeconds(waitTime);
        // ignoreWait = true;
        while (true)
        {
            yield return new WaitForSeconds(delay);
            transform.localEulerAngles = left;
            yield return new WaitForSeconds(delay);
            transform.localEulerAngles = start;
            yield return new WaitForSeconds(delay);
            transform.localEulerAngles = right;
            yield return new WaitForSeconds(delay);
            transform.localEulerAngles = start;
        }
    }

}

