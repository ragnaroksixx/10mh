﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;

public class SceneWorldTrigger : WorldTrigger
{
    public Scene s;
    public bool runTriggerAfterScene = false;
    private void Awake()
    {
        if (!runTriggerAfterScene)
            RegisterTriggerEnterEvent(() => { DialogueRenderer.Instance.PlayScene(s, null); });
    }
    protected override void OnTriggerEnter(Collider other)
    {
        if (runTriggerAfterScene)
        {
            DialogueRenderer.Instance.PlayScene(s, onTriggerEnter.Invoke);
        }
        else
            base.OnTriggerEnter(other);
    }
}
