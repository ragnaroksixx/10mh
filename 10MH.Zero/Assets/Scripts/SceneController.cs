﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif
public static class SceneController
{
    public static AreaData activeEnvironmentScene;
    public static AreaTransitionData activeSpawnPoint;
    static AsyncOperation Load(string scene)
    {
        return SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
    }
    public static void UnloadScene(string scene)
    {
        if (!SceneManager.GetSceneByName(scene).isLoaded)
            return;
        SceneManager.UnloadSceneAsync(scene);
    }
    public static void StartMapScene()
    {
        Load("MapScene");
        UnloadScene(activeEnvironmentScene.scene);
    }
    public static void UnloadMapScene()
    {
        UnloadScene("MapScene");
    }
    public static bool HackUnloadActiveArea()
    {
        if (activeEnvironmentScene)
        {
            UnloadScene(activeEnvironmentScene.scene);
            return true;
        }
        return false;
    }
    public static void SetActiveEnvironment(AreaTransitionData atd, Action a = null, string transitionScene = "SideSwipeAreaTransition")
    {
        activeSpawnPoint = atd;
        SetActiveEnvironment(atd.GetArea(), a, transitionScene);
    }
    public static void SetActiveEnvironment(AreaData area, Action a = null, string transitionScene = "SideSwipeAreaTransition")
    {
        LoadAreaTransition(transitionScene);

        if (activeEnvironmentScene)
            UnloadScene(activeEnvironmentScene.scene);
        activeEnvironmentScene = area;
        AsyncOperation ao = Load(activeEnvironmentScene.scene);
        ao.completed += OperationOnCompleted;
        ao.completed += (AsyncOperation obj) => { OnSceneLoaded(a); };
    }
    private static void OperationOnCompleted(AsyncOperation obj)
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(activeEnvironmentScene.scene));
    }
    private static void OnSceneLoaded(Action a)
    {
        a?.Invoke();
    }
    public static void LoadAreaTransition(string scene = "AreaTransitionLoading")
    {
        if (activeEnvironmentScene&& scene!= "RewindTransition")
            ScreenShotUtilities.CacheScreen();
        Load(scene);
    }
    public static void UnloadAreaTransition()
    {
        string scene = SceneTransitionEffect.Instance.gameObject.scene.name;
        UnloadScene(scene);
    }
    public static void LoadMainGame()
    {
        SceneTransitionEffect.waitTime = 1.5f;
        SceneManager.LoadScene("MasterScene");
    }
    public static void LoadStartMenu()
    {
        SceneManager.LoadScene(0);
    }
#if UNITY_EDITOR
    public static void LoadSceneEditor(string scene, OpenSceneMode mode = OpenSceneMode.Single)
    {
        if (string.IsNullOrEmpty(scene))
            return;
        string path = "Assets/Scenes/";
        path += scene + ".unity";
        EditorSceneManager.OpenScene(path, mode);
    }

    public static void LoadSceneEditor(SceneAsset sa, OpenSceneMode mode = OpenSceneMode.Single)
    {
        string scene = AssetDatabase.GetAssetPath(sa);
        if (string.IsNullOrEmpty(scene))
            return;
        EditorSceneManager.OpenScene(scene, mode);
    }
#endif

}
