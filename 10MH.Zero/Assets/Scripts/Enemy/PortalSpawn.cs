﻿using DG.Tweening;
using MoreMountains.Feedbacks;
using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;


public class PortalSpawn : SpawnController
{
    public Transform spawnPoint;
    public GameObject enemyPrefab;
    public float movePoint = 1;
    public EFollowState followState;
    public MMF_Player openVFX;

    protected override void Awake()
    {
        if (spawnAtCalamity)
            spawnTime = TimeController.Instance.calamityTime;
        transform.localScale = new Vector3(1, 0, 0);
        if (TimeController.IsPast(spawnTime))
        {
            RegularSpawn();
        }
        else
            base.Awake();
    }
    [Button]
    void PortalEditor()
    {
        if (Application.isPlaying)
        {
            gameObject.SetActive(true);
            StartCoroutine(SpawnWithPortal());
        }
    }

    protected override void Spawn()
    {
        base.Spawn();
        StartCoroutine(SpawnWithPortal());
    }

    public VoidVisualController SpawnAt(Vector3 pos)
    {
        VoidVisualController enemy = GameObject.Instantiate(enemyPrefab, pos, Quaternion.identity)
            .GetComponent<VoidVisualController>();
        return enemy;
    }

    public IEnumerator SpawnWithPortal()
    {
        VoidVisualController enemy = SpawnAt(spawnPoint.position);
        openVFX.PlayFeedbacks();

        spawnPoint.position = spawnPoint.position + spawnPoint.forward * movePoint;
        yield return null;
        yield return null;
        enemy.target = spawnPoint;
        enemy.SetState(followState);

        yield return new WaitForSeconds(openVFX.TotalDuration / 1.1f);

        openVFX.PlayFeedbacksInReverse();
        yield return new WaitForSeconds(1.5f);
        Destroy(gameObject);

    }

    public void RegularSpawn()
    {
        spawnPoint.position = spawnPoint.position + spawnPoint.forward * movePoint;
        VoidVisualController enemy = SpawnAt(spawnPoint.position);
    }

}
