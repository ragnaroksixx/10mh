﻿using UnityEngine;

[CreateAssetMenu(menuName = "Enemy State Machine/Follow")]
public class EFollowState : EMoveState
{
    public EMoveState nextState;
    public float moveSpeed = 10, angularSpeed = 200;
    public bool changeOnReach;

    public override void Enter(VoidVisualController vvc)
    {
    }

    public override void Exit(VoidVisualController vvc)
    {
    }

    public override void OnUpdate(VoidVisualController vvc)
    {
        VoidVisualController unit = vvc;
        //Vector3 flatForw = unit.body.forward;
        // Vector3 flatTarget = unit.lookAtTarget.position - unit.body.position;
        Vector3 target = unit.target.position - unit.head.position;
        //flatTarget.y = 0;
        //flatForw.y = 0;


        //unit.body.up = Vector3.SmoothDamp(unit.body.up, unit.target.position - unit.body.position, ref unit.bodyV, angularSpeed * Time.deltaTime);
        unit.head.forward = Vector3.SmoothDamp(unit.head.forward, target, ref unit.headV, angularSpeed * Time.deltaTime);

        //Vector3 targetPos = Vector3.SmoothDamp(unit.body.position, unit.moveTarget.position, ref unit.moveV, unit.sSpeed * Time.deltaTime);
        unit.body.position = Vector3.MoveTowards(unit.body.position, unit.body.position + unit.head.forward, moveSpeed * Time.deltaTime);

        if (changeOnReach)
        {
            Vector3 dis = unit.body.position - unit.target.position;
            dis.y = 0;
            if (dis.magnitude < 0.1f)
            {
                unit.SetState(nextState);
            }
        }
    }
}