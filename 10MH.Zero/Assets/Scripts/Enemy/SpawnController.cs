﻿using System.Collections;
using UnityEngine;


public class SpawnController : MonoBehaviour
{
    public TimeRef spawnTime;
    public bool spawnAtCalamity;
    protected virtual void Awake()
    {
        if (spawnAtCalamity)
            spawnTime = TimeController.Instance.calamityTime;
        gameObject.SetActive(false);
        TimeController.TriggerOrAdd(spawnTime, Spawn);
    }

    private void OnDestroy()
    {
        TimeController.RemoveTimeEvent(spawnTime, Spawn);
    }
    protected virtual void Spawn()
    {
        gameObject.SetActive(true);
    }
}
