using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoidVisualController : MonoBehaviour
{
    public Transform head, body, eye, halo;
    [HideInInspector]
    public Vector3 bodyV, headV, moveV;

    EMoveState currentState;
    public EMoveState initState;
    public Transform target;

    [HideInInspector]
    public IKManager iks;

    public EMoveState sleep;
    public EMoveState crawl, stand;
    public EMoveState fly, hover;

    bool flying;
    private void Awake()
    {
        iks = GetComponentInChildren<IKManager>();
    }
    // Start is called before the first frame update
    void Start()
    {
        SetState(initState);
    }

    [Button]
    public void SetState(EMoveState e)
    {
        currentState?.Exit(this);
        currentState = e;
        currentState.Enter(this);
    }
    // Update is called once per frame
    void Update()
    {
        currentState.OnUpdate(this);
    }

    public void WakeUp()
    {
        eye.DOScaleY(0.26f, 0.25f).SetEase(Ease.OutBack, 4.2f);
        halo.DOScale(new Vector3(1, 1, 1), 0.35f).SetEase(Ease.OutBack, 4.2f);
        halo.DORotate(new Vector3(0, 360, 0), 1.25f).SetOptions(true).SetRelative(true);
    }
    public void Sleep()
    {
        eye.DOScaleY(0.08f, 1.25f).SetEase(Ease.InBack);
        halo.DOScale(new Vector3(0.5f, 1, 0.5f), 2.5f).SetEase(Ease.InBack, 4.2f);
        halo.DORotate(new Vector3(0, 360, 0), 2f).SetOptions(true).SetRelative(true).SetDelay(0.5f);

        target = null;
        SetState(sleep);
    }

    public void SetupFlying()
    {

    }

    public void SetUpCrawl()
    {

    }

    public void Idle()
    {
        if (flying)
            SetState(hover);
        else
            SetState(stand);
    }

    public void Patrol()
    {
        if (flying)
            SetState(fly);
        else
            SetState(crawl);
    }

    public void TargetPlayer()
    {
        target = CameraController10MH.Instance.Cam.transform;
    }
}

public abstract class EMoveState : ScriptableObject
{
    public abstract void Enter(VoidVisualController vvc);
    public abstract void Exit(VoidVisualController vvc);
    public abstract void OnUpdate(VoidVisualController vvc);
}