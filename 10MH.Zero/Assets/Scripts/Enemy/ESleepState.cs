﻿using UnityEngine;

[CreateAssetMenu(menuName = "Enemy State Machine/Sleep")]
public class ESleepState : EMoveState
{
    public override void Enter(VoidVisualController vvc)
    {
        vvc.Sleep();
    }

    public override void Exit(VoidVisualController vvc)
    {
        vvc.WakeUp();
    }

    public override void OnUpdate(VoidVisualController vvc)
    {
    }
}
