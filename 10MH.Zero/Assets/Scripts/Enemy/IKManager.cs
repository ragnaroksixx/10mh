﻿using DG.Tweening;
using DitzelGames.FastIK;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class IKManager : MonoBehaviour
{
    public LayerMask mask;
    public Transform root;
    public Transform targetParent;
    public List<Transform> tails;
    Leg[] legs;
    public Transform body;
    public Vector3 velocity;
    private void Start()
    {
        legs = GetComponentsInChildren<Leg>();
    }

    [Button]
    public void SetUp()
    {
        if (root == null)
            root = transform;

        foreach (Transform item in tails)
        {
            InitTail(item);
        }
    }

    public void InitTail(Transform t)
    {
        ChainIKConstraint ik = t.GetComponent<ChainIKConstraint>() ?? t.gameObject.AddComponent<ChainIKConstraint>();

        if (ik.data.target == null)
        {
            GameObject target = new GameObject();
            target.name = ik.transform.name + "_TARGET";
            target.transform.SetParent(targetParent);

            ik.data.target = target.transform;
        }

        if (ik.data.root == null)

            ik.data.root = ik.transform.GetChild(0).GetChild(0);

        if (ik.data.root != null && ik.data.tip == null)
            ik.data.tip = GetDeepest(ik.data.root);


    }


    [Button]
    public void SpiderArrange(float distance)
    {
        foreach (Transform t in tails)
        {
            ChainIKConstraint ik = t.GetComponent<ChainIKConstraint>();
            Vector3 dir = root.position - ik.data.root.position;
            dir.y = 0;
            dir.Normalize();
            ik.data.target.position = root.position + (dir * distance);
        }
    }

    Transform GetDeepest(Transform t)
    {
        if (t.childCount == 0)
            return t;

        return GetDeepest(t.GetChild(0));
    }

    private void Update()
    {
        Vector3 avgPos = Vector3.zero;

        foreach (Leg leg in legs)
        {
            avgPos += (leg.HitPoint / (float)legs.Length);
        }

        Vector3 pos = body.transform.position;
        pos.y = avgPos.y + 0.5f;
        body.transform.position = pos;
    }
}

