﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy State Machine/Crawl")]
public class ECrawlState : EMoveState
{
    public EMoveState nextState;
    public float moveSpeed = 10, angularSpeed = 200;
    public bool changeOnReach;

    public override void Enter(VoidVisualController vvc)
    {
    }

    public override void Exit(VoidVisualController vvc)
    {
        vvc.iks.velocity = Vector3.zero;
    }

    public override void OnUpdate(VoidVisualController vvc)
    {
        VoidVisualController unit = vvc;

        Vector3 dis = unit.body.position - unit.target.position;
        dis.y = 0;
        if (dis.magnitude < 0.1f)
        {
            vvc.iks.velocity = Vector3.zero;
            if (changeOnReach)
                unit.SetState(nextState);
            return;
        }

        //Vector3 flatForw = unit.body.forward;
        // Vector3 flatTarget = unit.lookAtTarget.position - unit.body.position;
        Vector3 target = unit.target.position - unit.head.position;
        //flatTarget.y = 0;
        //flatForw.y = 0;


        //unit.body.up = Vector3.SmoothDamp(unit.body.up, unit.target.position - unit.body.position, ref unit.bodyV, angularSpeed * Time.deltaTime);
        unit.head.forward = Vector3.SmoothDamp(unit.head.forward, target, ref unit.headV, angularSpeed * Time.deltaTime);

        Vector3 headForw = unit.head.forward;
        headForw.y = 0;
        headForw.Normalize();

        unit.body.position = Vector3.MoveTowards(unit.body.position, unit.body.position + headForw, moveSpeed * Time.deltaTime);
        vvc.iks.velocity = moveSpeed * headForw;

    }
}
