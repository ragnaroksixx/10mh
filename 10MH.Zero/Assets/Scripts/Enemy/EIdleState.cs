﻿using DG.Tweening;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy State Machine/Idle")]
public class EIdleState : EMoveState
{
    float startTime;

    public override void Enter(VoidVisualController vvc)
    {
        startTime = 5;
    }

    public override void Exit(VoidVisualController vvc)
    {
    }

    public override void OnUpdate(VoidVisualController vvc)
    {
        VoidVisualController unit = vvc;
        Vector3 t = unit.target == null ? (vvc.head.position + vvc.head.forward) : unit.target.position;
        float bodyAng = 0;
        Vector3 flatForw = unit.body.forward;
        Vector3 flatTarget = t - unit.body.position;
        flatTarget.y = 0;
        flatForw.y = 0;

        bodyAng = Vector3.Angle(flatForw, flatTarget);

        if (bodyAng > 45 || bodyAng < -45 || startTime > 0)
        {
            unit.body.forward = Vector3.SmoothDamp(unit.body.forward, flatTarget, ref unit.bodyV, 200 * Time.deltaTime);
            startTime -= Time.deltaTime;
        }
        t.y = Mathf.Clamp(t.y, unit.head.position.y - 0.2f, unit.head.position.y + 0.4f);
        Vector3 target = t - unit.head.position;
        Vector3 newforward = Vector3.SmoothDamp(unit.head.forward, target, ref unit.headV, 75f * Time.deltaTime);
        //newforward.y = Mathf.Clamp(newforward.y, -0.2f, 0.4f);
        unit.head.forward = newforward;
        //Debug.Log(unit.head.forward);
        //Vector3 targetPos = Vector3.SmoothDamp(unit.body.position, unit.moveTarget.position, ref unit.moveV, unit.sSpeed * Time.deltaTime);
        //unit.body.position = Vector3.MoveTowards(unit.body.position, targetPos, unit.speed * Time.deltaTime);
    }
}
