﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class EnemyOVerworldAI : MonoBehaviour
{
    public TimeRef spawnTime;
    public float rotSpeed;
    float moveSpeed;
    public float patrolSpeed, followSpeed;
    public Rigidbody rBody;
    public float patrolRadius, patrolPointTime;
    float timeTrack;
    Transform target;
    Vector3 moveDir;
    GameObject customPoint;
    public BattleEncounter encounter;
    public Transform graphic;
    AIMoveTypeHandler moveLogic;
    public Transform detectionSphere;
    public Transform detectionImage;
    public Transform spawnEffect;
    public CustomAudioClip spawnSFX;
    public enum AIType
    {
        STATIONARY,
        PATROL,
        FOLLOW,
        NONE,
        SPAWN
    }
    AIType startType;
    public AIType moveType;
    // Use this for initialization
    void Start()
    {
        if (!TimeController.IsPast(spawnTime))
        {
            TimeController.AddTimeEvent(spawnTime, Spawn);
            transform.parent.gameObject.SetActive(false);
            return;
        }
        Init();
    }
    void Init()
    {
        startType = moveType;
        if (moveType == AIType.PATROL)
            RNG_10MH.RegisterRandom(encounter.Identifier);
        if (CycleManager.Instance.HasEvent(encounter.Identifier, "started"))
        {
            Destroy(transform.parent.gameObject);
        }
        SetType(moveType);
        Sequence s = DOTween.Sequence();
        Tween expand = graphic.DOScaleX(graphic.localScale.x, 0.4f);
        Tween shrink = graphic.DOScaleX(graphic.localScale.x * 0.9f, 0.4f);
        s.Append(shrink)
            .Append(expand)
            .SetLoops(-1);
        s.Play();
        moveLogic = new AIMoveTypeHandler(this, detectionSphere, detectionImage);
    }

    void Spawn()
    {
        float scaleY = graphic.localScale.y;
        graphic.localScale = new Vector3(
            graphic.localScale.x, 0, graphic.localScale.z);
        transform.parent.gameObject.SetActive(true);
        graphic.DOScaleY(scaleY, 0.5f);
        spawnSFX?.Play();
        Init();
        SetType(AIType.SPAWN);
    }
    void OnBattlerComplete()
    {
        SetType(startType);
        moveLogic.OnSpawnComplete();
    }
    private void OnDestroy()
    {
        TimeController.RemoveTimeEvent(spawnTime, Spawn);
        RNG_10MH.DeregisterRandom(encounter.Identifier);
    }
    // Update is called once per frame
    void Update()
    {
        if (TimeController.Instance.IsPaused.Value)
            return;
        switch (moveType)
        {
            case AIType.SPAWN:
            case AIType.STATIONARY:
            case AIType.NONE:
                moveDir = Vector3.zero;
                break;
            case AIType.PATROL:
                timeTrack -= Time.deltaTime;
                float dist = MoveTowardsTarget(target);
                if (dist <= 1f || timeTrack <= 0)
                {
                    NewPatrolPoint();
                }
                break;
            case AIType.FOLLOW:
                MoveTowardsTarget(target);
                break;
            default:
                break;
        }
    }
    public void NewPatrolPoint()
    {
        timeTrack = patrolPointTime;
        Vector3 pos = RNG_10MH.OutsideUnitCircle(encounter.Identifier, patrolRadius);
        customPoint.transform.localPosition = pos;
    }
    public float MoveTowardsTarget(Transform t)
    {
        Vector3 pos = t.position;
        pos.y = rBody.transform.position.y;
        Vector3 direction = pos - rBody.transform.position;
        Quaternion targetRot = Quaternion.LookRotation(direction, Vector3.up);
        rBody.transform.rotation = Quaternion.Slerp(rBody.transform.rotation, targetRot, Time.deltaTime * rotSpeed);
        return direction.magnitude;
    }
    private void FixedUpdate()
    {
        moveDir = rBody.transform.forward;
        rBody.MovePosition(rBody.transform.position + (moveDir * moveSpeed * Time.fixedDeltaTime));
    }
    public void SetType(AIType t)
    {
        Destroy(customPoint);
        moveType = t;
        switch (moveType)
        {
            case AIType.SPAWN:
            case AIType.STATIONARY:
                target = null;
                moveSpeed = 0;
                break;
            case AIType.PATROL:
                moveSpeed = patrolSpeed;
                customPoint = new GameObject("nav point");
                customPoint.transform.parent = transform.parent;
                target = customPoint.transform;
                NewPatrolPoint();
                break;
            case AIType.FOLLOW:
                moveSpeed = followSpeed;
                target = CameraController10MH.Instance.transform;
                break;
            default:
                break;
        }
    }
    bool once = true;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            StartBattle();
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            moveLogic.OnTriggerEnter();
        }
    }
    public void StartBattle()
    {
        if (once)
        {
            once = false;
            BattleController.StartBattle(encounter);
        }
    }
}
class AIMoveTypeHandler
{
    EnemyOVerworldAI ai;
    Transform detectionSphere;
    Transform dectionImage;
    public AIMoveTypeHandler(EnemyOVerworldAI ai, Transform detectionSphere, Transform dectionImage)
    {
        this.ai = ai;
        this.detectionSphere = detectionSphere;
        this.dectionImage = dectionImage;
        detectionSphere.gameObject.SetActive(false);
    }

    public void OnTriggerEnter()
    {
        if (ai.moveType == EnemyOVerworldAI.AIType.PATROL
        || ai.moveType == EnemyOVerworldAI.AIType.STATIONARY)
        {
            ai.SetType(EnemyOVerworldAI.AIType.NONE);
            float duration = .75f;
            Sequence s = DOTween.Sequence();
            s.Append(dectionImage.DOScaleY(1, duration)
                .SetEase(Ease.InOutElastic));
            s.Append(dectionImage.DOScaleY(0, duration)
                .SetEase(Ease.InOutElastic));
            s.OnComplete(OnDetectComplete);
            s.Play();
        }
    }
    void OnDetectComplete()
    {
        ai.SetType(EnemyOVerworldAI.AIType.FOLLOW);
        detectionSphere.gameObject.SetActive(false);
    }
    public void OnSpawnComplete()
    {
        detectionSphere.gameObject.SetActive(true);
    }
}

