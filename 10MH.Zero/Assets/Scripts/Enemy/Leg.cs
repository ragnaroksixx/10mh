﻿using DG.Tweening;
using FIMSpace.FTail;
using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class Leg : MonoBehaviour
{
    ChainIKConstraint ik;
    TailAnimator2 tailAnim;
    Transform raycastSource;
    public bool useTailAnim;
    public IKManager ikmanager;
    bool isMoving;
    public Leg altLeg;
    Vector3 hitPoint;
    public Transform target;

    public Vector3 HitPoint { get => hitPoint; set => hitPoint = value; }

    public void Start()
    {
        ik = GetComponent<ChainIKConstraint>();
        ik.enabled = !useTailAnim;
        tailAnim = GetComponent<TailAnimator2>();
        tailAnim.enabled = useTailAnim;
        raycastSource = new GameObject().transform;
        raycastSource.gameObject.name = target.transform.name + "_TARGET (runtime)";
        raycastSource.SetParent(ikmanager.targetParent);
        raycastSource.localPosition = target.localPosition;

        if (useTailAnim)
        {
            tailAnim.IKTarget = target;
        }
        else
        {
            ik.data.target = target;
        }

        target.SetParent(null);
    }
    public void Update()
    {
        if (isMoving) return;

        RaycastHit hit;
        if (Physics.Raycast(raycastSource.position, Vector3.down + (ikmanager.velocity.normalized / 2), out hit, 10, ikmanager.mask))
        {
            hitPoint = hit.point;
            float dist = Vector3.Distance(hit.point, ik.data.target.position);

            if (altLeg.isMoving && dist < 1.0f)
                return;

            if (dist > 0.5f)
                ikmanager.StartCoroutine(MoveTo(hit.point));
        }
    }
    IEnumerator MoveTo(Vector3 pos)
    {
        isMoving = true;
        float time = 0.35f;
        ik.data.target.DOJump(pos, 0.25f, 1, time);
        yield return new WaitForSeconds(time);
        isMoving = false;
    }

    [Button]
    void SetTargetFromIK()
    {
        ik = GetComponent<ChainIKConstraint>();
        if (ik)
        {
            target = ik.data.target;
        }
    }
}
