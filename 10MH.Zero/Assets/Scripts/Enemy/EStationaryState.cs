﻿using UnityEngine;

[CreateAssetMenu(menuName = "Enemy State Machine/Stationary")]
public class EStationaryState : EMoveState
{
    public override void Enter(VoidVisualController vvc)
    {
    }

    public override void Exit(VoidVisualController vvc)
    {
    }

    public override void OnUpdate(VoidVisualController vvc)
    {
    }
}
