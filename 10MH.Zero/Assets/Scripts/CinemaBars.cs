﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using System;
using UnityEngine.UI;

[System.Serializable]
public class CinemaBars : OnScreenAnimatorBASE
{
    public RectTransform cinemaBarTop, cinemaBarBottom;
    public float cinemaBarHeightTop, cinemaBarHeightBottom;
    public float hidePos = 0;
    public virtual void Init()
    {
        cinemaBarTop.anchoredPosition = new Vector2(0, cinemaBarHeightTop);
        cinemaBarBottom.anchoredPosition = new Vector2(0, -cinemaBarHeightBottom);
    }
    protected virtual void OnHideComplete()
    {
        cinemaBarTop.gameObject.SetActive(false);
        cinemaBarBottom.gameObject.SetActive(false);
    }
    void KillTweens()
    {
        cinemaBarTop.DOKill(false);
        cinemaBarBottom.DOKill(false);
    }

    protected override void OnScreenImpl(Action onComplete, bool instant)
    {
        MoveTo(cinemaBarHeightTop, cinemaBarHeightBottom, instant, onComplete);
        isOnScreen = true;
    }

    protected override void OnScreenImplEDITOR()
    {
        MoveTo(cinemaBarHeightTop, cinemaBarHeightBottom, true, null);
        isOnScreen = true;
    }

    protected override void OffScreenImpl(Action onComplete, bool instant)
    {
        MoveTo(hidePos, hidePos, instant, onComplete);
        isOnScreen = false;
    }

    protected override void OffScreenImplEDITOR()
    {
        MoveTo(hidePos, hidePos, true, null);
        isOnScreen = false;
    }

    void MoveTo(float top, float bot, bool instant, Action onComplete)
    {
        KillTweens();
        if (instant)
        {
            Vector2 pos = cinemaBarTop.anchoredPosition;
            pos.y = -top;
            cinemaBarTop.anchoredPosition = pos;

            pos = cinemaBarBottom.anchoredPosition;
            pos.y = bot;
            cinemaBarBottom.anchoredPosition = pos;

            onComplete?.Invoke();
            isTransitioning = false;
        }
        else
        {
            isTransitioning = true;
            cinemaBarTop.DOAnchorPosY(-top, transitionSpeed);
            cinemaBarBottom.DOAnchorPosY(bot, transitionSpeed).OnComplete
                (() => { onComplete?.Invoke(); isTransitioning = false; })
                .SetUpdate(true);
        }
    }
}
