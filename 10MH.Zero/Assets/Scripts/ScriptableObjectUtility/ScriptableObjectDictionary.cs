﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScriptableObjectDictionary<T> where T : IUnquieScriptableObject
{
    Dictionary<string, T> dict;
    string path;
    bool useCollections;
    T[] allItems;
    public ScriptableObjectDictionary(string directory, bool useCollection)
    {
        path = directory;
        this.useCollections = useCollection;
    }
    public ScriptableObjectDictionary(string directory)
    {
        path = directory;
        this.useCollections = false;
    }
    ~ScriptableObjectDictionary()
    {
        dict = null;
        allItems = null;
        Resources.UnloadUnusedAssets();
    }
    public T[] AllItems { get => allItems; }

    public T GetItem(string id)
    {
        id = id.ToLower().Trim();

        if (HasItem(id))
            return dict[id];

        Debug.LogError("Item " + id + " Not Found");
        return default(T);
    }
    public bool HasItem(string id)
    {
        id = id.ToLower().Trim();
        return dict.ContainsKey(id);
    }
    public void Init(bool initItems)
    {
        Object[] objs;
        if (useCollections)
        {
            objs = Resources.Load<ObjectBundleCollection>("Collections/" + path).objects;
        }
        else
        {
            objs = Resources.LoadAll<ScriptableObject>(path);
        }
        List<T> objTs = new List<T>();
        foreach (Object so in objs)
        {
            if (so is T)
            {
                objTs.Add((T)(object)(so));
            }
        }
        allItems = objTs.ToArray();
        dict = new Dictionary<string, T>();
        foreach (T item in allItems)
        {
            Add(item.Identifier, item);
            foreach (string id in item.Alias)
            {
                Add(id, item);
            }
            if (initItems)
                item.Init();
        }
    }
    void Add(string id, T i)
    {
        id = id.ToLower().Trim();
        if (!dict.ContainsKey(id))
            dict.Add(id, i);
        else
        {
            Debug.LogError("Duplicate ID: " + id + " on -> " + i, i as Object);
        }
    }
}
