﻿using UnityEngine;
using System.Collections;

public interface IUnquieScriptableObject
{
    void Init();
    string Identifier { get; }
    string[] Alias { get; }
}
