﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[CreateAssetMenu]
public class ObjectBundleCollection : ScriptableObject
{
    public string bundleName;
    public UnityEngine.Object[] objects;
#if UNITY_EDITOR
    [Button(DirtyOnClick = true)]
    public void FindALL()
    {
        string[] paths = AssetDatabase.GetAssetPathsFromAssetBundle(bundleName);
        List<UnityEngine.Object> result = new List<UnityEngine.Object>();
        Debug.Log("Files Found: " + paths.Length + " for bundle- " + bundleName);
        foreach (string assetPath in paths)
        {
            var asset = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(assetPath);
            if (asset != null)
            {
                result.Add(asset);
            }
        }
        objects = result.ToArray();
    }
#endif
}
