﻿using System.Collections;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ScriptableObjectCollection : ScriptableObject
{
    [ReadOnly]
    public List<object> objects;
    Dictionary<string, IUnquieScriptableObject> objectDictionary = null;
    [SerializeField]
    public System.Type type;
    public T GetItem<T>(string id) where T : IUnquieScriptableObject
    {
        if (objectDictionary == null)
        {
            InitDictionary<T>();
        }
        return (T)objectDictionary[id];
    }

    public void InitDictionary<T>() where T : IUnquieScriptableObject
    {
        objectDictionary = new Dictionary<string, IUnquieScriptableObject>();
        foreach (object item in objects)
        {
            if (item is T)
            {
                //IUnquieScriptableObject uItem = (T)item;
                //objectDictionary.Add(item as ScriptableObject);
            }
        }
    }
#if UNITY_EDITOR
    [Button(DirtyOnClick = true)]
    public void FindALL()
    {
        string path = AssetDatabase.GetAssetPath(this);
        path = Directory.GetParent(path).ToString();
        Object[] result = AssetDatabase.LoadAllAssetsAtPath(path);
        objects = new List<object>();
        foreach (Object item in result)
        {
            if (item is IUnquieScriptableObject)
            {
                objects.Add(item);
            }
        }
        Debug.Log("Files Found: " + objects.Count + " for collection- " + name);
    }
#endif

}
