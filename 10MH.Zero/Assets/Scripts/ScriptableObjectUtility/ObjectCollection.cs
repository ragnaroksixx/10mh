﻿//using Sirenix.OdinInspector;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//#if UNITY_EDITOR
//using UnityEditor;
//#endif
//using UnityEngine;

//public class ObjectCollection : ScriptableObject
//{
//#if UNITY_EDITOR
//    [ReadOnly]
//    public UnityEngine.Object[] objs;
//#endif
//    [HideInInspector]
//    public List<string> objPaths;


//    [Button(DirtyOnClick = true)]
//    public abstract void FindALL()
//    {
//#if UNITY_EDITOR
//        objPaths = new List<string>();
//        string[] paths = AssetDatabase.GetAssetPathsFromAssetBundle(bundleName);
//        List<UnityEngine.Object> result = new List<UnityEngine.Object>();
//        Debug.Log("Files Found: " + paths.Length + " for bundle- " + bundleName);
//        foreach (string assetPath in paths)
//        {
//            var asset = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(assetPath);
//            if (asset != null)
//            {
//                result.Add(asset);
//                objPaths.Add(assetPath);
//            }
//        }
//        objs = result.ToArray();
//#endif
//    }
//    public UnityEngine.Object[] LoadAllObjects(out string[] paths)
//    {
//        List<string> pathArr = new List<string>();
//        List<UnityEngine.Object> result = new List<UnityEngine.Object>();
//        foreach (string assetPath in objPaths)
//        {
//            var asset = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(assetPath);
//            if (asset != null)
//            {
//                result.Add(asset);
//                pathArr.Add(assetPath);
//            }
//        }
//        paths = pathArr.ToArray();
//        return result.ToArray();
//    }
//}
