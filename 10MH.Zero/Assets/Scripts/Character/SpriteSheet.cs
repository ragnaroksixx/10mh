﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu()]
public class SpriteSheet : ScriptableObject
{
    public List<CustomImageData> sprites;

}

[System.Serializable]
public struct CustomImageData
{
    public string key;
    public Sprite image;
}