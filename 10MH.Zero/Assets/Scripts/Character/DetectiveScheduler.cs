﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DetectiveScheduler : Scheduler
{
    public List<AreaData> areas;
    AreaData a;
    public override void Start()
    {
        int index = (SaveData.currentSave.numCycles - 1) % areas.Count;
        a = areas[index];
        base.Start();
    }


    public override void UpdateLocation(AreaData ad)
    {
        if (ad)
            ad = a;
        base.UpdateLocation(ad);
    }
}
