﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Scheduler : MonoBehaviour
{
    Timeline timeline;
    public Character c;
    bool startCheck = true;
    public virtual void Start()
    {
        UpdateTimeLine();
        startCheck = false;
    }
    public void UpdateTimeLine()
    {
        timeline = GetTimeline();
        timeline.GetEventAt(TimeController.Instance.CurrentTime)?.Invoke();
        timeline.RemoveTo(TimeController.Instance.CurrentTime);
    }

    // Update is called once per frame
    void Update()
    {
        timeline?.CheckEvents(TimeController.Instance.CurrentTime);
    }
    public virtual void UpdateLocation(AreaData ad)
    {
        bool isHere = SceneController.activeEnvironmentScene == ad;
        if (isHere)
            c.Enter();
        else
            c.Leave(startCheck);
    }
    public Timeline GetTimeline()
    {
        CharacterTimeline result = c.characterData.baseTimeline;
        foreach (CharacterTimeline item in c.characterData.altTimelines)
        {
            if (CycleManager.Instance.HasEvent("",item.trigger))
            {
                result = item;
            }
        }
        if (result == null)
            return new Timeline();
        return result.CreateTimeline(this);
    }
}
[System.Serializable]
public struct TimeScheduler
{
    public TimeRef time;
    public AreaData location;
    public CharacterSceneSets scenes;
}