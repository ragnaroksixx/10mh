﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using NHG.Dialogue;

public class CharacterDropArea : DropArea
{
    public CharacterData character;
    public BattleUnitUI ui;

    public override void Resolve(IClickableSource source)
    {
        if (BattleController.IsInBattle)
        {
            InventoryItemUI ui = source as InventoryItemUI;
            CombatInteractionAdapter.AddToQueue(ui, this);
            ui.Inventory.AddBackIfHeld(ui.ItemData, ui);
            ui.ReturnToCacheFancy();
        }
        else
        {
            DialogueInteractionAdapter.HandleInteraction(source as InventoryItemUI, this);
        }
        Publisher.Raise(new ItemHoverEvent(null));
        Publisher.Raise(new ItemPickupEvent(null));
    }
    public override bool CanResolve(IClickableSource source)
    {
        if (!(source is InventoryItemUI))
            return false;
        if (BattleController.IsInBattle)
        {
            return CombatInteractionAdapter.IsValidInteraction(source as InventoryItemUI, this);
        }
        else
        {
            return DialogueInteractionAdapter.IsValidInteraction(source as InventoryItemUI, this);
        }
    }

    Dictionary<Item, ItemEvents> itemEvents;

    public Dictionary<Item, ItemEvents> ItemEvents { get => itemEvents; }

    public void SetCharacter(CharacterData cd)
    {
        character = cd;
    }
    public void SetEvents(List<ItemEvents> ies)
    {
        itemEvents = new Dictionary<Item, ItemEvents>();
        foreach (ItemEvents ie in ies)
        {
            itemEvents.Add(ie.item, ie);
        }
        enabled = true;
    }
    public void ClearEvents()
    {
        itemEvents = new Dictionary<Item, ItemEvents>();
        enabled = false;
    }
}

