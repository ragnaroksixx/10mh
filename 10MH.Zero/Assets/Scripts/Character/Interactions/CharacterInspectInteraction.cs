﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;

public class CharacterInspectInteraction : CharacterInteraction
{
    public override void SetScene()
    {
        TextAsset ta = null;
        currentScene = null;
        if (CycleManager.IsFirstCycle())
        {
            if (c.characterData.cycleOneScenes == null)
                ta = null;
            else
            {
                if (TimeController.IsCalamity())
                    ta = c.characterData.cycleOneScenes.calamityInspect;
                else
                    ta = c.characterData.cycleOneScenes.preCalamityInspect;

            }
        }
        else if (SaveData.currentSave.numCycles == 2)
        {
            if (c.characterData.cycleTwoScenes == null)
                ta = null;
            else
            {
                if (TimeController.IsCalamity())
                    ta = c.characterData.cycleTwoScenes.calamityInspect;
                else
                    ta = c.characterData.cycleTwoScenes.preCalamityInspect;

            }
        }
        else
        {
            if (c.characterData.defualtScenes == null)
                ta = null;
            else
            {
                if (TimeController.IsCalamity())
                    ta = c.characterData.defualtScenes.calamityInspect;
                else
                    ta = c.characterData.defualtScenes.preCalamityInspect;

            }
        }

        if (ta != null)
        {
            currentScene = DialogueUtils.GetSceneFromTextAsset(ta);
        }
    }
    public override bool CanUse()
    {
        return currentScene != null && base.CanUse();
    }
    public override void PlayScene(Scene scene)
    {
        //overrie to remove hiding the character
        if (scene != null)
            scene.overideStartingCharacter = c.characterData;
        DialogueRenderer.Instance.PlayScene(scene, () => { OnSceneComplete(scene); });
    }
}
