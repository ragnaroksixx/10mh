﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;
using System.Collections.Generic;

public class CharacterTalkInteraction : CharacterInteraction
{
    public List<Scene> scenes;
    public override void Awake()
    {
        base.Awake();
    }
    public override void SetScene()
    {
        TextAsset ta = null;
        currentScene = null;
        if (CycleManager.IsFirstCycle())
        {
            if (c.characterData.cycleOneScenes == null)
                ta = null;
            else
            {
                if (TimeController.IsCalamity())
                    ta = c.characterData.cycleOneScenes.calamityInteract;
                else
                    ta = c.characterData.cycleOneScenes.preCalamityInteract;

            }
        }
        else if (SaveData.currentSave.numCycles == 2)
        {
            if (c.characterData.cycleTwoScenes == null)
                ta = null;
            else
            {
                if (TimeController.IsCalamity())
                    ta = c.characterData.cycleTwoScenes.calamityInteract;
                else
                    ta = c.characterData.cycleTwoScenes.preCalamityInteract;

            }
        }
        else
        {
            if (c.characterData.defualtScenes == null)
                ta = null;
            else
            {
                if (TimeController.IsCalamity())
                    ta = c.characterData.defualtScenes.calamityInteract;
                else
                    ta = c.characterData.defualtScenes.preCalamityInteract;

            }
        }

        if (ta != null)
        {
            currentScene = DialogueUtils.GetSceneFromTextAsset(ta);
        }
    }
    public override bool CanUse()
    {
        return currentScene != null && base.CanUse();
    }

   /* public override SubInteraction[] GetSubInteractions()
    {
        List<SubInteraction> subInteractions = new List<SubInteraction>();
        subInteractions.Add(new SubInteraction("Chat", 30));
        return new SubInteraction[1] {
            new SubInteraction("Chat",30)
        };
    }*/
}
