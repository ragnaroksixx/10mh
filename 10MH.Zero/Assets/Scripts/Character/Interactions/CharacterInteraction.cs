﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;
using DG.Tweening;

public abstract class CharacterInteraction : Interaction
{
    public Character c;
    protected Scene currentScene;
    public override void Awake()
    {
        SetScene();
        base.Awake();
    }
    public abstract void SetScene();
    public virtual void PlayScene(Scene scene)
    {
        //HideCharacter();
        if (scene != null)
            scene.overideStartingCharacter = c.characterData;
        DialogueRenderer.Instance.PlayScene(scene, () => { OnSceneComplete(scene); });
    }
    protected virtual void OnSceneComplete(Scene s)
    {
        //if (c != null)
       //     ShowCharacter();
        OpenInteractionMenu();
    }
    public override void OnInteract()
    {
        if (currentScene != null)
            PlayScene(currentScene);
    }
    protected void MoveTo(Vector3 pos)
    {
        c.MoveTo(pos);
    }
}
