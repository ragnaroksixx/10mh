﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Character/Timeline")]
public class CharacterTimeline : ScriptableObject
{
    public string trigger;
    public CharacterTimeline prevTimeline;
    public TimeScheduler startPoint;
    public List<TimeScheduler> timePoints;

    public List<TimeScheduler> GetTimeLine()
    {
        List<TimeScheduler> result = new List<TimeScheduler>();
        if (prevTimeline)
        {
            result.AddRange(prevTimeline.GetTimeLine());
        }
        result.AddRange(timePoints);
        return result;
    }
    public Timeline CreateTimeline(Scheduler s)
    {
        Timeline timeline = new Timeline();
        if (!prevTimeline)
        {
            timeline.AddTimeEvent(startPoint.time,
                   () => { s.UpdateLocation(startPoint.location); });
        }

        foreach (TimeScheduler ts in timePoints)
        {
            timeline.AddTimeEvent(ts.time,
               () => { s.UpdateLocation(ts.location); });
        }
        return timeline;
    }
}
