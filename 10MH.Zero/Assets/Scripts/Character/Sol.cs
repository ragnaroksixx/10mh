﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Sol : MonoBehaviour
{
    public MeshRenderer inner;

    public void Init(Character c)
    {
        SetColor(c.characterData.characterColor);
        gameObject.SetActive(false);
    }
    public void SetColor(Color c)
    {
        inner.material.SetColor("_Maincolor", c);
        inner.material.SetColor("_Edgecolor", c);
        inner.material.SetColor("_Secondarycolor", c);
    }
}

