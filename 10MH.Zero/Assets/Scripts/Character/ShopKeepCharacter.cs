﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;

public class ShopKeepCharacter : CharacterInteraction
{
    public Transform behindCounterLocation;
    public Scene intro, purchase, noItems;
    public WorldTrigger moveTrigger;
    public override void Start()
    {
        base.Start();/*
        if (CycleManager.Instance.HasEvent(hasCharMoved))
        {
            currentScene = null;
            Destroy(moveTrigger.gameObject);
            MoveToCounter();
        }
        else
        {
            moveTrigger.RegisterTriggerEnterEvent(MoveToCounter);
            currentScene = intro;
        }*/
    }
    protected override void OnSceneComplete(Scene s)
    {
        if (s == intro)
            MoveToCounter();
        base.OnSceneComplete(s);
    }
    public void MoveToCounter()
    {/*
        CycleManager.Instance.AddTempEvent(hasCharMoved);
        MoveTo(behindCounterLocation.position);
        currentScene = purchase;
        */
    }
    public override void OnInteract()
    {
        if (currentScene != intro)
        {
            if (PlayerInventory.Instance.HasPurchaseItems())
                currentScene = purchase;
            else
                currentScene = noItems;
        }
        base.OnInteract();
    }

    public override void SetScene()
    {
        throw new System.NotImplementedException();
    }
}
