﻿using System.Collections;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public static UIController Instance;
    public CinemaBars cinemaBars;
    MultiBoolLock showCinemaBars = new MultiBoolLock();
    float transitionDuration = 0.25f;
    public OnScreenAnimatorBASE playerAnim;
    // Use this for initialization
    void Awake()
    {
        Instance = this;
        cinemaBars.Init();
    }

    public void ShowCinemaBars(object o)
    {
        showCinemaBars.AddLock(o);
        if (!cinemaBars.IsOnScreen)
        {
            cinemaBars.AnimateOnScreen();
        }
    }
    public void HideCinemaBars(object o)
    {
        showCinemaBars.RemoveLock(o);
        if (cinemaBars.IsOnScreen && !showCinemaBars.Value)
        {
            cinemaBars.AnimateOffScreen();
        }
    }

    public void ShowPlayer(object o)
    {
        playerAnim.AnimateOnScreen();
    }

    public void HidePlayer(object o)
    {
        playerAnim.AnimateOffScreen();
    }
}
