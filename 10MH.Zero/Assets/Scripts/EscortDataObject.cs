﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class EscortDataObject : ScriptableObject, IUnquieScriptableObject
{
    public string id;
    public EscortData data;
    public string Identifier => id;

    public string[] Alias => new string[0];

    public void Init()
    {
        
    }
}

