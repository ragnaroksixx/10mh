﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MaterialEffects : MonoBehaviour
{
    protected Material mat;
    protected Color defaultColor;
    public Renderer meshRender;
    public bool instanceMaterial=true;
    public string colorParamater = "_Color";
    protected virtual void Awake()
    {
        if (instanceMaterial)
            mat = meshRender.material;
        else
            mat = meshRender.sharedMaterial;
        defaultColor = mat.GetColor(colorParamater);
    }
    public void Flash(Color c)
    {
        mat.DOFlash(defaultColor, c);
    }
    public void EFlash(Color c)
    {
        mat.DOFlash(defaultColor, c);
    }
    public void Squash()
    {
    }

    public void SetEmissive(Color value)
    {
        mat.SetColor("_EmissionColor", value);
    }

    public void SetMaterial(Material newMat)
    {
        if (instanceMaterial)
            newMat = Instantiate(newMat);
        meshRender.material = newMat;
        mat = newMat;

    }
}
