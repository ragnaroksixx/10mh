﻿using UnityEngine;
using System.Collections;

public class LightPulse : MonoBehaviour
{
    public float PULSE_RANGE = 4.0f;
    public float PULSE_SPEED = 3.0f;
    public float PULSE_MINIMUM = 1.0f;


    public Light light;

    // Update is called once per frame
    void Update()
    {
        light.intensity = PULSE_MINIMUM +
                       Mathf.PingPong(Time.time * PULSE_SPEED,
                                      PULSE_RANGE - PULSE_MINIMUM);
    }
}
