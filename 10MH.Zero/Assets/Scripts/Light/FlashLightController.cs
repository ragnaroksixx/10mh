﻿using System.Collections;
using UnityEngine;


public class FlashLightController : MonoBehaviour
{
    public Color calamityColor;
    public Light l;
    // Use this for initialization
    void Start()
    {
        l.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F))
        {
            l.gameObject.SetActive(!l.gameObject.activeInHierarchy);
        }
    }
}

