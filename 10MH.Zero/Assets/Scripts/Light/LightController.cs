﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Sirenix.OdinInspector;

public class LightController : CalamityTrigger
{
    public enum LightTypes
    {
        OUTDOOR,
        AMBIENT,
        INDOOR
    }
    //public static LightController Instance;
    public Color preCalamitySkyColor, calamitySkyColor;
    public Color preCalamityAmbientColor, calamityAmbientColor;
    public Material preCalamitySkybox, calamitySkybox;
    public Material windowMat;
    [ColorUsage(true, true)]
    public Color windowColorPreCalamity, windowColowCalamity;


    private void Awake()
    {
        //Instance = this;

    }

    public override void SetCalamity(bool isCalamity)
    {
        windowMat.SetColor("_EmissionColor", isCalamity ?
            windowColowCalamity : windowColorPreCalamity);
    }

    public void OnAreaChanged(Area area)
    {
        if (TimeController.IsPreCalamity())
        {
            TimeController.RemoveTimeEvent(TimeController.Instance.calamityTime, BoomCalamity);
            RenderSettings.skybox = Instantiate(preCalamitySkybox);
            TimeController.AddTimeEvent(TimeController.Instance.calamityTime, BoomCalamity);
        }
        else
        {
            RenderSettings.skybox = Instantiate(calamitySkybox);
        }
        SetLightinging(area);
    }

    public void SetLightinging(Area a)
    {
        EffectsManager.Instance.SetLighting(a.GetLightValue());
    }
    public void SetSkybox(Material m)
    {
        RenderSettings.skybox = m;
    }
    public void BoomCalamity()
    {
        RenderSettings.skybox = Instantiate(calamitySkybox);
    }
    public void FadeToNewSkybox(TimeRef time)
    {
        float duration = 30;
        FadeColor("_SunDiscColor", calamitySkybox, duration);
        FadeColor("_SunHaloColor", calamitySkybox, duration);
        FadeColor("_HorizonLineColor", calamitySkybox, duration);
        FadeColor("_SkyGradientTop", calamitySkybox, duration);
        FadeColor("_SkyGradientBottom", calamitySkybox, duration);

        FadeFloat("_SunDiscMultiplier", calamitySkybox, duration);
        FadeFloat("_SunDiscExponent", calamitySkybox, duration);
        FadeFloat("_SunHaloExponent", calamitySkybox, duration);
        FadeFloat("_SunHaloContribution", calamitySkybox, duration);
        FadeFloat("_HorizonLineExponent", calamitySkybox, duration);
        FadeFloat("_HorizonLineContribution", calamitySkybox, duration);
        FadeFloat("_SkyGradientExponent", calamitySkybox, duration);

    }
    void FadeColor(string prop, Material next, float duration)
    {
        RenderSettings.skybox.DOColor(next.GetColor(prop), prop, duration);
    }
    void FadeFloat(string prop, Material next, float duration)
    {
        RenderSettings.skybox.DOFloat(next.GetFloat(prop), prop, duration);
    }
    public void SetFog(bool val, Color c = new Color(), FogMode fm = FogMode.ExponentialSquared, float fogDensity = 0.2f, float densityTransitionTime = 1.0f)
    {
        RenderSettings.fog = val;
        RenderSettings.fogColor = c;
        RenderSettings.fogMode = fm;
        DOTween.To(() => RenderSettings.fogDensity, x => RenderSettings.fogDensity = x, fogDensity, densityTransitionTime);
    }
}
