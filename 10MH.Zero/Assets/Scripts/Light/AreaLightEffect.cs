﻿using System.Collections;
using UnityEngine;


public class AreaLightEffect : MonoBehaviour
{
    public AreaLightSource lightSource;
    public virtual void Awake()
    {
        if (lightSource == null)
            lightSource = GetComponentInParent<AreaLightSource>();

        if(lightSource!=null)//TODO: actually fix this later
        lightSource.onLightChangedEvent.AddListener(OnLightChanged);
    }
    public virtual void OnLightChanged(bool val)
    {

    }
}
