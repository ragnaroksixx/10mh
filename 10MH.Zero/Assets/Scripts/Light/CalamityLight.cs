﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;


public class CalamityLight : CalamityTrigger
{
    [SerializeField]
    Light _light;
    public bool isStatic => gameObject.isStatic;
    public override void SetCalamity(bool isCalamity)
    {
        if (isStatic) return;
        if (!enabled) return;
        UpdateColor(isCalamity);
    }
    public void UpdateColor(bool isCalamity)
    {
        SetColor(isCalamity ? LightControllerData.Instance.calamitySkyColor : LightControllerData.Instance.preCalamitySkyColor);
    }

    void SetColor(Color c)
    {
        _light.color = c;
    }

    [Button]
    public void SetCalamityData()
    {
        UpdateColor(true);
    }

    [Button]
    public void SetPreCalamityData()
    {
        UpdateColor(false);
    }
}
