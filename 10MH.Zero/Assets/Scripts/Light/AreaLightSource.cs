﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class AreaLightSource : MonoBehaviour
{
    public bool isOn;
    public bool persistThroughCycles;
    [FoldoutGroup("Callbacks")]
    public UnityEvent<bool> onLightChangedEvent;
    [FoldoutGroup("Callbacks")]
    public UnityEvent<bool> onLightChangedEventInverse;
    [FoldoutGroup("Callbacks")]
    public UnityEvent onEvent, offEvent;
    [Range(0, 1)]
    public float lightContribution = 1;
    AreaData CurrentArea => SceneController.activeEnvironmentScene;
    string key => name + "LIGHT";
    string keyON => key + "ON";
    string keyOFF => key + "OFF";
    private void Start()
    {
        Area a = Area.activeArea;
        a.LightSources.Add(this);
        if (CycleManager.Instance.HasEvent(CurrentArea.Identifier, keyON))
        {
            Set(true);
        }
        else if (CycleManager.Instance.HasEvent(CurrentArea.Identifier, keyOFF))
        {
            Set(false);
        }
        else
        {
            Set(isOn);
        }

        //TimeController.AddTimeEvent(LightController.powerOutageTime, CalamityOutage);
    }
    private void OnDestroy()
    {

    }
    public void Set(bool val)
    {
        SetInternal(val);
        UpdateObject();
    }
    void SetInternal(bool val)
    {
        CycleManager.Instance.RemoveEvent(CurrentArea.Identifier, keyON);
        CycleManager.Instance.RemoveEvent(CurrentArea.Identifier, keyOFF);
        isOn = val;
        if (persistThroughCycles)
        {
            CycleManager.Instance.AddPermanentEvent(CurrentArea.Identifier, val ? keyON : keyOFF);

        }
        else
        {
            CycleManager.Instance.AddTempEvent(CurrentArea.Identifier, val ? keyON : keyOFF);

        }
    }
    [Button]
    [DisableInEditorMode]
    public void Toggle()
    {
        Set(!isOn);
    }
    public void UpdateObject()
    {
        LightControllerData.Instance.SetLightinging(Area.activeArea);
        onLightChangedEvent.Invoke(isOn);
        onLightChangedEventInverse.Invoke(!isOn);
        if (isOn)
            onEvent.Invoke();
        else
            offEvent.Invoke();

    }

    bool cacheON;
    public void CalamityOutage()
    {
        cacheON = isOn;
        Set(false);
    }

    public void BackUpPower()
    {
        Set(cacheON);
    }
}
