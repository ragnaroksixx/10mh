﻿using System.Collections;
using UnityEngine;


public class AreaLightMonobaviourToggle : AreaLightEffect
{
    public MonoBehaviour mb;
    public bool invert;
    public override void OnLightChanged(bool val)
    {
        base.OnLightChanged(val);
        if (invert)
            mb.enabled = !val;
        else
            mb.enabled = val;
    }
}
