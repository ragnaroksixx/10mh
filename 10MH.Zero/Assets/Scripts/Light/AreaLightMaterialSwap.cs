﻿using System.Collections;
using UnityEngine;


public class AreaLightMaterialSwap : AreaLightEffect
{
    public Renderer rend;
    public Material onMat, offMat;
    public override void OnLightChanged(bool val)
    {
        base.OnLightChanged(val);
        rend.material = val ? onMat : offMat;
    }
}
