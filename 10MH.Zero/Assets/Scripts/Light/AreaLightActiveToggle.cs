﻿using System.Collections;
using UnityEngine;


public class AreaLightActiveToggle : AreaLightEffect
{
    public override void OnLightChanged(bool val)
    {
        base.OnLightChanged(val);
        gameObject.SetActive(val);
    }
}
