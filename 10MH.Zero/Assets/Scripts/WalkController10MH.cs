﻿using UnityEngine;
using System.Collections;
using CMF;
using UnityEngine.UI;
using System;
using Sirenix.OdinInspector;

public class WalkController10MH : AdvancedWalkerController
{
    private CharacterInput cacheInput;
    public CameraMouseInput camInput;
    public static WalkController10MH Instance;
    bool autoWalker = false;
    Vector3 autoWalkVel;
    public MultiBoolLock userInputLock;
    float colliderHeight, camHeight;
    public float crouchHeightCol, crouchSpeed, crouchHeightCam;
    public SmoothPosition cameraMover;
    bool crouched;
    bool squat;
    bool isRunning;
    public Action<SprintMoveData> OnRunStart, OnRunEnd;

    bool staminaLocked;
    float stamina;
    float rechargeStartTime = 0;
    float rechargeStartDelay = 0.5f;
    float runLerpEndTime;
    const float runLerpTime = 0.25f;

    public SprintMoveData runData;
    SprintMoveData sprintMoveData;
    public AudioVariable outOfBreathSFX;

    public bool IsRunning { get => isRunning; set => isRunning = value; }
    public bool IsRunLerp { get => Time.time < runLerpEndTime; }
    public float RunLerp { get => (runLerpEndTime - Time.time) / (runLerpTime * (staminaLocked ? 2 : 1)); }
    public bool IsCrouched { get => crouched; set => crouched = value; }
    public SprintMoveData SprintMoveData { get => sprintMoveData; }

    protected override void Setup()
    {
        base.Setup();
        cacheInput = characterInput;
        userInputLock = new MultiBoolLock();
        Instance = this;
    }
    private void Start()
    {
        Set(PlayerInventory.Instance);
        stamina = CycleManager.Instance.GetEventValue("staminia", Mathf.CeilToInt(sprintMoveData.duration));
        colliderHeight = mover.colliderHeight;
        camHeight = cameraMover.GetLocalOffest();
        Publisher.Subscribe<InventoryChangedEvent>(OnInventoryChanged);
        OnJump += StaminiaOnJump;
    }

    private void StaminiaOnJump(Vector3 _v)
    {
        if (isRunning)
            stamina -= 1.0f;
        else
            stamina -= 0.5f;
        rechargeStartTime = Time.time + (rechargeStartDelay*2);
    }

    protected override void Update()
    {
        base.Update();
        //isRunning = !ExcessInventory.overExerted && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift));
        bool hasStamina = stamina > 0;
        bool canRun = !crouched && hasStamina && !staminaLocked;
        bool wasRunning = isRunning;
        isRunning = canRun && Input.GetKey(KeyCode.LeftShift);

        if (wasRunning != isRunning)
        {
            if (isRunning)
            {
                isRunning = true;
                camInput.mouseSensitivity = 1f;
                OnRunStart.Invoke(sprintMoveData);
                sprintMoveData.Start(movementSpeed);
                HUDController.Instance.OnSprintStart();
            }
            else
            {
                rechargeStartTime = Time.time + rechargeStartDelay;
                runLerpEndTime = Time.time + (runLerpTime * (staminaLocked ? 2 : 1));
                isRunning = false;
                camInput.mouseSensitivity = 2;
                OnRunEnd.Invoke(sprintMoveData);
                sprintMoveData.End();
                HUDController.Instance.OnSprintEnd();
            }
        }
        if (isRunning)
        {
            stamina -= Time.deltaTime;
            if (stamina <= 0)
            {
                stamina = 0;
                staminaLocked = true;
                HUDController.Instance.SetStaminiaLock(staminaLocked);
                outOfBreathSFX.Play(transform.position)
                    .StopIn(5, 1);
            }
        }
        else if (stamina <= sprintMoveData.duration)
        {
            if (Time.time >= rechargeStartTime)
                stamina += Time.deltaTime * sprintMoveData.rechargeRate;
            if (stamina >= sprintMoveData.duration)
            {
                stamina = sprintMoveData.duration;
                staminaLocked = false;
                HUDController.Instance.SetStaminiaLock(staminaLocked);
            }
        }
        HUDController.Instance.UpdateStaminia(stamina / sprintMoveData.duration);

        if (!userInputLock.Value)
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                if (crouched)
                {
                    CrouchToggle(false);
                }
                else
                {
                    if (currentControllerState == ControllerState.Grounded)
                    {
                        CrouchToggle(true);
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.V))
            {
                if (squat)
                {
                    StandUp();
                }
                else
                {
                    if (mover.RBody.linearVelocity != Vector3.zero)
                        return;
                    if (currentControllerState != ControllerState.Grounded)
                        return;
                    if (!crouched)
                        CrouchToggle(true);
                    Time.timeScale = 16;
                    squat = true;
                }

            }
        }
    }

    private void OnDestroy()
    {
        StandUp();
        CycleManager.Instance.AddTempEvent("staminia", Mathf.CeilToInt(stamina));
        Publisher.Unsubscribe<InventoryChangedEvent>(OnInventoryChanged);
    }
    public void StandUp()
    {
        if (crouched)
            CrouchToggle(false);
        squat = false;
        Time.timeScale = 1;
    }
    public static void RemoveUserInput(object o)
    {
        Instance.userInputLock.AddLock(o);
        Instance.characterInput = null;
        //Instance.StandUp();
    }
    public static void RestoreUserInput(object o)
    {
        Instance.userInputLock.RemoveLock(o);
        if (!Instance.userInputLock.Value)
            Instance.characterInput = Instance.cacheInput;
    }

    public IEnumerator WalkInDirection(Vector3 forward, float duration)
    {
        autoWalkVel = forward;
        autoWalker = true;
        yield return new WaitForSeconds(duration);
        autoWalker = false;
        autoWalkVel = Vector3.zero;
    }
    protected override Vector3 CalculateMovementDirection()
    {
        if (autoWalker)
        {
            return autoWalkVel;
        }
        else if (squat)
        {
            return Vector3.zero;
        }
        else
            return base.CalculateMovementDirection();
    }
    protected override bool CanJump()
    {
        return !ExcessInventory.overExerted && !squat && !crouched && !autoWalker
            && !staminaLocked && base.CanJump();
    }
    public void CrouchToggle(bool val)
    {
        crouched = val;
        if (crouched)
        {
            mover.colliderHeight = crouchHeightCol;
            mover.RecalculateColliderDimensions();
            cameraMover.SetLocalOffset(crouchHeightCam);
        }
        else
        {
            mover.colliderHeight = colliderHeight;
            mover.RecalculateColliderDimensions();
            cameraMover.SetLocalOffset(camHeight);
        }
    }
    protected override float GetMoveSpeed()
    {
        if (squat)
            return 0;
        else if (crouched)
        {
            return crouchSpeed;
        }
        else if (isRunning)
        {
            return sprintMoveData.GetSpeed();
        }
        else if (ExcessInventory.overExerted)
        {
            return movementSpeed * 0.9f;
        }
        else if (IsRunLerp)
        {
            float runSpd = sprintMoveData.LastSpeedValue;
            float targetSpd = base.GetMoveSpeed();
            float t = RunLerp;
            return Mathf.Lerp(targetSpd, runSpd, t);
        }
        else
            return base.GetMoveSpeed();
    }

    public void OnInventoryChanged(InventoryChangedEvent e)
    {
        Set(e.i);
    }

    public void Set(Inventory i)
    {
        if (!(i is PlayerInventory)) return;

        sprintMoveData = runData;
        foreach (ItemPlacementData item in i.inventoryItems)
        {
            if (item.item is MovementItem)
            {
                sprintMoveData = (item.item as MovementItem).sprintData;
            }
        }
        stamina = Mathf.Clamp(stamina, 0, sprintMoveData.duration);
    }
}
