﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;
using EZCameraShake;
using Sirenix.OdinInspector;

public class NormanClassArea : Area
{
    public TimeRef issacDeathTime;
    public CharacterData issac;

    public TimeRef solphiaDeathTime;
    public CharacterData solphia;

    public CharacterData holloway;

    public TimeRef classStartTime;
    public DialogueVariable classStartScene;
    public DialogueVariable classCalamityScene;

    public InteractionPanel seatPanel;
    CameraShakeInstance shake;
    public AudioVariable rumbilingSFX;
    public AudioVariable firstCalamityBGM;
    public AudioVariable tvAlarm2D;

    public TV tv;
    public TimeRef firstEncounterTime;
    public DialogueVariable firstEncounterScript;

    public GameObject enemyTemp1, enemyTemp2, enemyTemp3;

    public override void OnAreaEnter()
    {
        base.OnAreaEnter();
        TimeController.TriggerOrAdd(issacDeathTime, KillIssac);
        TimeController.TriggerOrAdd(solphiaDeathTime, KillSolphia);
        HandleTimining();
        Publisher.Subscribe<SceneTriggerEvent>(HandleTriggers);
    }
    public void HandleTimining()
    {
        if (TimeController.Instance.CurrentTime < classStartTime)
        {
            TimeController.AddTimeEvent(classStartTime, StartClass);
        }
        else if (TimeController.IsPreCalamity())
        {
            Debug.LogWarning("Make specific dilogue for norman being late");
            StartClass();
        }
        else
        {

        }
    }
    public override void OnAreaExit()
    {
        base.OnAreaExit();
        TimeController.RemoveTimeEvent(issacDeathTime, KillIssac);
        TimeController.RemoveTimeEvent(solphiaDeathTime, KillSolphia);
        TimeController.RemoveTimeEvent(classStartTime, StartClass);
        Publisher.Unsubscribe<SceneTriggerEvent>(HandleTriggers);
    }
    void KillIssac()
    {
        //if (CycleManager.Instance.HasEvent(e.Identifier, "started"))
        //    return;

        //issac.Die();
    }

    void KillSolphia()
    {
        //if (CycleManager.Instance.HasEvent(e.Identifier, "started"))
        //   return;

        //solphia.Die();
    }
    void StartClass()
    {
        //PathManager.SetPath(pathToStorageRoom);
        DialogueRenderer.Instance.PlayScene(classStartScene, null);
    }

    public void WaitForClass()
    {
        seatPanel.Open();
    }

    public void HandleTriggers(SceneTriggerEvent e)
    {
        switch (e.action)
        {
            case "phone":
                tvAlarm2D.Play();
                break;
            case "tv":
                tv.SetChannel(0);
                break;
            case "tvlook":
                tv.LookAt(true);
                break;
            case "tvend":
                tv.LookAt(false);
                AudioSystem.Stop(tvAlarm2D, true, 10);
                break;
            case "sceneend":
                AudioSystem.Stop(tvAlarm2D, true, 10);
                break;
            case "panel":
                seatPanel.Close();
                break;
            case "rumbling":
                Rumbling();
                break;
            case "calamity":
                shake.StartFadeOut(1.0f);
                AudioSystem.Stop(rumbilingSFX, true, 1);
                ScriptedCalamity();
                AudioSystem.PlayBGM(firstCalamityBGM, Vector3.zero, -1);
                break;
            case "spawnenemy":
                enemyTemp1.gameObject.SetActive(true);
                enemyTemp2.gameObject.SetActive(true);
                enemyTemp3.gameObject.SetActive(true);
                break;
            case "attack":
                holloway.GetCharacter().SetSprite(("collapsed"));
                break;
            default:
                break;
        }


    }

    public void Rumbling()
    {
        shake = CameraShaker.Instance.StartShake(1, 5, 0.5f);
        rumbilingSFX.Play(transform.position);
    }

    public void ScriptedCalamity()
    {
        //roots.MovePlayerToSafeSpot();
        TimeController.Instance.SetTime(TimeController.Instance.calamityTime);
        TimeController.Instance.IsPaused.AddLock(this);
        DialogueRenderer.Instance.PlayScene(classCalamityScene, OnScriptedCalamityEnd);
    }

    public void OnScriptedCalamityEnd()
    {
        TimeController.Instance.IsPaused.RemoveLock(this);
        TimeController.AddTimeEvent(firstEncounterTime, FirstEncounter);
    }

    public void FirstEncounter()
    {
        DialogueRenderer.Instance.PlayScene(firstEncounterScript, null);

    }
}
