﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;
using Cinemachine;

public class NormansRoomArea : Area
{
    public TextAsset cycleOneScene, cycleTwoScene, cycleThreeScene, sleepScene;
    public AreaPath cycleOnePath, cycleTwoPath;
    public AreaTransitionData toClass;
    public LightsInteraction lights;
    public AreaLightSource lamp;
    public CustomAudioClip flashSFX;
    public TextAsset lightsOnScene;
    public BasicInventoryPanel dresserInv;
    public TextAsset grabPhoneScene;
    public Item phoneItem;
    public Item[] clothes;
    public ManualLock normanDoorLock;
    public WorldPhone phone;
    public DialogueVariable leavePhoneScene, noClothesScene, dressedScene, mirrorNoClothes;
    public SceneInteraction mirrorInteraction;
    public Interactable bathroomDoor;
    public ObjectiveObject objective;
    public LightsInteraction lightSwitch1, lightSwitch2;
    public BasicBlocker switch1Blocker, switch2Blocker;
    protected override void Awake()
    {
        base.Awake();
        EffectsManager.Instance.PreCalam.BlackBars(0, 0);
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        Publisher.Unsubscribe<SceneTriggerEvent>(HandleTriggers);
        Publisher.Unsubscribe<InventoryOpenCloseEvent>(ClothesCheck);
    }
    public override void OnAreaEnter()
    {
        if (CycleManager.IsFirstCycle() && !CycleManager.Instance.HasEvent("", "cycleStart"))
        {
            EffectsManager.Instance.PreCalam.BlackBars(1, 0);
            bathroomDoor.enabled = false;
            mirrorInteraction.scene.scene = mirrorNoClothes.scene;
            normanDoorLock.isLocked = true;
            normanDoorLock.lockedScene = leavePhoneScene;
            PathManager.SetPath(cycleOnePath);
            CycleManager.Instance.AddTempEvent("", "fatherTimeCycleOne");
            base.OnAreaEnter();

            HUDController.Instance.SetHUDVisibility(false);
            StartCoroutine(FirstCycleSequence());

            TimeController.Instance.SetHelperMaxTime(new TimeRef(2, 0));
            switch1Blocker.enabled = switch2Blocker.enabled = true;
            lightSwitch1.onUseCallback.AddListener(OnLightChangedFirstCycle);
            lightSwitch2.onUseCallback.AddListener(OnLightChangedFirstCycle);
        }
        else if (SaveData.currentSave.numCycles == 2 && !CycleManager.Instance.HasEvent("", "cycleStart"))
        {
            PathManager.SetPath(cycleTwoPath);
            base.OnAreaEnter();
            DialogueRenderer.Instance.PlayScene(cycleTwoScene, SceneTwoComplete);

        }
        else if (SaveData.currentSave.numCycles == 3 && !CycleManager.Instance.HasEvent("", "cycleStart"))
        {
            base.OnAreaEnter();
            DialogueRenderer.Instance.PlayScene(cycleThreeScene, null);
        }
        else
        {
            base.OnAreaEnter();
        }
        CycleManager.Instance.AddTempEvent("", "cycleStart");
    }

    IEnumerator FirstCycleSequence()
    {
        //Disable Health UI
        PlayerInfoMenu.Instance.healthBar.transform
            .parent
            .parent
            .gameObject.SetActive(false);

        //Disable Time UI
        HUDController.Instance.SetHUDVisibility(false);

        InteractionHandler.LockInteraction(this);
        InventoryManager.LockInput(this);
        WalkController10MH.RemoveUserInput(this);
        CursorController.HideCursor(this);

        Publisher.Subscribe<SceneTriggerEvent>(HandleTriggers);

        yield return new WaitForSeconds(2);

        DialogueRenderer.Instance.PlayScene(cycleOneScene, SceneOneComplete);
        Publisher.Subscribe<InventoryOpenCloseEvent>(ClothesCheck);

    }
    void SceneOneComplete()
    {
        phone.RingLoop(4);
        InteractionHandler.RemoveInteractionLock(this);
        WalkController10MH.RestoreUserInput(this);
        CursorController.ShowCursor(this);
    }
    void SceneTwoComplete()
    {
        //SceneController.SetActiveEnvironment(toClass);
    }
    void HandleTriggers(SceneTriggerEvent e)
    {
        switch (e.action)
        {
            case "eyes":
                EffectsManager.Instance.PreCalam.BlackBars(0, 1.5f);
                break;
            case "start ring":
                phone.RingLoop(4);
                break;
            case "stop":
                phone.StopRingLoop();
                break;
            case "phone":
                phone.Ring();
                break;
            case "time":
                break;
            case "timer":
                timer = StartCoroutine(Timer());
                break;
            case "etimer":
                StopCoroutine(timer);
                break;
            case "wakeup":
                break;
            case "sleepy":
                //HUDController.Instance.SetUIState(HUDController.Instance.dialogueView);
                break;
            case "showtime":
                HUDController.Instance.SetTimeVisibility(true);
                TimeController.Instance.EmphasizeTime(0.5f);
                TimeController.Instance.IsPaused.RemoveLock(this);
                TimeController.Instance.SetHelperMaxTime(new TimeRef(1, 0));
                break;
            case "wakey":
                WalkController10MH.Instance.movementSpeed /= 2.0f;
                //lamp.Set(true);
                EffectsManager.Instance.PreCalam.BlackBars(0, 1.5f);
                break;
            case "closeeyes":
                EffectsManager.Instance.PreCalam.BlackBars(1, 1.0f);
                break;
            case "lighton":
                switch1Blocker.enabled = switch2Blocker.enabled = false;
                lightSwitch1.Interact();
                break;
            default:
                break;
        }
    }
    private void OnLightChangedFirstCycle()
    {
        if (GetLightValue()>0.9f)
            objective.CompleteObjective(1);
        else
            objective.UncompleteObjective(1);
    }
    Coroutine timer;
    IEnumerator Timer()
    {
        yield return new WaitForSeconds(600);
        yield return new WaitForSeconds(1);
        Scene sleep = DialogueUtils.GetSceneFromTextAsset(sleepScene);
        DialogueRenderer.interruptScene = sleep;
        DialogueRenderer.onSceneFinished = null;
        DialogueRenderer.onSceneFinished += EndScene;

    }

    void EndScene()
    {
        StartCoroutine(EndSceneRoutine());
    }
    IEnumerator EndSceneRoutine()
    {
        EffectsManager.Instance.PreCalam.BlackBars(0, 3);
        yield return new WaitForSeconds(2.5f);
        SceneController.LoadStartMenu();
    }
    void OnLightsOn()
    {
        //flashSFX.Play(lights.transform.position);
        //lights.onUseCallback.RemoveListener(OnLightsOn);
        //DialogueRenderer.Instance.PlayScene(lightsOnScene, null);
    }
    bool isDressed = false;
    void ClothesCheck(InventoryOpenCloseEvent e)
    {
        foreach (Item item in clothes)
        {
            if (!PlayerInventory.Instance.Contains(item))
            {
                return;
            }
        }
        isDressed = true;
        mirrorInteraction.scene.scene = mirrorInteraction.GetComponent<DialogueSceneMB>();
        if (PlayerInventory.Instance.Contains(phoneItem)
            || PlayerInventory.HasPermanentInvItem(phoneItem))
            normanDoorLock.isLocked = false;
        Publisher.Unsubscribe<InventoryOpenCloseEvent>(ClothesCheck);
        DialogueRenderer.Instance.PlayScene(dressedScene, AfterPlayerDressesd);
    }

    void AfterPlayerDressesd()
    {
        objective.CompleteObjective(2);
    }
    public void OnPhoneObtained()
    {
        if (isDressed)
            normanDoorLock.isLocked = false;
        else
            normanDoorLock.lockedScene = noClothesScene;
        bathroomDoor.enabled = true;
        InventoryManager.UnlockInput(this);
        phone.StopRingLoop();
        phone.phoneTransform.gameObject.SetActive(false);
        //TODO: Cache regular speed and set that instead
        WalkController10MH.Instance.movementSpeed *= 2;

        ItemPlacementData ipd = new ItemPlacementData();
        ipd.item = phoneItem;
        //PlayerInventory.Instance.SpawnItem(ipd);
        PlayerInventory.AddPermanentInvItem(ipd);

        StartCoroutine(WaitForPhoneOpen());
    }
    private void AfterPhoneObtained()
    {
        HUDController.Instance.SetBagUI(true);
        Invoke(nameof(AfterObjectiveOpen), 1);
    }
    private void AfterObjectiveOpen()
    {
        objective.CompleteObjective(0);
        if (isDressed)
            objective.CompleteObjective(2);
        if (GetLightValue() > 0.9f)
            objective.CompleteObjective(1);
    }
    public IEnumerator WaitForPhoneOpen()
    {
        InteractionHandler.LockInteraction(this);
        Publisher.Raise(new TutorialEvent("Alpha Phone", true, false, "", -1, "You've obtained the ALPHA Phone 7. Press the TAB key to view your inventory and the αPhone"));


        while (InventoryManager.Instance.IsHidden)
        {
            yield return null;
        }

        InventoryManager.LockInput(this);

        while (!Phone.Instance.IsOpen)
        {

            yield return null;
        }

        //TODO: Need to disable phone from closing

        Publisher.Raise(new TutorialProceedEvent());
        Publisher.Raise(new TutorialEndEvent());

        //DialogueRenderer.Instance.PlayScene(openPhoneScene, OnPhoneSceneComplete);
        DialogueRenderer.Instance.PlayScene(grabPhoneScene, OnPhoneSceneComplete);

        void OnPhoneSceneComplete()
        {
            InputSystem10MH.phoneOpenClose.Enable(this);
            InteractionHandler.RemoveInteractionLock(this);
            InventoryManager.UnlockInput(this);
            AfterPhoneObtained();
        }
    }

}
