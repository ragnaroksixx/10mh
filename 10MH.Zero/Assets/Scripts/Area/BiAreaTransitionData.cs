﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "New Area Transition Data", menuName = "Area/Two-Way TransitionData")]
public class BiAreaTransitionData : AreaTransitionData
{
    public AreaData area2;

    public override AreaData GetArea()
    {
        AreaData a = area;
        if (Application.isPlaying)
        {
            if (SceneController.activeEnvironmentScene == a)
                a = area2;
        }
        else
        {

        }
        return a;
    }
}
