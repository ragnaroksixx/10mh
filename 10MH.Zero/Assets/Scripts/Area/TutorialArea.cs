﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;

public class TutorialArea : Area
{
    public Material overrideSkybox;
    public Scene initialScene;
    public Color fogColor = Color.black;
    public override void OnAreaEnter()
    {
        if (freezeTime)
            TimeController.Instance.IsPaused.AddLock(this);
        SpawnPlayer();
        LightControllerData.Instance.SetSkybox(overrideSkybox);
        DialogueRenderer.Instance.PlayScene("TutorialOne", null);
        //LightController.Instance.SetFog(true, fogColor, FogMode.ExponentialSquared, 0.5f, 0);
        //TimeController.Instance.timeText.text = "1ㄥ:0ñ";
    }
    public override void OnAreaExit()
    {
        //LightController.Instance.SetFog(false);
        base.OnAreaExit();
    }
    public void ClearFogOne()
    {
        //LightController.Instance.SetFog(true, fogColor, FogMode.ExponentialSquared, 0.2f, 1);
    }
}

