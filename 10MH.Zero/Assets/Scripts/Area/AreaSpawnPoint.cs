﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

public class AreaSpawnPoint : MonoBehaviour
{
    public AreaTransitionData spawnData;

    [Button(DirtyOnClick = true)]
    public void SetAsDebugSpawn()
    {
        PlaySetting10MH c = PlaySetting10MH.Instance;
        if (c != null)
        {
            c.useDebugSpawn = true;
            c.debugSpawn = spawnData;
        }
    }
}
