﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class RoofArea : Area
{
    string cloakingFieldDiscoverEvent = "cloakingfielddiscoverevent";
    public Interactable interactable;
    public Transform cloakingLine;
    protected override IEnumerator Start()
    {
        yield return base.Start();
        if (CycleManager.Instance.HasEvent(MemorySet.FIRST_TIME,cloakingFieldDiscoverEvent))
        {
            interactable.autoInteract = false;
        }
        else
        {
            interactable.enabled = false;
        }
    }
    public void StartRipple()
    {
        if (!CycleManager.Instance.HasEvent(MemorySet.FIRST_TIME, cloakingFieldDiscoverEvent))
        {
            CycleManager.Instance.AddPermanentEvent(MemorySet.FIRST_TIME, cloakingFieldDiscoverEvent);
            InteractionHandler.Instance.StartInteraction(interactable);
            interactable.autoInteract = false;
            interactable.enabled = true;
        }
        cloakingLine.DOKill();
        cloakingLine.DOScaleX(1.9f, 0.5f)
            .SetEase(Ease.InBounce);
        EffectsManager.Instance.StartRipple();
    }
    public void EndRipple()
    {
        cloakingLine.DOKill();
        cloakingLine.DOScaleX(0, 0.5f)
            .SetEase(Ease.OutBounce);
        EffectsManager.Instance.StopRipple();
    }
}
