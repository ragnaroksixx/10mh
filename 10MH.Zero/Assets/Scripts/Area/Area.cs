﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine.Rendering;

public class Area : MonoBehaviour
{
    public static Area activeArea;
    public GameObject playerPrefab;
    public bool freezeTime = false;
    float lightProbeWaitTime;
    public List<CharacterData> activeCharacters = new List<CharacterData>();

    List<AreaLightSource> lightSources = new List<AreaLightSource>();

    public List<AreaLightSource> LightSources { get => lightSources; set => lightSources = value; }
    PhysicsCacher pc = null;

    protected virtual void Awake()
    {
        activeArea = this;
        GameObject.FindObjectsOfType<CalamityTrigger>(true).ForEach(x => { x.Init(); });
        if (PhysicsCacher.enabled)
        {
            if (!PhysicsCacher.objects.ContainsKey(SceneController.activeEnvironmentScene))
            {
                pc = new PhysicsCacher();
                pc.Init(this, SceneController.activeEnvironmentScene);
            }
            else
            {
                pc = PhysicsCacher.objects[SceneController.activeEnvironmentScene];
                pc.OnAreaEnter(this);
            }
        }
    }
    private void LightProbeUpdateExtender()
    {
       // lightProbeWaitTime = Time.realtimeSinceStartup + 2;
    }
    protected virtual IEnumerator Start()
    {
        bool isSceneActive = false;
        EffectsManager.SetGroundHeight(transform.position.y);

        while (!isSceneActive || !DynamicGI.isConverged)
        {
            yield return null;
            isSceneActive = gameObject.scene == SceneManager.GetActiveScene();
        }
        //LightProbeUpdateExtender();
        //LightProbes.lightProbesUpdated += LightProbeUpdateExtender;
        //while (Time.realtimeSinceStartup < lightProbeWaitTime)
        //{
        //    //Debug.Log(Time.realtimeSinceStartup+"<"+lightProbeWaitTime);
        //    yield return null;
        //}
        //LightProbes.lightProbesUpdated -= LightProbeUpdateExtender;
        OnAreaEnter();
    }
    protected virtual void OnDestroy()
    {
        OnAreaExit();
        //LightProbes.lightProbesUpdated -= LightProbeUpdateExtender;
    }
    public virtual void OnAreaEnter()
    {
        if (freezeTime)
            TimeController.Instance.IsPaused.AddLock(this);

        SpawnPlayer();
        LightControllerData.Instance.OnAreaChanged(this);
        InventoryManager.Instance.CloseInventory();
        TextAsset enterScene = PathManager.GetEntryScene(SceneController.activeEnvironmentScene);
        if (enterScene)
            DialogueRenderer.Instance.PlayScene(enterScene, null);

        ProbeReferenceVolume.instance.lightingScenario = TimeController.IsCalamity() ? "Calamity" : "Default";
    }
    public virtual void OnAreaExit()
    {
        pc?.OnAreaExit(this);
        TimeController.Instance.IsPaused.RemoveLock(this);
    }
    protected void SpawnPlayer()
    {
        Vector3 pos;
        Quaternion rot;
        if (Global10MH.HasCachePose)
        {
            pos = Global10MH.cachePose.position;
            rot = Global10MH.cachePose.rotation;
        }
        else
        {
            AreaSpawnPoint asp = null;
            foreach (AreaSpawnPoint item in GetComponentsInChildren<AreaSpawnPoint>())
            {
                if (item.spawnData == SceneController.activeSpawnPoint)
                {
                    asp = item;
                    break;
                }
            }
            pos = asp.transform.position;
            rot = asp.transform.rotation;
            TimeController.Instance.SetAreaText(asp.spawnData.GetArea().displayName);
        }
        GameObject.Instantiate(playerPrefab, pos, rot);
        Global10MH.ClearCachePose();
    }
    public static void EnterCharacter(CharacterData cd)
    {
        if (!activeArea.activeCharacters.Contains(cd))
        {
            activeArea.activeCharacters.Add(cd);
            Publisher.Raise(new CharacterEnterExit(cd));
        }
    }
    public static void ExitCharacter(CharacterData cd)
    {
        activeArea.activeCharacters.Remove(cd);
        Publisher.Raise(new CharacterEnterExit(cd));
    }
    public static bool IsCharacterInArea(CharacterData cd)
    {
        return activeArea.activeCharacters.Contains(cd);
    }

    public void UpdateLights()
    {

    }

    public bool IsLightOn()
    {
        if (TimeController.IsCalamity()) return true;
        if (lightSources.Count == 0) return true;
        bool isLightOn = false;
        foreach (AreaLightSource item in lightSources)
        {
            if (item.isOn)
                return true;
        }
        return isLightOn;
    }

    public float GetLightValue()
    {
        float result = lightSources.Count == 0 ? 1 : 0;
        foreach (AreaLightSource item in lightSources)
        {
            if (item.isOn)
                result += item.lightContribution;
        }
        result = Mathf.Clamp(result, 0, 1);
        return result;
    }

    public void SetAllLightSources(bool val)
    {
        foreach (AreaLightSource item in lightSources)
        {
            item.Set(val);
        }
    }
    [Button]
    public void SetShaderValues()
    {
        EffectsManager.SetGroundHeight(transform.position.y);
    }
#if UNITY_EDITOR
    [Button]
    public void LoadSingleLevel()
    {
        ScriptableObjectDictionary<AreaData> areaDictionary;
        areaDictionary = new ScriptableObjectDictionary<AreaData>("ScriptableObjects/Area/Areas");
        areaDictionary.Init(false);

        foreach (AreaData item in areaDictionary.AllItems)
        {
            if (item.scene == gameObject.scene.name)
            {
                item.LoadSceneEditor();
                return;
            }
        }
    }
#endif
}

public class CharacterEnterExit : PublisherEvent
{
    public CharacterData cd;

    public CharacterEnterExit(CharacterData cd)
    {
        this.cd = cd;
    }
}
