﻿using UnityEditor;
using UnityEngine;


public class DevRoom : Area
{
    bool hackBattleonce = false;
    public override void OnAreaEnter()
    {
        base.OnAreaEnter();
        if (PlaySetting10MH.Instance.startInBattle && PlaySetting10MH.Instance.startEncounter)
        {
            if(!hackBattleonce)
            {
                hackBattleonce = true;
                BattleController.StartBattle(PlaySetting10MH.Instance.startEncounter);
            }
        }
    }
}
