﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class WorldPhone : MonoBehaviour
{
    public AudioVariable vibrateLong;
    public Light screenLight;
    public Transform phoneTransform;
    AudioReference activeClip;
    float _indicatorVolume;
    float _indicatorVel;
    const float VOLUME_INDICATOR_BOOST = 10;
    public MeshRenderer screenMesh;
    Coroutine ringloop;
    public Item phoneItem;
    private void Awake()
    {
        screenMesh.material = Object.Instantiate(screenMesh.material);
    }
    private void Start()
    {
        if (PlayerInventory.Instance.Contains(phoneItem) || PlayerInventory.HasPermanentInvItem(phoneItem))
        {
            phoneTransform.gameObject.SetActive(false);
        }
        else if (CycleManager.IsFirstCycle())
            RingLoop(3);
    }
    public void Ring()
    {
        activeClip = AudioSystem.PlaySFX(vibrateLong, phoneTransform.position);
        phoneTransform.DOShakeRotation(5, strength: new Vector3(0, 5, 0));
    }
    public void RingLoop(float interval)
    {
        StopRingLoop();
        ringloop = StartCoroutine(LoopRing(interval));
    }
    public void StopRingLoop()
    {
        if (ringloop != null)
            StopCoroutine(ringloop);
        activeClip = null;
    }
    IEnumerator LoopRing(float interval)
    {
        while (true)
        {
            Ring();
            yield return new WaitForSeconds(interval);
        }
    }
    void Update()
    {
        if (activeClip != null)
        {
            if (activeClip.audioObject.Source.isPlaying)
            {
                SetVolume(activeClip.audioObject.GetAmplitude());
            }
            else
            {
                activeClip = null;
                screenLight.intensity = 0;
                screenMesh.material.SetColor("_EmissionColor", Color.white * 0);
            }
        }
        else
        {
            screenLight.intensity = 0;
            screenMesh.material.SetColor("_EmissionColor", Color.white * 0);
        }
    }

    void SetVolume(float volume)
    {
        _indicatorVolume = Mathf.SmoothDamp(_indicatorVolume, Mathf.Clamp01(volume * VOLUME_INDICATOR_BOOST),
            ref _indicatorVel, .1f, 10, Time.unscaledDeltaTime);
        screenLight.intensity = _indicatorVolume / 3.0f;
        screenMesh.material.SetColor("_EmissionColor", Color.white * _indicatorVolume * 2);
    }
}
