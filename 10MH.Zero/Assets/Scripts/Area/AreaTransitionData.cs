﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;
[CreateAssetMenu(fileName = "New Area Transition Data", menuName = "Area/TransitionData")]
public class AreaTransitionData : ScriptableObject, IUnquieScriptableObject
{
    [SerializeField]
    protected AreaData area;
    public string[] alts = new string[0];
    public string Identifier => name;

    public string[] Alias => alts;

    public void Init()
    {
    }

    public virtual AreaData GetArea()
    {
        return area;
    }
}

public class AreaTransitionEvent : DialogueAction
{
    public override string eventName => "transition";
    public AreaData area { get => tags.GetArea(); }

    public override IEnumerator Process(Layer layer)
    {
        yield return base.Process(layer);
        DialogueRenderer.Instance.ForceInterruptScene(() =>
        {
            SceneController.SetActiveEnvironment(area);
        });
    }
}
