﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;
using NHG.Dialogue;
using System.Collections.Generic;
using UnityEditor;

[CreateAssetMenu(fileName = "New Area Data", menuName = "Area/AreaData")]
public class AreaData : ScriptableObject, IUnquieScriptableObject
{
    public string displayName;
    public string scene;
    public bool unlockAtStart;
    public Sprite mapImage;
    public string Identifier => displayName + scene;
    public string[] Alias => new string[] { displayName };
#if UNITY_EDITOR
    public SceneAsset sceneAsset;
    public bool isDemoArea;
    public List<string> tags = new List<string>();
#endif

    public void Init()
    {
    }


#if UNITY_EDITOR
    [Button]
    public void LoadSceneEditor()
    {
        SceneController.LoadSceneEditor("MasterScene");
        SceneController.LoadSceneEditor(sceneAsset, UnityEditor.SceneManagement.OpenSceneMode.Additive);
    }
    [Button]
    public void LoadSceneAdditiveEditor()
    {
        SceneController.LoadSceneEditor(sceneAsset, UnityEditor.SceneManagement.OpenSceneMode.Additive);
    }
    public bool IsTag(string val)
    {
        return tags.Contains(val);
    }
    [Button(DirtyOnClick = true)]
    public void SetScene(SceneAsset aScene)
    {
        string scenePath = AssetDatabase.GetAssetPath(aScene);
        scene = aScene.name;

        List<EditorBuildSettingsScene> scenes = new List<EditorBuildSettingsScene>(EditorBuildSettings.scenes);
        foreach (EditorBuildSettingsScene s in scenes)
        {
            if (s.path == scenePath)
                return;
        }

        EditorBuildSettingsScene ss = new EditorBuildSettingsScene(scenePath, true);
        scenes.Add(ss);
        EditorBuildSettings.scenes = scenes.ToArray();
    }
#endif
}

public class UnlockAreaEvent : DialogueAction
{
    public override string eventName => "unlock area";
    public AreaData area { get => tags.GetExpressArea(); }

    public override IEnumerator Process(Layer layer)
    {
        DialogueRenderer.SetLocation(area);
        yield return base.Process(layer);
    }
}
