﻿using UnityEngine;
using System.Collections;
using NHG.Dialogue;

public class DialogueArea : Area
{
    public static DialogueData sceneData;

    public override void OnAreaEnter()
    {
        if (freezeTime)
            TimeController.Instance.IsPaused.AddLock(this);
        SpawnPlayer();
        LightControllerData.Instance.OnAreaChanged(this);
        InventoryManager.Instance.CloseInventory();
        DialogueRenderer.Instance.PlayScene(sceneData.scene, OnSceneEnd);
    }
    void OnSceneEnd()
    {
        SceneController.SetActiveEnvironment(sceneData.nextScene);
    }
    public static void SetDialogueArea(TextAsset s, AreaTransitionData next)
    {
        sceneData = new DialogueData(s, next);
    }
}
[System.Serializable]
public struct DialogueData
{
    public Scene scene;
    public AreaTransitionData nextScene;

    public DialogueData(TextAsset scene, AreaTransitionData nextScene)
    {
        this.scene = DialogueUtils.GetSceneFromTextAsset(scene);
        this.nextScene = nextScene;
    }
}
