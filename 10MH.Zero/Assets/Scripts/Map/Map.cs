﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;
using DG.Tweening;
using System.Collections.Generic;
using NHG.Dialogue;

public class Map : MonoBehaviour
{
    public CanvasGroup canvasGroup;
    public Button goButton, backButton;
    public Button nextButton, prevButton;
    public LinearOnScreenAnimator animator;
    public TMP_Text timeText;
    public Text selectedText;
    public Image selectedImage;
    public Sprite unknownImage;

    List<AreaTransitionData> areas;
    public AreaTransitionData selectedArea => areas[index];
    AreaTransitionData startingArea;
    public Button[] pageButtons;
    public int index = 0;

    public TMP_Text timeAddText;
    public LinearOnScreenAnimator addAnim;

    public TextAsset onFirstTimeUse;
    public Image darkBackground;

    readonly string mapUsed = "firstTimeMap";
    // Use this for initialization
    void Start()
    {
        timeText.text = TimeController.Instance.CurrentTime.FormatString(TimeController.START_HOUR, timeText);
        goButton.onClick.AddListener(OnClick);
        backButton.onClick.AddListener(OnBack);
        nextButton.onClick.AddListener(OnNext);
        prevButton.onClick.AddListener(OnPrev);
        animator.AnimateOnScreen();
        Global10MH.ShowCursor(this);
        CursorController.HideCursor(this);
        TimeController.Instance.IsPaused.AddLock(this);
        InventoryManager.LockInput(this);
        index = 0;
        startingArea = null;
        areas = new List<AreaTransitionData>(GlobalData10MH.Instance.mapAreas);
        foreach (AreaTransitionData atd in areas)
        {
            if (atd.GetArea() == SceneController.activeEnvironmentScene)
            {
                startingArea = atd;
                break;
            }
            index++;
        }
        if (startingArea == null)
            index = 0;
        for (int i = 0; i < pageButtons.Length; i++)
        {
            int x = i;
            pageButtons[i].onClick.AddListener(
                () => { GoToPage(x); });
        }
        UpdateArea();
        if (!CycleManager.Instance.HasEvent("Tutorial", mapUsed))
        {
            CycleManager.Instance.AddPermanentEvent("Tutorial", mapUsed);
            TutorialEvent te = new TutorialEvent("Campus Map", false, true, "", 0,
                "The map will allow you to traverse the various parts of the campus. As you progress throughout the game, more areas will be available to travel to.",
                "Be careful! Different parts of the map will require a certain amount of time to reach"
                );
            StartDialogueUI();
            Publisher.Subscribe<TutorialEndEvent>(OnTutorialFinished);

            Publisher.Raise(te);
            //DialogueRenderer.Instance.PlayScene(onFirstTimeUse, OnSceneFinished);
        }
        InventoryManager.Instance.CloseInventory();
        HUDController.Instance.SetHUDVisibility(false);
    }
    void StartDialogueUI()
    {
        darkBackground.raycastTarget = true;
        canvasGroup.blocksRaycasts = false;
        darkBackground.DOFade(0.75f, 0.5f);
    }
    void EndDialogueUI()
    {
        canvasGroup.blocksRaycasts = true;
        darkBackground.raycastTarget = false;
        darkBackground.DOKill();
        darkBackground.DOFade(0, 0.5f);
    }
    void OnTutorialFinished(TutorialEndEvent e)
    {
        Publisher.Unsubscribe<TutorialEndEvent>(OnTutorialFinished);
        EndDialogueUI();
    }
    void GoToPage(int x)
    {
        index = x;
        UpdateArea();
    }
    private void OnDestroy()
    {
        InventoryManager.UnlockInput(this);
        HUDController.Instance.SetHUDVisibility(true);
    }
    public void UpdateArea()
    {
        if (CycleManager.Instance.HasAreaUnlocked(selectedArea.GetArea()))
        {
            selectedImage.sprite = selectedArea.GetArea().mapImage;
            selectedText.text = selectedArea.GetArea().displayName;
            timeAddText.text = selectedArea == startingArea ? "+0sec" : "+60sec";
            if (selectedArea != startingArea)
                addAnim.AnimateOnScreen();
            else
                addAnim.AnimateOffScreen();

        }
        else
        {
            selectedImage.sprite = unknownImage;
            selectedText.text = timeAddText.text = "???";
            addAnim.AnimateOffScreen();
        }
        for (int i = 0; i < pageButtons.Length; i++)
        {
            pageButtons[i].interactable = i != index;
            pageButtons[i].transform.localScale =
                i == index ? Vector3.one * 1.3f : Vector3.one;
        }
    }
    void OnNext()
    {
        index = (index + 1) % areas.Count;
        UpdateArea();
    }
    void OnPrev()
    {
        index--;
        if (index < 0)
            index = areas.Count - 1;
        UpdateArea();
    }
    void OnClick()
    {
        Go(selectedArea);
    }
    void Go(AreaTransitionData ad)
    {
        if (ad && !CycleManager.Instance.HasAreaUnlocked(ad.GetArea()))
            return;

        Scene scene = null;


        if (!PathManager.CanUse(ad, out scene))
        {
            if ((ad == startingArea) && (!PathManager.Path.forceMapMovement))
            {

            }
            else
            {
                StartDialogueUI();
                DialogueRenderer.Instance.PlaySceneInMap(scene, EndDialogueUI);
                return;
            }
        }

        if (ad == startingArea)
        {

        }
        else
        {
            TimeController.Instance.AddSeconds(60);
        }
        Global10MH.HideCursor(this);
        CursorController.ShowCursor(this);
        TimeController.Instance.IsPaused.RemoveLock(this);
        canvasGroup.blocksRaycasts = false;
        if (ad == null)
        {
            SceneController.SetActiveEnvironment(SceneController.activeEnvironmentScene);
        }
        else
        {
            Global10MH.ClearCachePose();
            SceneController.SetActiveEnvironment(ad);
        }
        SceneController.UnloadMapScene();
    }
    void OnBack()
    {
        Go(startingArea);
    }
}
