﻿using UnityEngine;
using System.Collections;
using Dreamteck.Splines;
using DG.Tweening;
using Sirenix.OdinInspector;

public class PowererdCable : MonoBehaviour
{
    bool powered;
    public TubeGenerator tube;
    Tween tween;

    [Button]
    void PowerOn()
    {
        if (!Application.isPlaying)
        {
            powered = true;
            tube.colorModifier.keys[0].end = 0.001f;
            tube.Rebuild();
        }
        else
        {
            SetPower(true);
        }
    }
    [Button]
    void PowerOff()
    {
        if (!Application.isPlaying)
        {
            powered = false;
            tube.colorModifier.keys[0].end = 1;
            tube.Rebuild();
        }
        else
        {
            SetPower(false);
        }
    }
    public void SetPower(bool value)
    {
        powered = value;
        if (!powered)
        {
            TweenTo(1);
        }
        else
        {
            TweenTo(0.001f);
        }
    }

    void TweenTo(double val)
    {
        tween?.Kill(false);
        tween = DOTween.To(() => tube.colorModifier.keys[0].end,
            x => tube.colorModifier.keys[0].end = x,
            val,
            0.5f);
        tween.OnUpdate(() => { tube.Rebuild(); });
    }
}
