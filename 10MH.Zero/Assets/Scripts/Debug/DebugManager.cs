﻿using UnityEngine;
using System.Collections;

public class DebugManager : MonoBehaviour
{
    CameraCommands cameraCommands = new CameraCommands();
    DialogueCommands dialogueCommands = new DialogueCommands();
    AudioCommands audioCommands = new AudioCommands();
    GeneralCommands generalCommands = new GeneralCommands();

    public static DebugManager Instance;
    private void Awake()
    {
        Instance = this;
    }
    private void Update()
    {
        if(Input.GetKey(KeyCode.Backspace))
        {
            cameraCommands.Update();
            dialogueCommands.Update();
            audioCommands.Update();
            generalCommands.Update();
        }
    }
    public static void Log(string text, bool output = true)
    {
        if (output)
            Debug.Log(text);
    }
    public static void LogError(string text, bool output = true)
    {
        if (output)
            Debug.LogError(text);
    }
    public static void Log(string text, Color color, bool output = true)
    {
        if (Instance == null) return;
        string coloredText = string.Format("<color=#{0:X2}{1:X2}{2:X2}>{3}</color>", (byte)(color.r * 255f), (byte)(color.g * 255f), (byte)(color.b * 255f), text);
        Log(coloredText, output);
    }
}
