﻿using UnityEngine;
using System.Collections;
using System;
using NHG.Dialogue;

public class DialogueCommands : DebugCommands
{
    public override void RegisterCommands()
    {
        Type t = typeof(DialogueCommands);
        AddCommand("PlayScene", "Plays dialogue", t, "play");
        AddCommand("SetTextSpeed", "Sets dialogue's type animation speed", t, "textspeed");
    }

    public static void PlayScene(string sceneName)
    {
        Scene s = DialogueUtils.GetSceneFromFile(sceneName);
        if (s != null)
        {
            DialogueRenderer.Instance.PlayScene(s, null);
        }
    }

    public static void SetTextSpeed(float f)
    {
        TextTyperSettings.SetTextSpeed(f);
        Debug.Log("Set speed -> " + f);
    }
}
