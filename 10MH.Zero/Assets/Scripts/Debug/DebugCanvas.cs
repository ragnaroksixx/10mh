﻿using System;
using System.Collections;
using UnityEngine;


[RequireComponent(typeof(Canvas))]
public class DebugCanvas : MonoBehaviour
{
    Canvas c;
    private void Awake()
    {
        c = GetComponent<Canvas>();
        if(c==null)
        {
            Destroy(this);
            return;
        }    
        Publisher.Subscribe<DebugCanvasEvent>(UpdateUI);
    }
    private void OnDestory()
    {
        Publisher.Unsubscribe<DebugCanvasEvent>(UpdateUI);
    }

    private void UpdateUI(DebugCanvasEvent e)
    {
        c.enabled = e.show;
    }
}

public class DebugCanvasEvent : PublisherEvent
{
    public bool show;

    public DebugCanvasEvent(bool show)
    {
        this.show = show;
    }
}
