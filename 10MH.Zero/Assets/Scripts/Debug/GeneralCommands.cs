﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using IngameDebugConsole;
using System.IO;

class GeneralCommands : DebugCommands
{
    public override void RegisterCommands()
    {
        Type t = typeof(GeneralCommands);
        AddCommand("RestartCycle", "Starts the next Cycle", t, "rc");
        AddCommand("NewGame", "Starts a new Game File", t, "ng");
        AddCommand("NewGamePlus", "Starts a developer New Game File", t, "ng+");
        AddCommand("GainStatus", "Gives Norman a Status Effect", t, "status");
        AddCommand("AddItem", "Adds Item to Norman's Inventory", t, "addto");
        AddCommand("QuickAddItem", "Adds Item to Norman's Inventory at (0,0)", t, "add");
        AddCommand("TakeScreenShot", "Takes a Screenshoot...duh", t, "screenshot", "p");
        AddCommand("GoTo", "Takes player to designated area", t, "go", "goto");
        AddCommand("GoTime", "Takes player to designated area adding time", t, "gotime");
        AddCommand("SetUIVisibility", "Hide UI for screenshots", t, "ui");
        AddCommand("StartBattle", "Starts a battle", t, "battle");
        AddCommand("SetQuality", "Sets Graphics Quality", t, "quality", "q");
        AddCommand("SetCalamity", "Sets Time to Calamity Phase", t, "calamity", "c");
        AddCommand("SaveGame", "Save the vidjah game", t, "save");
        AddCommand("TimeScale", "Set Time.timescale", t, "sTime");
        AddCommand("MainMenu", "", t, "mm");
        AddCommand("UseClockUI", "", t);
        AddCommand("UseTimeUI", "", t);

        RegisterShortcut("StartBattle test", KeyCode.B);

    }
    public static void MainMenu()
    {
        SceneController.LoadStartMenu();
    }
    public static void TimeScale(float time)
    {
        Time.timeScale = time;
    }
    public static void RestartCycle()
    {
        DebugLogManager.Instance.HideLogWindow();
        CycleManager.Instance.RestartCycle();
    }
    public static void NewGame()
    {
        DebugLogManager.Instance.HideLogWindow();
        StartScreen.CreateNewData();
        SceneController.LoadMainGame();
    }
    public static void NewGamePlus()
    {
        DebugLogManager.Instance.HideLogWindow();
        PathManager.SetPath(null);
        StartScreen.CreateNewGamePlusData();
        SceneController.LoadMainGame();
    }

    public static void GainStatus(string value)
    {

    }

    public static void AddItem(string key, int posX, int posY)
    {
        ItemPlacementData ipd = new ItemPlacementData();
        ipd.item = GlobalData10MH.Instance.itemDictionary.GetItem(key);
        if (ipd.item == null) return;
        ipd.x = posX;
        ipd.y = posY;
        ipd.orientation = ipd.item.defaultOrientation;
        PlayerInventory.Instance.TrySpawnItems(ipd);
    }
    public static void QuickAddItem(string key)
    {
        AddItem(key, 0, 0);
    }

    public static void TakeScreenShot()
    {
        DebugLogManager.Instance.HideLogWindow();
        Texture2D texture = ScreenCapture.CaptureScreenshotAsTexture(2);
        Sprite screenshot
            = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
        string name = "screenshot_" + DateTime.Now.ToString().Replace('/', '_').Trim(' ').Replace(':', '_') + ".png";
        string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) + "/10MH_Screenshots/";
        if (!Directory.Exists(folderPath))
            Directory.CreateDirectory(folderPath);
        //FileManagement.SaveJpgTexture(folderPath + name, texture, fullPath: true);
    }

    public static void GoTo(string area)
    {
        AreaTransitionData at = GlobalData10MH.Instance.areaTransitionDictionary
            .GetItem(area);
        if (area != null)
        {
            DebugLogManager.Instance.HideLogWindow();
            AreaTransitionInteraction.GoTo(at);
        }
    }
    public static void GoTime(string area, int min, int secs)
    {
        AreaTransitionData at = GlobalData10MH.Instance.areaTransitionDictionary
            .GetItem(area);
        if (area != null)
        {
            DebugLogManager.Instance.HideLogWindow();
            TimeController.Instance.AddSeconds(min * 60 + secs);
            AreaTransitionInteraction.GoTo(at);
        }
    }
    public static void SetUIVisibility(bool value)
    {
        Publisher.Raise(new DebugCanvasEvent(value));
    }
    public static void StartBattle(string name)
    {
        BattleEncounter b = GlobalData10MH.Instance.battleDictionary.GetItem(name);
        if (b != null)
        {
            BattleController.StartBattle(b);
            DebugLogManager.Instance.HideLogWindow();
        }
    }
    public static void SetQuality(string quality)
    {
        string[] names = QualitySettings.names;
        for (int i = 0; i < names.Length; i++)
        {
            if (quality.ToLower() == names[i].ToLower())
            {
                QualitySettings.SetQualityLevel(i, true);
                PlayerPrefs.SetInt("QUALITY", i);
                Debug.Log("Quality set to " + i);
                return;
            }
        }
        Debug.Log("Quality " + quality);
    }
    public static void SetCalamity()
    {
        DebugLogManager.Instance.HideLogWindow();
        Vector3 rot = CameraController10MH.Instance.transform.eulerAngles;
        rot.x = 0;
        Global10MH.SetCachePose(Player.Instance.transform.position,
        Quaternion.Euler(rot));
        InteractionHandler.Instance.ClearInteract();
        TimeController.Instance.SetTime(TimeController.Instance.calamityTime - new TimeRef(0, 2));
        SceneController.SetActiveEnvironment(SceneController.activeSpawnPoint);
    }

    public static void SaveGame()
    {
        SaveData.SaveGame();
    }

    public static void UseTimeUI()
    {
        TimeController.ShowTIMEUI();
    }
    public static void UseClockUI()
    {
        TimeController.ShowCLOCKUI();
    }
}

