﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IngameDebugConsole;
using System;
using System.Reflection;

public abstract class DebugCommands
{
    Dictionary<KeyCode, string> shortcutCommands;
    public DebugCommands()
    {
        shortcutCommands = new Dictionary<KeyCode, string>();
        RegisterCommands();
    }
    public abstract void RegisterCommands();
    public static void AddCommand(string command, string desc, string method, Type t, params string[] aliases)
    {
        DebugLogConsole.AddCommandStatic(command, desc, method, t);
        foreach (string item in aliases)
        {
            DebugLogConsole.AddCommandStatic(item.ToLower(), desc, method, t);
        }
    }
    public static void AddCommand(string command, string desc, Type t, params string[] aliases)
    {
        AddCommand(command.ToLower(), desc, command, t, aliases);
    }
    public void RegisterShortcut(string command, KeyCode key)
    {
        shortcutCommands.Add(key, command.ToLower());
    }
    public void Update()
    {
        foreach (KeyValuePair<KeyCode, string> item in shortcutCommands)
        {
            if(Input.GetKeyDown(item.Key))
            {
                DebugLogConsole.ExecuteCommand(item.Value);
            }
        }
    }
}
