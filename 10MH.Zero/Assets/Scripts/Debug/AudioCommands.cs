﻿using UnityEngine;
using System.Collections;
using System;

public class AudioCommands : DebugCommands
{

    public override void RegisterCommands()
    {
        Type t = typeof(AudioCommands);
        AddCommand("BGMVolume", "Set BGM volume percentage (0-100)", t, "bgm");
        AddCommand("SFXVolume", "Set SFX volume percentage (0-100)", t, "sfx");
        AddCommand("MasterVolume", "Set Master volume percentage (0-100)", t, "master");
        AddCommand("Mute", "Set Master volume to 0", t);
        AddCommand("UnMute", "Unmutes", t, "unmute");
        AddCommand("Sound", "Plays a sound", t);
    }

    public static void BGMVolume(int val)
    {
        AudioSystem.SetBGMPercentage(val);
    }
    public static void SFXVolume(int val)
    {
        AudioSystem.SetSFXPercentage(val);
    }
    public static void MasterVolume(int val)
    {
        AudioSystem.SetMasterPercentage(val);
        PlayerPrefs.DeleteKey("mute");
    }
    public static void Mute()
    {
        AudioSystem.SetMasterPercentage(0);
        PlayerPrefs.SetString("mute", "mute");
    }
    public static void UnMute()
    {
        AudioSystem.SetMasterPercentage(25);
        PlayerPrefs.DeleteKey("mute");
    }

    public static void Sound(string clipName)
    {
        CustomAudioClip clip = GlobalData10MH.Instance.audioDictionary.GetItem(clipName.Trim());
        clip?.Play();
    }
}
