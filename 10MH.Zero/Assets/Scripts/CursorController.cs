﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class CursorController : MonoBehaviour
{
    public Transform cursorObj;
    public static Vector3 CursorPosition { get => Input.mousePosition; }
    const float DOUBLE_CLICK_TIME = 0.25f;
    const float DOUBLE_MAX_DISTANCE = 5;
    public float lastClickTime;
    Vector3 lastClickPos;
    public ClickRipple rippleEffect;
    public static CursorController Instance;
    MultiBoolLock hideCursor;
    IClickableSource source;

    public bool IsInUse => source != null;
    private void Awake()
    {
        Instance = this;
        hideCursor = new MultiBoolLock();
    }
    private void Update()
    {
        //cursorObj.transform.position = Input.mousePosition;
        if (Input.GetMouseButtonDown(0))
        {
            OnClick(CursorPosition);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            float holdTime = Time.time - lastClickTime;
            if (source != null && holdTime > 0.15f)
            {
                OnClick(CursorPosition);
            }
        }
        else if (Input.GetMouseButtonDown(1))
        {
            CancelInteraction(CancelSource.USER_CANCELLED);
        }
    }
    public static void HideCursor(object o)
    {
        Instance.hideCursor.AddLock(o);
        Instance.cursorObj.gameObject.SetActive(false);
    }
    public static void ShowCursor(object o)
    {
        Instance.hideCursor.RemoveLock(o);
        if (!Instance.hideCursor.Value)
            Instance.cursorObj.gameObject.SetActive(true);
    }
    public static void CancelInteraction(CancelSource reason)
    {
        if (Instance.source != null && Instance.source.CanCancel(reason))
        {
            Instance.source?.OnCancel(reason);
            Instance.source = null;
        }
    }
    public T Raycast<T>(bool firstOnly) where T : MonoBehaviour
    {
        PointerEventData cursor = new PointerEventData(EventSystem.current);
        // This section prepares a list for all objects hit with the raycast
        cursor.position = CursorPosition;
        List<RaycastResult> objectsHit = new List<RaycastResult>();
        EventSystem.current.RaycastAll(cursor, objectsHit);
        T obj = null;

        if (firstOnly && objectsHit.Count > 0)
        {
            obj = objectsHit[0].gameObject.GetComponentInChildren<T>();
        }
        else
        {
            foreach (RaycastResult item in objectsHit)
            {
                obj = item.gameObject.GetComponentInChildren<T>();
            }
        }
        return obj;
    }
    public IClickable Raycast(bool findSource)
    {
        PointerEventData cursor = new PointerEventData(EventSystem.current);
        // This section prepares a list for all objects hit with the raycast
        cursor.position = CursorPosition;
        List<RaycastResult> objectsHit = new List<RaycastResult>();
        EventSystem.current.RaycastAll(cursor, objectsHit);
        IClickable obj = null;

        foreach (RaycastResult item in objectsHit)
        {
            if (findSource)
                obj = item.gameObject.GetComponentInChildren<IClickableSource>();
            else
                obj = item.gameObject.GetComponentInChildren<IClickableTarget>();


            if (obj != null && obj.IsInteractable)
            {
                break;
            }
        }
        return obj;
    }
    public void OnClick(Vector2 pos)
    {
        if (source != null
            && Time.time <= lastClickTime + DOUBLE_CLICK_TIME
            && Vector2.Distance(pos, lastClickPos) <= DOUBLE_MAX_DISTANCE)
        {
            source.OnDoubleClick();
            source = null;
            return;
        }
        IClickable obj = Raycast(source == null);
        if (source == null)
        {
            lastClickTime = Time.time;
            lastClickPos = pos;
            SetSource(obj as IClickableSource);
        }
        else
        {
            lastClickTime = 0;
            if (obj == null)
            {
                CancelInteraction(CancelSource.EMPTY_SOURCE);
            }
            else
            {
                IClickableTarget target = (obj as IClickableTarget);
                // a resolution can set the source so we cache off the value first
                IClickableSource cacheSource = (source);
                if (target.CanResolve(cacheSource))
                {
                    source = null;
                    target.ResolveInteraction(cacheSource);
                }
                else
                {
                    CancelInteraction(CancelSource.RESOLUTION_FAILED);
                }

            }
        }
    }
    public void SetSource(IClickableSource obj)
    {
        source = obj;
        source?.OnSource();
    }
    public void OnDoubleClick()
    {
        IClickable obj = Raycast(source == null);
        obj?.OnDoubleClick();
        source = null;
    }
}
public interface IClickable
{
    bool IsInteractable { get; }
    void OnDoubleClick();
}
public interface IClickableSource : IClickable
{
    void OnSource();
    bool CanCancel(CancelSource reason);
    void OnCancel(CancelSource reason);
}
public interface IClickableTarget : IClickable
{
    void ResolveInteraction(IClickableSource source);
    bool CanResolve(IClickableSource source);
}
public enum CancelSource
{
    NONE,
    RESOLUTION_FAILED,
    USER_CANCELLED,
    EMPTY_SOURCE,
    DESTORYED
}


