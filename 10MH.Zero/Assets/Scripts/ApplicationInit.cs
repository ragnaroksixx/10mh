﻿using UnityEngine;
using System.Collections;

public class ApplicationInit : MonoBehaviour
{
    public GlobalData data;
    private void Awake()
    {
        if (GlobalData10MH.Instance==null)
            data.Init();
#if UNITY_EDITOR
        if (SaveData.currentSave == null)
        {
            if (SaveData.HasSave())
                StartScreen.LoadData();
            else
                StartScreen.CreateNewData();
        }
#endif

    }
    private void Start()
    {
        Global10MH.HideCursor(this, true);
    }
    private void OnDestroy()
    {
        GlobalData10MH.Instance = null;
    }
}
