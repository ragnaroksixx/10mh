﻿using System.Collections;
using UnityEngine;


public class ItemVisual : MonoBehaviour
{
    public InventoryCollection inv;
    public Item item;
    // Use this for initialization
    void Start()
    {
        inv.OnInventoryChanged += OnInventoryChanged;
        OnInventoryChanged();
    }
    void OnInventoryChanged()
    {
        gameObject.SetActive(inv.Contains(item));
    }
}
