﻿using System.Collections;
using TMPro;
using UnityEngine;


public class DigitalClock : MonoBehaviour
{
    public TMP_Text text;

    // Update is called once per frame
    void Update()
    {
        text.text = TimeController.Instance.CurrentTime.FormatStringHRMIN(TimeController.START_HOUR);
    }
}
