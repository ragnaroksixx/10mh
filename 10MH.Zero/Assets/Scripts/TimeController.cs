﻿using UnityEngine;
using System.Collections;
using TMPro;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Rendering;

public class TimeController : MonoBehaviour
{
    public TimeRef calamityTime;
    public TimeRef doomsDayDuration;
    TimeRef startTime, doomsDayTimeStart;
    TimeRef currentTime;
    public MultiBoolLock IsPaused = new MultiBoolLock();
    public RectTransform timePopUpRoot;
    public TMP_Text timeTextHR;
    public TMP_Text timeTextMIN;
    public TMP_Text timeTextSEC;
    public Image timeCountdownImage;
    public GameObject clockUIRoot, timeUIRoot;
    //public ScrollingText areaNameText;
    [Range(0.1f, 2)]
    public float timeMultipler = 1;
    //public TimeResetEffectHandler subSpace;
    Timeline timeline;
    public static TimeController Instance;
    float secondsTrack;
    float defaultTimeMult;
    public CustomAudioClip bellsSFX;
    public const int START_HOUR = 2;
    TimeRef helperTime = null;
    public Image weatherImage;
    public Sprite calamityWeather;
    public TimeRef CurrentTime { get => currentTime; }

    private void Awake()
    {
        Instance = this;
        timeline = new Timeline();

        startTime = SaveData.currentSave.startTime;
        doomsDayTimeStart = SaveData.currentSave.endTime - doomsDayDuration;

        timeline.AddTimeEvent(doomsDayTimeStart + doomsDayDuration, ResetTheVideoGame);
        timeline.AddTimeEvent(calamityTime, OnCalamityStart);
        currentTime = new TimeRef();
        currentTime.SetTime(startTime);
        UpdateText();
        //areaNameText.Init();

        defaultTimeMult = 1.0f;
        ClearTimeMul();

        if (!BetterPlayerPrefs.GetBool("USETIMEUI", true))
        {
            ShowCLOCKUI();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (IsPaused.Value) return;
        secondsTrack += Time.deltaTime * timeMultipler;
        int seconds = Mathf.FloorToInt(secondsTrack);
        secondsTrack -= seconds;
        if (seconds > 0)
        {
            if (helperTime != null && currentTime >= helperTime)
            {
                return;
            }
            AddSeconds(seconds);
        }
    }
    public void AddSeconds(int seconds)
    {
        currentTime.AddSeconds(seconds);
        UpdateText();
        timeline.CheckEvents(currentTime);
    }
    public void AddSecondsTo(TimeRef t)
    {
        if (currentTime >= t)
            return;
        int seconds = (t - currentTime).TotalSeconds();
        AddSeconds(seconds);
    }
    public void SetTime(TimeRef t)
    {
        if (currentTime >= t)
            return;
        int seconds = (t - currentTime).TotalSeconds();
        secondsTrack = 0;
        AddSeconds(seconds);
    }

    public IEnumerator SkipTo(TimeRef t)
    {
        if (currentTime >= t)
            yield break;
        int seconds = (t - currentTime).TotalSeconds();
        yield return SkipSeconds(seconds);
    }

    public IEnumerator SkipSeconds(float sec)
    {
        AnimationCurve curve = AnimationCurve.Linear(0, 10, sec, 150);
        IsPaused.AddLock(this);
        secondsTrack = 0;

        while (sec > 0)
        {
            secondsTrack += Time.deltaTime * curve.Evaluate(sec);
            int seconds = Mathf.FloorToInt(secondsTrack);
            secondsTrack -= seconds;
            if (seconds > 0)
            {
                AddSeconds(seconds);
                sec -= seconds;
            }
            yield return null;
        }

        IsPaused.RemoveLock(this);
    }
    public IEnumerator WaitForTimeInGame(TimeRef t)
    {
        if (currentTime >= t)
            yield break;
        int seconds = (t - currentTime).TotalSeconds();
        yield return WaitForInGameSeconds(seconds);
    }
    public IEnumerator WaitForInGameSeconds(float sec)
    {
        IsPaused.AddLock(this);
        secondsTrack = 0;

        while (sec > 0)
        {
            secondsTrack += Time.deltaTime;
            int seconds = Mathf.FloorToInt(secondsTrack);
            secondsTrack -= seconds;
            if (seconds > 0)
            {
                AddSeconds(seconds);
                sec -= seconds;
            }
            yield return null;
        }

        IsPaused.RemoveLock(this);
    }
    float nextUpdateTime = 0;
    void UpdateText()
    {
        currentTime.FormatString(START_HOUR, timeTextHR, timeTextMIN, timeTextSEC);
        float ratio = (int)currentTime.minute / 10.0f;
        ratio += (currentTime.second / 60.0f) * 0.1f;
        ratio = 1 - ratio;
        if (Time.time >= nextUpdateTime)
        {
            //timeCountdownImage.fillAmount = ratio;
            timeCountdownImage.DOFillAmount(ratio, 0.25f);
            nextUpdateTime = Time.time + 2;
        }
    }
    public void ResetTheVideoGame()
    {
        CycleManager.Instance.RestartCycle();
    }
    public ScreenBreak sBreak;
    public void OnCalamityStart()
    {
        //sBreak.DoTheThing();
        weatherImage.sprite = calamityWeather;
        bellsSFX.Play();
        FindObjectOfType<ScreenTwistEffect>()?
            .Play();
        if (CycleManager.IsFirstCycle())
            EffectsManager.Instance.SetCalamity(5);
        else
            EffectsManager.Instance.SetCalamity(5);
        ProbeReferenceVolume.instance.lightingScenario = true ? "Calamity" : "Default";
    }
    public void SetAreaText(string name)
    {
        //areaNameText.SetText(name);
    }
    public static void SetTimeMul(float val)
    {
        Instance.timeMultipler = val;
    }
    public static void ClearTimeMul()
    {
        Instance.timeMultipler = Instance.defaultTimeMult;
    }
    public static bool IsCalamity()
    {
        return IsPast(Instance.calamityTime) || Instance.currentTime == Instance.calamityTime;
    }
    public static bool IsPreCalamity()
    {
        return !IsCalamity();
    }
    public static bool IsPast(TimeRef t)
    {
        return Instance.currentTime > t;
    }
    public static void AddTimeEvent(TimeRef t, Action e)
    {
        if (!IsPast(t))
            Instance.timeline.AddTimeEvent(t, e);
    }
    public static void AddTimeEventCalamity(Action e)
    {
        AddTimeEvent(Instance.calamityTime, e);
    }
    public static bool TriggerOrAdd(TimeRef t, Action e)
    {
        if (IsPast(t))
        {
            e.Invoke();
            return true;
        }
        else
        {
            Instance.timeline.AddTimeEvent(t, e);
            return false;
        }
    }
    public static bool TriggerOrAddCalamity(Action e)
    {
        return TriggerOrAdd(Instance.calamityTime, e);
    }

    public static void RemoveTimeEvent(TimeRef t, Action e)
    {
        Instance.timeline.RemoveTimeEvent(t, e);
    }
    public static void RemoveTimeEventCalamity(Action e)
    {
        RemoveTimeEvent(Instance.calamityTime, e);
    }
    public void SetHelperMaxTime(TimeRef t)
    {
        helperTime = t;
    }

    public void ClearHelperTiime()
    {
        helperTime = null;
    }

    public void EmphasizeTime(float delay)
    {
        float punch = 0.5f;
        timeTextHR.transform.DOPunchScale(Vector3.one * punch, 0.25f)
            .SetDelay(delay);
        timeTextMIN.transform.DOPunchScale(Vector3.one * punch, 0.25f)
            .SetDelay(delay + 0.125f);
        timeTextSEC.transform.DOPunchScale(Vector3.one * punch, 0.25f)
            .SetDelay(delay + 0.25f);
    }

    public static void ShowTIMEUI()
    {
        Instance.clockUIRoot.gameObject.SetActive(false);
        Instance.timeUIRoot.gameObject.SetActive(true);
        BetterPlayerPrefs.SetBool("USETIMEUI", true);
    }

    public static void ShowCLOCKUI()
    {
        Instance.clockUIRoot.gameObject.SetActive(true);
        Instance.timeUIRoot.gameObject.SetActive(false);
        BetterPlayerPrefs.SetBool("USETIMEUI", false);
    }
}

[System.Serializable]
public class Timeline
{
    public Dictionary<TimeRef, Action> timeRefs;
    public TimeRef nextEvent = null;
    public Timeline()
    {
        timeRefs = new Dictionary<TimeRef, Action>();
    }
    public void AddTimeEvent(TimeRef t, Action e)
    {
        if (timeRefs.ContainsKey(t))
        {
            Action timeEvent = timeRefs[t];
            timeEvent += e;
            timeRefs[t] = timeEvent;
        }
        else
        {
            timeRefs.Add(t, e);
        }
        if (nextEvent == null || t < nextEvent)
        {
            nextEvent = t;
        }
    }
    public void RemoveTimeEvent(TimeRef t, Action e)
    {
        if (timeRefs.ContainsKey(t))
        {
            timeRefs[t] -= e;
            if (timeRefs[t] == null)
            {
                timeRefs.Remove(t);
                if (nextEvent == t)
                {
                    nextEvent = null;
                    UpdateNextEvent();
                }
            }
        }
    }
    public void RemoveTo(TimeRef t)
    {
        List<TimeRef> toRemove = new List<TimeRef>();
        foreach (TimeRef time in timeRefs.Keys)
        {
            if (time <= t)
                toRemove.Add(time);
        }
        foreach (TimeRef time in toRemove)
        {
            timeRefs.Remove(time);
        }
        nextEvent = null;
        UpdateNextEvent();

    }
    public void CheckEvents(TimeRef currentTime)
    {
        UpdateNextEvent();
        if (nextEvent == null)
            return;
        if (nextEvent <= currentTime)
        {
            Action timeEvent = timeRefs[nextEvent];
            timeRefs.Remove(nextEvent);
            timeEvent.Invoke();
            nextEvent = null;
            CheckEvents(currentTime);
        }
    }
    public void UpdateNextEvent()
    {
        if (nextEvent == null)
        {
            foreach (TimeRef time in timeRefs.Keys)
            {
                if (nextEvent == null || time <= nextEvent)
                    nextEvent = time;
            }
            if (nextEvent == null)
                return;
        }
    }
    public Action GetEventAt(TimeRef time)
    {
        TimeRef last = null;
        foreach (TimeRef item in timeRefs.Keys)
        {
            if (last == null && item <= time)
            {
                last = item;
            }
            else if (item <= time && item > last)
            {
                last = item;
            }
        }

        if (last != null)
            return timeRefs[last];
        else
            return null;
    }
}
[System.Serializable]
public class TimeRef
{
    public int minute;
    public int second;
    private Vector2 cap;

    public TimeRef()
    {

    }
    public TimeRef(int minute, int second)
    {
        this.minute = minute;
        this.second = second;
    }
    public TimeRef(float minute, float second)
    {
        this.minute = (int)minute;
        this.second = (int)second;
    }
    public TimeRef(TimeRef t)
    {
        SetTime(t);
    }
    public void SetTime(TimeRef time)
    {
        SetTime(time.minute, time.second);
    }
    public TimeRef(int totalSeconds)
    {
        minute = totalSeconds / 60;
        second = totalSeconds % 60;
    }
    public void SetTime(int m, int s)
    {
        minute = m;
        second = s;
    }
    public TimeRef(Vector2 vec)
    {
        minute = (int)vec.x;
        second = (int)vec.y;
    }
    public void AddSeconds(int seconds)
    {
        second += seconds;
        while (second >= 60)
        {
            second -= 60;
            minute++;
        }
    }
    public void FormatString(int hour, TMP_Text hr, TMP_Text min, TMP_Text sec)
    {
        hr.text = hour + ":";
        min.text = minute.ToString("00");
        sec.text = "<size=150%>.</size>" + Mathf.FloorToInt(second).ToString("00");
    }
    public string FormatString(int hour, TMP_Text timeText)
    {
        string time = "";
        if (hour < 10)
            time += "0";
        time += hour;
        time += ":";
        if (minute < 10)
            time += "0";
        time += minute;
        if (timeText)
            time += "<size=" + timeText.fontSize / 2 + ">";
        time += ":";
        time += Mathf.FloorToInt(second).ToString("00");
        return time;
    }
    public string FormatStringHRMIN(int hour)
    {
        string time = "";
        if (hour < 10)
            time += "0";
        time += hour;
        time += ":";
        if (minute < 10)
            time += "0";
        time += minute;
        return time;
    }

    public override bool Equals(object obj)
    {
        if (obj is TimeRef)
        {
            TimeRef other = obj as TimeRef;
            return other == this;
        }
        return false;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return base.ToString();
    }

    public static bool operator <(TimeRef l, TimeRef r)
    {
        if (ReferenceEquals(l, null) || ReferenceEquals(r, null))
            return false;
        if (l.minute != r.minute)
            return l.minute < r.minute;
        if (l.second != r.second)
            return l.second < r.second;
        return false;
    }
    public static bool operator >(TimeRef l, TimeRef r)
    {
        if (ReferenceEquals(l, null) || ReferenceEquals(r, null))
            return false;
        if (l.minute != r.minute)
            return l.minute > r.minute;
        if (l.second != r.second)
            return l.second > r.second;
        return false;
    }
    public static bool operator <=(TimeRef l, TimeRef r)
    {
        if (ReferenceEquals(l, null) || ReferenceEquals(r, null))
            return false;
        if (l.minute != r.minute)
            return l.minute < r.minute;
        if (l.second != r.second)
            return l.second < r.second;
        return true;
    }
    public static bool operator >=(TimeRef l, TimeRef r)
    {
        if (ReferenceEquals(l, null) || ReferenceEquals(r, null))
            return false;
        if (l.minute != r.minute)
            return l.minute > r.minute;
        if (l.second != r.second)
            return l.second > r.second;
        return true;
    }
    public static bool operator ==(TimeRef l, TimeRef r)
    {
        if (ReferenceEquals(l, null) || ReferenceEquals(r, null))
            return ReferenceEquals(l, null) && ReferenceEquals(r, null);
        return l.minute == r.minute && l.second == r.second;
    }
    public static bool operator !=(TimeRef l, TimeRef r)
    {
        return !(l == r);
    }
    public static TimeRef operator +(TimeRef l, TimeRef r)
    {
        TimeRef result = new TimeRef(l.minute,
            l.second);
        result.AddSeconds(r.TotalSeconds());
        return result;
    }
    public static TimeRef operator -(TimeRef l, TimeRef r)
    {
        TimeRef result = new TimeRef(l.TotalSeconds() - r.TotalSeconds());
        return result;
    }
    public int TotalSeconds()
    {
        return (minute * 60) + second;
    }
    public string GetData()
    {
        return minute + ":" + second;
    }

    public TimeRef(string input)
    {
        string min = input.Split(':')[0];
        string sec = input.Split(':')[1];
        minute = Convert.ToInt32(min);
        second = Convert.ToInt32(sec);
    }


}
