﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;

public class StartScreen : MonoBehaviour
{
    public GlobalData data;
    public Button newGameButton, continueGameButton, wipeDataButton, settingsButton, yesButton, noButton, doneButton;
    public CanvasGroup newContinueGroup, confirmGroup, mainMenuGroup, settingsGroup;
    public CustomAudioClip bgm, clickSFX, clockBGM;
    public Image screenFader;
    public TextMeshProUGUI newGameText;
    public CharacterStartScreenImage char1, char2;
    public List<CharacterData> characterPool;
    private void Awake()
    {
        if (GlobalData10MH.Instance == null)
            data.Init();
    }
    // Use this for initialization
    void Start()
    {
        if (!SaveData.HasSave())
            continueGameButton.gameObject.SetActive(false);
        Global10MH.ShowCursor(this);

        newContinueGroup.blocksRaycasts = true;
        confirmGroup.blocksRaycasts = false;
        confirmGroup.gameObject.SetActive(false);

        AudioSystem.PlayBGM(bgm, Vector3.zero, 2);
        yesButton.onClick.AddListener(StartNewGame);
        noButton.onClick.AddListener(CloseNewGameMenu);
        newGameButton.onClick.AddListener(OpenNewGameMenu);
        continueGameButton.onClick.AddListener(LoadGame);
        wipeDataButton.onClick.AddListener(WipeData);
        StartCoroutine(CycleCharacters(char1));
        StartCoroutine(CycleCharacters(char2));

        settingsButton.onClick.AddListener(OpenSettingsMenu);
        doneButton.onClick.AddListener(CloseSettingsMenu);
    }
    public CharacterData GetRandomChar()
    {
        int index = Random.Range(0, characterPool.Count);
        CharacterData current = characterPool[index];
        while (current == char1.data || current == char2.data)
        {
            index = (index + 1) % characterPool.Count;
            current = characterPool[index];
        }
        return current;

    }
    public IEnumerator CycleCharacters(CharacterStartScreenImage cssi)
    {
        //yield return new WaitForSeconds(StarShakeEffect.waitTime);
        while (true)
            yield return cssi.SwitchCharacter(GetRandomChar());
    }
    private void OnDestroy()
    {
        Global10MH.HideCursor(this);
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            AudioSystem.PlaySFX(clickSFX, Vector3.zero);
        }
    }
    public void OpenNewGameMenu()
    {
        mainMenuGroup.gameObject.SetActive(false);
        newContinueGroup.blocksRaycasts = false;
        confirmGroup.transform.localScale = Vector3.one * 0.25f;
        confirmGroup.gameObject.SetActive(true);
        confirmGroup.transform.DOScale(Vector3.one, 0.25f)
            .OnComplete(() => { confirmGroup.blocksRaycasts = true; });
    }
    public void CloseNewGameMenu()
    {
        mainMenuGroup.gameObject.SetActive(true);
        newContinueGroup.blocksRaycasts = true;
        confirmGroup.blocksRaycasts = false;
        confirmGroup.transform.DOScale(Vector3.one * 0.1f, 0.25f)
            .OnComplete(() => { confirmGroup.gameObject.SetActive(false); });

    }
    public void StartNewGame()
    {
        newContinueGroup.blocksRaycasts = false;
        confirmGroup.blocksRaycasts = false;
        Sequence s = DOTween.Sequence();
        AudioSystem.PlayBGM(clockBGM, Vector3.zero, 4f);
        s.Append(screenFader.DOFade(1, 3))
            .Append(newGameText.DOFade(1, 2))
            .AppendInterval(1)
            .AppendCallback(() => { AudioSystem.PlayBGM(null, Vector3.zero, 3f); })
            .AppendInterval(2)
            .AppendCallback(()=> { SceneScreenFader.Instance.Play(); })
            .Append(newGameText.DOFade(0, 1))
            .OnComplete(CreateNewGame);

    }
    public void OpenSettingsMenu()
    {
        mainMenuGroup.gameObject.SetActive(false);
        settingsGroup.gameObject.SetActive(true);
    }
    public void CloseSettingsMenu()
    {
        mainMenuGroup.gameObject.SetActive(true);
        settingsGroup.gameObject.SetActive(false);
    }
    void WipeData()
    {
        PlayerPrefs.DeleteAll();
        AudioSystem.Instance.Start();
        SceneManager.LoadScene(0);
    }
    public static void LoadGame()
    {
        LoadData();
        SceneScreenFader.Instance.Play(SceneController.LoadMainGame);
    }
    public static void CreateNewGame()
    {
        CreateNewData();
        SceneController.LoadMainGame();
    }
    public void CreateNewGameDev()
    {
        CreateNewGamePlusData();
        SceneScreenFader.Instance.Play(SceneController.LoadMainGame);
    }
    public static void LoadData()
    {
        SaveData loadedSave = SaveData.LoadGame();
        SaveData.SetSave(loadedSave);
    }
    public static void CreateNewData()
    {
        List<ItemPlacementData> startingPlayerInventory = new List<ItemPlacementData>();
        MemorySet startingMemories = new MemorySet();
        ScriptableObjectDictionary<AreaData> allAreas = new ScriptableObjectDictionary<AreaData>("ScriptableObjects/Area/Areas");
        allAreas.Init(false);
        foreach (AreaData area in allAreas.AllItems)
        {
            if (area.unlockAtStart)
                startingMemories.Add("Unlocked Areas", area.Identifier);
        }

        ItemPlacementData hand = new ItemPlacementData();
        hand.item = GlobalData10MH.Instance.itemDictionary.GetItem("hand");
        hand.x = 0;
        hand.y = 0;

        startingPlayerInventory.Add(hand);
        startingMemories.UpdateInventory("player", startingPlayerInventory);

        SaveData newSave = new SaveData(startingMemories);
        newSave.numCycles = 0;
        TimeRef start = new TimeRef(0, 0);
        TimeRef end = new TimeRef(8, 0);
        newSave.SetTimes(start, end);
        newSave.hand = "hand";
        SaveData.SetSave(newSave);
    }
    public static SaveData CreateNewGamePlusData()
    {
        List<ItemPlacementData> defaultChestInventory = new List<ItemPlacementData>();
        List<ItemPlacementData> startingPlayerInventory = new List<ItemPlacementData>();
        List<ItemPlacementData> playerPermaItems = new List<ItemPlacementData>();
        MemorySet startingMemories = new MemorySet();

        ScriptableObjectDictionary<AreaData> allAreas = new ScriptableObjectDictionary<AreaData>("ScriptableObjects/Area/Areas");
        allAreas.Init(false);

        ScriptableObjectDictionary<Item> itemDictionary = new ScriptableObjectDictionary<Item>("ScriptableObjects/Items");
        itemDictionary.Init(false);

        foreach (AreaData area in allAreas.AllItems)
        {
            startingMemories.Add("Unlocked Areas", area.Identifier);
        }

        ItemPlacementData knife = new ItemPlacementData();
        knife.item = itemDictionary.GetItem("dullknife");
        knife.x = 0;
        knife.y = 0;

        ItemPlacementData knife2 = new ItemPlacementData();
        knife2.item = itemDictionary.GetItem("dullknife");
        knife2.x = 1;
        knife2.y = 0;

        ItemPlacementData bat = new ItemPlacementData();
        bat.item = itemDictionary.GetItem("bat");
        bat.x = 0;
        bat.y = 4;
        bat.orientation = ItemOrientationX.UP;

        ItemPlacementData hand = new ItemPlacementData();
        hand.item = itemDictionary.GetItem("solarm");
        hand.x = 0;
        hand.y = 0;


        ItemPlacementData soda = new ItemPlacementData();
        soda.item = itemDictionary.GetItem("soda");
        soda.x = 1;
        soda.y = 0;

        ItemPlacementData soda_1 = new ItemPlacementData();
        soda_1.item = itemDictionary.GetItem("soda");
        soda_1.x = 2;
        soda_1.y = 0;

        ItemPlacementData shield = new ItemPlacementData();
        shield.item = itemDictionary.GetItem("trashlid");
        shield.x = 3;
        shield.y = 1;

        ItemPlacementData nrg = new ItemPlacementData();
        nrg.item = itemDictionary.GetItem("power");
        nrg.x = 0;
        nrg.y = 3;
        nrg.orientation = ItemOrientationX.UP;

        ItemPlacementData hair = new ItemPlacementData();
        hair.item = itemDictionary.GetItem("fthair");
        hair.x = 4;
        hair.y = 3;

        ItemPlacementData note = new ItemPlacementData();
        note.item = itemDictionary.GetItem("neanote");
        note.x = 3;
        note.y = 3;

        ItemPlacementData phone = new ItemPlacementData();
        phone.item = itemDictionary.GetItem("phone");

        playerPermaItems.Add(phone);

        defaultChestInventory.Add(knife);
        //defaultChestInventory.Add(knife2);
        defaultChestInventory.Add(bat);
        defaultChestInventory.Add(soda);
        defaultChestInventory.Add(soda_1);
        //defaultChestInventory.Add(note);
        defaultChestInventory.Add(shield);
        defaultChestInventory.Add(nrg);
        defaultChestInventory.Add(hair);

        startingPlayerInventory.Add(hand);
        startingMemories.UpdateInventory("player", startingPlayerInventory);
        startingMemories.UpdateInventory("chest", defaultChestInventory);
        startingMemories.UpdateInventory("playerConst", playerPermaItems);
        //startingMemories.Add("", "neanote");
        SaveData newSave = new SaveData(startingMemories);
        TimeRef start = new TimeRef(0, 0);
        TimeRef end = new TimeRef(10, 0);
        newSave.SetTimes(start, end);
        newSave.numCycles = 2;
        newSave.hand = "solarm";
        SaveData.SetSave(newSave);
        return newSave;
    }

    public void FeedbackButton()
    {
        Application.OpenURL("https://forms.gle/rFGZ2NMWwfFwe5YZ8");
    }

    public void QuitButton()
    {
        Application.Quit();
    }
}
[System.Serializable]
public class CharacterStartScreenImage
{
    public LinearOnScreenAnimator animator;
    public Image image;
    public float delay;
    public CharacterData data;
    public IEnumerator SwitchCharacter(CharacterData nextChar)
    {
        data = nextChar;
        animator.AnimateOffScreen();
        yield return new WaitForSeconds(animator.transitionSpeed);
        image.sprite = nextChar.GetImage(ImageKey.NORMAL);
        yield return new WaitForSeconds(0.5f);
        animator.AnimateOnScreen();
        yield return new WaitForSeconds(delay + Random.Range(0, delay / 2));
    }
}
