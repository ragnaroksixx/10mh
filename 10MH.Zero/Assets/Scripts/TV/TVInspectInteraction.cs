﻿using NHG.Dialogue;
using System.Collections;
using UnityEngine;


public class TVInspectInteraction : InspectInteraction
{
    public TV tv;

    public override void OnInteract()
    {
        if (tv.CurrentChannel != null)
        {
            Scene s = DialogueUtils.GetSceneFrom(tv.CurrentChannel.inspectDialogue);
            PlayScene(s);
        }
        else
        {
            base.OnInteract();
        }
    }
}
