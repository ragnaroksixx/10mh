﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;

public class TV : MonoBehaviour
{
    TVChannel currentChannel;
    public TVChannel[] channels;
    public InteractionPanel iPanel;

    public TVChannel CurrentChannel { get => currentChannel; }

    [Button]
    public void SetChannel(TVChannel c)
    {
        currentChannel?.Hide();
        currentChannel = c;
        currentChannel?.Show();
    }

    public void SetChannel(int i)
    {
        SetChannel(channels[i]);
    }

    public void TurnOff()
    {
        SetChannel(null);
    }

    public void LookAt(bool val)
    {
        iPanel.cam.Priority = val ? InteractionPanel.focusPriority : 0;
        iPanel.cam.enabled = val;
    }
}
