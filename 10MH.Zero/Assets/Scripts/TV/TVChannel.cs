﻿using DG.Tweening;
using System.Collections;
using UnityEngine;


public class TVChannel : MonoBehaviour
{
    float startScale = 0;
    public AudioVariable sfx;
    AudioReference aRef;
    public DialogueVariable inspectDialogue;

    private void Awake()
    {
        startScale = transform.localScale.y;
        transform.ScaleY(0);
        gameObject.SetActive(false);
    }
    private void OnDestroy()
    {
        aRef?.Stop();
    }
    public void Show()
    {
        transform.ScaleY(0);
        gameObject.SetActive(true);
        transform.DOScaleY(startScale, 0.25f);
        aRef = sfx.Play(transform.position);
    }
    public void Hide()
    {
        aRef.Stop();
        transform.DOScaleY(0, 0.25f)
            .OnComplete(() => gameObject.SetActive(false));
    }
}
