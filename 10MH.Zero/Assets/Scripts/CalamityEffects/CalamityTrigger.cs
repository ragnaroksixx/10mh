﻿using System.Collections;
using UnityEngine;

public abstract class CalamityTrigger : MonoBehaviour
{
    public void Init()
    {
        SetCalamity();
        if (!TimeController.IsCalamity())
        {
            TimeController.TriggerOrAddCalamity(OnCalamity);
        }
    }
    private void OnDestroy()
    {
        TimeController.RemoveTimeEventCalamity(OnCalamity);
    }

    public abstract void SetCalamity(bool isCalamity);
    public virtual void OnCalamity()
    {
        try
        {
            SetCalamity();
        }
        catch
        {

        }
    }
    public void SetCalamity()
    {
        try
        {
            SetCalamity(TimeController.IsCalamity());
        }
        catch
        {

        }
    }
}
