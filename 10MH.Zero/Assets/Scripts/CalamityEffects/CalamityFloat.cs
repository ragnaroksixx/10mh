﻿using System.Collections;
using UnityEngine;


public class CalamityFloat : MonoBehaviour
{
    public enum WeightClass
    {
        LIGHT,
        MEDIUM,
        HEAVY
    }
    public WeightClass weight;
    // User Inputs
    float degreesPerSecond = 15.0f;
    float amplitude = 0.5f;
    float frequency = 1f;
    public float floatHeight = 0.5f;

    // Position Storage Variables
    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();
    float timeVar;

    // Use this for initialization
    void Start()
    {
        degreesPerSecond = 5;
        amplitude = 0.1f;
        frequency = 0.1f;
        switch (weight)
        {
            case WeightClass.LIGHT:
                break;
            case WeightClass.MEDIUM:
                break;
            case WeightClass.HEAVY:
                break;
            default:
                break;
        }
        degreesPerSecond += Random.Range(-1.0f, 1.0f);
        amplitude += Random.Range(-0.05f, 0.05f);
        frequency += Random.Range(-0.05f, 0.05f);
        floatHeight += Random.Range(0, 0.1f);
        timeVar = Random.Range(0, 10);
        // Store the starting position & rotation of the object
        posOffset = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!TimeController.IsCalamity()) return;
        // Spin object around Y-Axis
        transform.Rotate(Vector3.one * Time.deltaTime * degreesPerSecond, Space.World);

        // Float up/down with a Sin()
        tempPos = posOffset + (Vector3.up * floatHeight);
        tempPos.y += Mathf.Sin((Time.fixedTime+timeVar) * Mathf.PI * frequency) * amplitude;

        transform.position = tempPos;
    }
}
