﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;


public class MoveCalamity : CalamityTrigger
{
    public Pose preCalamityPose;
    public Pose postCalamityPose;
    public override void SetCalamity(bool isCalamity)
    {
        transform.position = isCalamity ? postCalamityPose.position : preCalamityPose.position;
        transform.rotation = isCalamity ? postCalamityPose.rotation : preCalamityPose.rotation;
    }

    [Button(DirtyOnClick = true)]
    public void SavePosePRE_CALAMITY()
    {
        EditorUtils.SimpleConfirmationPrompt(
            () => preCalamityPose = new Pose(transform.position, transform.rotation));
    }

    [Button(DirtyOnClick = true)]
    public void SavePosePOST_CALAMITY()
    {
        EditorUtils.SimpleConfirmationPrompt(
            () => postCalamityPose = new Pose(transform.position, transform.rotation));
    }

    [Button(DirtyOnClick = true)]
    public void SetPosePRE_CALAMITY()
    {
        SetCalamity(false);
    }
    [Button(DirtyOnClick = true)]
    public void SetPosePOST_CALAMITY()
    {
        SetCalamity(true);
    }
}
