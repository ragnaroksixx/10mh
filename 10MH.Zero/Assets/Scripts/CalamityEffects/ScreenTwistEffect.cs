﻿using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ScreenTwistEffect : MonoBehaviour
{
    public Image screenShootImage;

    private void Awake()
    {
        screenShootImage.SetOpacity(0);
    }
    [Button]
    public void Play()
    {
        StopAllCoroutines();
        StartCoroutine(PlayRoutine());
    }

    public IEnumerator PlayRoutine()
    {
        screenShootImage.material.SetFloat("_TwistUvAmount", 0);
        screenShootImage.material.SetFloat("_DistortAmount", 0);
        screenShootImage.SetOpacity(0);
        ScreenShotUtilities.CacheScreen();

        yield return null;

        screenShootImage.sprite = ScreenShotUtilities.Last(TimeController.Instance.CurrentTime.minute);
        screenShootImage.SetOpacity(1);

        screenShootImage.material.DOFloat(2, "_TwistUvAmount", 2f);
        screenShootImage.material.DOFloat(0.5f, "_DistortAmount", 1f);

        yield return new WaitForSeconds(1);
        screenShootImage.DOFade(0, 1.25f);


    }
}
