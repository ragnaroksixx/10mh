﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PhysicsCacher
{
    public static bool enabled = false;
    public static Dictionary<AreaData, PhysicsCacher> objects = new Dictionary<AreaData, PhysicsCacher>();
    Dictionary<string, PhysicsCacheObjects> cache = new Dictionary<string, PhysicsCacheObjects>();
    Dictionary<Rigidbody, string> idDictionary = new Dictionary<Rigidbody, string>();

    public void OnAreaEnter(Area a)
    {
        idDictionary = new Dictionary<Rigidbody, string>();
        string id;
        foreach (Rigidbody item in GameObject.FindObjectsOfType<Rigidbody>(true))
        {
            if (item.gameObject.isStatic) continue;
            if (item.gameObject.scene != a.gameObject.scene) continue;

            id = item.name
                + item.transform.position.x.ToString("0.00") + "_"
                + item.transform.position.y.ToString("0.00") + "_"
                + item.transform.position.z.ToString("0.00") + "_"
                + item.transform.localEulerAngles.x.ToString("0.00") + "_"
                + item.transform.localEulerAngles.y.ToString("0.00") + "_"
                + item.transform.localEulerAngles.z.ToString("0.00") + "_"
                ;
            idDictionary.Add(item, id);
            Load(item, a);
        }
    }

    public void OnAreaExit(Area a)
    {
        foreach (Rigidbody item in GameObject.FindObjectsOfType<Rigidbody>(true))
        {
            Save(item, a);
        }
        idDictionary.Clear();
    }

    public void Init(Area a, AreaData ad)
    {
        objects.Add(ad, this);
        idDictionary = new Dictionary<Rigidbody, string>();
        string id;
        foreach (Rigidbody item in GameObject.FindObjectsOfType<Rigidbody>(true))
        {
            if (item.gameObject.isStatic) continue;
            if (item.gameObject.scene != a.gameObject.scene) continue;

            id = item.name
                + item.transform.position.x.ToString("0.00") + "_"
                + item.transform.position.y.ToString("0.00") + "_"
                + item.transform.position.z.ToString("0.00") + "_"
                + item.transform.localEulerAngles.x.ToString("0.00") + "_"
                + item.transform.localEulerAngles.y.ToString("0.00") + "_"
                + item.transform.localEulerAngles.z.ToString("0.00") + "_"
                ;
            idDictionary.Add(item, id);
            Save(item, a);
        }
    }

    public static void Clear()
    {
        objects.Clear();
    }

    public void Save(Rigidbody g, Area a)
    {
        if (!idDictionary.ContainsKey(g)) return;
        string id = idDictionary[g];
        if (cache.ContainsKey(id))
        {
            if (cache[id].HasChanged(g))
                cache[id] = new PhysicsCacheObjects(g);
        }
        else
        {
            cache.Add(id, new PhysicsCacheObjects(g));
        }

    }

    public void Load(Rigidbody g, Area a)
    {

        if (!idDictionary.ContainsKey(g)) return;

        string id = idDictionary[g];

        if (!cache.ContainsKey(id)) return;

        PhysicsCacheObjects pco = cache[id];

        if (pco.HasChanged(g))
        {
            g.transform.localPosition = pco.pos;
            g.transform.localEulerAngles = pco.rot;
        }
    }
}

struct PhysicsCacheObjects
{
    public Vector3 pos;
    public Vector3 rot;


    public PhysicsCacheObjects(Rigidbody g)
    {
        pos = g.transform.localPosition;
        rot = g.transform.localEulerAngles;
    }

    public void Update(Rigidbody g)
    {
        pos = g.transform.localPosition;
        rot = g.transform.localEulerAngles;
    }

    public bool HasChanged(Rigidbody g)
    {
        bool result = false;
        if ((pos - g.transform.localPosition).sqrMagnitude > 0.1f)
            result = true;

        if ((rot - g.transform.localEulerAngles).sqrMagnitude > 0.1f)
            result = true;

        return result;
    }
}
