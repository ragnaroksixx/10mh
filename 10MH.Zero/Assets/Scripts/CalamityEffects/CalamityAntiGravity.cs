﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CalamityAntiGravity : MonoBehaviour
{
    List<Rigidbody> rBodies = new List<Rigidbody>();
    public float minMass = 1;
    public float antiGravity = 9.8f;
    public float rotStrenth = 1;

    private IEnumerator Start()
    {
        while (Player.Instance == null)
            yield return null;
        TimeController.TriggerOrAddCalamity(OnCalamityStart);
    }
    void OnCalamityStart()
    {
        Rigidbody[] Rigidbody = FindObjectsOfType<Rigidbody>();
        foreach (Rigidbody item in Rigidbody)
        {
            if (Player.Instance != null && item.transform == Player.Instance.transform) continue;
            if (item.isKinematic) continue;
            if (item.mass <= minMass)
            {
                item.useGravity = false;
                rBodies.Add(item);
            }
        }
    }
    private void FixedUpdate()
    {
        float force = antiGravity * Mathf.Cos(Time.time);
        //force += -Physics.gravity.y;
        foreach (Rigidbody rBody in rBodies)
        {
            rBody.AddForce(Vector3.up * force, ForceMode.Force);
            rBody.AddTorque(Random.insideUnitCircle * rotStrenth, ForceMode.Acceleration);
        }
    }
}
