﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class CalamityEventTrigger : CalamityTrigger
{
    public UnityEvent onCalamity;
    public UnityEvent onPreCalamity;
    public UnityEvent<bool> onCalamityChanged;

    public override void SetCalamity(bool isCalamity)
    {
        onCalamityChanged?.Invoke(isCalamity);
        if (isCalamity)
            onCalamity?.Invoke();
        else
            onPreCalamity?.Invoke();
    }
}
