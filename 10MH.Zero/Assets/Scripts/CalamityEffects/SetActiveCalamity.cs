﻿using System.Collections;
using UnityEngine;


public class SetActiveCalamity : CalamityTrigger
{
    public bool activeInCalamity;
    public override void SetCalamity(bool isCalamity)
    {
        gameObject.SetActive(isCalamity ? activeInCalamity : !activeInCalamity);
    }
    public void ForceActive()
    {
        gameObject.SetActive(true);
    }

}
