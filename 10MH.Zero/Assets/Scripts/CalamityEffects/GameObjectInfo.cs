﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;

public class GameObjectInfo : MonoBehaviour
{

    [Button]
    public void InstanceID()
    {
        Debug.Log(gameObject.GetInstanceID());
    }

    [Button]
    public void HashCode()
    {
        Debug.Log(gameObject.GetHashCode());
    }
}
