﻿using System.Collections;
using UnityEngine;


public class CalamityObject : CalamityTrigger
{
    public BoxCollider col;
    public Transform playerSafeSpot;
    bool once = false;
    public override void SetCalamity(bool isCalamity)
    {
        transform.GetChild(0).gameObject.SetActive(isCalamity);
    }
    public override void OnCalamity()
    {
        if (playerSafeSpot != null)
            CheckPlayer();
        base.OnCalamity();
    }
    public void CheckPlayer()
    {
        if (col == null) return;
        Collider[] cols = Physics.OverlapBox(col.bounds.center, col.size / 2, col.transform.rotation);
        foreach (Collider item in cols)
        {
            if (item.transform.IsChildOf(Player.Instance.transform) || item.transform == Player.Instance.transform)
            {
                MovePlayerToSafeSpot();
                return;
            }
        }
    }
    public void MovePlayerToSafeSpot()
    {
        if (once) return;
        once = true;
        SceneTransitionEffect.waitTime = 1.0f;
        SceneTransitionEffect.skipPlayerSpawn = true;
        SceneController.LoadAreaTransition();
        Pose p = new Pose(playerSafeSpot.position, playerSafeSpot.rotation);
        Player.SetPose(p);
    }
}
