﻿using System.Collections;
using UnityEngine;


[CreateAssetMenu]
public class ObjectiveObject : ScriptableObject, IUnquieScriptableObject
{
    public string id;
    public string title;
    public Objective[] objectives;

    public string Identifier => id;

    public string[] Alias => new string[0];

    public void CompleteObjective(int i)
    {
        ObjectiveUI ui = GameObject.FindAnyObjectByType<ObjectiveUI>();
        ui?.CompleteObjective(i, objectives[i]);
    }
    public void UncompleteObjective(int i)
    {
        ObjectiveUI ui = GameObject.FindAnyObjectByType<ObjectiveUI>();
        ui?.UncompleteObjective(i, objectives[i]);
    }
    public void Init()
    {

    }

    public void ShowObjective(int i)
    {
        ObjectiveUI ui = GameObject.FindAnyObjectByType<ObjectiveUI>();
        ui?.ShowObjective(i, objectives[i]);
    }
}

[System.Serializable]
public class Objective
{
    public bool isHidden;
    public string text;
}
