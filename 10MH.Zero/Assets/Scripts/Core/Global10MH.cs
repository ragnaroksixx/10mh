using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using System;

public class Global10MH : MonoBehaviour
{
    public NHG.Dialogue.Layer dialogueLayerPrefab;
    public Color solBoundColor = Color.white;
    public static Global10MH Instance10MH;
    public static Pose cachePose = Pose.identity;
    public static bool HasCachePose;
    public Cinemachine.CinemachineBrain cameraBrain;
    public CombatEffectsLibrary combatEffects;

    MultiBoolLock showCursor = new MultiBoolLock();
    protected void Awake()
    {
        Instance10MH = this;
        CombatEffectsLibrary.Instance = combatEffects;
    }
    private void OnDestroy()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }
    public static void ShowCursor(object o)
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Instance10MH.showCursor.AddLock(o);
    }
    public static void HideCursor(object o, bool force = false)
    {
        Instance10MH.showCursor.RemoveLock(o);
        if (force)
            Instance10MH.showCursor.ClearLocks();

        if (Instance10MH.showCursor.Value) return;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    public static void SetCachePose(Vector3 position, Quaternion rotation)
    {
        SetCachePose(new Pose(position, rotation));
    }
    public static void SetCachePose(Pose p)
    {
        HasCachePose = true;
        cachePose = p;
    }
    public static void ClearCachePose()
    {
        HasCachePose = false;
        cachePose = Pose.identity;
    }
    public static List<string> ParseLines(List<string> ls)
    {
        List<string> result = new List<string>();
        foreach (string item in ls)
        {
            result.Add(ParseLine(item));
        }
        return result;
    }
    public static string ParseLine(string l)
    {
        string result = l;
        result = result.Trim();
        result = result.Replace("|", Environment.NewLine);
        result = result.Replace(openChar, '<');
        result = result.Replace(closeChar, '>');
        result = ParseLinks(result);
        result = ParseItemEvents(result);
        return result;
    }
    const char openChar = '(';
    const char closeChar = ')';
    static string ParseLinks(string l)
    {
        string result = l;
        string linkStart = "<color=#F3E497><link";
        string linkEnd = "</color></link>";
        result = result.Replace("<link", linkStart);
        result = result.Replace("</link>", linkEnd);
        return result;
    }
    static string ParseItemEvents(string l)
    {
        string result = l;
        string itemStart = "<b><color=#B8F5F8><size=125%>";
        string itemEnd = "</color></b></size>";
        result = result.Replace("<item>", itemStart);
        result = result.Replace("</item>", itemEnd);
        return result;
    }
    static string ParseHint(string l)
    {
        string result = l;
        string itemStart = "<b><color=#53CBEE><size=125%>";
        string itemEnd = "</color></b></size>";
        result = result.Replace("<hint>", itemStart);
        result = result.Replace("</hint>", itemEnd);
        return result;
    }
}

public static class HashtableExtensions
{
    public static int GetInteger(this Hashtable h, string key, int defaultValue = -1)
    {
        if (h.ContainsKey(key))
        {
            int result;
            if (int.TryParse(h[key].ToString(), out result))
            {
                return result;
            }
        }
        return defaultValue;
    }
    public static string GetString(this Hashtable h, string key, string defaultValue = "")
    {
        if (h.ContainsKey(key))
        {
            return h[key] as string;
        }
        return defaultValue;
    }
    public static ArrayList GetArrayList(this Hashtable h, string key)
    {
        if (h.ContainsKey(key))
        {
            return h[key] as ArrayList;
        }
        return new ArrayList();
    }
    public static Hashtable GetHashTable(this Hashtable h, string key)
    {
        if (h.ContainsKey(key))
        {
            return h[key] as Hashtable;
        }
        return new Hashtable();
    }
    public static Sequence DOFlash(this Image image, Color normalColor, Color flashColor, float flashRate = 0.15f, int numLoops = 2, bool autoPlay = true)
    {
        Sequence s = DOTween.Sequence();
        Tween flashTween = image.DOColor(flashColor, flashRate);
        Tween normalTween = image.DOColor(normalColor, flashRate);
        s.Append(flashTween)
            .Append(normalTween)
            .SetLoops(numLoops)
            .OnKill(() => image.color = normalColor)
            .OnComplete(() => image.color = normalColor);
        if (autoPlay)
            s.Play();
        return s;
    }
    public static Sequence DOFlash(this TMP_Text text, Color normalColor, Color flashColor, float flashRate = 0.15f, int numLoops = 2, bool autoPlay = true)
    {
        Sequence s = DOTween.Sequence();
        Tween flashTween = text.DOColor(flashColor, flashRate);
        Tween normalTween = text.DOColor(normalColor, flashRate);
        s.Append(flashTween)
            .Append(normalTween)
            .SetLoops(numLoops)
            .OnKill(() => text.color = normalColor)
            .OnComplete(() => text.color = normalColor);
        if (autoPlay)
            s.Play();
        return s;
    }

    public static Sequence DoFadeFlash(this TMP_Text text, float o1, float o2, float holdTime = 0.25f, float flashRate = 0.15f, int numLoops = 2, bool autoPlay = true)
    {
        Sequence s = DOTween.Sequence();
        Tween flashTween = text.DOFade(o2, flashRate);
        Tween normalTween = text.DOFade(o1, flashRate);
        text.alpha = o1;
        s.Append(flashTween)
            .AppendInterval(holdTime)
            .Append(normalTween)
            .AppendInterval(holdTime)
            .SetLoops(numLoops)
            .OnKill(() => text.alpha = o1)
            .OnComplete(() => text.alpha = o1);
        if (autoPlay)
            s.Play();
        return s;
    }
    public static Sequence DoFadeFlash(this CanvasGroup text, float o1, float o2, float holdTime = 0.25f, float flashRate = 0.15f, int numLoops = 2, bool autoPlay = true)
    {
        Sequence s = DOTween.Sequence();
        Tween flashTween = text.DOFade(o2, flashRate);
        Tween normalTween = text.DOFade(o1, flashRate);
        text.alpha = o1;
        s.Append(flashTween)
            .AppendInterval(holdTime)
            .Append(normalTween)
            .AppendInterval(holdTime)
            .SetLoops(numLoops)
            .OnKill(() => text.alpha = o1)
            .OnComplete(() => text.alpha = o1);
        if (autoPlay)
            s.Play();
        return s;
    }

    public static Sequence DoFadeFlash(this Image image, float o1, float o2, float holdTime = 0.25f, float flashRate = 0.15f, int numLoops = 2, bool autoPlay = true)
    {
        Sequence s = DOTween.Sequence();
        Tween flashTween = image.DOFade(o2, flashRate);
        Tween normalTween = image.DOFade(o1, flashRate);
        Color c1 = image.color;
        c1.a = o1;

        image.color = c1;
        s.Append(flashTween)
            .AppendInterval(holdTime)
            .Append(normalTween)
            .AppendInterval(holdTime)
            .SetLoops(numLoops)
            .OnKill(() => image.color = c1)
            .OnComplete(() => image.color = c1);
        if (autoPlay)
            s.Play();
        s.target = image;
        return s;
    }
}

public static class FloatExtensions
{
    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}
