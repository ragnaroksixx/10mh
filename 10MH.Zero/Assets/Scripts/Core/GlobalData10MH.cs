﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Global")]
public class GlobalData10MH : GlobalData
{
    public NarrationBoxUI narratorBoxUI;
    public GameObject quickTextPrefab;
    public GameObject textBoxPrefab;
    public GameObject worldTextPrefab;
    public CustomAudioClip targetHighlight;
    public CustomAudioClip healSFX;

    public Sprite attackIcon, defenseIcon, healIcon, energyIcon, quickAttackIcon, heavyAttackIcon,cooldownIcon;
    public Color normalColor, attackColor, blockColor, healColor, energyColor,cooldownColor;
    public static GlobalData10MH Instance;
    public ScriptableObjectDictionary<CustomAudioClip> audioDictionary;
    public ScriptableObjectDictionary<CharacterData> characterDictionary;
    public ScriptableObjectDictionary<AreaData> areaDictionary;
    public ScriptableObjectDictionary<BattleEncounter> battleDictionary;
    public ScriptableObjectDictionary<TextStyle> textStyleDictionary;
    public ScriptableObjectDictionary<AreaTransitionData> areaTransitionDictionary;
    public ScriptableObjectDictionary<InventoryPropertyData> invPropDictionary;
    public ScriptableObjectDictionary<Item> itemDictionary;
    public ScriptableObjectDictionary<InventorySet> inventorySets;
    public ScriptableObjectDictionary<ObjectiveObject> objectives;

    public GameObject defaultShieldFX;
    public AreaTransitionData[] mapAreas;

    public override void Init()
    {
        base.Init();
        Instance = this;
        audioDictionary = new ScriptableObjectDictionary<CustomAudioClip>("AudioObjects");
        characterDictionary = new ScriptableObjectDictionary<CharacterData>("characters",true);
        areaDictionary = new ScriptableObjectDictionary<AreaData>("ScriptableObjects/Area/Areas");
        battleDictionary = new ScriptableObjectDictionary<BattleEncounter>("ScriptableObjects/Encounters");
        textStyleDictionary = new ScriptableObjectDictionary<TextStyle>("ScriptableObjects/TextStyles");
        areaTransitionDictionary = new ScriptableObjectDictionary<AreaTransitionData>("ScriptableObjects/Area/TransitionData");
        invPropDictionary = new ScriptableObjectDictionary<InventoryPropertyData>("ScriptableObjects/Inventories");
        itemDictionary = new ScriptableObjectDictionary<Item>("ScriptableObjects/Items");
        inventorySets = new ScriptableObjectDictionary<InventorySet>("ScriptableObjects/InventorySets");
        objectives = new ScriptableObjectDictionary<ObjectiveObject>("ScriptableObjects/Objectives");

        audioDictionary.Init(true);
        characterDictionary.Init(true);
        areaDictionary.Init(true);
        battleDictionary.Init(true);
        textStyleDictionary.Init(true);
        areaTransitionDictionary.Init(true);
        invPropDictionary.Init(true);
        itemDictionary.Init(true);
        inventorySets.Init(true);
        objectives.Init(true);
    }
    public void ShowCursor(GameObject key)
    {
        Global10MH.ShowCursor(key as object);
    }
    public void HideCursor(GameObject key)
    {
        Global10MH.HideCursor(key as object);
    }
}
