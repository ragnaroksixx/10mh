﻿using UnityEngine;
using System.Collections;
using System.Linq;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

public class GlobalData : ScriptableObject
{
    public Sprite transparentImage;
    private static GlobalData _instance;

    protected static GlobalData instance => _instance;

    public virtual void Init()
    {
        _instance = this;
    }

    public static Sprite LoadKey(KeyCode k)
    {
        string key = k.ToString();
        if (key.Contains("Alpha"))
            key = key.Substring(key.Length - 1);
        return Resources.Load<Sprite>("KeyboardImages\\Keyboard_Black_" + key);
    }

    public static GameObject GetResource(string folder, string prefabName)
    {
        return Resources.Load<GameObject>(folder + "\\" + prefabName);
    }

    public static Vector3 SampleParabola(Vector3 start, Vector3 end, float height, float t)
    {
        if (Mathf.Abs(start.y - end.y) < 0.1f)
        {
            //start and end are roughly level, pretend they are - simpler solution with less steps
            Vector3 travelDirection = end - start;
            Vector3 result = start + t * travelDirection;
            result.y += Mathf.Sin(t * Mathf.PI) * height;
            return result;
        }
        else
        {
            //start and end are not level, gets more complicated
            Vector3 travelDirection = end - start;
            Vector3 levelDirecteion = end - new Vector3(start.x, end.y, start.z);
            Vector3 right = Vector3.Cross(travelDirection, levelDirecteion);
            Vector3 up = Vector3.Cross(right, travelDirection);
            if (end.y > start.y) up = -up;
            Vector3 result = start + t * travelDirection;
            result += (Mathf.Sin(t * Mathf.PI) * height) * up.normalized;
            return result;
        }
    }

    public static Vector2 SampleParabolaDerivative2D(Vector3 start, Vector3 end, float height, float t, float delta)
    {
        if (t - delta < 0)
            return Vector2.zero;
        Vector3 v2 = SampleParabola(start, end, height, t);
        Vector3 v1 = SampleParabola(start, end, height, t - delta);
        return v2 - v1;
    }


}

public static class Extensions
{
    public static Sequence DOFlash(this Material mat, Color normalColor, Color flashColor, float flashRate = 0.15f, int numLoops = 2, bool autoPlay = true)
    {
        Sequence s = DOTween.Sequence();
        Tween flashTween = mat.DOColor(flashColor, flashRate);
        Tween normalTween = mat.DOColor(normalColor, flashRate);
        s.Append(flashTween)
            .Append(normalTween)
            .SetLoops(numLoops)
            .OnKill(() => mat.color = normalColor)
            .OnComplete(() => mat.color = normalColor);
        if (autoPlay)
            s.Play();
        return s;
    }
    public static Sequence DOeFlash(this Material mat, Color normalColor, Color flashColor1, Color flashColor2, float flashRate = 0.15f, int numLoops = 2, bool autoPlay = true)
    {
        Sequence s = DOTween.Sequence();
        Tween flashTween = mat.DOColor(flashColor1, "_EmissionColor", flashRate);
        Tween normalTween = mat.DOColor(flashColor2, "_EmissionColor", flashRate);
        s.Append(flashTween)
            .Append(normalTween)
            .SetLoops(numLoops)
            .OnKill(() => mat.SetColor("_EmissionColor", normalColor))
            .OnComplete(() => mat.SetColor("_EmissionColor", normalColor));
        if (autoPlay)
            s.Play();
        return s;
    }
}
