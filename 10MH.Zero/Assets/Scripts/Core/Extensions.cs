﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System.Linq;

public static class Vector2Extension
{
    public static Vector2 RotateDEG(this Vector2 input, float degrees)
    {
        return input.RotateRAD(degrees * Mathf.Deg2Rad);
    }
    public static Vector2 RotateRAD(this Vector2 input, float radians)
    {
        float cos = Mathf.Cos(radians);
        float sin = Mathf.Sin(radians);
        return new Vector2(cos * input.x - sin * input.y, sin * input.x + cos * input.y);
    }
    public static Vector3 V3(this Vector2 input)
    {
        return input;
    }
}
public static class StringExtensions
{
    public static int ToInt(this string text)
    {
        if (string.IsNullOrEmpty(text))
            return 0;
        return Convert.ToInt32(text);
    }
    public static TEnum ToEnum<TEnum>(this string text, Type t)
    {
        object o = Enum.Parse(t, text);
        return (TEnum)o;
    }

    public static string[] SplitFirst(this string text, params char[] delimiters)
    {
        string[] split = text.Split(delimiters);
        string a = split[0];
        string b = text.Substring(a.Length);
        return new string[2] { a, b };
    }

    public static string Colorize(this string text, Color c)
    {
        return
            "<color=#" + ColorUtility.ToHtmlStringRGBA(c) + ">"
            + text
            + "</color>";
    }

    public static string Bold(this string text)
    {
        return "<b>" + text + "</b>";
    }

    public static string AsKey(this string text)
    {
        return text.ToLower().Replace(" ", "");
    }
}
public static class ColorExtension
{
    public static void SetOpacity(this SpriteRenderer item, float value)
    {
        Color c = item.color;
        c.a = value;
        item.color = c;
    }
    public static void SetOpacity(this Image item, float value)
    {
        Color c = item.color;
        c.a = value;
        item.color = c;
    }
    public static void SetColor(this SpriteRenderer item, Color value)
    {
        Color c = value;
        value.a = item.color.a; ;
        item.color = value;
    }
    public static void SetOpacity(this TMP_Text item, float value)
    {
        Color c = item.color;
        c.a = value;
        item.color = c;
    }
    public static void SetColor(this TMP_Text item, Color value)
    {
        Color c = value;
        value.a = item.color.a; ;
        item.color = value;
    }
}
public static class TransformDeepChildExtension
{
    //Breadth-first search
    public static Transform FindDeepChild(this Transform aParent, string aName)
    {
        return aParent.FindDeepChild<Transform>(aName);
    }

    public static T FindDeepChild<T>(this Transform aParent, string aName)
    {
        Transform result = aParent.Find(aName);
        if (result != null)
            return result.GetComponent<T>();
        foreach (Transform child in aParent)
        {
            result = child.FindDeepChild<Transform>(aName);
            if (result != null)
                return result.GetComponent<T>();
        }
        return default(T);
    }

    public static void FindDeepChildren<T>(this Transform aParent, ref List<T> list) where T : Component
    {
        foreach (T child in aParent.GetComponentsInChildren<T>(true))
        {
            if (aParent == child.transform)
                continue;
            list.Add(child);
            child.transform.FindDeepChildren<T>(ref list);
        }
    }

    public static List<T> FindDeepChildren<T>(this Transform aParent) where T : Component
    {
        List<T> list = new List<T>();
        aParent.FindDeepChildren<T>(ref list);
        return list;
    }


}
static class LevenshteinDistance
{
    /// <summary>
    /// Compute the distance between two strings.
    /// </summary>
    public static int Compute(string s, string t)
    {
        int n = s.Length;
        int m = t.Length;
        int[,] d = new int[n + 1, m + 1];

        // Step 1
        if (n == 0)
        {
            return m;
        }

        if (m == 0)
        {
            return n;
        }

        // Step 2
        for (int i = 0; i <= n; d[i, 0] = i++)
        {
        }

        for (int j = 0; j <= m; d[0, j] = j++)
        {
        }

        // Step 3
        for (int i = 1; i <= n; i++)
        {
            //Step 4
            for (int j = 1; j <= m; j++)
            {
                // Step 5
                int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

                // Step 6
                d[i, j] = Mathf.Min(
                    Mathf.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                    d[i - 1, j - 1] + cost);
            }
        }
        // Step 7
        return d[n, m];
    }
}

public static class DoTweenExtensions
{
    //public static Tween DoFloatParameter()
}

public static class EnumerableExtensions
{
    public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
    {
        if (source == null) throw new ArgumentNullException("source");
        if (action == null) throw new ArgumentNullException("action");

        foreach (T item in source)
        {
            action(item);
        }
    }
    public static T RandomElementByWeight<T>(this IEnumerable<T> sequence, Func<T, float> weightSelector, System.Random rng)
    {
        float totalWeight = sequence.Sum(weightSelector);
        // The weight we are after...
        float itemWeightIndex = (float)rng.NextDouble() * totalWeight;
        float currentWeightIndex = 0;

        foreach (var item in from weightedItem in sequence select new { Value = weightedItem, Weight = weightSelector(weightedItem) })
        {
            currentWeightIndex += item.Weight;

            // If we've hit or passed the weight we are after for this item then it's the one we want....
            if (currentWeightIndex > itemWeightIndex)
                return item.Value;

        }

        return default(T);
    }
    public static T RandomElementByWeight<T>(this Dictionary<T, float> sequence, System.Random rng)
    {
        return sequence.RandomElementByWeight(e => e.Value, rng).Key;
    }
}

public static class TransformExtensions
{
    public static void ScaleY(this Transform transform, float value)
    {
        transform.localScale = new Vector3(
            transform.localScale.x,
            value,
            transform.localScale.z
            );
    }

}

public static class ListExtensions
{
    public static void Shuffle<T>(this List<T> list, System.Random rng)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}


