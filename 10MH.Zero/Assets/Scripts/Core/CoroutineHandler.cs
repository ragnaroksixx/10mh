﻿using UnityEngine;
using System.Collections;

public class CoroutineHandler
{
    Coroutine coroutine = null;
    Coroutine coroutine2 = null;
    MonoBehaviour source;
    public delegate void OnCoroutineStop();
    OnCoroutineStop onStop = null;
    public bool IsRunning
    {
        get { return coroutine != null; }
    }

    public MonoBehaviour Source { get => source; set => source = value; }

    public CoroutineHandler(MonoBehaviour mb)
    {
        source = mb;
    }

    public Coroutine StartCoroutine(IEnumerator e, OnCoroutineStop stop = null)
    {
        if (IsRunning)
            StopCoroutine();
        coroutine = source.StartCoroutine(UpdateLoop(e));
        onStop = stop;
        return coroutine;
    }
    IEnumerator UpdateLoop(IEnumerator e)
    {
        yield return coroutine2 = source.StartCoroutine(e);
        StopCoroutine();
    }
    public void StopCoroutine()
    {
        if (coroutine != null)
        {
            source.StopCoroutine(coroutine);
        }
        if (coroutine2 != null)
        {
            source.StopCoroutine(coroutine2);
        }
        coroutine = null;
        coroutine2 = null;
        onStop?.Invoke();
    }

}