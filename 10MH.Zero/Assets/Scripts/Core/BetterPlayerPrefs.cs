﻿using System.Collections;
using UnityEngine;

public class BetterPlayerPrefs : PlayerPrefs
{
    public static void SetBool(string key, bool value)
    {
        SetInt(key, value ? 1 : 0);
    }

    public static bool GetBool(string key, bool defaultValue)
    {
        if (!HasKey(key))
            return defaultValue;
        return GetInt(key) == 1;
    }

    public static bool GetBool(string key)
    {
        if (!HasKey(key))
            return false;
        return GetInt(key) == 1;
    }
}
