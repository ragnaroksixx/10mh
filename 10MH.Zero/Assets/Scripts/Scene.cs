﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace NHG.Dialogue
{
    public class Scene : DialogueEvent, IMemorable
    {
        public CharacterData[] characters;
        public bool autoEnterCharacter { get => tags.GetBool("autoEnterCharacter"); }
        public bool IsDraft { get => !tags.GetBool("final"); }// tags.GetBool("draft"); }
        public bool insertInsteadOfReplace { get => tags.GetBool("insert"); }
        public bool isSilent { get => tags.GetBool("silent"); }
        public bool hideInv { get => tags.GetBool("hideinv"); }
        public bool hidePlayer { get => tags.GetBool("hideplayer"); }
        public string sceneName { get => tags.GetString("name", "untitled"); }
        public int time { get => tags.GetInt("time", 0); }
        public bool isPhoneScene => tags.GetBool("phone", false);
        public bool nonPlayerScene
        {
            get
            {
                return tags.GetBool("noplayer");
            }

            set
            {
                if (value)
                {
                    tags.AddTag("noplayer");
                }
                else
                    tags.RemoveTag("noplayer");
            }
        }
        [HideInInspector]
        public CharacterData overideStartingCharacter = null;
        public CustomAudioClip bgmAudio { get => tags.GetAudio("bgm"); }
        public int bgmAudioSection { get => tags.GetInt("section", -1); }

        public string memoryCategory => "scenes";
        public string memoryID => sceneName;

        [ListDrawerSettings(NumberOfItemsPerPage = 30)]
        public List<DialogueEvent> events = new List<DialogueEvent>();

        public override IEnumerator Process(Layer layer)
        {
            DialogueRenderer.interruptScene = this;
            yield return base.Process(layer);
        }
    }

    public class SceneAction : DialogueAction
    {
        public override string eventName => "scene";
        string sceneName { get => tags.GetExpressString(null); }
        bool insertInsteadOfReplace { get => tags.GetBool("insert"); }

        public override IEnumerator Process(Layer layer)
        {
            yield return base.Process(layer);
            Scene s = DialogueUtils.GetSceneFromFile(sceneName);
            if (insertInsteadOfReplace)
                s.Tags.AddTag("insert");
            else
                s.Tags.RemoveTag("insert");
            yield return s.Process(layer);
        }
    }
    public class EndAction : DialogueAction
    {
        public override string eventName => "end";
        public override string JumpIndex => "end";
        public bool End()
        {
            return true;
        }
    }

    public class RepeatAction : DialogueAction
    {
        public override string eventName => "repeat";
        public override string JumpIndex => "repeat";
    }
}
