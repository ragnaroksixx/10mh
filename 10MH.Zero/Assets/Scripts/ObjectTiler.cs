using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectTiler : MonoBehaviour
{
    public float distance, minDelta,maxDelta;
    public enum AXIS
    {
        forward,
        back,
        right,
        left,
        up,
        down
    }
    public AXIS axis;
    public Vector3 Axis
    {
        get
        {
            switch (axis)
            {
                case AXIS.forward:
                    return transform.forward;
                case AXIS.back:
                    return -transform.forward;
                case AXIS.right:
                    return transform.right;
                case AXIS.left:
                    return -transform.right;
                case AXIS.up:
                    return transform.up;
                case AXIS.down:
                    return -transform.up;
                default:
                    return Vector3.zero;
            }
        }
    }
    [Button]
    void Tile()
    {
        Vector3 pos = Vector3.zero;
        foreach (Transform item in transform)
        {
            item.localPosition = pos;
            pos += (Axis * (distance + Random.Range(-minDelta, maxDelta)));
        }
    }
}
