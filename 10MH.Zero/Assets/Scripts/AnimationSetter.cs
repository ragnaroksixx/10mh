﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using UnityEngine.Playables;

public class AnimationSetter : MonoBehaviour
{
    Animator anim;
    PlayableGraph playableGraph;
    public Animator Animator => anim ?? (anim = GetComponent<Animator>());
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    [Button]
    public void SetAnimation(string name)
    {
        Animator.Play(name);
    }

    [Button]
    public void PlayClip(AnimationClip clip)
    {
        //playableGraph.Stop();
        AnimationPlayableUtilities.PlayClip(Animator, clip, out playableGraph);
    }
    void OnDisable()

    {

        // Destroys all Playables and Outputs created by the graph.

        playableGraph.Destroy();

    }
}
