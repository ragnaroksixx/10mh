﻿using NHG.Dialogue;
using System.Collections.Generic;
using UnityEngine;

public static class PathManager
{
    static AreaPath path;
    static Dictionary<string, Scene> scenes;
    public static AreaPath Path { get => path; }

    public static void SetPath(AreaPath a)
    {
        if (a == null)
        {
            path = null;
            scenes = null;
            return;
        }
        path = Object.Instantiate(a);
        scenes = DialogueUtils.GetScenesFromTextAsset(path.wrongWayScenes);
    }

    public static bool CanUse(AreaTransitionData a, out Scene scene)
    {
        scene = null;
        if (path == null)
            return true;
        if (a.GetArea() == path.destinationArea)
            return true;
        if (path.validAreaTransitions.Contains(a))
        {
            return true;
        }
        else
        {
            scene = scenes[path.GetScenName(a).ToLower()];
            return false;
        }
    }
    public static TextAsset GetEntryScene(AreaData a)
    {
        if (path == null)
            return null;

        AreaSceneLink asl = path.GetEntryScene(a);
        if (asl != null)
        {
            path.RemoveEntryScene(asl);
            return asl.scene;
        }
        return null;

    }
}

