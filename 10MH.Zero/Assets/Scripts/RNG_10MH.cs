﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class RNG_10MH
{
    public static string gameSeed;
    public static Dictionary<string, Random> activeRandoms = new Dictionary<string, Random>();

    public static Random RegisterRandom(string seedKey)
    {
        string fullKey = gameSeed + seedKey;
        Random r = new Random(fullKey.GetHashCode());
        activeRandoms.Add(seedKey, r);
        return r;
    }
    public static Random DeregisterRandom(string seedKey)
    {
        if (!activeRandoms.ContainsKey(seedKey))
            return null;
        Random r = activeRandoms[seedKey];
        activeRandoms.Remove(seedKey);
        return r;
    }
    public static Random GetRandom(string seedKey)
    {
        return activeRandoms[seedKey];
    }
    //inclusive max
    public static int Range(string key, int min, int max)
    {
        Random rand = activeRandoms[key];
        int result = rand.Next(min, max + 1);
        return result;
    }
    public static float Range(string key, float min, float max)
    {
        Random rand = activeRandoms[key];
        float result = (float)(rand.NextDouble() * (max - min));
        result += min;
        return result;
    }
    public static UnityEngine.Vector2 InsideUnitCircle(string key, float range)
    {
        Random rand = activeRandoms[key];
        float x = Range(key, 0, range);
        float y = Range(key, 0, range);
        return new UnityEngine.Vector2(x, y);
    }
    public static UnityEngine.Vector2 OutsideUnitCircle(string key, float range)
    {
        Random rand = activeRandoms[key];
        float angle = Range(key, 0, 360);
        float x = UnityEngine.Mathf.Cos(angle);
        float y = UnityEngine.Mathf.Sin(angle);
        return new UnityEngine.Vector2(x, y) * range;
    }
}

