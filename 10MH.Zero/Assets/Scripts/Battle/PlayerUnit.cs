﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;
using System;

[System.Serializable]
public class PlayerUnit : BattleUnit
{
    public int maxAP = 3;
    public int currentAP = 3;
    float stamina = 100;
    float maxStamina = 100;
    float regenRate = 20;
    float regenLockTime;
    public bool isBlocking = false;
    public bool isParrying = false;
    public InventoryItemUI lastUsedItem;
    public InventoryItemUI blockingItem;
    public Action OnParry;
    //public override int Block
    //{
    //    get
    //    {
    //        if (isBlocking && stamina > 0)
    //        {
    //            if (isParrying)
    //                return 999;
    //            return Mathf.Max(data.stats.DEF, base.Block);
    //        }
    //        else
    //        {
    //            return 0;
    //        }
    //    }

    //    set => base.Block = value;
    //}
    public float Stamina { get => stamina; }
    public float MaxStamina { get => maxStamina; }

    public PlayerBattleEvents BattleEvents => battleEvents as PlayerBattleEvents;
    public override void Init(CharacterData c)
    {
        base.Init(c);
        hp = maxHp = c.MaxHP;
        maxAP = data.stats.AP;
        currentAP = maxAP;
        battleEvents = new PlayerBattleEvents();
        blockingItem = null;

    }
    public override void StartBattle(BattleUnitController buc, BattleUnitUI ui)
    {
        base.StartBattle(buc, ui);
        PlayerInfoMenu.Instance.ChangeSprite("normal");
    }
    public override void Die()
    {
    }

    public override IEnumerator OnTurnStart()
    {
        yield return base.OnTurnStart();
        lastUsedItem = null;
    }
    public override IEnumerator DetermineActions()
    {
        currentAP = maxAP;
        yield return base.DetermineActions();
        CombatInteractionAdapter.ClearQueue();
    }
    public IEnumerator DoAction(ActionItem action)
    {
        string qwip = action.CombatItem.GetQwip();
        if (!string.IsNullOrEmpty(qwip))
        {
            yield return PlayerInfoMenu.Instance.Say(qwip);
        }
        if (action.target == this)
        {
            yield return new WaitForSeconds(0.4f);
            CombatInteractionAdapter.PerformAction(action);
        }
        else
        {
            yield return BattleController.Instance.battleUI.Focus(action.target);
            unitController.OnPerformAction(action);
            yield return new WaitForSeconds(0.5f);

            CombatInteractionAdapter.PerformAction(action);

            yield return new WaitForSeconds(0.5f);
            yield return BattleController.Instance.battleUI
                .UnFocusDelay(action.target);

            yield return new WaitForSeconds(0.5f);
        }
    }
    public override void TakeDamage(int amount, BattleUnit source)
    {
        base.TakeDamage(amount, source);
        if (Block > 0)
        {
            //lastUsedItem.LoseDurability();
            //if (lastUsedItem.IsBroken)
            //{
            //    amount = 0;
            //    LoseBlock(Block);
            //}
            float atb = (BattleController.Instance.brains as ATBCombatBrain)
                .DecrementItem(blockingItem.ItemData, 1.0f);
            if (atb <= 0.9f)
            {
                RemoveAllBlock();
                blockingItem.SetIsBlock(false);
                blockingItem = null;
            }
        }

    }
    public override void ResolveHPLoss(int amount)
    {
        if (EscortSystem.IsEscorting)
        {
            amount = Mathf.CeilToInt(amount / 2.0f);
            EscortSystem.battleData.ResolveHPLoss(amount);
        }
        base.ResolveHPLoss(amount);
        if (isBlocking && isParrying)
        {
            OnParry.Invoke();
            HitStop.Pause(0.2f);
        }
    }

    public bool HasStamina(float amount)
    {
        return stamina >= amount;
    }
    public bool UseStamina(float amount)
    {
        stamina -= amount;
        if (stamina <= 0)
        {
            stamina = 0;
            return true;
        }
        return false;
    }
    public void RestoreStamina(float amount)
    {
        stamina += amount;
        if (stamina > maxStamina)
            stamina = maxStamina;
    }
    public void LockStaminaRegen(float duration)
    {
        float time = Time.time + duration;
        regenLockTime = Mathf.Max(time, regenLockTime);
    }
    public void RegenStamina()
    {
        if (Time.time > regenLockTime && stamina < maxStamina)
        {
            RestoreStamina(Time.deltaTime * regenRate);
        }
    }

}
