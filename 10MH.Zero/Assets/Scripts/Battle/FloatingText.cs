﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class FloatingText : MonoBehaviour
{
    public TMP_Text text;
    public enum TextType
    {
        DAMAGE,
        HEAL,
        BLOCK,
        TIME,
        ADV,
        BREAK
    }
    public void Play(int number, Color color, Vector3 pos, TextType type)
    {
        Play(number.ToString(), color, pos, type);
    }
    public void Play(string number, Color color, Vector3 pos, TextType type)
    {
        color.a = 0.5f;
        text.color = color;
        text.text = number;
        float fadeInTime = 0.25f;
        float fadeOutTime = 0.25f;
        transform.position = pos;
        switch (type)
        {
            case TextType.DAMAGE:
                transform.localScale = Vector3.one * 0.5f;
                transform.DOScale(Vector3.one * 1.2f, 0.5f);
                transform.DOJump(transform.position, 50, 1, 0.5f);
                break;
            case TextType.HEAL:
                transform.localScale = Vector3.one * 0.5f;
                transform.DOScale(Vector3.one * 1.2f, 0.5f);
                transform.DOMoveY(100, 0.75f).SetRelative(true);
                break;
            case TextType.BLOCK:
                transform.localScale = Vector3.one * 0.5f;
                transform.DOScale(Vector3.one * 0.8f, 0.5f);
                transform.DOJump(transform.position, 50, 1, 0.5f);
                fadeOutTime = 1.0f;
                break;
            case TextType.BREAK:
            case TextType.TIME:
                transform.localScale = Vector3.one * 0.5f;
                transform.DOMoveY(25, 0.75f).SetRelative(true);
                fadeOutTime = 1.5f;
                break;
            case TextType.ADV:
                transform.localScale = Vector3.one * 0.25f;
                transform.DOScale(Vector3.one * 0.5f, 0.5f);
                transform.DOMoveY(25, 0.75f).SetRelative(true);
                fadeOutTime = 1.0f;
                break;
            default:
                break;
        }

        Sequence fadeSequence = DOTween.Sequence();
        fadeSequence.Append(text.DOFade(1, fadeInTime));
        fadeSequence.AppendInterval(0.5f);
        fadeSequence.Append(text.DOFade(0, fadeOutTime));
        fadeSequence.OnComplete(OnComplete);
        fadeSequence.Play();
    }
    void OnComplete()
    {
        FloatingTextController.Stop(this);
    }
}
