﻿using DG.Tweening;
using NHG.Dialogue;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BattleMenu : MonoBehaviour
{
    public Image screenShootImage;
    public TMP_Text titleText;
    public Color backgroundColor = Color.grey;

    public static Dictionary<int, string[]> positionOrders = new Dictionary<int, string[]>()
        {
            { 0, new string[] { "d" } },
            { 1, new string[] { "d" } },
            { 2, new string[] { "c","e" } },
            { 3, new string[] { "b","c#","e" } },
            { 4, new string[] { "a#", "c" ,"e","e#" } }, //a# b(3/4) d(1/4) e#
            { 5, new string[] { "a", "b","c#","e","f" } },
        };

    private void NotAwake()
    {
        screenShootImage.material.SetFloat("_TwistUvAmount", 0);
        screenShootImage.material.SetFloat("_DistortAmount", 0);
        screenShootImage.sprite =
            BattleController.screenshot;
    }
    private void NotStart()
    {
        screenShootImage.material.DOFloat(2, "_TwistUvAmount", 2f);
        screenShootImage.material.DOFloat(0.5f, "_DistortAmount", 1f);
    }

    private void Start()
    {
        gameObject.SetActive(false);
    }
    public void StartBattle(BattleEncounter e)
    {
        titleText.DOText(e.encounterName, 0.5f, scrambleMode: ScrambleMode.All);
        InventoryManager.Instance.OpenInventory();

    }

    public void EndBattle()
    {
        if (CycleManager.PlayerCombatUnit.hp > 0)
            InventoryManager.Instance.CloseInventory();
    }

    public IEnumerator Focus(BattleUnit unit)
    {
        foreach (BattleUnit item in BattleController.Instance.Enemies)
        {
            if (item == unit)
            {
                Foreground(item);
            }
            else
            {
                BackGround(item);
            }
        }
        yield return new WaitForSeconds(transTime);
    }
    public IEnumerator UnFocus()
    {
        foreach (BattleUnit item in BattleController.Instance.Enemies)
        {
            StandardFocus(item);
        }
        yield return new WaitForSeconds(transTime);
    }
    public IEnumerator UnFocusDelay(BattleUnit unit)
    {
        foreach (BattleUnit item in BattleController.Instance.Enemies)
        {
            if (item != unit)
                StandardFocus(item);
        }
        yield return new WaitForSeconds(transTime / 2);
        if (unit.hp > 0)
            StandardFocus(unit);

    }
    float transTime = 0.25f;
    public void BackGround(BattleUnit unit)
    {
        //SetScale to 0.9f
        //unit.ui.SetScale(0.85f, transTime);
        //Set Color grey
        unit.unitController.UI.SetColor(backgroundColor, transTime);
    }
    public void Foreground(BattleUnit unit)
    {
        //Move to center
        //DialogueRenderer.Instance.GetCharacterRenderer(unit.data)
        //     .MoveCharacter(unit.data, ConvertToXPos("c#"));

        //Set Scale to 1.25
        //unit.unitController.UI.SetScale(1.075f, transTime);
        //Set as First Sibiling
        unit.unitController.UI.transform.parent.SetAsLastSibling();
        //Set Color White
        unit.unitController.UI.SetColor(Color.white, transTime);
    }
    public void StandardFocus(BattleUnit unit)
    {
        //Set Color White
        unit.unitController.UI.SetColor(Color.white, transTime);
        //SetScale to 1.0f
        //unit.unitController.UI.SetScale(1.0f, transTime);

        CharacterEffectData eData = new CharacterEffectData()
        {
            character = unit.data,
            xPos = GetXPos(unit),
        };

        DialogueRenderer.Instance.MoveCharacter(eData);
    }
    public float GetXPos(BattleUnit unit, int overrideNumEnemies = -1)
    {
        int numEnemeies = overrideNumEnemies != -1 ? overrideNumEnemies : BattleController.Instance.Enemies.Count;
        int index = BattleController.Instance.Enemies.IndexOf(unit as EnemyUnit);
        if (index == -1)
            return GetNextXPos(overrideNumEnemies);
        return ConvertToXPos(positionOrders[numEnemeies][index]);
    }
    public float GetNextXPos(int overrideNumEnemies = -1)
    {
        int numEnemeies = overrideNumEnemies != -1 ? overrideNumEnemies : BattleController.Instance.Enemies.Count;
        int index = BattleController.Instance.Enemies.Count;
        return ConvertToXPos(positionOrders[numEnemeies][index]);
    }
    public float ConvertToXPos(string pos)
    {
        return WorldElement.CalcX(pos);
    }
}

