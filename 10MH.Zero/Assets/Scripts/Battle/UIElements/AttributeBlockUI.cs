﻿using System.Collections;
using TMPro;
using UnityEngine;


public class AttributeBlockUI : MonoBehaviour
{
    public TMP_Text header, body, numeric;
    public bool isCollapsed = true, isCollapsable = false;
    private void Awake()
    {
        SetCollapse(isCollapsed);
    }
    public void SetCollapse(bool collapse)
    {
        isCollapsed = collapse;
        body.gameObject.SetActive(!collapse);
    }
    public void Set(IAttributeInfo aInfo)
    {
        header.text = aInfo.Header;
        body.text = aInfo.Body;
        numeric.text = aInfo.Numeric > 0 ? aInfo.Numeric.ToString() : "";
    }
    public void Set(string header, string body, int numeric)
    {
        Set(new SimpleAttributeInfo(header, body, numeric));
    }
    public void OnClick()
    {
        if (isCollapsable)
        {
            SetCollapse(!isCollapsed);
        }
    }
}
public struct SimpleAttributeInfo : IAttributeInfo
{
    public string header, body;
    public int numeric;

    public SimpleAttributeInfo(string header, string body, int numeric)
    {
        this.header = header;
        this.body = body;
        this.numeric = numeric;
    }

    public string Header => header;

    public string Body => body;

    public int Numeric => numeric;
}
public interface IAttributeInfo
{
    public string Header { get; }
    public string Body { get; }
    public int Numeric { get; }
}
