﻿using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;


public class AttributesViewer : MonoBehaviour
{
    public AttributeBlockUI hp, currentAction, skill;
    public CanvasGroup cGroup;
    public RectTransform attributeListRoot;
    bool isVisible;
    EnemyUnitUI current;

    public float width;
    private void Awake()
    {
        cGroup.alpha = 0;
        isVisible = false;
        cGroup.blocksRaycasts = cGroup.interactable = false;
        current = null;
        Publisher.Subscribe<ItemPickupEvent>(OnItemPickup);
    }
    private void OnDestroy()
    {
        Publisher.Unsubscribe<ItemPickupEvent>(OnItemPickup);
    }

    private void OnItemPickup(ItemPickupEvent e)
    {
        if (isVisible)
            Hide();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (!isVisible && CursorController.Instance.IsInUse)
                return;

            EnemyUnitUI eUI = CursorController.Instance.Raycast<EnemyUnitUI>(true);
            if (!isVisible)
            {
                if (eUI)
                    Show(eUI);
            }
            else
            {
                if (eUI && eUI != current)
                    Show(eUI);
                else
                    Hide();
            }
        }
    }
    public void Show(EnemyUnitUI enemy)
    {
        float charPos = (enemy.transform.parent as RectTransform).anchoredPosition.x;

        Vector2 anchoredPos = Vector2.zero;

        anchoredPos.x = (Mathf.Abs(charPos) + 70 + (width / 2) + UnityEngine.Random.Range(0, 40)) * Mathf.Sign(charPos);
        anchoredPos.y = attributeListRoot.anchoredPosition.y;

        attributeListRoot.anchoredPosition = anchoredPos;
        hp.Set("Health", enemy.Unit.hp + "/" + enemy.Unit.maxHp, enemy.Unit.hp);
        currentAction.Set((enemy.Unit as EnemyUnit).turnAction);

        cGroup.DOKill();
        cGroup.DOFade(1, 0.15f);
        isVisible = true;
        cGroup.blocksRaycasts = cGroup.interactable = true;
        current = enemy;
    }

    public void Hide()
    {
        cGroup.DOKill();
        cGroup.DOFade(0, 0.15f);
        isVisible = false;
        cGroup.blocksRaycasts = cGroup.interactable = false;
        current = null;
    }
}
