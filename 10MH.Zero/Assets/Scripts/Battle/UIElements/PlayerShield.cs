﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PlayerShield : MonoBehaviour
{
    public RectTransform shieldImage;
    public RectTransform shieldTextRoot;
    public TMP_Text shieldText;
    public Image shieldFillImage;

    bool isBlocking = false;
    KeyCode shieldInput = KeyCode.Mouse1;
    KeyCode parryInput = KeyCode.Mouse0;    

    CoroutineHandler parryRoutine;

    public float parryCooldown;
    // Use this for initialization
    void Start()
    {
        shieldImage.gameObject.SetActive(false);
        parryRoutine = new CoroutineHandler(this);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(shieldInput))
        {
            if (CycleManager.PlayerCombatUnit.HasStamina(6))
            {
                CycleManager.PlayerCombatUnit.UseStamina(5);
                StartShield();
            }
        }
        else if (isBlocking && Input.GetKey(shieldInput))
        {
            //if (!CycleManager.PlayerCombatUnit.isParrying && Input.GetKeyDown(parryInput))
            //{
            //    CycleManager.PlayerCombatUnit.UseStamina(5);
            //    parryRoutine.StartCoroutine(Parry());
            //}
            CycleManager.PlayerCombatUnit.UseStamina(Time.deltaTime * 5);
            CycleManager.PlayerCombatUnit.LockStaminaRegen(1);
            if (CycleManager.PlayerCombatUnit.Stamina <= 0)
            {
                StopShield();
                CycleManager.PlayerCombatUnit.LockStaminaRegen(2);
            }
        }
        else if (isBlocking && Input.GetKeyUp(shieldInput))
        {
            StopShield();
        }
    }

    private void StopShield()
    {
        CycleManager.PlayerCombatUnit.isBlocking = false;
        isBlocking = false;
        shieldImage.gameObject.SetActive(false);
        EndParry();
    }

    private void StartShield()
    {
        CycleManager.PlayerCombatUnit.OnParry -= OnSuccesfullParry;
        CycleManager.PlayerCombatUnit.OnParry += OnSuccesfullParry;
        CycleManager.PlayerCombatUnit.isBlocking = true;
        isBlocking = true;
        shieldImage.gameObject.SetActive(true);
        parryRoutine.StartCoroutine(Parry());
    }

    private IEnumerator Parry()
    {
        CycleManager.PlayerCombatUnit.isParrying = true;
        //shieldImage.localScale = Vector3.one * 1.25f;
        shieldImage.DOScale(1.25f, 0.125f);
        yield return new WaitForSeconds(0.25f);
        EndParry();
    }

    private void EndParry()
    {
        parryRoutine.StopCoroutine();
        CycleManager.PlayerCombatUnit.isParrying = false;
        shieldImage.DOKill();
        shieldImage.localScale = Vector3.one;
    }

    private void OnSuccesfullParry()
    {
        StopShield();
        CycleManager.PlayerCombatUnit.LockStaminaRegen(2);
    }
}
