﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class UnitTimeUI : MonoBehaviour
{
    public RectTransform graphicsRoot;
    public Image rageIcon;
    TimeAction action;
    TimelineUI timeline;
    TimelineBrain vBrains;

    public virtual void Init(TimeAction u, TimelineUI tl, TimelineBrain brain)
    {
        vBrains = brain;
        timeline = tl;
        action = u;
        gameObject.SetActive(true);
        (transform as RectTransform).anchoredPosition = new Vector2(0, 0);
    }

    // Update is called once per frame
    public void Update()
    {
        float ratio = action.CurrentTime / action.CastTime;
        float x = ratio * timeline.waitBar.sizeDelta.x;
        (transform as RectTransform).anchoredPosition = new Vector2(x, 0);
    }
    public IEnumerator StunEffect()
    {
        float duration = 0.1f;
        Sequence s = DOTween.Sequence();
        Tween left = graphicsRoot.DOAnchorPosX(-4, duration);
        Tween right = graphicsRoot.DOAnchorPosX(4, duration);
        Tween center = graphicsRoot.DOAnchorPosX(0, duration);
        s.Append(left)
            .Append(right)
            .SetLoops(3)
            .OnKill(() => { graphicsRoot.anchoredPosition = new Vector2(0, 0); })
            .OnComplete(() => { graphicsRoot.anchoredPosition = new Vector2(0, 0); });

        s.Play();
        yield return new WaitForSeconds(duration * 6);
    }
    public void SetRageIcon(bool value)
    {
        rageIcon.DOFade(value ? 1 : 0, 0.25f);
    }
}
