﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TugOfWarApIcon : MonoBehaviour
{
    [HideInInspector]
    public Image image;
    Sequence s;
    public enum State
    {
        UNUSED,
        PREVIEW,
        USED
    }
    private State currentState;
    private void Awake()
    {
        image = GetComponentInChildren<Image>();
    }
    public void SetState(State e)
    {
        if (currentState == e)
            return;
        currentState = e;
        switch (currentState)
        {
            case State.UNUSED:
                SetUnused();
                break;
            case State.PREVIEW:
                SetPreviewUse();
                break;
            case State.USED:
                SetUsed();
                break;
            default:
                break;
        }
    }

    public void SetUsed()
    {
        KillTweens();

        transform.DOScale(0.75f, 0.25f);
        image.DOFade(0.25f, 0.25f);
    }

    public void SetPreviewUse()
    {
        KillTweens();

        transform.DOScale(1, 0.25f);
        s = image.DoFadeFlash(1.0f, 0.25f, 0.1f, 0.15f, -1);
    }
    public void SetUnused()
    {
        KillTweens();

        transform.DOScale(1, 0.25f);
        image.DOFade(1.0f, 0.25f);
    }

    void KillTweens()
    {
        transform.DOKill();
        image.DOKill();
        s?.Kill();
        s = null;
    }
}
