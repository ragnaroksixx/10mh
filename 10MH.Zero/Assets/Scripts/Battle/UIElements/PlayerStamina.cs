﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStamina : MonoBehaviour
{
    public Image staminiaImage, staminaAfterImageImage;


    public void OnStaminaLost(float amountLost)
    {        
        //staminaAfterImageImage.fillAmount = (CycleManager.PlayerCombatUnit.Stamina + amountLost) / CycleManager.PlayerCombatUnit.MaxStamina;
    }

    private void Update()
    {
        staminiaImage.fillAmount = CycleManager.PlayerCombatUnit.Stamina / CycleManager.PlayerCombatUnit.MaxStamina;
        staminaAfterImageImage.fillAmount = Mathf.Lerp(staminaAfterImageImage.fillAmount, staminiaImage.fillAmount, 3 * Time.deltaTime);
    }
}
