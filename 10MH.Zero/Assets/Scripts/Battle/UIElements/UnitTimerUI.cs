﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UnitTimerUI : MonoBehaviour
{
    public Image timerUI, characterIcon;
    public Color normalColor, attackColor, defenseColor;
    TimeAction action;
    Gradient colorGradient;

    public virtual void Init(TimeAction u, Transform battleTimerRoot)
    {
        action = u;
        gameObject.SetActive(true);
        //float y = (brain.battleTimerRoot.transform as RectTransform).anchoredPosition.y;
        //(transform as RectTransform).anchoredPosition = new Vector2(0, y);

        float y = battleTimerRoot.position.y;
        float x = transform.position.x;
        transform.position = new Vector3(x, y, 0);
        action.Unit.OnDetermineAction += OnDetermineAction;

        colorGradient = new Gradient();

        GradientColorKey first = new GradientColorKey(normalColor, 0);
        GradientColorKey second = new GradientColorKey(normalColor, 0.75f);
        colorGradient.colorKeys = new GradientColorKey[] { first, second };
        colorGradient.alphaKeys = new GradientAlphaKey[] { new GradientAlphaKey(1, 0) };
    }
    private void OnDestroy()
    {
        action.Unit.OnDetermineAction -= OnDetermineAction;
    }
    // Update is called once per frame
    public void Update()
    {
        float ratio = action.CurrentTime / action.CastTime;
        //float x = ratio * timeline.waitBar.sizeDelta.x;
        timerUI.fillAmount = 1 - ratio;

        timerUI.color = characterIcon.color = colorGradient.Evaluate(ratio);
    }

    void OnDetermineAction()
    {
        Color end;
        if ((action.Unit as EnemyUnit).turnAction is AttackAction)
        {
            end = attackColor;
        }
        else
        {
            end = defenseColor;
        }

        GradientColorKey first = new GradientColorKey(normalColor, 0);
        GradientColorKey second = new GradientColorKey(end, 0.75f);
        colorGradient.colorKeys = new GradientColorKey[] { first, second };
    }
}
