﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class APTugOfWarBar : MonoBehaviour
{
    public GameObject allyAPRoot, enemyAPRoot;
    List<TugOfWarApIcon> allyAP, enemyAP;
    // Use this for initialization
    private void Awake()
    {
        allyAP = new List<TugOfWarApIcon>(allyAPRoot.GetComponentsInChildren<TugOfWarApIcon>());
        enemyAP = new List<TugOfWarApIcon>(enemyAPRoot.GetComponentsInChildren<TugOfWarApIcon>());
    }
    private void Start()
    {
        BattleController.Instance.OnEncounterStart += OnEncounterStart;
        BattleController.Instance.OnTurnStart += OnTurnStart;
        Publisher.Subscribe<ItemPickupEvent>(OnItemPickUpChanged);
        //OnItemPickup
        //OnItemDrop
        //OnTurnStart
    }
    private void OnDestroy()
    {

        BattleController.Instance.OnEncounterStart -= OnEncounterStart;
        BattleController.Instance.OnTurnStart -= OnTurnStart;
        Publisher.Unsubscribe<ItemPickupEvent>(OnItemPickUpChanged);
        //OnItemPickup
        //OnItemDrop
        //OnTurnStart

    }
    private void OnTurnStart()
    {
        UpdatePlayerAP(0);
        UpdateEnemyAP();
    }

    private void OnEncounterStart()
    {
        for (int i = 0; i < allyAP.Count; i++)
        {
            allyAP[i].gameObject.SetActive(i < CycleManager.PlayerCombatUnit.maxAP);
        }

        int enemyMaxAP = BattleController.Instance.encounter.units.Count;

        for (int i = 0; i < enemyAP.Count; i++)
        {
            enemyAP[i].gameObject.SetActive(i < enemyMaxAP);
        }
    }

    public void UpdatePlayerAP(int preview)
    {
        Color c = allyAP[0].image.color;
        int usedAP = CycleManager.PlayerCombatUnit.maxAP - CycleManager.PlayerCombatUnit.currentAP;
        int previewAP = usedAP + preview;
        for (int i = 0; i < allyAP.Count; i++)
        {
            allyAP[i].gameObject.SetActive(i < CycleManager.PlayerCombatUnit.maxAP);

            if (i >= CycleManager.PlayerCombatUnit.maxAP)
                continue;
            else if (i < usedAP)
                allyAP[i].SetState(TugOfWarApIcon.State.USED);
            else if (i < previewAP)
                allyAP[i].SetState(TugOfWarApIcon.State.PREVIEW);
            else
                allyAP[i].SetState(TugOfWarApIcon.State.UNUSED);
        }
    }
    private void UpdateEnemyAP()
    {
        int enemyMaxAP = BattleController.Instance.Enemies.Count;

        for (int i = 0; i < enemyAP.Count; i++)
        {
            enemyAP[i].gameObject.SetActive(i < enemyMaxAP);
        }
    }
    private void OnItemPickUpChanged(ItemPickupEvent e)
    {
        UpdatePlayerAP(e.itemUI == null ? 0 : e.itemUI.Item.AP);
    }
}
