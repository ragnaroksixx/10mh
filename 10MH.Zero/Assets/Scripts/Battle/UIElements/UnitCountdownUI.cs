﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class UnitCountdownUI : MonoBehaviour
{
    public TMP_Text countdownText;
    public Image countdownImage;
    public RectTransform root;
    public Color normalColor, closeColor;
    private void Awake()
    {
        Hide();
    }
    public void SetCountdown(int countdown, bool animate = true)
    {
        if (animate)
        {
            Tweener t = root.DOPunchScale(Vector3.one * 0.5f, 0.25f);
            if(countdown==0)
                t.OnComplete(Hide);
        }
        else
        {
            if (countdown == 0)
                Hide();
        }
        countdownImage.color = countdown <= 1 ? closeColor : normalColor;
        countdownText.text = countdown.ToString();

    }

    public void Hide()
    {
        root.gameObject.SetActive(false);
        countdownText.text = "";
    }

    public void Show(int value)
    {
        SetCountdown(value, false);
        root.gameObject.SetActive(true);
    }

    public void InitPosition(EnemyCountdownHandler ech, CooldownBrain brain)
    {
        gameObject.SetActive(true);
        ech.ui = this;

        float y = brain.battleTimerRoot.transform.position.y;
        float x = transform.position.x;
        transform.position = new Vector3(x, y, 0);
    }
}
