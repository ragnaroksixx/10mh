﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EndTurnButton : MonoBehaviour
{
    public TMP_Text text;
    public TurnBasedBrain battleBrain;
    public Button button;
    // Use this for initialization
    void Start()
    {
        gameObject.SetActive(false);
        battleBrain.OnStateChangedEvent += OnStateChaged;
        button.onClick.AddListener(OnClick);
    }

    void UpdateText()
    {
        text.text = CombatInteractionAdapter.HasAnyQueuedActions() ? "END TURN" : "PASS";
    }

    void OnClick()
    {
        battleBrain.EndPlayerTurn();
    }

    void OnStateChaged(BattleState bs)
    {
        UpdateText();
        gameObject.SetActive(bs == BattleState.PLAYER);
    }
}
