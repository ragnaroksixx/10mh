﻿using System.Collections;
using UnityEngine;


[CreateAssetMenu()]
public class CombatEffect : ScriptableObject
{
    public Sprite image;

    public virtual void OnAdd(BattleUnit unit) { }
    public virtual void OnRemove(BattleUnit unit) { }

    public virtual void OnTurnStart(BattleUnit unit) { }
    public virtual void OnTurnEnd(BattleUnit unit) { }
    public virtual void OnActionPerformed(BattleUnit unit) { }
    public virtual void OnDamageReceived(BattleUnit unit) { }
    public virtual void OnBattleEnd(BattleUnit unit) { }

    public int StackCount(BattleUnit unit)
    {
        return unit.statusFXs[this];
    }

}
