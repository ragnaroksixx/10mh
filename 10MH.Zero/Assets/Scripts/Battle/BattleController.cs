﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using NHG.Dialogue;
using Sirenix.OdinInspector;
using DG.Tweening;
using UnityEngine.Events;

public class BattleController : MonoBehaviour
{
    //public BattleUnitUI enemyPrefab;
    public BattleEncounter encounter = null;
    public PlayerUnit player = null;
    private int turn = 0;
    List<EnemyUnit> enemies;
    public static BattleController Instance;
    public static bool IsInBattle { get => Instance && Instance.encounter != null; }
    public static PlayerUnit Player { get => Instance.player; }
    public bool IsBattleStarted { get; private set; }
    public List<EnemyUnit> Enemies { get => enemies; }

    public static Sprite screenshot = null;
    public BattleUnitController playerCont;
    public PlayerBattleUI playerUI;

    [HideInInspector]
    public UnityAction<EnemyUnit> OnEnemySummon;

    [HideInInspector]
    public UnityAction OnEncounterStart;
    [HideInInspector]
    public UnityAction OnTurnStart;
    [HideInInspector]
    public UnityAction PrePlayerActionStart;
    [HideInInspector]
    public UnityAction PreEnemyActionStart;

    private void Awake()
    {
        Instance = this;
        enemies = new List<EnemyUnit>();
    }

    private void OnDestroy()
    {
        Instance = null;
    }
    public void SetPlayerCharacter(PlayerUnit pu)
    {
        player = pu;
        pu.unitController = playerCont;
        playerUI.SetUp(pu);
        pu.unitController.Init(player, playerUI);
    }
    public static void StartBattle(BattleEncounter e)
    {
        CycleManager.Instance.AddTempEvent(e.Identifier, "started");
        Scene s = DialogueUtils.CreateBattleScene(e);
        DialogueRenderer.Instance.PlayScene(s, null);

    }

    //public IEnumerator TutorialCheck()
    //{
    //    if (CycleManager.IsFirstCycle())
    //    {
    //        Publisher.Subscribe<TutorialEndEvent>(OnTutorialFinished);

    //        tutorialLocked = true;
    //        TutorialEvent te = new TutorialEvent("Combat", false, true, "", 0,
    //            "During combat Norman can use items he's collected on his journey to fight back!"
    //            );
    //        Publisher.Raise(te);

    //        while (tutorialLocked)
    //            yield return null;

    //        tutorialLocked = true;
    //        te = new TutorialEvent("Attacking & Blocking", false, true, "", 0,
    //            "Drag items to enemies or Norman to initiate attacks or blocks\n\n" +
    //            "Performing attacks and blocks will cost <color=#FFCB42>ACTION POINTS (AP)</color> \n\n" +
    //            "<color=#FFCB42>AP</color> is restored at the end of each turn, so use multiple items when possible!"
    //            );
    //        Publisher.Raise(te);

    //        //InventoryInfoPanel.Instance.Set(GlobalData10MH.Instance.itemDictionary.GetItem(SaveData.currentSave.hand));
    //        //Sequence s1 = InventoryInfoPanel.Instance.apGroup.DoFadeFlash(1, 0, 0.25f, 0.5f, -1);
    //        Sequence s2 = PlayerInfoMenu.Instance.apGroup.DoFadeFlash(1, 0, 0.25f, 0.5f, -1);

    //        while (tutorialLocked)
    //            yield return null;

    //        //s1.Kill();
    //        s2.Kill();

    //        //InventoryInfoPanel.Instance.Set(null);

    //        Publisher.Unsubscribe<TutorialEndEvent>(OnTutorialFinished);
    //    }
    //}
    public BattleBrain brains;
    public BattleMenu battleUI => brains.BMenu;

    public int Turn { get => turn; set => turn = value; }

    public IEnumerator StartBattleScene(BattleEncounter e)
    {
        encounter = e;
        MusicPlayer.Instance.Interrupt(encounter.battleTrack, 1);
        GameRules.SetGameRules(brains.InitGameRules());
        yield return new WaitForSeconds(0.5f);
        yield return brains.StartBattle();
        IsBattleStarted = true;
        Global10MH.ShowCursor(this);

        DialogueRenderer.Instance.OnBattleStart();
        yield return battleUI.UnFocus();
        yield return encounter.OnEncounterStart();
        OnEncounterStart?.Invoke();
        yield return brains.CheckYieldEvents();

        yield return brains.MainBattleLoop();

        BattleSceneCleanUp();
    }
    public IEnumerator Summon(CharacterData cd, int overrideNumEnemies)
    {
        float pos = battleUI.GetNextXPos(overrideNumEnemies);
        CharacterEnterData eData = new CharacterEnterData()
        {
            character = cd,
            xPos = pos,
            sprite = "battle",
        };

        yield return DialogueRenderer.Instance.EnterCharacterAndWait(eData);

        BattleUnitUI ui = DialogueRenderer.Instance.GetCharacterRenderer(cd)
            .GetComponentInChildren<BattleUnitUI>();

        BattleUnitController unitController = DialogueRenderer.Instance.GetCharacterRenderer(cd)
            .GetComponentInParent<BattleUnitController>();

        EnemyUnit unit = new EnemyUnit();
        unit.Init(cd);
        unit.StartBattle(unitController, ui);
        enemies.Add(unit);
        OnEnemySummon?.Invoke(unit);
    }
    public void BattleSceneCleanUp()
    {
        encounter = null;
        IsBattleStarted = false;
        InventoryManager.Instance.CloseInventory();
        InventoryManager.UnlockInput(this);
        CombatInteractionAdapter.ClearQueue();
        Global10MH.HideCursor(this);
        CursorController.ShowCursor(this);
        BattleController.Player.EndBattle();
        battleUI.gameObject.SetActive(false);
        foreach (EnemyUnit enemy in enemies)
        {
            enemy.EndBattle();
            enemy.Uninit();
        }
        brains.EndBattle();
        Enemies.Clear();

    }
    public List<CharacterData> summonsQueue = new List<CharacterData>();
    public IEnumerator HandleSummons()
    {
        int newMax = Mathf.Min(3, enemies.Count + summonsQueue.Count);
        foreach (CharacterData summon in summonsQueue)
        {
            if (enemies.Count > 2) break;
            bool isOnField = false;
            foreach (EnemyUnit item in enemies)
            {
                if (item.data == summon)
                {
                    isOnField = true;
                    break;
                }
            }
            if (isOnField)
                continue;
            yield return Summon(summon, newMax);
        }
        yield return battleUI.UnFocus();
        summonsQueue.Clear();
        yield return null;
    }
    public void QueueSummon(CharacterData cd)
    {
        if (cd == null)
            Debug.LogError("Ugh oh stinky");
        summonsQueue.Add(cd);
    }
    public void OnEnemyDeath(EnemyUnit unit)
    {
        enemies.Remove(unit);
        //battleMenu.actors.Remove(unit.ui);
        unit.EndBattle();
        unit.Uninit();
    }

}


