﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using System;


public class TimelineUI : MonoBehaviour
{
    public RectTransform waitBar, vulnerableBar;

    public void SetUp(TimelineBrain tb)
    {
        float vLength = waitBar.sizeDelta.x * tb.vulnerableStartTimeNormalized;
        vulnerableBar.DOSizeDelta(new Vector2(vLength, vulnerableBar.sizeDelta.y), 0.5f);
    }
    //public void SetActionLength(float actionLength)
    //{
    //    currentActionLength = actionLength;
    //    currentActionLength = Mathf.Min(currentActionLength, fullLength);
    //    currentActionLength = Mathf.Max(currentActionLength, standardActionLength);
    //    float waitLength = 1 - (currentActionLength / fullLength);
    //    waitBar.DOFillAmount(waitLength, 0.1f);
    //}
}

