﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate float AttributeModifier(BattleUnit u, float value);

public class BattleUnitEvents
{
    public delegate void BattleUnitEvent(BattleUnit u);
    public delegate void BlockEvent(BattleUnit u, float amount);
    public delegate void DamageEvent(BattleUnit u, float amount);
    public delegate void HealEvent(BattleUnit u, float amount);

    

    public BattleUnitEvent onTurnStart, onTurnEnd;
    public BlockEvent onBlockGain, onBlockLoss;
    public DamageEvent onDamageRecieved;

    public List<AttributeModifier> blockGainModifiers = new List<AttributeModifier>();
    public List<AttributeModifier> blockLossModifiers = new List<AttributeModifier>();
    public List<AttributeModifier> damageDealtModifiers = new List<AttributeModifier>();
    public List<AttributeModifier> damageRecievedModifiers = new List<AttributeModifier>();
}
public class PlayerBattleEvents : BattleUnitEvents
{
    public delegate void BattleItemActionEvent(BattleUnit u, ActionItem a);

    public BattleItemActionEvent onActionPreformed;
}

public class NonPlayerBattleEvents : BattleUnitEvents
{
    public delegate void BattleActionEvent(BattleUnit u, CombatAction a, CombatInfo ci);

    public BattleActionEvent onDetermineAction;
    public BattleActionEvent onActionPreformed;
}
