﻿using System.Collections;
using UnityEngine;


public class HitStop : MonoBehaviour
{
    public static HitStop Instance;

    CoroutineHandler coroutine;
    private void Awake()
    {
        Instance = this;
        coroutine = new CoroutineHandler(this);
    }
    public static void Pause(float duration, float timeScale = 0.25f)
    {
        //Instance.Stop(duration, timeScale);
    }
    public void Stop(float duration, float timeScale)
    {
        Time.timeScale = timeScale;
        coroutine.StartCoroutine(Wait(duration));
    }
    public void Stop(float duration)
    {
        Stop(duration, 0.0f);
    }
    IEnumerator Wait(float duration)
    {
        yield return new WaitForSecondsRealtime(duration);
        Time.timeScale = 1.0f;
    }
}
