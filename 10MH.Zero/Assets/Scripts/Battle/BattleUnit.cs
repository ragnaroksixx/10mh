﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System.ComponentModel;
using Sirenix.OdinInspector;

[System.Serializable]
public abstract class BattleUnit
{
    public CharacterData data;
    public BattleUnitController unitController;
    public int hp;
    public int maxHp;
    public int block;
    public float punchIntensity = 25, punchDuration = 0.5f, punchElasticity = 1.5f;
    public int punchVibrato = 10;
    protected BattleUnitEvents battleEvents;

    public Dictionary<CombatEffect, int> statusFXs;
    public Action OnDetermineAction;
    public string Description => data.Description;
    public string Name => data.name;
    public int AP => -1;

    public virtual int Block { get => block; set => block = value; }

    public virtual void Init(CharacterData c)
    {
        data = c;
        hp = maxHp = c.hp;
        statusFXs = new Dictionary<CombatEffect, int>();
        battleEvents = new BattleUnitEvents();
    }
    public virtual void StartBattle(BattleUnitController buc, BattleUnitUI ui)
    {
        data.RegisterRandom();
        unitController = buc;
        unitController.Init(this, ui);
    }

    public virtual void EndBattle()
    {
        data.DeregisterRandom();
        Dictionary<CombatEffect, int> tempStatusFXs = new Dictionary<CombatEffect, int>(statusFXs);
        foreach (KeyValuePair<CombatEffect, int> fx in tempStatusFXs)
        {
            fx.Key.OnBattleEnd(this);
        }
    }
    public virtual void Uninit()
    {
    }
    public virtual void TakeDamage(int amount, BattleUnit source)
    {
        int newAmount = amount;
        //newAmount = Mathf.CeilToInt(newAmount * source.GetValue(StatusEffect.ATKUP, 1));
        //newAmount = Mathf.CeilToInt(newAmount * source.GetValue(StatusEffect.ATKDOWN, 1));

        //newAmount = Mathf.CeilToInt(newAmount * GetValue(StatusEffect.DEFUP, 1));
        //newAmount = Mathf.CeilToInt(newAmount * GetValue(StatusEffect.DEFDOWN, 1));

        newAmount = Mathf.Max(0, amount - Block);
        if (Block > 0 && GameRules.LoseBlockAfterHit)
        {
            RemoveAllBlock();
        }
        else if (Block > 0 && GameRules.IsBlockTempHP)
        {
            LoseBlock(amount);
        }
        ResolveHPLoss(newAmount);
    }
    public virtual void LoseHP(int amount)
    {
        ResolveHPLoss(amount);
    }
    public virtual void ResolveHPLoss(int amount)
    {
        int newAmount = amount;

        hp -= newAmount;
        battleEvents.onDamageRecieved?.Invoke(this, amount);
        if (hp <= 0)
        {
            hp = 0;
            unitController.OnTakeDamage(amount, Block > 0);
            Die();
        }
        else
        {
            unitController.OnTakeDamage(amount, Block > 0);
        }
        HitStop.Pause(0.1f);
    }
    public bool IsDead()
    {
        return hp <= 0;
    }

    public float HPRatio()
    {
        return (float)hp / (float)maxHp;
    }
    public virtual void Heal(int amount)
    {
        GlobalData10MH.Instance.healSFX.Play();
        hp = Mathf.Min(hp + amount, maxHp);
        unitController.OnHeal(amount);
    }
    public void HealPercentage(float percentage)
    {
        Heal(Mathf.CeilToInt(percentage * maxHp));
    }
    public virtual void GainBlock(int amount)
    {
        Block = amount;
        unitController.OnBlockGained(amount);
        battleEvents.onBlockGain?.Invoke(this, amount);
    }
    public virtual void RemoveAllBlock()
    {
        Block = 0;
        unitController.OnBlockLoss();
    }
    public virtual void LoseBlock(int amount)
    {
        Block = Mathf.Max(Block - amount, 0);
        unitController.OnBlockLoss();
        battleEvents.onBlockLoss?.Invoke(this, amount);
    }
    public abstract void Die();
    public virtual IEnumerator OnTurnStart()
    {
        Dictionary<CombatEffect, int> tempStatusFXs = new Dictionary<CombatEffect, int>(statusFXs);
        foreach (KeyValuePair<CombatEffect, int> fx in tempStatusFXs)
        {
            fx.Key.OnTurnStart(this);
        }
        yield return DetermineActions();
    }
    public virtual IEnumerator DetermineActions()
    {
        if (Block > 0 && GameRules.LoseBlockAtTurnEnd)
        {
            RemoveAllBlock();
        }
        yield return null;
    }
    public virtual IEnumerator TakeTurn()
    {
        yield return null;
    }

    public IEnumerator LogCombat(string text, bool userInput = false)
    {
        Publisher.Raise(new CombatNotificationEvent(text));
        if (userInput)
            yield return WaitForUserInput();
        else
            yield return new WaitForSeconds(1.1f / 2.0f);
        Publisher.Raise(new ClearNotificationEvent(.5f));
    }
    IEnumerator WaitForUserInput()
    {
        bool wait = true;
        while (wait)
        {
            yield return null;
            if (InputSystem10MH.proceedDialogue.GetUp() && InventoryManager.Instance.IsHidden && !Phone.IsOnPhone)
                wait = false;
        }
        wait = true;
        while (wait)
        {
            yield return null;
            if (InputSystem10MH.proceedDialogue.GetUp() && InventoryManager.Instance.IsHidden && !Phone.IsOnPhone)
                wait = false;
        }
    }
    public void AddStatus(CombatEffect cFX, int stackCount = 1)
    {
        if (statusFXs.ContainsKey(cFX))
        {
            statusFXs[cFX] = statusFXs[cFX] + stackCount;
        }
        else
        {
            statusFXs.Add(cFX, stackCount);
            cFX.OnAdd(this);
        }
    }
    public void RemoveStatus(CombatEffect cFX, int stackCount = 999)
    {
        if (statusFXs.ContainsKey(cFX))
        {
            statusFXs[cFX] = statusFXs[cFX] - stackCount;
            if (statusFXs[cFX] <= 0)
            {
                statusFXs.Remove(cFX);
                cFX.OnRemove(this);
            }
        }
    }
    public int GetStatusCount(CombatEffect cFX)
    {
        if (statusFXs.ContainsKey(cFX))
        {
            return statusFXs[cFX];
        }
        else
        {
            return 0;
        }
    }
}
[System.Serializable]
public class EscortUnit : BattleUnit
{

    public override void Die()
    {
        EscortSystem.EndEscort();
    }
    public override void StartBattle(BattleUnitController buc, BattleUnitUI ui)
    {
        base.StartBattle(buc, ui);
        EscortSystem.ui.OnInvOpen();
    }
    public override void EndBattle()
    {
        base.EndBattle();
        EscortSystem.ui.OnInvClose();
    }
}
