﻿using NHG.Dialogue;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public enum CombatActionType
{
    DAMAGE,
    HEAL,
    STATUS,
    BLOCK
}
[System.Serializable]
public abstract class CombatActionEvent
{
    public abstract string eventName { get; }
    public virtual string[] eventAlias { get => new string[0] { }; }
    public int Amount => tags.GetInt("amt", 0);
    public bool TargetSelf => tags.GetBool("self", false);
    protected Tags tags = new Tags();
    public Tags Tags { get => tags; }
    public void PerformAction(BattleUnit user, BattleUnit target)
    {
        if (TargetSelf)
        {
            target = user;
        }
        PerformActionImpl(user, target);
    }
    protected abstract void PerformActionImpl(BattleUnit user, BattleUnit target);
    public virtual string BasicLog()
    {
        return "[" + eventName + "_" + Amount + "] ";
    }
}

public class DamageCombatAction : CombatActionEvent
{
    public override string eventName => "damage";
    public override string[] eventAlias => new string[] { "dmg" };
    protected override void PerformActionImpl(BattleUnit user, BattleUnit target)
    {
        target.TakeDamage(Amount, user);
    }
    public override string BasicLog()
    {
        return base.BasicLog() + "Dea1 " + Amount + " damage to target";
    }

    public static DamageCombatAction BasicDamageAction(int amount)
    {
        DamageCombatAction dca = new DamageCombatAction();
        dca.tags = new Tags();
        dca.tags.AddTag("amt", amount.ToString());
        return dca;
    }
}
public class HealCombatAction : CombatActionEvent
{
    public override string eventName => "heal";
    protected override void PerformActionImpl(BattleUnit user, BattleUnit target)
    {
        target.Heal(Amount);
    }
    public override string BasicLog()
    {
        return base.BasicLog() + "Restore " + Amount + " HP to target";
    }
}
public class BlockCombatAction : CombatActionEvent
{
    public override string eventName => "block";
    public override string[] eventAlias => new string[] { "blk" };
    protected override void PerformActionImpl(BattleUnit user, BattleUnit target)
    {
        target.GainBlock(Amount);
    }
    public override string BasicLog()
    {
        return base.BasicLog() + "Gain " + Amount + " block";
    }
    public static BlockCombatAction BasicBlockAction(int amount)
    {
        BlockCombatAction dca = new BlockCombatAction();
        dca.tags = new Tags();
        dca.tags.AddTag("amt", amount.ToString());
        return dca;
    }
}

public abstract class GainStatusCombatAction : CombatActionEvent
{
    public abstract CombatEffect GetStatusEffect();
    protected override void PerformActionImpl(BattleUnit user, BattleUnit target)
    {
        target.AddStatus(GetStatusEffect(), Amount);
    }
    public override string BasicLog()
    {
        return base.BasicLog() + "Gain " + GetStatusEffect() + "(" + Amount + ")";
    }
}
