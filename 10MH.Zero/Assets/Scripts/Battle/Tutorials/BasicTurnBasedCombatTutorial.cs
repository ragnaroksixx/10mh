﻿using System.Collections;
using UnityEngine;

public class TBCTutorial : BattleTutorial
{
    protected bool tutorialLocked = false;
    protected void OnTutorialFinished(TutorialEndEvent e)
    {
        tutorialLocked = false;
        Publisher.Unsubscribe<TutorialEndEvent>(OnTutorialFinished);
    }
}
public class TBCTutorialBasics : TBCTutorial
{
    public override IEnumerator OnBattleStart()
    {
        yield return base.OnBattleStart();
        Publisher.Subscribe<TutorialEndEvent>(OnTutorialFinished);
        tutorialLocked = true;

        TutorialEvent te = new TutorialEvent("Combat", false, true, "", 0,
        "TODO: Write combat tutorial");

        Publisher.Raise(te);

        while (tutorialLocked)
        {
            yield return null;
        }
        EndTutorial();

    }

}

public class TBCTutorialDefense : TBCTutorial
{
    public override IEnumerator OnBattleStart()
    {
        yield return base.OnBattleStart();
        Publisher.Subscribe<TutorialEndEvent>(OnTutorialFinished);
        tutorialLocked = true;

        TutorialEvent te = new TutorialEvent("Defense", false, true, "", 0,
        "TODO: Write combat tutorial");

        Publisher.Raise(te);

        while (tutorialLocked)
        {
            yield return null;
        }
        EndTutorial();

    }
}
