﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.UI.CanvasScaler;


public class CombatActionSelector : MonoBehaviour
{
    protected CombatSheet combatSheet;
    public BattleUnitController unitController;
    public BattleUnit Unit { get => unitController.Unit; }
    public virtual void Init(BattleUnit unit)
    {
        combatSheet = Activator.CreateInstance(Type.GetType(unit.data.aiHandlerClassName), unit) as CombatSheet;
    }
    public CombatMove GetAction(BattleUnit unit)
    {
        return combatSheet.GetMove();
    }

    public void LoopFinal()
    {
        combatSheet.patterns[combatSheet.patterns.Count - 1].Loops = -1;
    }
}
public class CombatMove : IAttributeInfo
{
    public List<CombatAction> actions;
    public float castTime;
    public string name;

    public CombatMove(string name, float castTime, params CombatAction[] actions)
    {
        this.name = name;
        this.castTime = castTime;
        this.actions = new List<CombatAction>(actions);
    }

    public string Header => name;

    public string Body => "";

    public int Numeric => (int)castTime;
}
public class CombatSheet
{
    public List<ICombatPattern> patterns;
    public List<ConditionalAction> conditionalActions;
    int index = 0;
    public BattleUnit unit;
    ICombatPattern currentPattern { get => patterns[index]; }
    public CombatSheet(BattleUnit unit)
    {
        this.unit = unit;
        patterns = new List<ICombatPattern>();
        conditionalActions = new List<ConditionalAction>();
    }
    public CombatMove CheckConditional()
    {
        ConditionalAction result = null;
        foreach (ConditionalAction item in conditionalActions)
        {
            if (item.criteria(unit) && (item.lastTimeUsed + item.cooldown) >= (Time.time))
            {
                result = item;
                break;
            }
        }
        if (result != null)
        {
            result.lastTimeUsed = Time.time;
            if (result.oneTimePattern)
                conditionalActions.Remove(result);
            return result.action;
        }
        return null;
    }
    public CombatMove GetMove()
    {
        CombatMove action = null;
        action = CheckConditional();
        if (action == null)//no conditional triggered or active
        {
            action = currentPattern.ChoseMove(unit);
            if (action == null)
            {
                index++;
                action = currentPattern.ChoseMove(unit);
            }
        }

        return action;
    }
    ICombatPattern AddPattern(ICombatPattern cp)
    {
        patterns.Add(cp);
        return (cp);
    }
    public ICombatPattern AddSingleAction(string name, float castTime, CombatAction action)
    {
        CombatMove cm = new CombatMove(name, castTime, action);
        return AddSingleAction(cm);
    }
    public ICombatPattern AddSingleAction(CombatMove action)
    {
        return AddPattern(new SinglePattern(unit, 1, action));
    }
    public ICombatPattern AddAlternatingActions(int loops, CombatMove action1, CombatMove action2)
    {
        return AddPattern(new AlternatePattern(unit, loops, action1, action2));
    }
    public ICombatPattern AddOrderedActions(int loops, params CombatMove[] actions)
    {
        return AddPattern(new BasicPatternList(unit, loops, actions));
    }
    public ICombatPattern AddRandomActions(int loops, params CombatMove[] actions)
    {
        return AddPattern(new RandomPattern(unit, loops, actions));
    }
    public ICombatPattern AddWeightedActions(int loops, params KeyValuePair<CombatMove, float>[] actions)
    {
        return AddPattern(new WeightedSelectionPattern(loops, actions));
    }
    public void AddConditonalAction(CombatMove action, Func<BattleUnit, bool> criteria, int cooldown, bool oneUse)
    {
        ConditionalAction temp = new ConditionalAction(action, criteria, cooldown, oneUse);
        conditionalActions.Add(temp);
    }
}

public interface ICombatPattern
{
    public CombatMove ChoseMove(BattleUnit unit);
    public int Loops { get; set; }
}
public class BasicPatternList : ICombatPattern
{
    public List<CombatMove> orderedActionStack;
    public List<CombatMove> originalList;
    public BasicPatternList(BattleUnit unit, int loops, params CombatMove[] actionList)
    {
        originalList = new List<CombatMove>(actionList);
        CreateOrderedList(unit);
    }

    public int Loops { get; set; }

    protected virtual void CreateOrderedList(BattleUnit unit)
    {
        orderedActionStack = new List<CombatMove>(originalList);
    }
    public CombatMove ChoseMove(BattleUnit unit)
    {
        if (orderedActionStack.Count == 0)
        {
            if (Loops != -1)
            {
                Loops--;
                if (Loops <= 0)
                    return null;
            }
            CreateOrderedList(unit);
        }
        return PopAction(unit);
    }
    protected CombatMove PopAction(BattleUnit unit)
    {
        CombatMove ca = orderedActionStack[0];
        orderedActionStack.RemoveAt(0);
        return ca;
    }
}
public class SinglePattern : BasicPatternList
{

    public SinglePattern(BattleUnit unit, int loops, CombatMove action) : base(unit, loops, action)
    {
    }

}
public class RandomPattern : BasicPatternList
{
    public RandomPattern(BattleUnit unit, int loops, params CombatMove[] actionList) : base(unit, loops, actionList)
    {
    }
    protected override void CreateOrderedList(BattleUnit unit)
    {
        base.CreateOrderedList(unit);
        orderedActionStack.Shuffle(unit.data.GetRandom());
    }
}
public class AlternatePattern : RandomPattern
{
    public AlternatePattern(BattleUnit unit, int loops, CombatMove action1, CombatMove action2) : base(unit, loops, action1, action2)
    {
    }
}

public class WeightedSelectionPattern : ICombatPattern
{
    public Dictionary<CombatMove, float> weightedActions;
    public int Loops { get; set; }
    public WeightedSelectionPattern(int loops, params KeyValuePair<CombatMove, float>[] actionList)
    {
        Loops = loops;
        weightedActions = new Dictionary<CombatMove, float>();
        foreach (KeyValuePair<CombatMove, float> action in actionList)
        {
            weightedActions.Add(action.Key, action.Value);
        }
    }
    public CombatMove ChoseMove(BattleUnit unit)
    {
        if (Loops != -1)
        {
            Loops--;
            if (Loops <= 0)
                return null;
        }
        return weightedActions.RandomElementByWeight(unit.data.GetRandom());
    }
}

public class ConditionalAction
{
    public CombatMove action;
    public Func<BattleUnit, bool> criteria;
    public int cooldown = 0;
    public bool oneTimePattern;
    public float lastTimeUsed = 0;

    public ConditionalAction(CombatMove action, Func<BattleUnit, bool> criteria, int cooldown, bool oneTimePattern)
    {
        this.action = action;
        this.criteria = criteria;
        this.cooldown = cooldown;
        this.oneTimePattern = oneTimePattern;
    }
    //<=0 no cooldown
}


