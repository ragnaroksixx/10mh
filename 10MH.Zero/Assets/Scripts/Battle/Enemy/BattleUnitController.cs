﻿using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class BattleUnitController : MonoBehaviour
{
    BattleUnitUI ui;
    BattleUnit unit;
    DialogueCharacterRenderer dcr;
    public CombatActionSelector actionSelector;
    public ActionVisualInfo basicAttack, basicDefense, basicSpecial;
    public Action<BattleUnit> OnHPChangedEvent;

    public BattleUnitUI UI { get => ui; set => ui = value; }
    public BattleUnit Unit { get => unit; }

    public void Init(BattleUnit unit, BattleUnitUI ui)
    {
        this.ui = ui;
        this.unit = unit;
        ui.SetUp(unit);
        actionSelector?.Init(unit);
        actionSelector?.LoopFinal();
        basicAttack.OnStateUse.AddListener(DoBasicAttackSequence);
        basicDefense.OnStateUse.AddListener(DoBasicDefenseEffect);
        basicSpecial.OnStateUse.AddListener(DoBasicSpecialEffect);
        dcr = GetComponent<DialogueCharacterRenderer>();
    }
    public void OnHeal(int amount)
    {
        ui.OnHeal(amount);
        OnHPChangedEvent?.Invoke(unit);
    }
    public void OnBlockGained(int amount)
    {
        ui.OnBlockChanged(amount);
    }
    public void OnBlockLoss()
    {
        ui.OnBlockChanged(unit.Block);
    }
    public void OnTakeDamage(int amount, bool wasBlocked)
    {
        ui.OnTakeDamage(amount, wasBlocked);
        if (unit.hp <= 0)
        {
            OnDeath();
        }
    }
    public void OnDeath()
    {
        MonoBehaviour.Destroy(ui.image.GetComponent<EventTrigger>());
        ui.Unhighlight();
        ui.OnDeath();
    }
    public CombatMove DetermineAction()
    {
        CombatMove result = actionSelector.GetAction(unit);
        //ActionVisualInfo vfx = GetActionVisuals(result);
        //vfx.OnStateEnter.Invoke();
        return result;
    }
    public IEnumerator OnPerformAction(CombatAction action)
    {
        ActionVisualInfo vfx = GetActionVisuals(action);
        vfx.OnStateUse.Invoke();
        yield return new WaitForSeconds(vfx.onUseWaitTime);
    }
    public void OnPerformAction(ActionItem action)
    {
        ui.PlaySquence(DebugActionAnimation(false));
    }
    private void DoBasicAttackSequence()
    {
        ui.PlaySquence(DebugActionAnimation(true, 1));
    }
    private void DoBasicDefenseEffect()
    {
        Transform t = GameObject.Instantiate(GlobalData10MH.Instance.defaultShieldFX, ui.hpRoot).transform;
        t.localPosition = Vector3.zero;
        t.localScale = Vector3.one;
    }

    private void DoBasicSpecialEffect()
    {

    }
    private Sequence DebugActionAnimation(bool dir, float speedMult = 1)
    {
        ui.image.DOKill(true);
        Sequence s = DOTween.Sequence();
        int direction = dir ? 1 : -1;
        Vector2 startPos = ui.image.rectTransform.anchoredPosition;
        Tween pullBack = ui.image.rectTransform.DOAnchorPosX(startPos.x + (50 * direction), 0.25f / speedMult);
        Tween launch = ui.image.rectTransform.DOAnchorPosX(startPos.x - (50 * direction), 0.15f / speedMult);
        Tween returnToNormal = ui.image.rectTransform.DOAnchorPosX(startPos.x, 0.25f / speedMult);
        s.Append(pullBack)
            .AppendInterval(0.2f / speedMult)
            .Append(launch)
            .Append(returnToNormal)
            .OnComplete(() => { ui.image.rectTransform.anchoredPosition = startPos; })
            .OnKill(() => { ui.image.rectTransform.anchoredPosition = startPos; })
            .SetTarget(ui.image);
        return s;
    }

    [Button]
    void ShowAttackEditor()
    {
        DialogueCharacterRenderer editorUI = GetComponentInChildren<DialogueCharacterRenderer>();
        editorUI.characterImage.sprite = basicAttack.sprite;
        editorUI.outline.sprite = basicAttack.sprite;
    }
    [Button]
    void ShowDefenseEditor()
    {
        DialogueCharacterRenderer editorUI = GetComponentInChildren<DialogueCharacterRenderer>();
        editorUI.characterImage.sprite = basicDefense.sprite;
        editorUI.outline.sprite = basicDefense.sprite;
    }
    [Button]
    void ShowSpecialEditor()
    {
        DialogueCharacterRenderer editorUI = GetComponentInChildren<DialogueCharacterRenderer>();
        editorUI.characterImage.sprite = basicSpecial.sprite;
        editorUI.outline.sprite = basicSpecial.sprite;
    }

    public ActionVisualInfo GetActionVisuals(CombatAction action)
    {
        ActionVisualInfo vfx = action.CustomVisualInfo;
        if (vfx == null)
        {
            if (action is AttackAction)
            {
                vfx = basicAttack;
            }
            else if (action is DefenseAction)
            {
                vfx = basicDefense;
            }
            else
            {
                vfx = basicSpecial;
            }
        }
        return vfx;
    }
}

[System.Serializable]
public class ActionVisualInfo
{
    public Sprite sprite;

    public AudioVariable onEnterSFX;

    public AudioVariable onUseSFX;
    public float onUseWaitTime = 0.5f;

    [FoldoutGroup("Events")]
    public UnityEvent OnStateEnter;
    [FoldoutGroup("Events")]
    public UnityEvent OnStateUse;

    [FoldoutGroup("Events")]
    public UnityEvent OnStateExit;
}
