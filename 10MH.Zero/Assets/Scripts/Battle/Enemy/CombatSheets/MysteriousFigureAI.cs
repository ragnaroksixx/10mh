﻿using System.Collections;
using UnityEngine;
using static UnityEngine.UI.CanvasScaler;


public class MysteriousFigureAI : CombatSheet
{

    public MysteriousFigureAI(BattleUnit unit) : base(unit)
    {
        AttackAction firstAttack = new AttackAction(2);
        DefenseAction block = new DefenseAction(3);
        AttackAction basicAttack = new AttackAction(1);
        AttackAction basicAttack2 = new AttackAction(3);

        CombatMove basicMove1 = new CombatMove("Swift Strikes", 3, basicAttack, basicAttack);
        CombatMove basicMove2 = new CombatMove("Heavy Strike", 5, basicAttack2);
        CombatMove phase2Buff = new CombatMove("Limit Break", 1, new SpecialAction(PhaseTwoBuffs));

        AddSingleAction("Strike", 3, firstAttack);
        AddSingleAction("Shields Up", 2, block);
        AddRandomActions(-1, basicMove1, basicMove2);//, block);
        AddConditonalAction(phase2Buff, PhaseTwoHealthCheck, 0, true);
    }

    public bool PhaseTwoHealthCheck(BattleUnit bu)
    {
        return bu.HPRatio() <= 0.7f;
    }
    public void PhaseTwoBuffs(CombatInfo ci)
    {
        Debug.Log("PHASE TWO!!!!");
        ci.source.AddStatus(CombatEffectsLibrary.Instance.attackUp, 99);
    }
}
