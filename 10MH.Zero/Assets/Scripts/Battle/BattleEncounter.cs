﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NHG.Dialogue;

public class BattleEncounter : ScriptableObject, IUnquieScriptableObject
{
    public List<CharacterData> units;
    public AudioVariable battleTrack;
    public string encounterName;
    public string Identifier => encounterName;
    public string[] alias = new string[0];
    public string[] Alias => alias;

    public void Init()
    {
    }
    public virtual IEnumerator OnEncounterStart()
    {
        yield return null;
    }
    public virtual IEnumerator OnEncounterEnd()
    {
        yield return null;
    }

}
