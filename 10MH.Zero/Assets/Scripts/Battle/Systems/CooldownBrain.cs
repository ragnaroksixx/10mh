﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CooldownBrain : BattleBrain
{
    public Transform battleTimerRoot;
    Dictionary<EnemyUnit, EnemyCountdownHandler> enemyCountdowns;
    public UnitCountdownUI countdownUIPrefab;
    public override GameRules InitGameRules()
    {
        return new GameRules()
        {
            loseBlockAfterHit = false,
            loseBlockAtTurnEnd = true,
            isBlockTempHP = true
        };
    }
    public override IEnumerator StartBattle()
    {
        enemyCountdowns = new Dictionary<EnemyUnit, EnemyCountdownHandler>();
        InventoryManager.LockInput(this);
        CursorController.HideCursor(this);

        battleUI.gameObject.SetActive(true);

        Player.StartBattle(bController.playerCont, bController.playerUI);
        foreach (CharacterData c in encounter.units)
        {
            yield return bController.Summon(c, encounter.units.Count);
            EnemyCountdownHandler ech = new EnemyCountdownHandler();
            EnemyUnit e = Enemies[Enemies.Count - 1];
            ech.Init(e);
            SpawnIcon(ech);
            enemyCountdowns.Add(e, ech);
            yield return ech.Start();
        }
        battleUI.StartBattle(encounter);
    }

    public override IEnumerator MainBattleLoop()
    {
        foreach (ItemPlacementData item in PlayerInventory.Instance.inventoryItems)
        {
            //item.runtimeUI.durabilityUI.Show();
            //item.runtimeUI.chargeUI.OnBattleStart(item.runtimeUI);
        }
        while (true)
        {
            yield return CheckYieldEvents();

            //update Charges
            if (Player.lastUsedItem != null)
            {
                //Update ALL
                //foreach (ItemPlacementData item in PlayerInventory.Instance.inventoryItems)
                //{
                //    if (item.runtimeUI == Player.lastUsedItem)
                //    {
                //        continue;
                //    }
                //    if (item.item is CombatItem)
                //    {
                //        CombatItem cItem = item.item as CombatItem;
                //        item.runtimeUI.AddCharge();
                //    }
                //}

                //Add to random
                //List<ItemPlacementData> unchargedItems = PlayerInventory.Instance.GetChargeableItems();
                //unchargedItems.Remove(Player.lastUsedItem.ItemData);
                //if (unchargedItems.Count > 0)
                //{
                //    InventoryItemUI ui = PlayerInventory.Instance.inventoryItems
                //            [Player.data.Range(0, PlayerInventory.Instance.inventoryItems.Count)].runtimeUI;
                //    ui.AddCharge();
                //}
            }

            yield return BattleController.Player.OnTurnStart();
            BattleController.Instance.PrePlayerActionStart?.Invoke();

            Player.currentAP = 999;
            //Player turn
            //Wait until player does a thing
            while (Player.currentAP > 0)
            {
                yield return null;
            }

            BattleController.Instance.PreEnemyActionStart?.Invoke();
            yield return CheckYieldEvents();
            yield return new WaitForSeconds(0.2f);

            //Enemy turns
            List<EnemyUnit> enemies = new List<EnemyUnit>(Enemies);
            for (int i = 0; i < enemies.Count; i++)
            {
                if (enemies[i] == null || enemies[i].IsDead()) continue;

                yield return enemyCountdowns[enemies[i]].Decrement();

                if (Enemies.Count == 0 || BattleController.Player.hp <= 0)
                    break;
            }

            if (Enemies.Count == 0 || BattleController.Player.hp <= 0)
                break;
            //yield return new WaitForSeconds(0.5f);
        }
    }
    public override void EndBattle()
    {
        foreach (ItemPlacementData item in PlayerInventory.Instance.inventoryItems)
        {

        }
    }
    public override bool CanPlayerUseItem(InventoryItemUI i)
    {
        return base.CanPlayerUseItem(i) && Player.currentAP > 0;
    }
    public override bool CanPlayerPickUpItem(InventoryItemUI i)
    {
        return base.CanPlayerPickUpItem(i) && !i.IsBroken;
    }
    public override void OnActionUsed(ActionItem ai)
    {
        base.OnActionUsed(ai);
        CombatItem cItem = ai.item.Item as CombatItem;
        Player.currentAP = 0;
        Player.lastUsedItem = ai.item;
    }
    public void SpawnIcon(EnemyCountdownHandler ech)
    {
        UnitCountdownUI icon = GameObject.Instantiate(countdownUIPrefab, ech.eUnit.unitController.UI.transform);
        icon.InitPosition(ech, this);
    }
}

public class EnemyCountdownHandler
{
    public int countdown;
    public EnemyUnit eUnit;
    public UnitCountdownUI ui;
    public void Init(EnemyUnit e)
    {
        eUnit = e;
    }
    public IEnumerator Start()
    {
        yield return NextAction();
    }
    public IEnumerator Decrement()
    {
        countdown--;
        ui.SetCountdown(countdown);
        if (countdown <= 0)
        {
            yield return new WaitForSeconds(0.25f);
            //ui.Hide();
            yield return eUnit.TakeTurn();
            yield return NextAction();
            ui.Show(countdown);
        }
    }
    public IEnumerator NextAction()
    {
        yield return eUnit.DetermineActions();
        //if (eUnit.turnAction is DefenseAction)
        //{
        //    countdown = 2;
        //}
        //else if (eUnit.turnAction is AttackAction)
        //{
        //    countdown = (eUnit.turnAction as AttackAction).basePower;
        //}
        //else
        //    countdown = 3;
        countdown = (int)eUnit.turnAction.castTime/2;
        ui.SetCountdown(countdown, false);
    }
}
