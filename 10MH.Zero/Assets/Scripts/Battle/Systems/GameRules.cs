﻿using System.Collections;
using UnityEngine;

public class GameRules
{
    private static GameRules Instance;
    public bool loseBlockAtTurnEnd = false;
    public static bool LoseBlockAtTurnEnd => Instance.loseBlockAtTurnEnd;

    public bool loseBlockAfterHit = false;
    public static bool LoseBlockAfterHit => Instance.loseBlockAfterHit;

    public bool isBlockTempHP = false;
    public static bool IsBlockTempHP => Instance.isBlockTempHP;

    public static void SetGameRules(GameRules rules)
    {
        Instance = rules;
    }
}
