﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BattleBrain : MonoBehaviour
{
    [SerializeField]
    protected BattleController bController;
    [SerializeField]
    protected BattleMenu battleUI;

    protected List<IEnumerator> executionYields = new List<IEnumerator>();
    protected BattleEncounter encounter => bController.encounter;
    public List<EnemyUnit> Enemies { get => BattleController.Instance.Enemies; }

    public BattleMenu BMenu { get => battleUI; }
    protected PlayerUnit Player => CycleManager.PlayerCombatUnit;

    protected bool YieldExecution { get => executionYields.Count > 0; }

    public abstract GameRules InitGameRules();
    public abstract IEnumerator StartBattle();
    public abstract IEnumerator MainBattleLoop();
    public abstract void EndBattle();
    public virtual bool CanPlayerUseItem(InventoryItemUI i)
    {
        return true;
    }
    public virtual bool CanPlayerPickUpItem(InventoryItemUI i)
    {
        return true;
    }

    public virtual void OnActionUsed(ActionItem ai)
    {

    }
    public IEnumerator CheckYieldEvents()
    {
        while (executionYields.Count > 0)
        {
            IEnumerator temp = executionYields[0];
            executionYields.RemoveAt(0);
            yield return StartCoroutine(temp);
        }
    }
    public void AddYieldEvents(IEnumerator ie)
    {
        executionYields.Add(ie);
    }
    public void RemoveYieldEvents(IEnumerator ie)
    {
        executionYields.Remove(ie);
    }
}
