﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TurnBasedBrain : BattleBrain
{
    BattleState state = BattleState.NONE;
    public BattleState State
    {
        get => state;
        set
        {
            if (state != value)
            {
                state = value;
                OnStateChangedEvent?.Invoke(state);
            }
        }
    }
    int turn = 0;
    public delegate void OnStateChanged(BattleState nextState);
    public OnStateChanged OnStateChangedEvent;
    public override void EndBattle()
    {
        State = BattleState.NONE;
        CursorController.ShowCursor(this);
        InventoryManager.UnlockInput(this);
        battleUI.EndBattle();
        battleUI.gameObject.SetActive(false);
    }

    public override GameRules InitGameRules()
    {
        return new GameRules()
        {
            loseBlockAfterHit = true,
            loseBlockAtTurnEnd = false
        };
    }
    public override IEnumerator StartBattle()
    {
        turn = 0;
        InventoryManager.LockInput(this);
        CursorController.HideCursor(this);

        battleUI.gameObject.SetActive(true);
        Player.StartBattle(bController.playerCont, bController.playerUI);
        foreach (CharacterData c in encounter.units)
        {
            yield return bController.Summon(c, encounter.units.Count);
        }
        battleUI.StartBattle(encounter);
    }
    public override IEnumerator MainBattleLoop()
    {
        while (State != BattleState.END)
        {
            turn = turn + 1;

            yield return new WaitForSeconds(.75f);

            for (int i = 0; i < Enemies.Count; i++)
            {
                yield return Enemies[i].OnTurnStart();
            }
            yield return BattleController.Player.OnTurnStart();

            BattleController.Instance.OnTurnStart?.Invoke();
            yield return CheckYieldEvents();

            State = BattleState.PLAYER;

            BattleController.Instance.PrePlayerActionStart?.Invoke();
            yield return CheckYieldEvents();

            yield return BattleController.Player.TakeTurn();

            while (State == BattleState.PLAYER)
            {
                if (Player.currentAP <= 0)
                    EndPlayerTurn();
                yield return null;
            }

            BattleController.Instance.PreEnemyActionStart?.Invoke();
            yield return CheckYieldEvents();

            yield return new WaitForSeconds(0.5f);

            for (int i = 0; i < Enemies.Count; i++)
            {
                yield return Enemies[i].TakeTurn();

                if (Enemies.Count == 0 || BattleController.Player.hp <= 0)
                    break;
            }


            if (Enemies.Count == 0 || BattleController.Player.hp <= 0)
            {
                State = BattleState.END;
            }
            yield return new WaitForSeconds(0.25f);

            if (State != BattleState.END)
                yield return BattleController.Instance.HandleSummons();
        }
    }



    public void EndPlayerTurn()
    {
        if (State != BattleState.PLAYER) return;
        State = BattleState.ENEMY;
    }
    public override bool CanPlayerUseItem(InventoryItemUI i)
    {
        return State == BattleState.PLAYER
            && Player.currentAP >= i.Item.AP;
    }
    public override bool CanPlayerPickUpItem(InventoryItemUI i)
    {
        return base.CanPlayerPickUpItem(i)
            && CombatInteractionAdapter.CountInQueue(i) == 0;
    }
}

public enum BattleState
{
    NONE,
    PLAYER,
    ENEMY,
    END
}
