﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TimelineBrain : BattleBrain
{
    public TimelineUI timelineUI;
    public float vulnerableStartTimeNormalized = 0.2f;
    public Transform battleTimerRoot;
    List<TimeAction> actions;

    public UnitTimerUI timeUIPrefab;
    public override void EndBattle()
    {
        timelineUI.gameObject.SetActive(false);
    }

    public override GameRules InitGameRules()
    {
        return new GameRules()
        {
            loseBlockAfterHit = false,
            loseBlockAtTurnEnd = false
        };
    }
    public override IEnumerator StartBattle()
    {
        actions = new List<TimeAction>();

        InventoryManager.LockInput(this);
        CursorController.HideCursor(this);

        timelineUI.gameObject.SetActive(false);
        battleUI.gameObject.SetActive(true);

        timelineUI.SetUp(this);

        Player.StartBattle(bController.playerCont, bController.playerUI);
        foreach (CharacterData c in encounter.units)
        {
            yield return bController.Summon(c, encounter.units.Count);
            EnemyTimeAction eta = new EnemyTimeAction(Enemies[Enemies.Count - 1], 3f);
            SpawnIcon(eta);
            actions.Add(eta);
        }
        battleUI.StartBattle(encounter);
    }


    public override IEnumerator MainBattleLoop()
    {
        foreach (TimeAction item in actions)
        {
            StartCoroutine(item.MainLoop());
        }
        while (true)
        {
            Player.RegenStamina();
            float deltaTime = Time.deltaTime;

            //yield return CheckYieldEvents();

            //Handle Summons
            //Check End State
            if (Enemies.Count == 0 || BattleController.Player.hp <= 0)
                break;
            yield return null;
        }
    }
    public void SpawnIcon(TimeAction action)
    {
        UnitTimerUI icon = GameObject.Instantiate(timeUIPrefab, action.Unit.unitController.UI.transform);
        icon.Init(action, battleTimerRoot);
    }
    public override bool CanPlayerUseItem(InventoryItemUI i)
    {
        return base.CanPlayerUseItem(i)
            && Player.HasStamina(i.Item.AP * 10);
    }
    public override bool CanPlayerPickUpItem(InventoryItemUI i)
    {
        return base.CanPlayerPickUpItem(i)
            && CombatInteractionAdapter.CountInQueue(i) == 0;
    }
    public override void OnActionUsed(ActionItem ai)
    {
        base.OnActionUsed(ai);
    }
}

public class TimeAction
{
    float time;
    protected BattleUnit unit;
    float castTime;

    public float CurrentTime { get => time; set => time = value; }
    public float CastTime { get => castTime; set => castTime = value; }
    public BattleUnit Unit { get => unit; set => unit = value; }

    public Coroutine actionRoutine;
    public TimeAction(BattleUnit bu, float cTime)
    {
        unit = bu;
        castTime = cTime;
    }
    public IEnumerator MainLoop()
    {
        yield return OnTurnStart();
        while (!unit.IsDead())
        {
            time += Time.deltaTime;
            if (time >= CastTime)
            {
                yield return OnTurnEnd();
                time -= CastTime;
                yield return OnTurnStart();
            }
            else
            {
                yield return null;
            }
        }
    }

    public virtual IEnumerator OnTurnStart() { yield return null; }
    public virtual IEnumerator OnTurnEnd() { yield return null; }
}

public class EnemyTimeAction : TimeAction
{
    EnemyUnit eUnit => unit as EnemyUnit;
    public EnemyTimeAction(EnemyUnit bu, float cTime) : base(bu, cTime)
    {
    }

    public override IEnumerator OnTurnEnd()
    {
        eUnit.RemoveAllBlock();
        yield return base.OnTurnEnd();
        yield return unit.TakeTurn();
    }
    public override IEnumerator OnTurnStart()
    {
        yield return base.OnTurnStart();
        yield return unit.DetermineActions();
        CastTime = eUnit.turnAction.castTime;
    }
}
