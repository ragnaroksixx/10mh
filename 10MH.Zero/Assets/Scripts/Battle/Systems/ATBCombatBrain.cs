﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ATBCombatBrain : BattleBrain
{
    public Dictionary<ItemPlacementData, ItemATBManager> itemATBS;
    public Transform battleTimerRoot;
    List<TimeAction> actions;
    public UnitTimerUI timeUIPrefab;
    public ItemPlacementData blockingItem;

    public override GameRules InitGameRules()
    {
        return new GameRules()
        {
            loseBlockAfterHit = false,
            loseBlockAtTurnEnd = false,
            isBlockTempHP = false
        };
    }

    public override IEnumerator StartBattle()
    {
        actions = new List<TimeAction>();
        itemATBS = new Dictionary<ItemPlacementData, ItemATBManager>();
        InventoryManager.LockInput(this);
        CursorController.HideCursor(this);

        battleUI.gameObject.SetActive(true);
        Player.StartBattle(bController.playerCont, bController.playerUI);
        foreach (CharacterData c in encounter.units)
        {
            yield return bController.Summon(c, encounter.units.Count);
            EnemyTimeAction eta = new EnemyTimeAction(Enemies[Enemies.Count - 1], 3f);
            SpawnIcon(eta);
            actions.Add(eta);
        }
        battleUI.StartBattle(encounter);
    }
    public override void EndBattle()
    {
        battleUI.EndBattle();
        battleUI.gameObject.SetActive(false);
        InventoryManager.UnlockInput(this);
        CursorController.ShowCursor(this);
    }
    public override IEnumerator MainBattleLoop()
    {
        foreach (TimeAction item in actions)
        {
            StartCoroutine(item.MainLoop());
        }
        while (true)
        {
            foreach (ItemPlacementData item in PlayerInventory.Instance.inventoryItems)
            {
                if (item.runtimeUI == Player.blockingItem)
                    continue;
                IncrementItem(item, Time.deltaTime );
            }
            if (Enemies.Count == 0 || BattleController.Player.hp <= 0)
                break;
            yield return null;
        }
    }
    private void IncrementItem(ItemPlacementData i, float amt)
    {
        if (!itemATBS.ContainsKey(i))
        {
            ItemATBManager iam = new ItemATBManager(i);
            iam.Init(4, PlayerInventory.GetHand());
            itemATBS.Add(i, iam);
            //i.runtimeUI.chargeUI.OnBattleStart(4);
            i.runtimeUI.atbUI.Show();
        }
        itemATBS[i].OnUpdate(amt);
        i.runtimeUI.atbUI.UpdateATB(itemATBS[i].atb);
        //i.runtimeUI.chargeUI.UpdateCharge((int)itemATBS[i]);
    }
    public float DecrementItem(ItemPlacementData i, float amt)
    {
        if (!itemATBS.ContainsKey(i))
        {
            ItemATBManager iam = new ItemATBManager(i);
            iam.Init(4, PlayerInventory.GetHand());
            itemATBS.Add(i, iam);
            //i.runtimeUI.chargeUI.OnBattleStart(4);
            i.runtimeUI.atbUI.Show();
        }
        itemATBS[i].Decrement(amt);
        i.runtimeUI.atbUI.UpdateATB(itemATBS[i].atb);
        //i.runtimeUI.chargeUI.UpdateCharge((int)itemATBS[i]);
        return itemATBS[i].atb;
    }
    private void FloorToInt(ItemPlacementData i)
    {
        if (!itemATBS.ContainsKey(i)) return;
        float amt = itemATBS[i].atb;
        int nearest=Mathf.FloorToInt(amt);
        DecrementItem(i, amt - nearest);

    }
    public override bool CanPlayerPickUpItem(InventoryItemUI i)
    {
        bool hasOneCharge = false;
        if (itemATBS.ContainsKey(i.ItemData))
        {
            hasOneCharge = itemATBS[i.ItemData].atb >= 1;
        }
        return base.CanPlayerPickUpItem(i) && hasOneCharge;
    }
    public override bool CanPlayerUseItem(InventoryItemUI i)
    {
        return base.CanPlayerUseItem(i);
    }
    public override void OnActionUsed(ActionItem ai)
    {
        base.OnActionUsed(ai);

        if (ai.item == Player.blockingItem)
        {
            Player.RemoveAllBlock();
            Player.blockingItem.SetIsBlock(false);
            Player.blockingItem = null;
        }

        if (ai.IsBlock)
        {
            Player.blockingItem = ai.item;
            Player.blockingItem.SetIsBlock(true);
            FloorToInt(ai.item.ItemData);
        }
        else
        {
            DecrementItem(ai.item.ItemData, 1.0f);
        }
    }
    public void SpawnIcon(TimeAction action)
    {
        UnitTimerUI icon = GameObject.Instantiate(timeUIPrefab, action.Unit.unitController.UI.transform);
        icon.Init(action, battleTimerRoot);
    }
}

public class ItemATBManager
{
    public ItemPlacementData item;
    public float atb;
    public float chargeRate;
    public int maxATB;

    public ItemATBManager(ItemPlacementData i)
    {
        item = i;
    }
    public void Init(int max, ItemPlacementData hand)
    {
        if (item == hand)
            chargeRate = 1;
        else
        {
            chargeRate = 1f / (float)(item.item.size.x * item.item.size.y);
        }

        //chargeRate = 1;
        maxATB = max;
    }
    public void OnUpdate(float deltaTime)
    {
        float chargeFallOff = (1f / ((int)atb + 1));
        deltaTime *= chargeFallOff;
        deltaTime *= chargeRate;
        atb = Mathf.Clamp(atb + deltaTime, 0, maxATB);
    }
    public void Decrement(float val)
    {
        atb = Mathf.Clamp(atb - val, 0, maxATB);
    }
}
