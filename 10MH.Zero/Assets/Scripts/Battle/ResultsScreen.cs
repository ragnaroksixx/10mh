﻿using UnityEngine;
using System.Collections;
using TMPro;
using NHG.Dialogue;
using DG.Tweening;
using UnityEngine.UI;

public class ResultsScreen : MonoBehaviour
{
    public static ResultsScreen Instance;
    public OnScreenAnimatorBASE gameOverScreen;
    public RectTransform root;
    public TMP_Text theEndText;
    public TMP_Text reason;
    public CanvasGroup proceedCanvas;
    public TMP_Text proceedText;
    public Image proceedButton;
    public Image flashImage;
    public Image borderImage;
    public DialogueVariable firstCycleEndScene;
    public Color black;
    public Color white;

    public CustomAudioClip reverseSFX;
    public CustomAudioClip ticksSFX;
    float max = 0.75f;
    float min = 0;
    float hold = 0.25f;
    float flash = 0.75f;

    private void Awake()
    {
        Instance = this;
    }
    public IEnumerator ShowGameOver(string reasonText)
    {
        MusicPlayer.Instance.Stop();
        AudioSystem.PlayBGM(null, Vector3.zero);
        reason.text = reasonText;
        proceedCanvas.alpha = 0;
        gameOverScreen.AnimateOnScreen();



        float t = 0;
        while (t < 1)
        {
            yield return null;
            t += Time.deltaTime;
            if (InputSystem10MH.proceedDialogue.GetUp())
            {
                CycleManager.Instance.RestartCycle();
                yield break;
            }
        }

        proceedCanvas.DoFadeFlash(max, min, hold, flash, -1);

        while (true)
        {
            yield return null;
            if (InputSystem10MH.proceedDialogue.GetUp())
            {
                CycleManager.Instance.RestartCycle();
                yield break;
            }
        }
    }

    float flashMax = 1;
    float flashMin = 0;
    float FlashRate = 0.15f;

    public IEnumerator FirstCycleGameOver()
    {
        AudioReference audioRef = null;
        float holdTime;
        MusicPlayer.Instance.Stop();
        AudioSystem.PlayBGM(null, Vector3.zero);

        theEndText.text = "END.";
        reason.text = "";

        proceedCanvas.alpha = 1;
        proceedButton.DOFade(0, 0);
        proceedText.color = black;
        borderImage.color = black;

        gameOverScreen.AnimateOnScreen();

        yield return new WaitForSeconds(0.25f);

        bool wait = true;
        holdTime = 0;
        while (wait)
        {
            yield return null;
            if (Input.anyKeyDown)
            {
                audioRef = reverseSFX.Play();
                root.DOShakePosition(FlashRate * 2, 5, 20)
                    .SetEase(Ease.InExpo);
                flashImage.DoFadeFlash(flashMin, flashMax, holdTime, FlashRate, 1);
                wait = false;
            }
        }

        yield return new WaitForSeconds(holdTime + FlashRate);
        theEndText.text = "END?";
        audioRef.Pause();
        yield return new WaitForSeconds(holdTime + FlashRate);

        wait = true;
        holdTime = 0.4f;
        while (wait)
        {
            yield return null;
            if (Input.anyKeyDown)
            {
                audioRef.RePlay();
                root.DOShakePosition(holdTime * 2, 5, 20)
                    .SetEase(Ease.InExpo);
                flashImage.DoFadeFlash(flashMin, flashMax, holdTime, FlashRate, 1);
                wait = false;
            }
        }

        yield return new WaitForSeconds(holdTime + FlashRate);
        borderImage.DOColor(white, FlashRate);
        yield return new WaitForSeconds(holdTime + FlashRate);
        audioRef.Pause();

        wait = true;
        holdTime = 1.5f;
        while (wait)
        {
            yield return null;
            if (Input.anyKeyDown)
            {
                audioRef.UnPause();
                borderImage.DOColor(black, FlashRate);
                theEndText.DOColor(black, FlashRate);
                reason.DOColor(black, FlashRate);
                root.DOShakePosition(holdTime * 2, 20, 30)
                    .SetEase(Ease.InExpo);
                flashImage.DoFadeFlash(flashMin, flashMax, holdTime, FlashRate, 1);
                wait = false;
            }
        }

        yield return new WaitForSeconds(holdTime + FlashRate);
        borderImage.DOColor(white, FlashRate);
        proceedText.DOColor(white, FlashRate);
        theEndText.DOColor(white, FlashRate);
        reason.DOColor(white, FlashRate);
        proceedButton.DOFade(1, 1);
        proceedCanvas.DoFadeFlash(max, min, hold, flash, -1);
        audioRef.Stop(3);
        //audioRef = ticksSFX.Play(2);
        yield return new WaitForSeconds(FlashRate / 1.5f);
        root.DOKill(true);

        CycleManager.Instance.RestartCycle();
    }

    void OnDialogueDone()
    {
        CycleManager.Instance.RestartCycle();
    }
}
