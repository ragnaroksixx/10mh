﻿using UnityEngine;

[CreateAssetMenu]
public class CombatEffectsLibrary : ScriptableObject
{
    public static CombatEffectsLibrary Instance;
    public CombatEffect stun, burn, poison;

    public CombatEffect attackUp;
    public void Stun(CombatInfo c)
    {
        c.target.AddStatus(stun);
    }
    public void Burn(CombatInfo c)
    {
        c.target.AddStatus(burn);
    }
    public void Poison(CombatInfo c)
    {
        c.target.AddStatus(poison);
    }
    public void EscapeBattle(CombatInfo c)
    {
        Debug.Log("NOPE");
        //BattleController.Instance.State = BattleState.END;
    }
    public void Heal(CombatInfo c)
    {
        c.target.Heal((int)c.value);
    }
    public void FireWork(CombatInfo c)
    {
        if (c.target == c.source)
        {
            EscapeBattle(c);
        }
        else
        {
            Stun(c);
        }
    }
    public void Summon(CombatInfo c)
    {
        CharacterData cd;
        cd = GlobalData10MH.Instance.characterDictionary.GetItem(c.value as string);
        BattleController.Instance.QueueSummon(cd);
    }
}

[System.Serializable]
public class CombatInfo
{
    public BattleUnit source;
    public BattleUnit target;
    public object value;

    public bool TargetsSelf { get => source == target; }
    public CombatInfo(BattleUnit source, BattleUnit target, object value)
    {
        this.source = source;
        this.target = target;
        this.value = value;
    }
}

