﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class BattleCinemaBars : CinemaBars
{
    public CanvasGroup background;
    public Image slash;
    public override void Init()
    {
        base.Init();
        background.alpha = 0;
        slash.rectTransform.localScale = new Vector3(0, 1, 0);
    }
    protected override void OnHideComplete()
    {
        //base.OnHideComplete();
    }
    IEnumerator SlashEffect(float duration)
    {
        //streach slash from 0 to 1000
        slash.rectTransform.DOScaleX(1000, duration);
        //sliughtly increase its size
        slash.rectTransform.DOScaleY(5f, duration);
        //fade out
        slash.DOFade(0, duration / 2).SetDelay(duration / 2);
        yield return new WaitForSeconds(duration / 1.5f);
    }
}
