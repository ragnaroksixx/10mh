﻿using System.Collections;
using UnityEngine;


public abstract class BattleTutorial
{
    BattleController bController;

    public virtual IEnumerator OnBattleStart() { yield return null; }
    public virtual IEnumerator OnTurnStart() { yield return null; }
    public virtual IEnumerator PrePlayerActions() { yield return null; }
    public virtual IEnumerator PreEnemyActions() { yield return null; }
    public virtual IEnumerator OnEscapeReady() { yield return null; }

    public void RegisterTutorial(BattleController bCont)
    {
        bController = bCont;

        bController.OnEncounterStart += BattleStart;
        bController.OnTurnStart += TurnStart;
        bController.PrePlayerActionStart += PrePlayerAction;
        bController.PreEnemyActionStart += PreEnemyAction;
    }

    public void EndTutorial()
    {
        bController.OnEncounterStart -= BattleStart;
        bController.OnTurnStart -= TurnStart;
        bController.PrePlayerActionStart -= PrePlayerAction;
        bController.PreEnemyActionStart -= PreEnemyAction;
    }

    void BattleStart()
    {
        bController.brains.AddYieldEvents(OnBattleStart());
    }
    void TurnStart()
    {
        bController.brains.AddYieldEvents(OnTurnStart());
    }
    void PrePlayerAction()
    {
        bController.brains.AddYieldEvents(PrePlayerActions());
    }
    void PreEnemyAction()
    {
        bController.brains.AddYieldEvents(PreEnemyActions());
    }

}
