﻿using System;
using UnityEngine;

public class EnemyUnitUI : BattleUnitUI
{
    public override void OnDeath()
    {
        base.OnDeath();
    }
    protected override void DeathAnimation()
    {
        base.DeathAnimation();
        StartCoroutine(DialogueRenderer.Instance.ExitCharacterAndWait(unit.data));
    }
}

