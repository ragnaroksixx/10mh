﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System;

public abstract class BattleUnitUI : MonoBehaviour
{
    public Image image;
    protected BattleUnit unit;
    Sequence animationTween;
    public BattleUnit Unit { get => unit; }
    public Transform hpRoot;

    public virtual void SetUp(BattleUnit e)
    {
        image.material = Instantiate(image.material);
        unit = e;
        image.material.SetColor("_OutlineColor", unit.data.characterColor);
        Unit.data.battleEntrySound?.Play();
    }

    protected virtual void DeathAnimation()
    {
    }
    public virtual void OnTakeDamage(int amount, bool wasBlocked)
    {
        Color color = wasBlocked ? Color.white : Color.red;
        FloatingTextController.PopUpText(amount,
            color,
            hpRoot.position, FloatingText.TextType.DAMAGE);
        OnHPChanged();
    }
    public virtual void OnHeal(int amount)
    {
        FloatingTextController.PopUpText(amount,
            Color.green,
            hpRoot.position, FloatingText.TextType.HEAL);
        OnHPChanged();
    }
    public virtual void OnBlockChanged(int amount)
    {
        //FloatingTextController.PopUpText("GUARD",
        //    Color.blue,
        //    hpRoot.position, FloatingText.TextType.BLOCK);
    }
    public virtual void OnHPChanged()
    {
        
    }    
    public virtual void OnDeath()
    {
        DeathAnimation();
    }
    public void PlaySquence(params Tween[] tweens)
    {
        Sequence s = DOTween.Sequence();
        foreach (Tween t in tweens)
        {
            s.Append(t);
        }
        PlaySquence(s);
    }
    public void PlaySquence(Sequence s)
    {
        animationTween?.Kill(true);
        animationTween = s;
        animationTween.Play();
    }
    protected CombatItem heldItem { get => PlayerInventory.Instance.HeldItem.item as CombatItem; }
    public virtual void Highlight()
    {
        if (PlayerInventory.Instance.HasHeldItem)
            GlobalData10MH.Instance.targetHighlight.Play();
        //InventoryInfoPanel.Instance.Set(unit.data);
        image.material.SetFloat("_OutlineAlpha", 1);
    }
    public virtual void Unhighlight()
    {
        //InventoryInfoPanel.Instance.Set(null);
        image.material.SetFloat("_OutlineAlpha", 0);
    }
    public IEnumerator Flash()
    {
        Sequence s = DOTween.Sequence();
        float flashRate = 0.15f;
        Tween yellow = image.DOColor(unit.data.characterColor, flashRate);
        Tween white = image.DOColor(Color.white, flashRate);
        s.Append(yellow)
            .Append(white)
            .SetLoops(2)
            .OnKill(() => image.color = Color.white)
            .OnComplete(() => image.color = Color.white);
        PlaySquence(s);
        yield return new WaitForSeconds(flashRate * 2);

    }

    public void SetColor(Color c, float duration = 0)
    {
        image.DOColor(c, duration);
    }

}
