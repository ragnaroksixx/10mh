﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

public class PlayerBattleUI : BattleUnitUI
{
    public override void SetUp(BattleUnit e)
    {
        base.SetUp(e);
        PlayerInfoMenu.Instance.healthText.text = (Unit.hp.ToString());
        PlayerInfoMenu.Instance.healthBar.fillAmount = e.HPRatio();
        image.rectTransform.anchoredPosition = new Vector2(0, -250);
    }
    public override void Highlight()
    {
        if (PlayerInventory.Instance.HasHeldItem && PlayerInventory.Instance.HeldItem.item is CombatItem)
        {
            image.material.SetColor("_OutlineColor", Color.white);
            PlayerInfoMenu.Instance.playerImage.material.SetColor("_OutlineColor", Color.white);
            PlayerInfoMenu.Instance.playerImage.material.SetFloat("_OutlineAlpha", 1);
            base.Highlight();
        }
    }
    public override void Unhighlight()
    {
        base.Unhighlight();
        PlayerInfoMenu.Instance.playerImage.material.SetFloat("_OutlineAlpha", 0);
    }
    public override void OnTakeDamage(int amount, bool wasBlocked)
    {
        base.OnTakeDamage(amount, wasBlocked);
    }
    public override void OnBlockChanged(int amount)
    {
        base.OnBlockChanged(amount);
        PlayerInfoMenu.Instance.UpdateBlockUI(unit.block);
    }
    public override void OnHPChanged()
    {
        base.OnHPChanged();
        if (PlayerInfoMenu.Instance.healthText.text != Unit.hp.ToString())
        {
            PlayerInfoMenu.Instance.healthText.DOText(Unit.hp.ToString(), 0.25f, true, ScrambleMode.Numerals);
            PlayerInfoMenu.Instance.healthBar.DOFillAmount(Unit.HPRatio(), 0.25f);
        }
    }

    protected override void DeathAnimation()
    {
        base.DeathAnimation();
        PlaySquence(image.rectTransform.DOAnchorPosY(-450, 1));
    }
}
