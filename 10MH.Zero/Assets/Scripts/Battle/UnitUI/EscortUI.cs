﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using TMPro;
using DG.Tweening;
using System;
using System.Collections.Generic;

public class EscortUI : BattleUnitUI, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject graphicRoot;
    public TMP_Text healthText;
    public EscortData e;
    public LinearOnScreenAnimator anim;
    public Image imageBorder;
    private void Start()
    {
        EscortSystem.ui = this;
        graphicRoot.SetActive(false);
        Publisher.Subscribe<StartEscortEvent>(StartEscort);
        Publisher.Subscribe<EndEscortEvent>(EndEscort);
    }

    private void OnDestroy()
    {
        Publisher.Unsubscribe<StartEscortEvent>(StartEscort);
        Publisher.Unsubscribe<EndEscortEvent>(EndEscort);
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        //InventoryInfoPanel.Instance.Set(EscortSystem.battleData);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        //InventoryInfoPanel.Instance.Set(null);
    }
    public void StartEscort(StartEscortEvent e)
    {
        SetUp(EscortSystem.battleData);
        graphicRoot.SetActive(true);
    }
    public void EndEscort(EndEscortEvent e)
    {
        graphicRoot.SetActive(false);
    }
    public override void SetUp(BattleUnit e)
    {
        base.SetUp(e);
        healthText.text = (Unit.hp.ToString());
    }
    public override void OnTakeDamage(int amount, bool wasBlocked)
    {
        imageBorder.rectTransform.DOShakeAnchorPos(0.75f, 10);
        imageBorder.DOFlash(Color.white, Color.red);
        OnHPChanged();
    }
    public override void OnHeal(int amount)
    {
        OnHPChanged();
    }

    protected override void DeathAnimation()
    {
        //base.DeathAnimation();
    }
    public override void OnBlockChanged(int amount)
    {
        //base.OnBlock(amount);
    }
    public override void Highlight()
    {
        // base.Highlight();
    }
    public override void Unhighlight()
    {
        //base.Unhighlight();
    }
    public override void OnHPChanged()
    {
        if (healthText.text != Unit.hp.ToString())
            healthText.DOText(Unit.hp.ToString(), 0.25f, true, ScrambleMode.Numerals);
    }

    [Button]
    public void DebugEscort()
    {
        EscortSystem.StartEscort(e);
    }

    public void OnInvClose()
    {
        anim.AnimateOffScreen();
    }
    public void OnInvOpen()
    {
        anim.AnimateOnScreen();
    }
}
[System.Serializable]
public class EscortData
{
    public CharacterData escortee;
    public AreaData endArea;
    public CharacterData endCharacter;
    public bool endDuringCalamity;
    public TextAsset interactScene;
    public TextAsset abandonScene;
    public TextAsset defaultWrongWayScene;
    public List<AreaTransitionData> validLocations;
}
public static class EscortSystem
{
    public static EscortData escortData;
    public static EscortUnit battleData;
    public static EscortUI ui;

    public static bool IsEscorting => escortData != null;
    public static void StartEscort(EscortData e)
    {
        escortData = e;
        battleData = new EscortUnit();
        battleData.Init(e.escortee);
        Publisher.Raise(new StartEscortEvent(e));
    }
    public static void EndEscort()
    {
        Publisher.Raise(new EndEscortEvent());
        escortData = null;
        battleData = null;
    }
}

public class StartEscortEvent : PublisherEvent
{
    EscortData data;

    public StartEscortEvent(EscortData data)
    {
        this.data = data;
    }

    public EscortData Data { get => data; }
}
public class EndEscortEvent : PublisherEvent
{
}
