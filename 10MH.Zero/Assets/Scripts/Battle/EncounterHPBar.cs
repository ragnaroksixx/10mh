﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class EncounterHPBar : MonoBehaviour
{
    public Image hpBar;
    float maxHP = 0;
    private void Start()
    {
        BattleController.Instance.OnEnemySummon += OnEnemySummon;
        BattleController.Instance.OnEncounterStart += OnEncounterStart;
    }
    public void OnEncounterStart()
    {
        hpBar.fillAmount = 0;
    }
    public void OnEnemySummon(BattleUnit unit)
    {
        maxHP += unit.maxHp;
        unit.unitController.OnHPChangedEvent += OnEnemyHPChanged;
        UpdateHP();
    }
    public void UpdateHP()
    {
        float currentHP = 0;
        foreach (BattleUnit unit in BattleController.Instance.Enemies)
        {
            currentHP += unit.hp;
        }
        float ratio = currentHP / maxHP;
        hpBar.DOFillAmount(ratio, 1.5f);
    }
    public void OnEnemyHPChanged(BattleUnit unit)
    {
        UpdateHP();
    }
}
