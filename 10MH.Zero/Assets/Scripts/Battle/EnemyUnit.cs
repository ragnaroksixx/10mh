﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class EnemyUnit : BattleUnit
{
    public CombatMove turnAction;

    public override void Die()
    {
        BattleController.Instance.OnEnemyDeath(this);
    }

    public override IEnumerator DetermineActions()
    {
        yield return base.DetermineActions();
        turnAction = unitController.DetermineAction();
        ActionVisualInfo vfx = unitController.GetActionVisuals(turnAction.actions[0]);
        yield return DialogueRenderer.Instance.SpriteCharacterAndWait(data, vfx.sprite);
        OnDetermineAction?.Invoke();
    }
    public override IEnumerator TakeTurn()
    {
        yield return base.TakeTurn();

        yield return PerformAction(turnAction);
    }

    public IEnumerator PerformAction(CombatMove move)
    {
        yield return BattleController.Instance.battleUI
    .       Focus(this);

        foreach (CombatAction action in move.actions)
        {
            if (IsDead())
                yield break;

            ActionVisualInfo vfx = unitController.GetActionVisuals(action);
            if (!vfx.onUseSFX.IsNULL())
            {
                vfx.onUseSFX.Play();
                //yield return new WaitForSeconds(vfx.onUseSFX.AudioInfo.clip.length);
            }

            //check conditionals
            //check randoms
            BattleUnit[] nextActionTargets = new BattleUnit[1] { CycleManager.PlayerCombatUnit };
            string logText = action.GetLogText(this);

            //yield return LogCombat(logText, false);

            if (IsDead())
                yield break;

            yield return action.DoAction(this, nextActionTargets);

            //yield return new WaitForSeconds(0.25f);
        }

        yield return BattleController.Instance.battleUI
            .UnFocusDelay(this);

        yield return new WaitForSeconds(0.125f);
    }
}
