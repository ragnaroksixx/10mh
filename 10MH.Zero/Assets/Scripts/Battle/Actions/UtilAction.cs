﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Combat/Actions/Util Action")]
public class UtilAction : CombatAction
{
    public int valueInt = -1;
    public string valueString = "";
    public bool targetSelf = false;
    public float onUseWaitTime = 0;
    public UnityEvent<CombatInfo> additionalEffects;

    public UtilAction() : base(null)
    {
    }

    public override object Value
    {
        get
        {
            if (valueInt == -1)
            {
                return valueString;
            }
            else
            {
                return valueInt;
            }
        }
    }
    protected override IEnumerator DoAction(CombatInfo c)
    {
        if (targetSelf)
            c.target = c.source;
        yield return base.DoAction(c);
        additionalEffects?.Invoke(c);
        yield return new WaitForSeconds(onUseWaitTime);
    }

}
