﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

public class AttackAction : CombatAction
{
    public int basePower;
    public override object Value => 0;

    public override string Header => "Attack";

    public override string Body => basePower.ToString();

    public override int Numeric => -1;

    public AttackAction(ActionVisualInfo vfx, int power) : base(vfx)
    {
        this.basePower = power;
    }
    public AttackAction(int power) : base(null)
    {
        this.basePower = power;
    }
    protected override IEnumerator DoAction(CombatInfo c)
    {
        yield return c.source.unitController.OnPerformAction(this);
        yield return base.DoAction(c);
        int damage = CombatInteractionAdapter.CalcDamage(basePower, 0, c.source, c.target);

        CombatInteractionAdapter.Attack(c.source, c.target, damage);
        //c.target.TakeDamage(damage, c.source);

    }
}
