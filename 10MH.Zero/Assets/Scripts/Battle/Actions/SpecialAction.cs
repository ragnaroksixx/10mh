﻿using System;
using System.Collections;
using UnityEngine;


public class SpecialAction : CombatAction
{
    public Action<CombatInfo> onUseAction;

    public SpecialAction(Action<CombatInfo> onUseAction) : base(null)
    {
        this.onUseAction = onUseAction;
    }
    public SpecialAction(ActionVisualInfo vfx, Action<CombatInfo> onUseAction) : base(vfx)
    {
        this.onUseAction = onUseAction;
    }
    protected override IEnumerator DoAction(CombatInfo c)
    {
        yield return c.source.unitController.OnPerformAction(this);
        yield return base.DoAction(c);
        onUseAction?.Invoke(c);
        yield return new WaitForSeconds(0.1f);
    }
}
