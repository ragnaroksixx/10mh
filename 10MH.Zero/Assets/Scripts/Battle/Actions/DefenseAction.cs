﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

public class DefenseAction : CombatAction
{
    [BoxGroup("Parameters")]
    public int block;
    public AttackAction counterAttack;

    public override string Header => "Defend";

    public override string Body => block.ToString();

    public override int Numeric => -1;

    public DefenseAction(ActionVisualInfo vfx, int block) : base(vfx)
    {
        this.block = block;
    }
    public DefenseAction(int block) : base(null)
    {
        this.block = block;
    }

    protected override IEnumerator DoAction(CombatInfo c)
    {
        c.target = c.source;
        yield return c.source.unitController.OnPerformAction(this);
        //c.source.ui.PlaySquence(c.source.DebugActionAnimation(true));
        //yield return new WaitForSeconds(0.4f);
        yield return base.DoAction(c);
        int value = block;
        c.source.GainBlock(value);
        yield return new WaitForSeconds(0.1f);

    }
    public override string GetLogText(EnemyUnit e)
    {
        string result = "";
        result = e.data.displayName + " blocked";
        return result;
    }
}
