﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;
using System;

[System.Serializable]
public class CombatAction : IAttributeInfo
{
    public virtual object Value { get; }
    public ActionVisualInfo CustomVisualInfo { get; private set; }

    public virtual string Header => throw new NotImplementedException();

    public virtual string Body => throw new NotImplementedException();

    public virtual int Numeric => throw new NotImplementedException();

    [TextArea(5, 20)]
    public string logText = "";

    public IEnumerator DoAction(BattleUnit source, params BattleUnit[] targets)
    {
        foreach (BattleUnit target in targets)
        {
            CombatInfo ci = new CombatInfo(source, target, Value);
            yield return DoAction(ci);
        }
    }

    public CombatAction(ActionVisualInfo vfx)
    {
        CustomVisualInfo = vfx;
    }

    protected virtual IEnumerator DoAction(CombatInfo c)
    {
        yield return null;
    }
    public virtual string GetLogText(EnemyUnit e)
    {
        return "";
    }

}