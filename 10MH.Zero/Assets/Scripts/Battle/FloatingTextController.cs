﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FloatingTextController : MonoBehaviour
{
    FloatingTextPool textPool;
    public static FloatingTextController Instance;
    public GameObject FloatingTextPrefabGameObject;
    private void Awake()
    {
        Instance = this;
        textPool = new FloatingTextPool(FloatingTextPrefabGameObject, 3, transform);
    }
    private void OnDestroy()
    {
        Instance = null;
        textPool = null;
    }
    public static void PopUpText(int amount, Color c, Vector3 pos, FloatingText.TextType tType)
    {
        Instance.textPool.Pop(amount, c, pos, tType);
    }
    public static void PopUpText(string amount, Color c, Vector3 pos, FloatingText.TextType tType)
    {
        Instance.textPool.Pop(amount, c, pos, tType);
    }
    public static void Stop(FloatingText obj)
    {
        Instance.textPool.Push(obj);
    }
}

public class FloatingTextPool : Pool<FloatingText>
{
    public FloatingTextPool(GameObject b, int initialSize, Transform root) : base(b, initialSize, root)
    {
    }

    public FloatingTextPool(GameObject b, int initialSize) : base(b, initialSize)
    {
    }

    protected override void Init(FloatingText obj)
    {
        base.Init(obj);
        obj.transform.localScale = Vector3.zero;
    }
    protected override void OnPop(FloatingText obj)
    {
        base.OnPop(obj);
    }
    protected override void OnReturn(FloatingText obj)
    {
        base.OnReturn(obj);
        obj.DOKill();
        obj.transform.localScale = Vector3.zero;
    }
    public FloatingText Pop(int amount, Color c, Vector3 position, FloatingText.TextType tType)
    {
        return Pop(amount.ToString(), c, position, tType);
    }
    public FloatingText Pop(string amount, Color c, Vector3 position, FloatingText.TextType tType)
    {
        FloatingText text = Pop();
        text.Play(amount, c, position, tType);
        return text;
    }
}
