﻿using UnityEngine;
using System.Collections;

public class Player : Character
{
    public static Player Instance;
    public Rigidbody rBody;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        SceneTransitionEffect.OnPlayerSpawned();
    }
    private void OnDestroy()
    {
        Instance = null;
    }
    public override void SetSprite(Sprite s)
    {
    }

    public static Pose GetPose()
    {
        Pose p;
        Vector3 rot = CameraController10MH.Instance.transform.eulerAngles;
        rot.x = 0;
        p = new Pose(Instance.transform.position,
                Quaternion.Euler(rot));

        return p;
    }

    public static void SetPose(Pose p)
    {
        Instance.rBody.MovePosition(p.position);
        Instance.rBody.MoveRotation(p.rotation);
        CameraController10MH.Instance.SetRotationAngles(0, 0);
    }

    public void OnDialogueStart()
    {
        collision.isTrigger = true;
    }

    public void OnDialogueEnd()
    {
        collision.isTrigger = false;
    }

    public static void LockMovement()
    {

    }

    public static void LockCameraMovement()
    {

    }
}
