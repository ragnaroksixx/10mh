﻿using UnityEngine;
using System.Collections;
[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Inventory Item")]
public class InventoryItem : Item
{
    public InventoryPropertyData invData;
}
