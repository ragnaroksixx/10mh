﻿using System.Collections;
using UnityEngine;


    [CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Movement Item")]
    public class MovementItem : Item
{
    public SprintMoveData sprintData;
    }
