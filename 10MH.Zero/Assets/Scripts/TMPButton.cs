﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;
public class TMPButton : Button
{
    public TMP_Text text;
    public ColorBlock textColors;
    CanvasGroup group;

    public CanvasGroup Group
    {
        get
        {
            if (group == null)
                group = gameObject.AddComponent<CanvasGroup>();
            return group;
        }
    }
    protected override void Awake()
    {
        base.Awake();
        group = GetComponent<CanvasGroup>();
    }
    protected override void DoStateTransition(SelectionState state, bool instant)
    {
        base.DoStateTransition(state, instant);
        Color c;
        switch (state)
        {
            case SelectionState.Normal:
                c = textColors.normalColor;
                break;
            case SelectionState.Highlighted:
                c = textColors.highlightedColor;
                break;
            case SelectionState.Pressed:
                c = textColors.pressedColor;
                break;
            case SelectionState.Selected:
                c = textColors.selectedColor;
                break;
            case SelectionState.Disabled:
                c = textColors.disabledColor;
                break;
            default:
                throw new System.Exception();
        }
        text.color = c;
    }
    public void SetText(string t)
    {
        text.text = t;
    }
}
#if UNITY_EDITOR
[UnityEditor.CanEditMultipleObjects]
[UnityEditor.CustomEditor(typeof(TMPButton), true)]
public class TMPButtonEditor : UnityEditor.UI.ButtonEditor
{
    UnityEditor.SerializedProperty textColorsProperty;
    protected override void OnEnable()
    {
        base.OnEnable();
        textColorsProperty = serializedObject.FindProperty("textColors");
    }
    public override void OnInspectorGUI()
    {
        TMPButton b = (TMPButton)target;
        serializedObject.Update();
        b.text = (TMP_Text)UnityEditor.EditorGUILayout
            .ObjectField("Text", b.text, typeof(TMP_Text), true);
        ++UnityEditor.EditorGUI.indentLevel;
        UnityEditor.EditorGUILayout.PropertyField(textColorsProperty);
        --UnityEditor.EditorGUI.indentLevel;
        serializedObject.ApplyModifiedProperties();
        base.OnInspectorGUI();
    }
}
#endif
