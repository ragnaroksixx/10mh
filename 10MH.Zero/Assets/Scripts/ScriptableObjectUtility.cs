﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;
using System;
using System.Reflection;
using System.Text;

public static class ScriptableObjectUtility
{
#if UNITY_EDITOR
    public static T CreateAsset<T>(string name) where T : ScriptableObject
    {
        T asset = ScriptableObject.CreateInstance<T>();

        string path = OpenedPath();// AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path == "")
        {
            path = "Assets";
        }
        else if (Path.GetExtension(path) != "")
        {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }
        string parsedName = RemoveSpecialCharacters(name);
        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/" + parsedName + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        //Selection.activeObject = asset;

        return asset;

    }
    public static void OnDestroy()
    {

        Debug.Log("SOU on Destory");
        AssetDatabase.SaveAssets();

    }

    public static void OnApplicationQuit()
    {
        Debug.Log("SOU on Quit");
        AssetDatabase.SaveAssets();
    }

    public static string OpenedPath()
    {
        Type projectWindowUtilType = typeof(ProjectWindowUtil);
        MethodInfo getActiveFolderPath = projectWindowUtilType.GetMethod("GetActiveFolderPath", BindingFlags.Static | BindingFlags.NonPublic);
        object obj = getActiveFolderPath.Invoke(null, new object[0]);
        string pathToCurrentFolder = obj.ToString();
        Debug.Log(pathToCurrentFolder);
        return pathToCurrentFolder;
    }
#endif
    public static string RemoveSpecialCharacters(this string str)
    {
        StringBuilder sb = new StringBuilder();
        foreach (char c in str)
        {
            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
            {
                sb.Append(c);
            }
        }
        return sb.ToString();
    }
}
