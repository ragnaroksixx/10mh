﻿using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.UI;

public class ScrollingText : MonoBehaviour
{
    float width;
    public float scrollSpeed = 50;
    float scrollPos = 0;
    public TMP_Text text;
    TMP_Text textClone;
    public void Init()
    {
        width = text.preferredWidth;
        textClone = Instantiate(text) as TextMeshProUGUI;
        Destroy(textClone.gameObject.GetComponent<ScrollingText>());
        textClone.rectTransform.SetParent(text.rectTransform);
        textClone.rectTransform.anchorMin = new Vector2(1, 0.5f);
        textClone.rectTransform.anchoredPosition = Vector3.zero;
        textClone.rectTransform.localScale = Vector3.one;
        textClone.rectTransform.localRotation = Quaternion.identity;
    }
    public void SetText(string t)
    {
        text.text = t;
        textClone.text = text.text;
    }
    private void Update()
    {
        width = text.preferredWidth;
        text.rectTransform.anchoredPosition = new Vector2(-scrollPos % width,
            text.rectTransform.anchoredPosition.y);
        scrollPos += scrollSpeed * Time.deltaTime;
    }
}
