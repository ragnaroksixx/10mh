﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Color Pallete")]
public class AreaColorPallete : ScriptableObject
{

    public Color main=Color.white;
    public Color secondary = Color.white;
    public Color tertiary = Color.white;
    public Color white = Color.white;
    public Color outline = Color.white;

}
