using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BuildNumber : MonoBehaviour
{
    public TMP_Text buildText;
    public BuildInfo buildInfo;
    // Start is called before the first frame update
    void Start()
    {
        buildText.text =
            buildText.text.Replace("X", Application.version+"-a"+ buildInfo.buildNumber);
    }
}
