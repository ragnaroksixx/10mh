﻿using System.Collections;
using UnityEngine;

public class TransformEvents : MonoBehaviour
{
public void SetLocalRotation(Vector3 vec)
    {

    }

    public void FlipXRotation()
    {
        Vector3 newRot = transform.localEulerAngles;
        newRot.x = -newRot.x;
        transform.localEulerAngles = newRot;
    }
}
