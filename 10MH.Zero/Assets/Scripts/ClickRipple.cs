﻿using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickRipple : MonoBehaviour
{
    Image[] images;
    public float delay;
    public float lifetime;
    private void Start()
    {
        images = GetComponentsInChildren<Image>();
        OnReset(new Color());
    }
    private void OnReset(Color c)
    {
        Color clear = c;
        foreach (Image item in images)
        {
            item.DOKill();
            item.color = clear;
            item.transform.localScale = Vector3.zero;
        }
    }
    [Button]
    public void RippleEditor()
    {
        Ripple(Vector3.zero, Color.red);
    }
    public void Ripple(Vector3 postion, Color color)
    {
        Color clear = color;
        clear.a = 0.2f;
        OnReset(clear);
        clear.a = 0f;
        transform.position = postion;
        float delayUse = 0;

        foreach (Image item in images)
        {
            item.DOKill();
            item.transform.localScale = Vector3.one*0.1f;
            Sequence colorSequence = DOTween.Sequence();
            colorSequence.AppendInterval(delayUse)
                .Append(item.DOColor(color, lifetime / 2))
                .Append(item.DOColor(clear, lifetime / 2));
            Sequence scaleSequence = DOTween.Sequence();
            scaleSequence.AppendInterval(delayUse)
                .Append(item.transform.DOScale(Vector3.one, lifetime).SetEase(Ease.OutBounce));
            colorSequence.Play();
            scaleSequence.Play();
            delayUse += delay;
        }
    }
}
