﻿using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using System;

public class RotationOnScreenAnimator : OnScreenAnimatorBASE
{
    public float onScreenRot;
    public float offScreenRot;
    protected override void OffScreenImpl(Action onComplete, bool instant)
    {
        RotateTo(offScreenRot, false, onComplete);
    }

    protected override void OffScreenImplEDITOR()
    {
        RotateTo(offScreenRot, true, null);
    }

    protected override void OnScreenImpl(Action onComplete, bool instant)
    {
        RotateTo(onScreenRot, false, onComplete);
    }

    protected override void OnScreenImplEDITOR()
    {
        RotateTo(onScreenRot, true, null);
    }

    void RotateTo(float val, bool instant, Action onComplete = null)
    {
        Vector3 rot = rectTransform.localEulerAngles;
        rot.z = val;

        if (instant)
        {
            rectTransform.localEulerAngles = rot;
            onComplete?.Invoke();
            isTransitioning = false;
        }
        else
        {
            isTransitioning = true;
            rectTransform.DOLocalRotate(rot, transitionSpeed).OnComplete
                (() => { onComplete?.Invoke(); isTransitioning = false; })
                .SetEase(Ease.Linear)
                .SetUpdate(true);
        }
    }
}
