﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using NHG.Dialogue;
using System;

[CreateAssetMenu(fileName = "New Character", menuName = "Character/Data")]
public class CharacterData : ScriptableObject, IUnquieScriptableObject
{
    public List<ImageData> images;
    public SpriteSheet spriteSheet;
    Dictionary<string, Sprite> imageDict;
    public string displayName;
    public string hiddenDisplayName;
    public string firstName, lastName;
    public List<string> alias;
    public Color characterColor = Color.magenta;
#if UNITY_EDITOR
    [OnValueChanged("OnAIChanged")]
    [FoldoutGroup("Combat")]
    public TextAsset AIHandler;
#endif
    [ReadOnly]
    [FoldoutGroup("Combat")]
    public string aiHandlerClassName;
    [FoldoutGroup("Combat")]
    public int hp;
    [FoldoutGroup("Combat")]
    public CustomAudioClip battleEntrySound;
    [FoldoutGroup("Combat")]
    public Stats stats;
    [FoldoutGroup("Combat")]
    public GameObject dialogueCharacterPrefab;
    public int MaxHP => hp + ((stats.HP - 1) * 10);
    public string Identifier => displayName + firstName + lastName;
    public Character GetCharacter()
    {
        return Character.FindCharacter(this);
    }
    public string[] Alias
    {
        get
        {
            List<string> temp = new List<string>(alias);
            temp.Add(displayName);
            return temp.ToArray();
        }
    }

    public List<ScriptData> scripts;

    [FoldoutGroup("Schedule")]
    public CharacterTimeline baseTimeline;
    [FoldoutGroup("Schedule")]
    public List<CharacterTimeline> altTimelines;
    public string Description => "";
    public string Name => firstName + " " + lastName;
    public virtual int AP => -1;

    [FoldoutGroup("Scenes")]
    public CharacterSceneSets cycleOneScenes, cycleTwoScenes, defualtScenes;

    public void Init()
    {
        imageDict = new Dictionary<string, Sprite>();
        foreach (ImageData item in images)
        {
            imageDict.Add(item.key.ToString().ToLower(), item.image);
        }
        if (spriteSheet)
        {
            foreach (CustomImageData item in spriteSheet.sprites)
            {
                imageDict.Add(item.key.ToLower(), item.image);
            }
        }
    }

    public Sprite GetImage(ImageKey key)
    {
        return GetImage(key.ToString());
    }
    public Sprite GetImage(string key)
    {
        return imageDict[key.ToLower()];
    }
    public Sprite GetImageEditor(ImageKey key)
    {
        return GetImageEditor(key.ToString());
    }
    public Sprite GetImageEditor(string key)
    {
        Dictionary<string, Sprite> imageDictEditor = new Dictionary<string, Sprite>();
        foreach (ImageData item in images)
        {
            imageDictEditor.Add(item.key.ToString().ToLower(), item.image);
        }
        if (spriteSheet)
        {
            foreach (CustomImageData item in spriteSheet.sprites)
            {
                imageDictEditor.Add(item.key.ToLower(), item.image);
            }
        }

        return imageDictEditor[key.ToLower()];
    }
    public void RegisterRandom()
    {
        RNG_10MH.RegisterRandom(Identifier);
    }
    public void DeregisterRandom()
    {
        RNG_10MH.DeregisterRandom(Identifier);
    }
    public System.Random GetRandom()
    {
        return RNG_10MH.GetRandom(Identifier);
    }
    public int Range(int min, int max)
    {
        return RNG_10MH.Range(Identifier, min, max);
    }
    public float Range(float min, float max)
    {
        return RNG_10MH.Range(Identifier, min, max);
    }
    public string FullName()
    {
        return firstName + " " + lastName;
    }
    public TextAsset GetScript(string key)
    {
        foreach (ScriptData script in scripts)
        {
            if (script.key == key)
                return script.script;
        }
        return null;
    }

    public void Die()
    {
        CycleManager.Instance.AddTempEvent(
            Identifier, "dead");
    }
#if UNITY_EDITOR
    public void OnAIChanged()
    {
        aiHandlerClassName = AIHandler.name;
    }
#endif
}
[System.Serializable]
public struct ImageData
{
    public ImageKey key;
    public Sprite image;
}

[System.Serializable]
public struct ScriptData
{
    public string key;
    public TextAsset script;
}
[System.Serializable]
public class CharacterSceneSets
{
    public TextAsset preCalamityInspect, preCalamityInteract;
    public TextAsset calamityInspect, calamityInteract;
}
[System.Serializable]
public struct Stats
{
    [Range(1, 10)]
    public int HP, ATK, DEF, AP, LCK, SPD;
}
[System.Serializable]
public enum ImageKey
{
    NONE = 0,
    CUSTOM,
    NORMAL,
    BATTLE,
    HIDDEN
}
