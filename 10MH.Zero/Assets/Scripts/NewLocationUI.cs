﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class NewLocationUI : MonoBehaviour
{
    public CanvasGroup group;
    public RectTransform root;
    public float showTime = 1;
    public static NewLocationUI Instance;
    public CustomAudioClip audioClip;
    private void Awake()
    {
        Instance = this;
        group.alpha = 0;
        root.localScale = Vector3.zero;
    }
    public IEnumerator Show(string text)
    {
        float transitionTime = 0.25f;
        group.DOFade(1, transitionTime);
        root.DOScale(Vector3.one, transitionTime);
        audioClip.Play();
        yield return new WaitForSeconds(showTime);
        yield return DialogueRenderer.Instance.WaitForPlayerInput();
        group.DOFade(0, transitionTime/2);
        root.DOScale(Vector3.zero, transitionTime);
    }
}
