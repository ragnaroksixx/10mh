using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABCameraMovement : MonoBehaviour
{
    public CinemachineTargetGroup targetGroup;
    public enum Axis
    {
        X, Y, Z
    }
    public Axis axis;

    public float min;
    public float max;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float value = 0;
        Transform target;
        target = Player.Instance.transform;
        switch (axis)
        {
            case Axis.X:
                value = target.position.x;
                break;
            case Axis.Y:
                value = target.position.y;
                break;
            case Axis.Z:
                value = target.position.z;
                break;
            default:
                break;
        }
        float ratio = (value - min) / (max - min);
        targetGroup.m_Targets[0].weight = ratio;
        targetGroup.m_Targets[1].weight = 1 - ratio;
    }
}
