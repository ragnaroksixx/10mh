﻿using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
public class BreakableObject : MonoBehaviour
{
    public GameObject brokenAsset;
    public bool spawnAsset;

    [Button]
    public void Break()
    {
        gameObject.SetActive(false);
        if (spawnAsset)
        {
            spawnAsset = false;
            //TODO Instantiate Obj
        }
        brokenAsset.gameObject.SetActive(true);

    }

    [Button]
    public void Repair()
    {
        gameObject.SetActive(true);
        brokenAsset.gameObject.SetActive(false);
    }

    public void SetBroken(bool isBroken)
    {
        if (isBroken)
            Break();
        else
            Repair();
    }
    public void SetFix(bool isFixed)
    {
        if (!isFixed)
            Break();
        else
            Repair();
    }
}
