﻿using System;
using System.Collections;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


public static class EditorUtils
{
    public static void SimpleConfirmationPrompt(Action okAction)
    {
        ConfirmationPrompt("Confirm", "Are you Sure?", "Yes", "No", okAction);
    }
    public static void ConfirmationPrompt(string title, string message, string ok, string cancel, Action okAction)
    {
#if UNITY_EDITOR
        if (EditorUtility.DisplayDialog(title, message, ok, cancel))
            okAction?.Invoke();
#endif
    }
}
