﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering.Universal;
using SCPE;
using DG.Tweening;
using UnityEngine.Rendering;

public class EffectsManager : MonoBehaviour
{
    public static EffectsManager Instance;
    EffectProfile preCalam, postCalam;
    public VolumeProfile preCalamityProfile, calamityProfile;
    float lighting = 1;
    // Use this for initialization
    void Awake()
    {
        Instance = this;
        preCalam = new EffectProfile(preCalamityProfile);
        postCalam = new EffectProfile(calamityProfile);
    }
    private void Start()
    {
        SetCalamityVariables();
        SetLighting(1);
    }
    private void OnDestroy()
    {
        SetLighting(1);
        preCalam.Reset();
        postCalam.Reset();
    }
    public void SetLighting(float val)
    {
        lighting = val;
        ColorAdjustments ca;
        preCalamityProfile.TryGet(out ca);
        float exposure = val.Remap(0, 1, 0, 1);
        float saturation = val.Remap(0, 1, -100, 0);
        float contrast = val.Remap(0, 1, 50, 0);
        Shader.SetGlobalFloat("unlitLighting", val);
        ca.postExposure.value = exposure;
        ca.saturation.value = saturation;
        ca.contrast.value = contrast;
    }
    public float GetLighting()
    {
        return lighting;
    }
    public void SetMainColor(AreaColorPallete c)
    {
        SetMainColor(c);
    }
    public void SetCalamityVariables()
    {
        if (TimeController.IsCalamity())
            SetCalamity();
        else
            SetPreCalamity();
    }
    public static float HeightClamp
    {
        get => Shader.GetGlobalFloat("heightClamp");
        set => Shader.SetGlobalFloat("heightClamp", value);
    }
    internal EffectProfile PreCalam { get => preCalam; }

    public void SetCalamity(float transitionTime)
    {
        DOTween.To(() => HeightClamp,
            x => HeightClamp = x,
            1,
            transitionTime);

        DOTween.To(() => Shader.GetGlobalFloat("_globalSat"),
            x => Shader.SetGlobalFloat("_globalSat", x),
            0.7f,
            transitionTime);
    }
    public void SetCalamity()
    {
        HeightClamp = 1;
        Shader.SetGlobalFloat("_globalSat", 0.7f);
    }
    public void SetPreCalamity()
    {
        HeightClamp = 10;
        Shader.SetGlobalFloat("_globalSat", 1);
    }
    public static void SetGroundHeight(float value)
    {
        Shader.SetGlobalFloat("groundheight", value);
    }
    public void StartRipple()
    {
        preCalam.StartRipple();
        postCalam.StartRipple();
    }
    public void StopRipple()
    {
        preCalam.StopRipple();
        postCalam.StopRipple();
    }
    public void Blur(float amount)
    {
        preCalam.Blur(amount);
        postCalam.Blur(amount);
    }
}

class EffectProfile
{
    public VolumeProfile profile;
    Ripples rippleEffect;
    Blur blurEffect;
    BlackBars blackEffect;
    Tween rippleTween, blurTween, blackTween;

    public EffectProfile(VolumeProfile profile)
    {
        this.profile = profile;
        profile.TryGet<Ripples>(out rippleEffect);
        profile.TryGet<Blur>(out blurEffect);
        profile.TryGet<BlackBars>(out blackEffect);
        if(blackEffect!= null)
        blackEffect.maxSize.value = 2.0f;
        rippleEffect.strength.value = 0;
    }

    public void Reset()
    {
        rippleEffect.strength.value = 0;
        blurEffect.amount.value = 0;
    }

    public void StartRipple()
    {
        rippleTween?.Kill();
        rippleTween = DOTween.To(() => rippleEffect.strength.value,
            x => rippleEffect.strength.value = x,
            2,
            1);
    }
    public void StopRipple()
    {
        rippleTween?.Kill();
        rippleTween = DOTween.To(() => rippleEffect.strength.value,
            x => rippleEffect.strength.value = x,
            0,
            0.35f);
    }

    public void Blur(float amount)
    {
        blurTween?.Kill();
        blurTween = DOTween.To(() => blurEffect.amount.value,
            x => blurEffect.amount.value = x,
            amount,
           0.35f);
    }
    public void BlackBars(float value, float time = 0.35f)
    {
        if (value == 1)
            value = 1.1f;
        blackTween?.Kill();
        blackTween = DOTween.To(() => blackEffect.size.value,
            x => blackEffect.size.value = x,
            value,
           time);
    }
}
