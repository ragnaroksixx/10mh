﻿using System.Collections;
using UnityEngine;


public class ParallaxUIElement : MonoBehaviour
{
    Vector2 startPos;
    public int moveModifier;

    private void Awake()
    {
        startPos = transform.localPosition;
    }


    // Update is called once per frame
    void Update()
    {
        Vector2 pz = Camera.main.ScreenToViewportPoint(Input.mousePosition);

        float x = startPos.x + pz.x * moveModifier;
        float y = startPos.y + pz.y * moveModifier;

        transform.localPosition = new Vector3(x, y, 0);
    }
}
