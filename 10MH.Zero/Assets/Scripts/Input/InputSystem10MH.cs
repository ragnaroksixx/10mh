﻿using System.Collections;
using UnityEngine;


public class InputSystem10MH : InputSystem
{
    public static InputSystem10MH Input;

    public static InputAction proceedDialogue = new InputAction();
    public static InputAction phoneOpenClose = new InputAction();
    protected override void Awake()
    {
        base.Awake();
        allActions.Add(proceedDialogue);
        allActions.Add(phoneOpenClose);
    }
}
