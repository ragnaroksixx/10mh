﻿using System.Collections;
using UnityEngine;


public class PCInputHelper : InputHelper
{
    protected override void Subscribe()
    {
        base.Subscribe();
        InputSystem10MH.proceedDialogue.impl = new PCInputImpl(KeyCode.Mouse0);
        InputSystem10MH.phoneOpenClose.impl = new PCInputImpl(KeyCode.Escape);
    }
}
