﻿using System.Collections;
using UnityEngine;


public class InputHelper : MonoBehaviour
{
    private void OnEnable()
    {
        Subscribe();
    }

    private void OnDisable()
    {
        Unsubscribe();
    }

    protected virtual void Subscribe()
    {

    }

    private void Unsubscribe()
    {
        if (InputSystem10MH.Input == null)
            return;
        foreach (InputAction item in InputSystem10MH.Input.allActions)
        {
            item.impl = null;
        }
    }
}
