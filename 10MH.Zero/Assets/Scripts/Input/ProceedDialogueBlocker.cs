﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class ProceedDialogueBlocker : MonoBehaviour
{
    IEnumerator Start()
    {
        EventTrigger b = gameObject.GetComponent<EventTrigger>();
        if (b == null)
            b = gameObject.AddComponent<EventTrigger>();
        yield return null;


        EventTrigger.Entry onHighlight = new EventTrigger.Entry();
        onHighlight.eventID = EventTriggerType.PointerEnter;
        onHighlight.callback.AddListener((BaseEventData e) => { InputSystem10MH.proceedDialogue.Disable(this); });
        b.triggers.Add(onHighlight);

        EventTrigger.Entry onUnhighlight = new EventTrigger.Entry();
        onUnhighlight.eventID = EventTriggerType.PointerExit;
        onUnhighlight.callback.AddListener((BaseEventData e) => { InputSystem10MH.proceedDialogue.Enable(this); });
        b.triggers.Add(onUnhighlight);


    }
}

