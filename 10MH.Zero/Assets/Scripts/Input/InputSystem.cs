﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputSystem : MonoBehaviour
{
    public List<InputAction> allActions = new List<InputAction>();

    protected virtual void Awake()
    {

    }
}

public class InputAction
{
    MultiBoolLock disabled = new MultiBoolLock();
    public bool Enabled => !disabled.Value && impl != null;
    public InputImpl impl;
    public bool Get() { return Enabled && impl.Get(); }
    public bool GetUp() { return Enabled && impl.GetUp(); }
    public bool GetDown() { return Enabled && impl.GetDown(); }

    public void Disable(object o)
    {
        disabled.AddLock(o);
    }
    public void Enable(object o)
    {
        disabled.RemoveLock(o);
    }
}

public abstract class InputImpl
{
    public abstract bool Get();
    public abstract bool GetUp();
    public abstract bool GetDown();
}

public class PCInputImpl : InputImpl
{
    public KeyCode key;

    public PCInputImpl(KeyCode k)
    {
        key = k;
    }
    public override bool Get()
    {
        return Input.GetKey(key);
    }

    public override bool GetDown()
    {
        return Input.GetKeyDown(key);
    }

    public override bool GetUp()
    {
        return Input.GetKeyUp(key);
    }
}
