using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenBreak : MonoBehaviour
{
    public Transform exprosionSource;
    public Material shatterMaterail;
    public GameObject toSpawnPrefab;
    public float waitTime = 1;

    public void DoTheThing()
    {
        Texture2D texture = ScreenCapture.CaptureScreenshotAsTexture(2);
        shatterMaterail.SetTexture("_BaseMap", texture);
        StartCoroutine(DoScatter());
    }
    Transform Spawn()
    {
        Transform parentObj = GameObject.Instantiate(toSpawnPrefab, transform).transform;
        Destroy(parentObj.gameObject, 5);
        return parentObj.GetChild(2);
    }
    void Scatter(Transform obj)
    {
        foreach (Transform child in obj)
        {
            Rigidbody rbody = child.GetComponent<Rigidbody>();
            if (rbody == null) continue;
            rbody.useGravity = true;
            rbody.isKinematic = false;
            rbody.AddExplosionForce(150, exprosionSource.position, 10f);
        }
    }

    IEnumerator DoScatter()
    {
        WalkController10MH.RemoveUserInput(this);
        CameraController10MH.RemoveUserInput(this);
        TimeController.Instance.IsPaused.AddLock(this);


        Transform obj = Spawn();
        yield return new WaitForSeconds(waitTime);

        Scatter(obj);

        WalkController10MH.RestoreUserInput(this);
        CameraController10MH.RestoreUserInput(this);
        TimeController.Instance.IsPaused.RemoveLock(this);
    }
}
